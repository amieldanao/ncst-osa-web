(function() {
    // just drag your whole code from your script.js and drop it here
    var roles_string = ['Viewer', 'Editor', 'Admin'];
    addUserCallback();
    // var initial_role;
    // var myRole;
    function addUserCallback() {
        let email = $("#user_email").text();

        let query = db.collection('users').where('email', '==', email);

        query.onSnapshot(function(snapshot) {
                snapshot.docChanges().forEach(function(change) {


                    if (change.type === "added") {
                        let role = change.doc.data().role;
                        initial_role = role;
                        setRole(role);
                        if (role == 2 || role == 1) {
                            $("#table_tr th").last().attr('id', 'actions_th'); //append('<th id="actions_th" class="role_admin">Actions</th>');
                        }
                    }

                    if (change.type === "modified") {
                        setRole(change.doc.data().role);
                        $("body").empty();
                        $.blockUI({
                            fadeIn: 1,
                            message: "Your user credential has changed. <br> You need to login again."
                        });

                        setTimeout(() => {
                            location.replace("index.html");
                        }, 3000);
                    }

                    if (change.type === "removed") {
                        // console.log("Removed user: ", change.doc.data());
                        // window.location = "index.html";
                    }
                });
            },
            function(error) {
                console.log('retry add user callback');
                addUserCallback();
            });
    }

    function setRole(newRole) {
        switch (newRole) {
            case 0:
                $(".role_editor").remove();
                $(".role_admin").remove();
                break;
            case 1:
                $(".role_admin").remove();
                break;
        }

        $("#user_role").text("Role: " + roles_string[newRole]);
        $("#user_role").attr("data-done", true);
    }

})();



'user strict'


var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = {
    attributes: true,
    childList: true,
    subtree: true
};

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for (let mutation of mutationsList) {
        if (mutation.type === 'childList') {

        } else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);



function Start() {
    // To allow td elements and data-option attributes on td elements
    myDefaultWhiteList.span = ['style'];

    initVariables();
    loadDataFromDBNew();

    // window.addEventListener("resize", resizeFunction);
}

$(document).ready(function() {
    observer.observe(targetNode, config);
    //    initButtonCallbacks();
});


function tempAddDescriptions() {
    Object.keys(descriptions).forEach((key) => {
        db.collection("descriptions").doc(key).set({
            opposite: descriptions[key].opposite
        });
    });
}


var descriptions = {

    "(eye color) black" : {opposite: ["(eye color) brown", "(eye color) blue"]},
    "(eye color) brown" : {opposite: ["(eye color) black", "(eye color) blue"]},
    "(eye color) blue" : {opposite: ["(eye color) black", "(eye color) brown"]},
    

    "(nose shape) flat" : {opposite : ["(nose shape) roman", "(nose shape) pointed", "(nose shape) fleshy"]},
    "(nose shape) roman" : {opposite : ["(nose shape) flat", "(nose shape) pointed", "(nose shape) fleshy"]},
    "(nose shape) pointed" : {opposite : ["(nose shape) flat", "(nose shape) roman", "(nose shape) fleshy"]},
    "(nose shape) fleshy" : {opposite : ["(nose shape) flat", "(nose shape) roman", "(nose shape) pointed"]},


    "(face shape) round" : {opposite : ["(face shape) oval", "(face shape) square", "(face shape) rectangle", "(face shape) diamond", "(face shape) heart"]},
    "(face shape) oval" : {opposite : ["(face shape) round", "(face shape) square", "(face shape) rectangle", "(face shape) diamond", "(face shape) heart"]},
    "(face shape) square" : {opposite : ["(face shape) round", "(face shape) oval", "(face shape) rectangle", "(face shape) diamond", "(face shape) heart"]},
    "(face shape) rectangle" : {opposite : ["(face shape) round", "(face shape) oval", "(face shape) square", "(face shape) diamond", "(face shape) heart"]},
    "(face shape) diamond" : {opposite : ["(face shape) round", "(face shape) oval", "(face shape) square", "(face shape) rectangle", "(face shape) heart"]},
    "(face shape) heart" : {opposite : ["(face shape) round", "(face shape) oval", "(face shape) square", "(face shape) rectangle", "(face shape) diamond"]},

    // "bearded",
    // "goatee",
    // "stubble beard",
    // "moustache",
    // "clean-shaven",

    "(facial hair) beard" : {opposite : ["(facial hair) goatee", "(facial hair) stubble", "(facial hair) moustache"]},
    "(facial hair) goatee" : {opposite : ["(facial hair) beard", "(facial hair) stubble", "(facial hair) moustache"]},
    "(facial hair) stubble" : {opposite : ["(facial hair) beard", "(facial hair) moustache", "(facial hair) goatee"]},
    "(facial hair) goatee" : {opposite : ["(facial hair) beard", "(facial hair) stubble", "(facial hair) moustache"]},


    "(skin color) light" : {opposite:["(skin color) Fair", "(skin color) medium", "(skin color) brown", "(skin color) dark brown"]},
    "(skin color) Fair" : {opposite:["(skin color) light", "(skin color) medium", "(skin color) brown", "(skin color) dark brown"]},
    "(skin color) medium" : {opposite:["(skin color) light", "(skin color) Fair", "(skin color) brown", "(skin color) dark brown"]},
    "(skin color) brown" : {opposite:["(skin color) light", "(skin color) Fair", "(skin color) medium", "(skin color) dark brown"]},
    "(skin color) dark brown" : {opposite:["(skin color) light", "(skin color) Fair", "(skin color) medium", "(skin color) brown"]},

    "(eye size) small" : {opposite: ["(eye size) medium", "(eye size) large"]},
    "(eye size) medium" : {opposite: ["(eye size) small", "(eye size) large"]},
    "(eye size) large" : {opposite: ["(eye size) medium", "(eye size) small"]},

    "(eye shape) monolid" : {opposite : ["(eye shape) almond", "(eye shape) round", "(eye shape) hooded", "(eye shape) upturned", "(eye shape) downturned"]},
    "(eye shape) almond" : {opposite : ["(eye shape) monolid", "(eye shape) round", "(eye shape) hooded", "(eye shape) upturned", "(eye shape) downturned"]},
    "(eye shape) round" : {opposite : ["(eye shape) monolid", "(eye shape) almond", "(eye shape) hooded", "(eye shape) upturned", "(eye shape) downturned"]},
    "(eye shape) hooded" : {opposite : ["(eye shape) monolid", "(eye shape) almond", "(eye shape) round", "(eye shape) upturned", "(eye shape) downturned"]},
    "(eye shape) upturned" : {opposite : ["(eye shape) monolid", "(eye shape) almond", "(eye shape) round", "(eye shape) hooded", "(eye shape) downturned"]},
    "(eye shape) downturned" : {opposite : ["(eye shape) monolid", "(eye shape) almond", "(eye shape) round", "(eye shape) hooded", "(eye shape) upturned"]},


    "(hair length) long": {opposite: [ "(hair length) short", "(hair length) shoulder-length", "(hair style) bald"] },
    "(hair length) short": {opposite: [ "(hair length) long", "(hair length) shoulder-length", "(hair style) bald"] },
    "(hair length) shoulder-length": {opposite: [ "(hair length) long", "(hair length) short", "(hair style) bald"] },
    
    "(hair style) straight": {opposite: [ "(hair style) bald", "(hair style) afro", "(hair style) thin", "(hair style) braid", "(hair style) army cut", "(hair style) curly"] },
    "(hair style) bald": {opposite: [ "(hair style) straight", "(hair style) afro", "(hair style) thin", "(hair style) braid", "(hair style) army cut", "(hair style) curly", "(hair color) black", "(hair color) brown", "(hair color) blonde", "(hair color) gray", "(hair color) red", "(hair color) white", "(hair length) long", "(hair length) short", "(hair length) shoulder-length"] },
    "(hair style) afro": {opposite: [ "(hair style) straight", "(hair style) bald", "(hair style) thin", "(hair style) braid", "(hair style) army cut", "(hair style) curly"] },
    "(hair style) thin": {opposite: [ "(hair style) straight", "(hair style) bald", "(hair style) afro", "(hair style) braid", "(hair style) army cut", "(hair style) curly"] },
    "(hair style) braid": {opposite: [ "(hair style) straight", "(hair style) bald", "(hair style) afro", "(hair style) thin", "(hair style) army cut", "(hair style) curly"] },
    "(hair style) army cut": {opposite: [ "(hair style) straight", "(hair style) bald", "(hair style) afro", "(hair style) thin", "(hair style) braid", "(hair style) curly"] },
    "(hair style) curly": {opposite: [ "(hair style) straight", "(hair style) bald", "(hair style) afro", "(hair style) thin", "(hair style) braid", "(hair style) army cut"] },

    // "short hair",
    // "shoulder-length hair",
    // "straight hair",
    // "bald",
    // "afro hair",
    // "thin hair",
    // "braid hair",
    // "army hair",
    // "curly hair",


    "(hair color) black" : {opposite: ["(hair color) brown", "(hair color) blonde", "(hair color) gray", "(hair color) red", "(hair color) white", "(hair style) bald"]},
    "(hair color) brown" : {opposite: ["(hair color) black", "(hair color) blonde", "(hair color) gray", "(hair color) red", "(hair color) white", "(hair style) bald"]},
    "(hair color) blonde" : {opposite: ["(hair color) black", "(hair color) brown", "(hair color) gray", "(hair color) red", "(hair color) white", "(hair style) bald"]},
    "(hair color) gray" : {opposite: ["(hair color) black", "(hair color) brown", "(hair color) blonde", "(hair color) red", "(hair color) white", "(hair style) bald"]},
    "(hair color) red" : {opposite: ["(hair color) black", "(hair color) brown", "(hair color) blonde", "(hair color) gray", "(hair color) white", "(hair style) bald"]},
    "(hair color) white" : {opposite: ["(hair color) black", "(hair color) brown", "(hair color) blonde", "(hair color) gray", "(hair color) red", "(hair style) bald"]},


    // "brown hair",
    // "blonde hair",
    // "gray hair",
    // "red hair",
    // "white hair",



    "(height) tall" : {opposite:["(height) short", "(height) average"]},
    "(height) short" : {opposite:["(height) tall", "(height) average"]},
    "(height) average" : {opposite:["(height) tall", "(height) short"]},

    // "short height",
    // "average height",


    "(body) muscular":{opposite : ["(body) fit", "(body) fat", "(body) skinny"]},
    "(body) fit":{opposite : ["(body) muscular", "(body) fat", "(body) skinny"]},
    "(body) fat":{opposite : ["(body) muscular", "(body) fit", "(body) skinny"]},
    "(body) skinny":{opposite : ["(body) muscular", "(body) fit", "(body) fat"]}

    // "fit body",
    // "fat body",
    // "skinny body"
};

var rootRef;
var categories = [];
var previousPoint = null;
var dataSet = [];
var options = {};
var xticks = [
    [0, "Jan"],
    [1, "Feb"],
    [2, "Mar"],
    [3, "Apr"],
    [4, "May"],
    [5, "Jun"],
    [6, "Jul"],
    [7, "Aug"],
    [8, "Sep"],
    [9, "Oct"],
    [10, "Nov"],
    [11, "Dec"]
];
var latest_month_actions = [];
var report_counts = []; //per category, from the latest action to the latest month
var largest_report_count = 0;
var actions = [];
var allActionsThisYear = [];
var actions_pos = [];
var maxChartY = 0;
var thePlot;
var maxLabelString = 17;
var showPrintLabels = false;
var actions_local = [];
var checked_categories = "";
var myDefaultWhiteList = $.fn.tooltip.Constructor.Default.whiteList;


var myCanvas;
var myChart;
var myActions = [];
var actions_datasets = [];
var showingActions = false;
var months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

var resolved_report_count;//total from january to current month
var resolved_counts = [];
var resolved_per_month = [];//[Category][monthIndex] = resolvedCount
var selected_month_to_show = -1;

var dragOptions = {
    animationDuration: 1000
};

var reportCountPerMonth = [];//per category
var reportCountTotalPerMonth = [];//per each report regardless how many category
var resolvedCountTotalPerMonth = [];//per each report regardless how many category
var suggestions_listener = [];
var seminar_colors = [];

var DDSCharts = [];
var resolved_reports_data = [];// array of array, [Category][Seminar name]

var maxReportCountThisMonth = 0;
var maxReportCountThisMonthCategory = new Array();
var previousMaxReportCountThisMonthCategory = new Array();

var actions_colors = [];


function enablePrintButton()
{
    $("#printChartButton").css("display", "inline-block");
}

function fetchActions() {
    try {
        db.collection("actions").onSnapshot(function(querySnapshot) {


            let requests = querySnapshot.docChanges().map((change) => {
                // console.log("change type " + change.type);
                // let splittedActionDate = change.doc.data().start_date.split("-");
                // let thisActionMonthIndex = parseInt(splittedActionDate[1]);
                var title = toTitleCase(change.doc.data().title);
                if (change.type === "added") {

                    
                    actions[title] = change.doc.data();
                    actions_colors[title] = change.doc.data().color;
                    // updateActionData(change.doc.data().title, change.doc.data().start_date, change.doc.data().categories);
                    
                }

                if (change.type === "modified") {
                    // updateActionData(change.doc.data().title, change.doc.data().start_date, change.doc.data().categories);
                    actions[title] = change.doc.data();
                }

                if (change.type === "removed") {
                    removeDatas(change.doc.data().title);
                    // fadeInOutElement(".t_students", querySnapshot.size);
                }
                return new Promise((resolve) => {
                    asyncFunction(change, resolve);
                });

            });

            Promise.all(requests).then(() => {
                
                if(Object.keys(categories).length == 0)
                {
                    enablePrintButton();
                    $(".pie_canvas").css("display", "none");
                }

                let requests = Object.keys(categories).map((e) => {

                    initDSSCharts(e);
                    return new Promise((resolve) => {
                        asyncFunction(e, resolve);
                    });
                });
                    
                Promise.all(requests).then(() => {

                    var key = $("#category_filter").val();
                    if(key in resolved_reports_data)
                    {
                        $("#pie_no_data").css("display", "none");  
                    }
                    else
                    {
                        $("#pie_no_data").css("display", "block");  
                    }

                    $(".pie_canvas").css("display", "none");

                    enablePrintButton();

                });

                console.log('$("#month_filter").change();');
                $("#month_filter").change();

            
        }, function(error) {
            setTimeout(() => {
                fetchActions();
            }, 3000);
        });
    });
    } catch (error) {
        setTimeout(() => {
            fetchActions();
        }, 3000);
    }
}

function initDSSCharts(key)
{
    var initial = true;
    if(key == undefined || key == null){
        key = $("#category_filter").val();
        initial = false;        
    }

    var formatted_key = key.split(" ").join("_");

    if(key in resolved_reports_data)
    {
        if(DDSCharts[key] != null)
            DDSCharts[key].destroy();
        
        // if(initial == false)
        $("#pie_no_data").css("display", "none");

        

        // if(initial == false)
        // $("#DSSCanvas_" + formatted_key).css("display", "block");
        

        var dssCtx = document.getElementById("DSSCanvas_" + formatted_key).getContext("2d");
        // var dssCtx = document.getElementById("DSSCanvas").getContext("2d");

        let resolve_action_count = new Array();
        let resolve_action_count_colors = new Array();
        let resolve_action_count_labels = new Array();        

        //generate data based on resolved actions
        if(resolved_reports_data[key] != undefined)
        {
            Object.keys(resolved_reports_data[key]).forEach((z)=>{
                resolve_action_count.push(resolved_reports_data[key][z]);
                console.log("~~~~" + z);
                
                var c = "#696969";//default unspecified
                if(z in actions_colors)
                    c = actions_colors[z];
                resolve_action_count_colors.push(c);
                resolve_action_count_labels.push(z);
            });
        }

        console.log(resolve_action_count_colors);

        let data = {
            datasets: [{
                data: resolve_action_count,
                backgroundColor: resolve_action_count_colors
            }],
        
            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: resolve_action_count_labels
        };

        // if(initial == true)
        //     $("#DSSCanvas_" + formatted_key).parent().css("display", "none");
        // else
        $("#DSSCanvas_" + formatted_key).parent().css("display", "block");

        Chart.defaults.global.legend.position = 'bottom';
        // For a pie chart
        DDSCharts[key] = new Chart(dssCtx, {
            type: 'pie',
            data: data,
            options: {
                rotation : 180,
                responsive : true, 
                maintainAspectRatio: false,
                legend: {
                    display: true,
                    labels: {
                        fontColor: "gray",
                        boxWidth: 20,
                        padding: 8,
                        fontSize:14,
                    },
                    align:'start'
                },
                tooltips: {
                    enabled: false
                },
                plugins: {
                    datalabels: {
                        formatter: (value, ctx) => {
                        
                            let sum = 0;
                            let dataArr = ctx.chart.data.datasets[0].data;
                            let action_count = dataArr.length;

                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value*100 / sum).toFixed(2)+"%";
                            return percentage;
                        //   return ((action_count/100) * value).toFixed(2) + "%";
        
                        
                        }
                        ,
                        color: function(context) {
                            var index = context.dataIndex;
                            return invertColor(context.chart.data.datasets[0].backgroundColor[index], true);
                        }
                    }
                }
            }
        }); 
    }
    else
    {
        // if(initial == false)
        $("#pie_no_data").css("display", "block");
        $("#DSSCanvas_" + formatted_key).parent().css("display", "none");
    }
}

// function resizeFunction()
// {
    
// }

$(window).resize(function() {
    //  $( "#log" ).append( "<div>Handler for .resize() called.</div>" );
    resizeWindow();
});

function resizeWindow() {

    // let screenWidth = window.innerWidth;//screen.width;

    // let widthPerPie = 420;// 1680 divide 3
    // let columnCount = Math.max(1, Math.floor( screenWidth / widthPerPie ));

    // $("#bottom_container").css("column-count", columnCount);
    // $("#bottom_container").css("-webkit-column-count", columnCount);
    // $("#bottom_container").css("-moz-column-count", columnCount);

    // console.log("Resized ! screenWidth : " + screenWidth);
}



function generateRandomColor()
{
    // var randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
    var randomColor = "#000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
    return randomColor;
    //random color will be freshly served
}


function fetchReports() {

    let tempArray = new Array(12);
    let tempArray2 = new Array(12);
    reportCountTotalPerMonth = tempArray.fill(0);
    resolvedCountTotalPerMonth = tempArray2.fill(0);

    resolved_report_count = 0;
    total_reports_count = 0;

    try {
        db.collection("reports").onSnapshot(function(querySnapshot) {

                // querySnapshot.docChanges().forEach(function(change) {
                let requests = querySnapshot.docChanges().map((change) => {

                    // console.log("change type " + change.type);
                    if (change.type === "added") {

                        // total_reports_count = querySnapshot.size;
                        
                        let thisReportData = change.doc.data();
                        let thisReportCategories = change.doc.data().categories;

                        
                        var splittedDate = change.doc.data().postedAt.split(" ");
                        var thisReportMonthIndex = months.indexOf(splittedDate[1]); //getMonthIndex(change.doc.data().date_posted.month);    

                        
                        // if (reportCountTotalPerMonth[thisReportMonthIndex] == undefined) {
                        //     // let tempArray = new Array(12);
                        //     // reportCountTotalPerMonth[thisReportCategoryName] = tempArray.fill(0);
                        //     reportCountTotalPerMonth[thisReportMonthIndex] = 1;
                        // } else {
                            reportCountTotalPerMonth[thisReportMonthIndex] += 1;
                        // }

                        if (change.doc.data().status == 0)
                            resolvedCountTotalPerMonth[thisReportMonthIndex] += 1;


                        Object.keys(thisReportCategories).forEach((i, key) => {

                            // Sum of this report category
                            let thisReportCategoryName = thisReportCategories[i].name.toString();

                            if (!(thisReportCategoryName in report_counts))
                                report_counts[thisReportCategoryName] = 1;
                            else
                                report_counts[thisReportCategoryName] += 1;

                            categories[thisReportCategories[i].name] = thisReportCategories[i];

                            // let splittedDate = change.doc.data().postedAt.split(" ");
                            // let thisReportMonthIndex = months.indexOf(splittedDate[1]); //getMonthIndex(change.doc.data().date_posted.month); 

                            // Count report per month
                            if (reportCountPerMonth[thisReportCategoryName] == undefined) {
                                let tempArray = new Array(12);
                                reportCountPerMonth[thisReportCategoryName] = tempArray.fill(0);
                                reportCountPerMonth[thisReportCategoryName][thisReportMonthIndex] = 1;
                            } else {
                                reportCountPerMonth[thisReportCategoryName][thisReportMonthIndex] += 1;
                            }

                            //resolved count per month
                            if (change.doc.data().status == 0)
                            {
                                if(resolved_per_month[thisReportCategoryName] == undefined)
                                {
                                    let tempArray = new Array(12);
                                    resolved_per_month[thisReportCategoryName] = tempArray.fill(0);
                                    resolved_per_month[thisReportCategoryName][thisReportMonthIndex] = 1;
                                }
                                else
                                {
                                    resolved_per_month[thisReportCategoryName][thisReportMonthIndex] += 1;
                                }
                                
                                if (!(thisReportCategoryName in resolved_counts))
                                    resolved_counts[thisReportCategoryName] = 1;
                                else
                                    resolved_counts[thisReportCategoryName] += 1;

                                // resolved_report_count++;

                                //store resolved seminar
                                if(resolved_reports_data[thisReportCategoryName] == undefined)
                                {
                                    resolved_reports_data[thisReportCategoryName] = [];
                                    for(let z = 0; z < thisReportData.resolve_seminars.length; z ++)
                                    {
                                        resolved_reports_data[thisReportCategoryName][thisReportData.resolve_seminars[z]] = 1;
                                    }

                                    // 
                                    
                                    if(thisReportData.resolve_seminars.length == 0)
                                        tryAddSeminarResolve(thisReportCategoryName, "unspecified");
                                }
                                else
                                {
                                    if("resolve_seminars" in thisReportData)
                                    {
                                        for(let z = 0; z < thisReportData.resolve_seminars.length; z ++)
                                        {
                                            let seminar_name = thisReportData.resolve_seminars[z];
                                            if(seminar_name.trim().length == 0)
                                                seminar_name = "unspecified";

                                            tryAddSeminarResolve(thisReportCategoryName, seminar_name);
                                        }

                                        if(thisReportData.resolve_seminars.length == 0)
                                            tryAddSeminarResolve(thisReportCategoryName, "unspecified");
                                    }
                                    else
                                    {
                                        tryAddSeminarResolve(thisReportCategoryName, "unspecified");
                                    }
                                }
                            }

                            // total_reports_count ++;

                            // if(!change.doc.data().date_posted.month in reportCountPerMonth[thisReportCategories[i].name])
                            //     reportCountPerMonth[thisReportCategories[i].name][change.doc.data().date_posted.month] = 1;
                            // else
                            //     reportCountPerMonth[thisReportCategories[i].name][change.doc.data().date_posted.month] += 1;

                            updateData(thisReportCategoryName, change.doc.data().postedAt, report_counts[thisReportCategoryName])

                            // updateReportCountToolTip();
                            // updateResolvedCountToolTip();

                            

                            
                        });

                        if (change.doc.data().status == 0)
                            resolved_report_count++;

                        total_reports_count ++;

                        // refreshActionSummary();

                        fetchSuggestions(thisReportCategories);
                    }

                    if (change.type === "modified") {

                    }

                    if (change.type === "removed") {

                    }

                    return new Promise((resolve) => {
                        asyncFunction(change, resolve);
                    });

                });

                Promise.all(requests).then(() => {
                    
                    
                    fetchActions();

                    

                });
            },
            function(error) {
                setTimeout(() => {
                    fetchReports();
                }, 3000);

            });
    } catch (error) {
        setTimeout(() => {
            fetchReports();
        }, 3000);
    }
}




function tryAddSeminarResolve(thisReportCategoryName, seminar_name)
{
    if(resolved_reports_data[thisReportCategoryName][seminar_name] == undefined)
        resolved_reports_data[thisReportCategoryName][seminar_name] = 1;
    else
        resolved_reports_data[thisReportCategoryName][seminar_name] ++;
}

function printAll()
{
    //open window
    var tWindow = window.open();

    var newImg = new Image();
    newImg.onload = function () {
        // win1.print();
    }

    //add main chart
    newImg.src = myCanvas.toDataURL("image/png", 1);

    var canPrint = false;


    var scaleBy = 5;
    
    // var div = document.querySelector('#action_summary_container');
    var w = 1366;//div.getBoundingClientRect().width;
    var hh = $("#action_summary_container").height() / Object.keys(categories).length;
    console.log("hh" + hh);
    var h = hh * scaleBy * scaleBy;//$(div).height();//768;//div.getBoundingClientRect().height;
    console.log("h" + h);
    var cc = document.createElement('canvas');

    // var w = $(div).width();
    // var h = $(div).height();

    cc.width = w * scaleBy;
    cc.height = h * scaleBy;
    cc.style.width = w + 'px';
    cc.style.height = h + 'px';
    var context = cc.getContext('2d');

    context.scale(scaleBy, scaleBy);

    // html2canvas(div, {
    //     canvas:cc,
    //     onrendered: function (canvas) {
            console.log("ON RENDERED!");

            // let croppedCanvas = trimCanvas(canvas);
            // var myImage = croppedCanvas.toDataURL("image/png");

            var style = `h3{margin:0; padding : 0;}
            @media print {
                .pagebreak { 
                    // page-break-before: always; 
                    page-break-after: always;
                    page-break-inside: avoid;
                }

                .dssCard {
                    page-break-inside: avoid;
                }
            }`;

            

            var bottomSummary = "";

            bottomSummary += "<h1>Month : " + $('#month_filter > option[selected="true"]').text() + " </h1><br>";
            bottomSummary += "<h3>" + $(".t_reports").text() + " </h3><br>";

            $("#reports_count_container")
            .children('span').each(function(i) { 
                bottomSummary += $(this).text() + "<br>";
            });

            bottomSummary += "<h3>" + $(".t_resolved").text() + " </h3><br>";

            $("#resolved_count_container")
            .children('span').each(function(i) { 
                bottomSummary += $(this).text() + "<br>";
            });

            bottomSummary += "<h3>Total Users : " + $(".t_users").text() + " </h3><br>";
            bottomSummary += "<h3>Total Students : " + $(".t_students").text() + " </h3><br>";

            var styleSheet = document.createElement("style")
            styleSheet.type = "text/css";
            styleSheet.innerText = style;
            tWindow.document.head.appendChild(styleSheet);

            
            // var array = $(".pie_canvas");
            // $(".pie_canvas").css("display", "inline-block");

            bottomSummary += '<div class="pagebreak"> </div><hr><div class="row"><div class="col-12" style="column-count:1;-webkit-column-count:1;-moz-column-count:1;column-count:1;">';
            let maxPiePerPage = 4;
            var current_counter = 0;

            var all = Object.keys(resolved_reports_data).length;
            Object.keys(resolved_reports_data).forEach((e, i) => {

                
                var theID = "#DSSCanvas_" + e.split(" ").join("_");
                console.log(theID);
                let elem = $(theID).get(0);

                var alreadyShown = false;

                if($(elem).parent().css("display") != "none")
                {
                    alreadyShown = true
                }
                else
                {
                    $(elem).parent().css("display", "inline-block");
                }

                var thisCategoryProperName = elem.id.split("DSSCanvas").join("").split("_").join(" ");

                let tempImg = new Image();

                let imgUrl = elem.toDataURL("image/png", 1);
                
                tempImg.src = imgUrl;

                bottomSummary += `<div class="col-12 mb-2 dssCard">
                    <div class="card border-left-warning shadow">
                        <div class="card-header" style="background-color:#f2c70045;">
                            <h4 style="text-align:center;">`+toTitleCase(thisCategoryProperName)+`</h4>
                        </div>
                        <div class="card-body">
                        
                            <img style="width:100%;display:block;margin:auto;" src="`+imgUrl+`"/>
                            
                        </div>
                    </div>
                </div>`;

                // current_counter++;

                // if(current_counter >= maxPiePerPage)
                // {
                //     current_counter = 0;
                    // bottomSummary += '</div></div>';
                //     bottomSummary += '<div class="pagebreak"> </div><hr><div class="row"><div class="col-12" style="column-count:1;-webkit-column-count:1;-moz-column-count:1;column-count:1;">';
                // }

                tempImg.onload = function () {
                    console.log(tempImg.height);
                    if(i == all-1)
                    {                                   
                        tWindow.print();
                    }
                }

                if(i == all-1)
                {
                    bottomSummary += '</div></div>';
                    tWindow.document.body.appendChild(newImg);
                    // tWindow.document.body.insertAdjacentHTML( 'beforeend',  "<img id='Image' src=" + myImage + " style='width:80%;   left: 0;  top: 0;'></img><br>" + bottomSummary);
                    tWindow.document.body.insertAdjacentHTML( 'beforeend',   bottomSummary);
                }

                // var key = $("#category_filter").val();
                // var formatted_key = key.split(" ").join("_");
                //$("#DSSCanvas_" + formatted_key).parent().css("display", "inline-block");
                // if("DSSCanvas_" + formatted_key != theID)
                if(alreadyShown == false)
                    $(elem).parent().css("display", "none");
            });

            // $(".pie_canvas").css("display", "none");


            
            // if(key in resolved_reports_data)
            // {
            //     $("#pie_no_data").css("display", "none");  
                
            // }
            // else
            // {
            //     $("#pie_no_data").css("display", "block");  
            // }

            // $(".pie_canvas").css("display", "none");

            tWindow.focus();             
    //     }
    // });
}

function pendPrint(tWindow)
{
    
}

function attachButtonCallbacks() {
    $('#printChartButton').on('click', function() {
        printAll();
    });

    $("#show_actions").on('click', function() {
        if (showingActions)
            hideActionLabels();
        else
            showActionLabels();

        showingActions = !showingActions;
    });

    $("#reset_zoom").on('click', function() {
        myChart.resetZoom();
    });

    $('body').on('contextmenu', '#myCanvas', function(e) {
        return false;
    });


    var zoomOptions = myChart.options.plugins.zoom.zoom;
    zoomOptions.drag = false;
    myChart.update();

    // $("#drag_switch").on('click', function(){
    //       // var chart = window.myLine;
    //       // var zoomOptions = myChart.options.plugins.zoom.zoom;
    //       // zoomOptions.drag = zoomOptions.drag ? false : dragOptions;

    //       // myChart.update();
    //       // document.getElementById('drag_switch').innerText = !zoomOptions.drag ? 'Disable drag mode' : 'Enable drag mode';

    //       $("#myCanvas").css('cursor', (zoomOptions.drag)? 'move' : 'default');
    // });

}



function showActionLabels() {
    

    Object.keys(myActions).forEach((key, i) => {
        // console.log(myActions[key]);
        let datasetIndex = myActions[key].index;
        let pointIndex = myActions[key].subIndex;

        myChart.data.datasets[datasetIndex].data = myActions[key].data;
        // myChart.update();
        // myChart.data.datasets[datasetIndex].data = cached_action_hover;
        myChart.update();


        if (myChart.tooltip._active == undefined)
            myChart.tooltip._active = []


        var activeElements = myChart.tooltip._active;
        var requestedElem = myChart.getDatasetMeta(datasetIndex).data[pointIndex];
        for (var i = 0; i < activeElements.length; i++) {
            if (requestedElem._index == activeElements[i]._index)
                break;
        }

        activeElements.push(requestedElem);



        myChart.tooltip._active = activeElements;

        // myChart.update();

    });


    
    myChart.tooltip.update(true);
    
    // myChart.tooltip.update(true);

    myChart.draw();
}

var cached_action_hover = [];

function hideActionLabels() {

    Object.keys(myActions).forEach((key, i) => {



        console.log(myActions[key]);
        let datasetIndex = myActions[key].index;
        let pointIndex = myActions[key].subIndex;

        // cached_action_hover = [...myChart.data.datasets[datasetIndex].data];
        myChart.data.datasets[datasetIndex].data = [];
        // myChart.tooltip._active = [];


        var activeElements = myChart.tooltip._active;
        if (!(activeElements == undefined || activeElements.length == 0)) {
            var requestedElem = myChart.getDatasetMeta(datasetIndex).data[pointIndex];
            for (var i = 0; i < activeElements.length; i++) {
                if (requestedElem._index == activeElements[i]._index) {
                    activeElements.splice(i, 1);
                    break;
                }
            }

            myChart.tooltip._active = activeElements;
        }



    });

    myChart.tooltip.update(true);
    myChart.update();
    myChart.draw();
}

function closeTips(datasetIndex, pointIndex) {
    var activeElements = myChart.tooltip._active;
    if (activeElements == undefined || activeElements.length == 0)
        return;
    var requestedElem = myChart.getDatasetMeta(datasetIndex).data[pointIndex];
    for (var i = 0; i < activeElements.length; i++) {
        if (requestedElem._index == activeElements[i]._index) {
            activeElements.splice(i, 1);
            break;
        }
    }

    myChart.tooltip._active = activeElements;
    myChart.tooltip.update(true);
    myChart.draw();
}


function openTip(datasetIndex, pointIndex) {
    if (myChart.tooltip._active == undefined)
        myChart.tooltip._active = []
    var activeElements = myChart.tooltip._active;
    var requestedElem = myChart.getDatasetMeta(datasetIndex).data[pointIndex];
    for (var i = 0; i < activeElements.length; i++) {
        if (requestedElem._index == activeElements[i]._index)
            return;
    }
    activeElements.push(requestedElem);
    //window.oChart.tooltip._view.body = window.oChart.getDatasetMeta(datasetIndex).data;
    myChart.tooltip._active = activeElements;
    myChart.tooltip.update(true);
    myChart.draw();
}



function initChart() {
    let initial_datas = new Array();

    // let blank_datas = new Array(12);
    // blank_datas = blank_datas.fill({t:"2020-01-01 00:00", y: -100});

    Object.keys(categories).forEach((key, i) => {

        $('#category_filter').append('<option value="'+key+'">'+key+'</option>');

        var formatted_key = key.split(" ").join("_");
        $("#chart_pie_container").append('<div class="pie_canvas col-12" style="display:inline-block;"><canvas id="DSSCanvas_'+formatted_key+'"  style="width: 512px; height: 256px; "> </canvas></div>');


        initial_datas.push({
            label: capitalizeFLetter(categories[key].name),
            backgroundColor: hexToRgbA(categories[key].color, 0.5),
            // borderColor: invertColor(categories[key].color, true),
            borderColor: categories[key].color,
            borderWidth: 3,
            lineTension: 0,
            fill : false,
            showLine : true,
            data: 
            //     // blank_datas
                [
                {
                    t: "2020-01-01 00:00",
                    y: -0
                },
                {
                    t: "2020-02-01 00:00",
                    y: -0
                },
                {
                    t: "2020-03-01 00:00",
                    y: -0
                },
                {
                    t: "2020-04-01 00:00",
                    y: -0
                },
                {
                    t: "2020-05-01 00:00",
                    y: -0
                },
                {
                    t: "2020-06-01 00:00",
                    y: -0
                },
                {
                    t: "2020-07-01 00:00",
                    y: -0
                },
                {
                    t: "2020-08-01 00:00",
                    y: -0
                },
                {
                    t: "2020-09-01 00:00",
                    y: -0
                },
                {
                    t: "2020-10-01 00:00",
                    y: -0
                },
                {
                    t: "2020-11-01 00:00",
                    y: -0
                },
                {
                    t: "2020-12-01 00:00",
                    y: -0
                }
            ]
            ,
            pointStyle: 'circle',
            radius: 3,
            hoveRadius: 7
        });
    });

    myCanvas = document.getElementById('myCanvas');
    let ctx = myCanvas.getContext('2d');
    myChart = new Chart(ctx, {
        // plugins: [ChartDataLabels],

        type: 'line',
        data: {
            // labels : months,
            datasets: initial_datas
        },

        options: {
            animation: {
                onComplete: function() {
                    let height = $("#chart-panel-body").height();
                    console.log("height is " + height);
                    $(".recommend_card").height(height);
                }
             },
            bezierCurve: false,
            lineTension : 0,
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1,
                        suggestedMax: 25,
                        min: 0
                        // ,
                        // callback: function(value, index) {
                        //     if (value !== 0) return value + '%';
                        //     /* OR *
                        //     if (value === 0) return 100 + '%';
                        //     else return value + '%'; */
                        //  }
                    }
                }],
                xAxes: [{
                    type: 'time',
                    time: {
                        displayFormats: {
                            month: 'MMMM'
                        },
                        unit : 'month'
                    }
                    // time: {
                    //     unit: 'day'
                    // }
                }]
            },

            legend: {
                labels: {
                    filter: function(item, chart) {
                        // Logic to remove a particular legend item goes here
                        // console.log(item.pointStyle);
                        // return !item.text.includes('Oplan') && item.pointStyle == "recRot";
                        return item.pointStyle == undefined;
                        // return  true;
                    }
                }
            }
            ,

            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';



                        // Don't show count if this is an action
                        // if(!label in myActions)
                        if (label.toLowerCase() in categories) {
                            if (label) {
                                label += ': ';
                            }
                            label += Math.round(tooltipItem.yLabel * 100) / 100;
                        }

                        return label;
                    }
                }
            },



            plugins: {
                datalabels: {
                    formatter: (value, ctx) => {
                    
                    //   let sum = 0;
                    //   let dataArr = ctx.chart.data.datasets[0].data;
                    //   dataArr.map(data => {
                    //       sum += data;
                    //   });
                    //   let percentage = (value*100 / sum).toFixed(2)+"%";
                    //   return percentage;
                      return null;
    
                  
                    },
                    color: '#ffffff',
                },
                zoom: {
                    // Container for pan options
                    pan: {
                        // Boolean to enable panning
                        enabled: true,

                        // Panning directions. Remove the appropriate direction to disable
                        // Eg. 'y' would only allow panning in the y direction
                        // A function that is called as the user is panning and returns the
                        // available directions can also be used:
                        //   mode: function({ chart }) {
                        //     return 'xy';
                        //   },
                        mode: 'xy',

                        rangeMin: {
                            // Format of min zoom range depends on scale type
                            x: 0,
                            y: 0
                        },
                        rangeMax: {
                            // Format of max zoom range depends on scale type
                            x: 0,
                            y: Infinity
                        },

                        // On category scale, factor of pan velocity
                        speed: 20,

                        // Minimal pan distance required before actually applying pan
                        threshold: 10,

                        // Function called while the user is panning
                        onPan: function({
                            chart
                        }) {
                            console.log(`I'm panning!!!`);
                        },
                        // Function called once panning is completed
                        onPanComplete: function({
                            chart
                        }) {
                            console.log(`I was panned!!!`);
                        }
                    },

                    // Container for zoom options
                    zoom: {
                        // Boolean to enable zooming
                        enabled: true,

                        // Enable drag-to-zoom behavior
                        drag: true,

                        // Drag-to-zoom effect can be customized
                        // drag: {
                        //   borderColor: 'rgba(225,225,225,0.3)'
                        //   borderWidth: 5,
                        //   backgroundColor: 'rgb(225,225,225)',
                        //   animationDuration: 0
                        // },

                        // Zooming directions. Remove the appropriate direction to disable
                        // Eg. 'y' would only allow zooming in the y direction
                        // A function that is called as the user is zooming and returns the
                        // available directions can also be used:
                        //   mode: function({ chart }) {
                        //     return 'xy';
                        //   },
                        mode: 'xy',

                        rangeMin: {
                            // Format of min zoom range depends on scale type
                            x: 0,
                            y: 0
                        },
                        rangeMax: {
                            // Format of max zoom range depends on scale type
                            x: 0,
                            y: Infinity
                        },

                        // Speed of zoom via mouse wheel
                        // (percentage of zoom on a wheel event)
                        speed: 0.1,

                        // Minimal zoom distance required before actually applying zoom
                        threshold: 2,

                        // On category scale, minimal zoom level before actually applying zoom
                        sensitivity: 3,

                        // Function called while the user is zooming
                        onZoom: function({
                            chart
                        }) {
                            console.log(`I'm zooming!!!`);
                        },
                        // Function called once zooming is completed
                        onZoomComplete: function({
                            chart
                        }) {
                            console.log(`I was zoomed!!!`);
                        }
                    }
                }
            }
        }
    });

    myChart.options.plugins.zoom.pan.rangeMin.x = myChart.scales["x-axis-0"].min.valueOf();
    myChart.options.plugins.zoom.pan.rangeMax.x = myChart.scales["x-axis-0"].max.valueOf();

    myChart.options.plugins.zoom.zoom.rangeMin.x = myChart.scales["x-axis-0"].min.valueOf();
    myChart.options.plugins.zoom.zoom.rangeMax.x = myChart.scales["x-axis-0"].max.valueOf();

    // generateLegend();
    attachButtonCallbacks();
    fetchReports();

    $('#category_filter').selectpicker('setStyle', 'btn-info');
    
    var key = $("#category_filter").val();

    if(key in resolved_reports_data)
    {
        $("#pie_no_data").css("display", "none");  
    }
    else
    {
        $("#pie_no_data").css("display", "block");  
    }
    // selected_month_to_show = parseInt(e.options[e.selectedIndex].value);
    
    $("#category_filter").on('changed.bs.select', function(e, clickedIndex, isSelected, previousValue){
        
        console.log("click index is " + clickedIndex);
        if(clickedIndex == undefined){
            // let e = $('#month_filter').get(0);
            selected_category_to_show = parseInt(this.options[this.selectedIndex].value);
        }
        else
            selected_category_to_show = clickedIndex-1;

        $(".pie_canvas").css("display", "none");
        initDSSCharts(null);
    });

}

function removeZeroActions()
{
    for (var i = 1; i <= myChart.data.datasets[0].data.length - 1; i++) {
        if (myChart.data.datasets[0].data[i - 1] === myChart.data.datasets[0].data[i])
            myChart.datasets[0].points[i].display = false;
    }

}

function hexToRgbA(hex, alpha) {
    if (!alpha)
        alpha = 1;
    var c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex.substring(1).split('');
        if (c.length == 3) {
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c = '0x' + c.join('');
        return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + alpha + ')';
    }
    throw new Error('Bad Hex');
}

function getLargestReportCountByCategories(categories_array, monthIndex) {
    let max = {
        value: 0,
        cat: ""
    };
    for (var i = 0; i < categories_array.length; i++) {
        // console.log(categories_array[i]);

        if (categories_array[i] in reportCountPerMonth) {

            var thisReportCount = reportCountPerMonth[categories_array[i]][monthIndex];

            // console.log(categories_array[i] + " monthIndex is " + monthIndex + ", thisReportCount is " + thisReportCount);

            if (thisReportCount > max.value) {
                max = {
                    value: thisReportCount,
                    cat: categories_array[i]
                };
                // console.log("found max " + categories_array[i]);
            }
        }
    }

    return max;
}


function getMonthIndex(month) {
    return months.indexOf(capitalizeFLetter(month));
}

function addChartCategory(new_category_name) {
    myChart.data.datasets.push({
        label: new_category_name,
        data: [],
        pointStyle: 'circle'
    });

    return myChart.data.datasets.length - 1;
}

function getChartLabelIndex(chart_label) {
    chart_label = chart_label.toLowerCase();

    for (var i = 0; i < myChart.data.datasets.length; i++) {
        let dataset = myChart.data.datasets[i];

        if (dataset.label.toLowerCase() == chart_label)
            return i;
    }

    return -1;
}

function updateData(theLabel, date, value) {
    let index = getChartLabelIndex(theLabel);

    if (index == -1)
        index = addChartCategory(theLabel);


    let splittedDate = date.split(' ');
    let monthIndex = months.indexOf(splittedDate[1]);
    if (!isNaN(monthIndex) && monthIndex > -1) {
        // console.log(monthIndex);

        // let dataCopy = [...myChart.data.datasets[index].data];
        // dataCopy[monthIndex] = {
        //     t: date,
        //     y: value
        // };

        myChart.data.datasets[index].data[monthIndex] = {
            t: date,
            y: value
        };
        myChart.clear();
        myChart.update();
    }
}

function updateActionData(theLabel, date, actionCategories) {
    let index = getChartLabelIndex(theLabel);

    if (index == -1)
        index = addChartAction(theLabel);

    let splittedDate = date.split('-');
    let monthIndex = parseInt(splittedDate[1]);
    if (!isNaN(monthIndex) && monthIndex > -1) {
        console.log(theLabel + " adding");
        let myColor = "#f2c700";
        let myBorderColor = "#000000";
        let largest_report_count = getLargestReportCountByCategories(actionCategories, monthIndex - 1);


        myChart.data.datasets[index].data[monthIndex - 1] = {
            t: date,
            y: largest_report_count.value
        };

        let dataCopy = [...myChart.data.datasets[index].data];

        myActions[theLabel] = {
            data: dataCopy,
            index: index,
            subIndex: monthIndex - 1,
            myCategories: actionCategories
        };

        if (largest_report_count.cat in categories) {
            console.log("update color");

            myColor = categories[largest_report_count.cat].color;
            myBorderColor = invertColor(categories[largest_report_count.cat].color, true);
        }

        myChart.data.datasets[index].backgroundColor = myColor;
        myChart.data.datasets[index].borderColor = myBorderColor;

        myChart.update();
    }
}

function showSummaryByMonth()
{
    //show all
    var showAll = (selected_month_to_show == -1);

    //compute sum of resolved report
    var resolved = 0;
    // var resolved = resolved_report_count;
    // let reports = total_reports_count;
    var reports = 0;

    if(showAll)
    {
        resolved = 0;
        // Object.keys(resolved_per_month).forEach(function(i){
        //     resolved += resolved_per_month[i][selected_month_to_show];
        // });
        for(var j=0; j < resolvedCountTotalPerMonth.length; j ++)
        {
            resolved += resolvedCountTotalPerMonth[j];
        }

        reports = 0;
        // Object.keys(reportCountPerMonth).forEach(function(i){
        //     reports += reportCountPerMonth[i][selected_month_to_show];
        // });

        for(var i=0; i < reportCountTotalPerMonth.length; i ++)
        {
            reports += reportCountTotalPerMonth[i];
        }

        reports -= resolved;
    }
    else
    {
        resolved = resolvedCountTotalPerMonth[selected_month_to_show];
        reports = reportCountTotalPerMonth[selected_month_to_show]-resolved;        
    }


    console.log("reports" + reports);
    // fadeInOutElement(".t_reports", "Unresolve Reports : " + reports);    
    $(".t_reports").text("Unresolve Reports : " + reports);


    // fadeInOutElement(".t_resolved", "Resolved Reports : " + resolved);
    $(".t_resolved").text("Resolved Reports : " + resolved);

    
}

function refreshActionSummary() {
    console.log('refreshActionSummary');

    // fetchServerTime(function() {
        $('#action_summary_container').empty();

        Object.keys(categories).forEach((key, i) => {


            if (key in reportCountPerMonth) {
                let date = getCurrentDateServerOffset();
                let nowMonth = date.getMonth();

                let nowCount = reportCountPerMonth[key][Math.max(0, nowMonth)];

                // console.log("nowCount of " + key + " " + nowCount);
                let lastCount = report_counts[key] - nowCount; //reportCountPerMonth[key][Math.max(0, nowMonth-1)];

                // console.log("lastCount of " + key + " " + lastCount);

                if (nowMonth == 0)
                    lastCount = 0;

                let percentChange = ((lastCount/(nowMonth+1))/nowCount + (nowCount/(nowMonth+1))).toFixed(2);//(getPercentageChange(nowCount + lastCount, lastCount)/ 12).toFixed(2);

                // console.log("percentChange of " + key + " " + percentChange );
                // percentChange = isFinite(percentChange) ? percentChange : 0.0;
                if(!isFinite(percentChange))
                {
                    if(nowCount == 0)
                        percentChange = (0 - (lastCount/(nowMonth+1))).toFixed(2);
                    else
                        percentChange = 0;
                }

                let infoText = percentChange + '% ' + ((percentChange > 0) ? 'increase' : 'decrease') + ' reported from previous months overall reports(' + lastCount + ') ';

                if (percentChange == 0)
                    infoText = 'overall reports from previous months(' + lastCount + ')';

                let txtColor = 'white';

                if (percentChange > 0)
                    txtColor = lerpColor('#FFFFFF', '#FF0000', percentChange / 100);
                if (percentChange < 0)
                    txtColor = lerpColor('#68ff00', '#FFFFFF', percentChange / 100);

                let toAppend =
                    `<div class="p-1 m-1 action_summary_entry">
                  <span class="badge badge-success" style="background-color:` + categories[key].color + `; color:` + invertColor(categories[key].color, true) + `;">(` + report_counts[key] + ")" + key + `</span>
                  <small style="color:` + txtColor + `;">  ` + ((percentChange > 0) ? '+' : '') + infoText + ` </small>   
              </div>`;

                // console.log(toAppend);

                $('#action_summary_container').append(toAppend);
            }
        });
    // });
}

function addChartAction(new_action_name) {
    myChart.data.datasets.push({
        label: new_action_name,
        data: [{
                t: "2020",
                y: -100
            },
            {
                t: "2020",
                y: -100
            },
            {
                t: "2020",
                y: -100
            },
            {
                t: "2020",
                y: -100
            },
            {
                t: "2020",
                y: -100
            },
            {
                t: "2020",
                y: -100
            },
            {
                t: "2020",
                y: -100
            },
            {
                t: "2020",
                y: -100
            },
            {
                t: "2020",
                y: -100
            },
            {
                t: "2020",
                y: -100
            },
            {
                t: "2020",
                y: -100
            },
            {
                t: "2020",
                y: -100
            }
        ],
        type: 'bubble',
        radius: 12,
        pointStyle: 'rectRot',

    });

    return getChartLabelIndex(new_action_name);
}

function removeDatas(theLabel) {
    let index = getChartLabelIndex(theLabel);
    if (index > -1) {
        myChart.data.datasets.splice(index, 1);
        myChart.update();
    }
}

// function alreadyHaveOnGoingActionForThisCategorySuggestion(category_name)
// {
//     Object.keys(myActions).forEach((key, i) => {

//         let thisAction = myActions[key];
//         if(category_name in thisAction.myCategories)
//         {
//             //we are only getting the latest category action month index
//             if(ret == null || ret.subIndex < thisAction.subIndex)
//                 ret = key;
//         }
//     });

//     return false;
// }

function updateSuggestion(key, data) {
    let date = getCurrentDateServerOffset();
    let nowMonth = date.getMonth();

    let nowCount = 0;
    if((data.category in reportCountPerMonth))
      nowCount = reportCountPerMonth[data.category][Math.max(0, nowMonth)];
    

    console.log(data.category + " == " + nowCount);
    var pushed = false;

    if(maxReportCountThisMonthCategory.includes(data.category))
    {
        pushed = true;
    }

    if(nowCount == maxReportCountThisMonth)
    {
        if(!maxReportCountThisMonthCategory.includes(data.category))
        {
            maxReportCountThisMonthCategory.push(data.category);
            // pushed = true;
        }
    }
    else if(nowCount > maxReportCountThisMonth)
    {
        maxReportCountThisMonth = nowCount;
        maxReportCountThisMonthCategory = new Array();
        previousMaxReportCountThisMonthCategory = new Array();
        
        maxReportCountThisMonthCategory.push(data.category);
        pushed = true;        
    }

    

    if(pushed)
    {
        if(!previousMaxReportCountThisMonthCategory.includes(data.category))
        {
            $("#suggestions_container").empty();
        }

        let active = (document.getElementById("suggestions_container").childElementCount == 0) ? 'active' : '';
        let toAppend = '<div class="carousel-item text-center alert alert-danger ' + active + ' p-4" id="' + key + '">' +'</div>';

        // check if there's already existing slide item
        if (document.getElementById(key) == null) {
            $("#suggestions_container").append(toAppend);
        }

        $("#" + key).html(data.suggestion);

        if (document.getElementById("no_action_recommended") != null) {
            $("#no_action_recommended").remove();
            $("#" + key).addClass('active');
        }

        $("#suggestionCarousel").carousel();

        if(!previousMaxReportCountThisMonthCategory.includes(data.category))
        {
            previousMaxReportCountThisMonthCategory.push(data.category);
        }
    }
}

function updateSuggestionOLD(key, data) {
    let date = getCurrentDateServerOffset();
    let nowMonth = date.getMonth();

    let nowCount = 0;
    if((data.category in reportCountPerMonth))
      nowCount = reportCountPerMonth[data.category][Math.max(0, nowMonth)];

    let lastCount = 0; //reportCountPerMonth[key][Math.max(0, nowMonth-1)];

    if((data.category in report_counts))
      lastCount = report_counts[data.category] - nowCount

    if (nowMonth == 0)
        lastCount = 0;

    // let percentChange = ((lastCount/(nowMonth-1))/nowCount + (nowCount/(nowMonth-1))).toFixed(2);//(getPercentageChange(nowCount + lastCount, lastCount)/ 12).toFixed(2) ;
    let percentChange = ((lastCount/(nowMonth+1))/nowCount + (nowCount/(nowMonth+1))).toFixed(2);

    percentChange = parseFloat(percentChange);

    // console.log(percentChange + '% for ' + data.category);

    let show = (percentChange >=  data.lower_limit && percentChange <= data.upper_limit); //((data.lower_limit > 0 && diff >= data.lower_limit) && diff <= data.upper_limit && data.active == true);
    if (show) {
        console.log("suggestion valid");
        let headerSummary =
            '<small style="font-style:italic;">' +
            '<p>The percentage of reports for category "' + data.category + '" increased by ' + percentChange + '%</p>';


        let active = (document.getElementById("suggestions_container").childElementCount == 0) ? 'active' : '';
        let toAppend = '<div class="carousel-item text-center alert alert-danger ' + active + ' p-4" id="' + key + '">' +
            
            '</div>';

        // check if there's already existing slide item
        if (document.getElementById(key) == null) {
            $("#suggestions_container").append(toAppend);

        }
        $("#" + key).html(headerSummary + data.suggestion);

        if (document.getElementById("no_action_recommended") != null) {
            $("#no_action_recommended").remove();
            $("#" + key).addClass('active');
        }

        $("#suggestionCarousel").carousel();
    }
}

function getLatestCategoryActionTitle(category_name) {
    let ret = null;

    Object.keys(myActions).forEach((key, i) => {
        // Lets see if this category has any action
        let thisAction = myActions[key];
        if (thisAction.myCategories.indexOf(category_name) > -1) {
            //we are only getting the latest category action month index
            if (ret == null || ret.subIndex < thisAction.subIndex)
                ret = key;
        }
    });

    return ret;
}

function getLatestCategoryAction(category_name) {
    let ret = null;

    Object.keys(myActions).forEach((key, i) => {
        // Lets see if this category has any action
        let thisAction = myActions[key];
        if (thisAction.myCategories.indexOf(category_name) > -1) {
            //we are only getting the latest category action month index
            if (ret == null || ret.subIndex < thisAction.subIndex)
                ret = thisAction;
        }
    });


    return ret;
}


var fetchingSuggestion = false;
function fetchSuggestions() {
    
    if(fetchingSuggestion == true)
        return;

    fetchingSuggestion = true;
    console.log("fetchSuggestions");

    $("#suggestions_container").empty();
    maxReportCountThisMonth = 0;
    maxReportCountThisMonthCategory = "";

//   fetchServerTime(function(){

    // try {

        // let date = getCurrentDateServerOffset();
        // if((category_names[0] in suggestions_listener))
        //   suggestions_listener[category_names[0]]();

        // suggestions_listener[category_names[0]] = 
        db.collection("suggestions").where('active', '==', true).onSnapshot(function(querySnapshot) {

            let requests = querySnapshot.docChanges().map((change) => {

                // console.log("change type " + change.type);
                // let splittedActionDate = change.doc.data().start_date.split("-");
                // let thisActionMonthIndex = parseInt(splittedActionDate[1]);
                if (change.type === "added") {

                    updateSuggestion(change.doc.id, change.doc.data());


                    // console.log("added suggestions");

                }

                if (change.type === "modified") {

                    updateSuggestion(change.doc.id, change.doc.data());
                    console.log("suggestion modified " + change.doc.data());
                }

                if (change.type === "removed") {

                }

                return new Promise((resolve) => {
                    asyncFunction(change, resolve);
                });

            });

            Promise.all(requests).then(() => {

                fetchingSuggestion = false;

                if (document.getElementById("suggestions_container").childElementCount == 0) {
                    $("#suggestions_container").append('<div id="no_action_recommended" class="carousel-item alert alert-success active stext-center p-4">' +
                        '<h3>Everything looks good! <br> No recommended actions.</h3>' +
                        '</div>');
                }

                $("#suggestionCarousel").carousel();

            });



        });
        
        // , function(error) {

        //     console.error(error.message);
        //     setTimeout(() => {
        //         fetchSuggestions(category_names);

        //     }, 3000);

        // });

//   });
      
}

function fetchStudentsCount() {
    try {
        db.collection("students").onSnapshot(function(querySnapshot) {

            querySnapshot.docChanges().forEach(function(change) {
                // console.log("change type " + change.type);
                if (change.type === "added") {
                    fadeInOutElement(".t_students", querySnapshot.size);
                }
            });
        }, function(error) {
            setTimeout(() => {
                fetchStudentsCount();
            }, 3000);
        });
    } catch (error) {
        setTimeout(() => {
            fetchStudentsCount();
        }, 3000);
    }
}

function fetchUsersCount() {
    try {
        db.collection("users").onSnapshot(function(querySnapshot) {

                querySnapshot.docChanges().forEach(function(change) {

                    // console.log("change type " + change.type);
                    if (change.type === "added") {
                        // console.log('start fading');
                        fadeInOutElement(".t_users", querySnapshot.size);
                    }
                });
            },
            function(error) {
                setTimeout(() => {
                    fetchUsersCount();
                }, 3000);

            });
    } catch (error) {
        setTimeout(() => {
            fetchUsersCount();
        }, 3000);
    }
}

function printSummary()
{
    var scaleBy = 5;
    
    var div = document.querySelector('#action_summary_container');
    var w = 1366;//div.getBoundingClientRect().width;
    var h = 768;//div.getBoundingClientRect().height;
    var canvas = document.createElement('canvas');
    canvas.width = w * scaleBy;
    canvas.height = h * scaleBy;
    canvas.style.width = w + 'px';
    canvas.style.height = h + 'px';
    var context = canvas.getContext('2d');

    context.scale(scaleBy, scaleBy);

    html2canvas(div, {
        canvas:canvas,
        onrendered: function (canvas) {
            // theCanvas = canvas;
            let croppedCanvas = trimCanvas(canvas);
            var myImage = croppedCanvas.toDataURL("image/png");
            var tWindow = window.open("");

            var style = '<style>h3{margin:0; padding : 0;}</style>';

            var bottomSummary = "";

            bottomSummary += "<h3>Total Reports : " + $(".t_reports").text() + " </h3><br>";
            bottomSummary += "<h3>Resolved Reports : " + $(".t_resolved").text() + " </h3><br>";
            bottomSummary += "<h3>Total Users : " + $(".t_users").text() + " </h3><br>";
            bottomSummary += "<h3>Total Students : " + $(".t_students").text() + " </h3><br>";

            tWindow.document.write('<html><head><title>Print Summary</title> '+style+'</head><body>');
            tWindow.document.write(bottomSummary + "<img id='Image' src=" + myImage + " style='width:80%;   left: 0;  top: 0;'></img><br>");
            tWindow.document.write('</body></html>');

            tWindow.focus();
            tWindow.print();
        }
    });
}


function updateReportCountToolTip() {
    let html_categories = "";
    $("#reports_count_container").empty();

    let requests = Object.keys(report_counts).map((report_name) => {
        // console.log(report_name);

        // html_categories += 
        // ('<button type="button" class="btn btn-primary"  style="outline-width: 2px; outline-color: black; color: '+invertColor(categories[report_name].color, true)+'; background-color: ' + categories[report_name].color + ' !important;">'+
        //   report_name +' <span class="badge badge-light">'+report_counts[report_name]+'</span>'+
        //   '<span class="sr-only"></span>'+
        // '</button><br>');
        let showAll = (selected_month_to_show == -1);
        let thisCategoryCount = report_counts[report_name];
        if(showAll == false)
            thisCategoryCount = reportCountPerMonth[report_name][selected_month_to_show];

        html_categories += '<span class="badge badge-primary my-1" style="font-size:1rem; max-width:100%; border-style:solid; border-width: 1px; border-color: gray; color:'+ invertColor(categories[report_name].color, true)+' !important; background-color: ' + categories[report_name].color + ' !important;"> <b>' + thisCategoryCount + ' : ' + report_name + '</b></span>' + '<br>';

        return new Promise((resolve) => {
            asyncFunction(report_name, resolve);
        });
    });

    Promise.all(requests).then(() => {
        if($("#reports_count_container").children().length > 0)
            $("#reports_count_container").empty();

        $("#reports_count_container").append(html_categories);

        // $("#month_filter").change();
        // console.log(html_categories);

        // var options = {
        //     content: html_categories,
        //     html: true,
        //     placement: 'auto',
        //     trigger: 'hover | click'
        //     // ,
        //     // template: '<div class="popover" style="max-width: max-content;" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
        // }

        // $('#pop_categories_count').popover('dispose');

        // $('#pop_categories_count').popover(options);
        // $("#pop_categories_count").off('click');

        // $("#pop_categories_count").on('click', function() {
        //     $("#pop_categories_count").popover('toggle');
        // });
    });
}


function updateResolvedCountToolTip() {
    let html_categories = "";
    $("#resolved_count_container").empty();

    let requests = Object.keys(resolved_counts).map((report_name) => {

        let showAll = (selected_month_to_show == -1);
        let thisCategoryCount = resolved_counts[report_name];
        if(showAll == false)
            thisCategoryCount = resolved_per_month[report_name][selected_month_to_show];

        html_categories += '<span class="badge badge-primary my-1" style="font-size:1rem;max-width:100%; border-style:solid; border-width: 1px; border-color: gray; color:'+ invertColor(categories[report_name].color, true)+' !important; background-color: ' + categories[report_name].color + ' !important;"> <b>' + thisCategoryCount + ' : ' + report_name + '</b></span>' + '<br>';

        return new Promise((resolve) => {
            asyncFunction(report_name, resolve);
        });
    });

    Promise.all(requests).then(() => {

        if($("#resolved_count_container").children().length > 0)
            $("#resolved_count_container").empty();

        $("#resolved_count_container").append(html_categories);
        // console.log(html_categories);

        // var options = {
        //     content: html_categories,
        //     html: true,
        //     placement: 'auto',
        //     trigger: 'hover | click'
        //     // ,
        //     // template: '<div class="popover" style="max-width: max-content;" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
        // }

        // $('#pop_resolved_count').popover('dispose');

        // $('#pop_resolved_count').popover(options);
        // $("#pop_resolved_count").off('click');

        // $("#pop_resolved_count").on('click', function() {
        //     $("#pop_resolved_count").popover('toggle');
        // });
    });
}



function loadCategories() {


    db.collection('categories').get().then(function(querySnapshot) {
        

        let requests = querySnapshot.docs.map((category) => {
            // console.log(category);
            categories[category.data().name.toString()] = category.data();



            return new Promise((resolve) => {
                asyncFunction(category, resolve);
            });
        });

        Promise.all(requests).then(() => {
            initChart();
            
            
        });

    });
}

function loadDataFromDBNew() {
    fetchStudentsCount();
    fetchUsersCount();
    // fetchReports();
    loadCategories();
}



function initVariables() {
    // rootRef = firebase.database().ref();
    // $("#printChart").on('click', function() {
    //     PrintChart();
    // });

    var on = false;
    if ("showActions" in sessionStorage)
        on = sessionStorage.getItem("showActions");
    $('#show_actions').prop('checked', on);

    $("#show_actions").on('click', function() {
        toggleShowPrintLabels($(this).prop('checked'));
        ToggleSeries();
    });

    // $("#month_filter").selectpicker();
    $('#month_filter').selectpicker('setStyle', 'btn-info');
    

    // selected_month_to_show = parseInt(e.options[e.selectedIndex].value);
    
    $("#month_filter").on('changed.bs.select', function(e, clickedIndex, isSelected, previousValue){
        
        
        if(clickedIndex == undefined){
            // let e = $('#month_filter').get(0);
            selected_month_to_show = parseInt(this.options[this.selectedIndex].value);
        }
        else
            selected_month_to_show = clickedIndex-1;

        console.log("click index is " + selected_month_to_show);
        showSummaryByMonth();
        updateReportCountToolTip();
        updateResolvedCountToolTip();
    });
}

function cloneCanvas(oldCanvas) {

    //create a new canvas
    var newCanvas = document.createElement('canvas');
    var context = newCanvas.getContext('2d');

    //set dimensions
    newCanvas.width = oldCanvas.width;
    newCanvas.height = oldCanvas.height;

    //apply the old canvas to the new one
    context.drawImage(oldCanvas, 0, 0);

    //return the new canvas
    return newCanvas;
}

function newPrint() {
    var printContents = document.getElementById('chart-panel-body').innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}




function beforePrint () {
  for (const id in Chart.instances) {
    Chart.instances[id].resize()
  }
}

if (window.matchMedia) {
  let mediaQueryList = window.matchMedia('print')
  mediaQueryList.addListener((mql) => {
    if (mql.matches) {
      beforePrint()
    }
  })
}

window.onbeforeprint = beforePrint;


// function PrintChart() {
//     showPrintLabels = true;
//     ToggleSeries();

//     var elem = $('#chart-panel-body').get(0).cloneNode(true);

//     var origCanvas = $(".flot-base").get(0);
//     var newCanvas = cloneCanvas(origCanvas);

//     //origCanvas.parentNode.removeChild(origCanvas);
//     $(elem).find(".flot-base").remove();

//     var imgObj = origCanvas.toDataURL("image/png");

//     var imgElem = document.createElement("IMG");
//     imgElem.src = imgObj;

//     $(elem).find("#dashboard_flot").get(0).insertBefore(imgElem, $(elem).find("#dashboard_flot").get(0).firstChild);

//     $(elem).find("input").remove();
//     //    $(elem).find("#header_buttons").remove();
//     $(elem).find(".card-header").remove();
//     $(elem).find("#dashboard_flot").css({
//         "height": ''
//     });

//     $(elem).find("#legend_container").empty();
//     $(elem).find("#legend_container").css('padding-left', '20px');
//     $(elem).find("#legend_container").css('padding-right', '20px');
//     $("#legend_container div").each(function(index) {
//         var txt = document.createElement("DIV");
//         txt.innerText = $(this).get(0).innerText;
//         $(txt).css({
//             'cssText': 'background-color: ' + $(this).css("background-color") + ' !important; line-height:normal; margin-bottom: 5px; color : white !important;'
//         });
//         $(txt).addClass("badge");
//         $(txt).addClass("badge-pill");
//         $(elem).find("#legend_container").get(0).appendChild(txt);
//         $(elem).find("#legend_container").append("<br>");
//     });

//     var mywindow = window.open('', 'PRINT', 'height=600,width=1280');


//     var includes = '<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css">' +
//         '<link rel="stylesheet" href="css/custom/dashboard.css">' +
//         '<style media="print" type="text/css"> *{ color-adjust: exact; -webkit-print-color-adjust: exact; }</style>';


//     var endScript = '<script>setTimeout(lol, 2000); function lol(){window.print(); window.close(); window.opener.toggleShowPrintLabels(false); window.opener.ToggleSeries();}</script>';

//     var endImports = '<script src="assets/vendor/jquery/jquery.js"></script>' +
//         '<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>' +

//         '<script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>' +

//         '<script src="assets/vendor/flot/jquery.flot.js"></script>' +

//         '<script src="assets/vendor/flot/jquery.flot.resize.js"></script>' +

//         '<script src="assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>' +

//         '<script src="assets/vendor/flot/jquery.flot.pie.js"></script>' +

//         '<script src="assets/vendor/flot/jquery.flot.categories.js"></script>' +

//         '<script src="js/custom/html2canvas.min.js"></script>';

//     mywindow.document.write('<html><head><title>' + document.title + '</title>');
//     mywindow.document.write(includes);


//     // mywindow.document.write(excludePrint);

//     mywindow.document.write('</head><body >');
//     mywindow.document.write('<section class="panel">' + elem.innerHTML + '</section>');


//     mywindow.document.write(endImports);

//     mywindow.document.write(endScript);

//     mywindow.document.write('</body></html>');

//     mywindow.focus();



//     return true;
// }


function raw(plot, ctx) {
    var canvas = document.getElementsByClassName("flot-base")[0];
    context = canvas.getContext("2d");
    cw = canvas.width;
    ch = canvas.height;
    thePlot = $(".flot-base");
    reinitTestCanvas();

    actions_pos = [];
    // var maxLabelString = 17;
    var data = plot.getData();
    var axes = plot.getAxes();
    var offset = plot.getPlotOffset();
    for (var i = 0; i < data.length; i++) {
        var series = data[i];
        //console.log(series);

        for (var j = 0; j < series.data.length; j++) {
            var d = (series.data[j]);
            var plotX = d[0];
            var plotY = d[1];

            var thisActions = getAllActionByCategory(series.label, plotX);
            if (thisActions.length > 0) {




                //console.log(actions_pos[plotY]);
                // newY = y;
                // var key = series.label;
                // if (actions_pos[key] != undefined) {
                //     var resultY = getEmptySpaceOnPlot(plotX, plotY, series.label);
                //     newY = offset.top + axes.yaxis.p2c(resultY);
                // }
                // else {
                //     actions_pos[key] = [];
                //     actions_pos[key].push([plotX, plotY]);
                // }

                var x = offset.left + axes.xaxis.p2c(plotX);
                var y = offset.top + axes.yaxis.p2c(plotY);
                var r = 5; //radius[j];
                ctx.lineWidth = 2;
                ctx.beginPath();
                ctx.arc(x, y, r, 0, Math.PI * 2, true);
                ctx.closePath();
                ctx.fillStyle = series.color;
                ctx.fill();

                //this will be used to combine actions that lies on the same plot
                for (let index = 0; index < thisActions.length; index++) {
                    const element = thisActions[index];
                    if (actions_local[series.label] == undefined)
                        actions_local[series.label] = [];
                    actions_local[series.label].push({
                        title: element.title,
                        x: plotX,
                        y: plotY,
                        categories: element.categories
                    });
                }


                if (showPrintLabels) {

                    for (let index = 0; index < thisActions.length; index++) {

                        var title = thisActions[index].title;

                        var label = addLabel(title, x, y, fontSize, fontFace, dotRadius)

                        drawLabel(label);

                        y += fontSize;
                    }
                }
            }
        }
    }



    // context = ctx;
    // cw = ctx.width;
    // ch = ctx.height;



};

function checkRightSidePlot(thisX, thisY) {

    var retValue = false;
    console.log("begin checking right");
    Object.keys(actions_pos).forEach(function(key, i) {
        for (let index = 0; index < actions_pos[key].length; index++) {
            const element = actions_pos[key][index];

            console.log(element);

            var num = parseInt(element[0]);
            var num2 = parseInt(element[1]);

            if (num == thisX && num2 == thisY) {
                console.log("X : " + thisX + "==" + num);
                retValue = true;
                break;
            } else {
                console.log("X : " + thisX + "!=" + num);
            }

            if (i >= actions_pos.length && index >= actions_pos[key].length) {
                console.log(actions_pos);
                // return false;
                retValue = false;
                break;
            }
        }
    });

    return retValue;
}

function getEmptySpaceOnPlot(x, y, newKey) {
    var resultY = y;
    if (!(newKey in actions_pos)) {
        actions_pos[newKey].push([x, y]);
    } else {
        for (let index = 0; index < actions_pos[newKey].length; index++) {
            const element = actions_pos[newKey][index];

            if (element[1] == resultY) {
                resultY++;
            }
        }
        actions_pos[newKey].push([x, resultY]);
    }

    console.log(actions_pos);
    return resultY;
}



function getMonthIndex(monthStr) {
    for (let index = 0; index < months.length; index++) {
        const element = months[index];
        if (element == monthStr)
            return index;
    }
}

function toggleShowPrintLabels(show) {
    showPrintLabels = show;
}

function ToggleSeries() {
    console.log("begin toggle series");
    var d = [];
    checked_categories = "";
    $(".legendBox").each(function() {
        //var attr = $(this).attr('show');

        // For some browsers, `attr` is undefined; for others,
        // `attr` is false.  Check for both.
        var key = $(this).attr("id");
        if ($(this).get(0).checked) {
            if (checked_categories.length == 0)
                checked_categories += key;
            else
                checked_categories += "," + key;

            d.push({
                label: key,
                data: dataSet[key].myDatas,
                color: dataSet[key].origColor,
                points: {
                    radius: 3,
                    symbol: "circle",
                    show: false
                },
                lines: {
                    show: true,
                    fill: true,
                }
            });
        } else {
            // checked_categories.replace(key, "");
            var r = new RegExp(key, "g");
            checked_categories.replace(r, '');
            checked_categories.replace(/,,/g, ",");
            d.push({
                label: key,
                data: []
            });
        }


    });

    $.plot($("#dashboard_flot"), d, options);

    $("#dashboard_flot").UseTooltip();

    resizeWindow();
}



$.fn.UseTooltip = function() {
    $(this).off("plothover");

    $(this).bind("plothover", function(event, pos, item) {
        if (item) {
            //if (previousPoint != item.dataIndex) {
            previousPoint = item.dataIndex;

            $("#tooltip").empty();
            $("#tooltip").remove();

            var x = item.datapoint[0];
            var y = item.datapoint[1];

            // console.log(x+","+y);

            //add aditional action info if it has one
            var action_text = "";

            var thisActions = getAllActionByCategory(item.series.label, x);
            // if(actions[x] != undefined)
            // {
            //     if(actions[x].categories.includes(item.series.label))
            //     {
            //         action_text = actions[x].title + "<br>";
            //     }
            // }

            action_text += "<strong>" + y + "</strong> (" + item.series.label + ")";

            if (thisActions.length > 0) {
                for (let index = 0; index < thisActions.length; index++) {
                    const element = thisActions[index];
                    if (!action_text.includes(element.title))
                        action_text += element.title + "<br>";
                }
            }



            //this will combine actions that lies on the same plot
            var overLappingOtherActions = "";
            // var retValue = [];
            console.log(checked_categories);

            var k = 0;

            // var hasSimilarActionHovered = false;

            Object.keys(actions_local).forEach(function(key, i) {
                var array = actions_local[key];

                for (let index = 0; index < array.length; index++) {
                    const element = array[index];

                    // var hasDisabled = false;
                    if (element.y == y && element.x == x) {
                        // var thisCategories = element.categories.split(",");

                        // console.log(thisCategories);
                        //show only if this category is checked
                        // for (let o = 0; o < thisCategories.length; o++) {
                        if (!checked_categories.includes(key)) {
                            // hasDisabled = true;
                            //console.log(thisCategories[o] + " is not checked");
                            // break;
                        }

                        // if(o >= thisCategories.length-1)
                        // {
                        else {
                            // hasSimilarActionHovered = true;

                            var upperAdd = "<strong>" + y + "</strong> (" + key + ") <br>";
                            var toAdd = element.title + "<br>";
                            if (!overLappingOtherActions.includes(upperAdd))
                                toAdd = "<strong>" + y + "</strong> (" + key + ") <br>" + element.title + "<br>";
                            if (!overLappingOtherActions.includes(toAdd))
                                overLappingOtherActions += toAdd;

                            action_text = overLappingOtherActions;
                            //                                    console.log(action_text);
                        }


                        k++;


                        if (k > actions_local.length - 1) {
                            if (action_text.length == 0)
                                action_text = "<strong>" + y + "</strong> (" + key + ") <br>";
                            showTooltip(item.pageX, item.pageY, action_text);
                        }
                        // }
                        // }
                    }


                    // retValue.push(element);

                    // if(index > array.length-1 && hasSimilarActionHovered == false && k > actions_local.length-1)
                    // {
                    //     action_text = "<strong>" + y + "</strong> (" + key + ") <br>";
                    //     showTooltip(item.pageX, item.pageY, action_text);
                    // }
                }


            });




            //}
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });

};


function showTooltip(x, y, contents) {
    if ($(".tooltips").length > 0) {
        $(".tooltips").remove();
    }

    $('<div id="tooltip" class="tooltips">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x + 20,
        border: '2px solid #4572A7',
        padding: '2px',
        size: '10',
        'border-radius': '6px 6px 6px 6px',
        'background-color': '#000',
        'color': '#fff',
        'padding': '5px',
        opacity: 0.80
    }).appendTo("body").fadeIn(200);
}




//var canvas=thePlot;
var context; //=canvas.getContext("2d");
var cw; //=canvas.width;
var ch; //=canvas.height;

var offsetX, offsetY;

var fontSize = 12;
var fontFace = 'verdana';
var dotRadius = 3;
var legendX = 350;
var legendY = 0;
var legendYincrement = 10;

var labels = [];
var nextId = 0;

function reinitTestCanvas() {
    // thePlot = $(".flot-overlay");
    labels = [];
    nextId = 0;

    reOffset();
    window.onscroll = function(e) {
        reOffset();
    }



    // context.textAlign='left';
    // context.textBaseline='top';
    // context.font='10px arial';
    // context.strokeRect(legendX-5,0,cw-legendX+5,ch);
    // context.fillText('Other labels',legendX-3,legendY+2);
    // legendY+=legendYincrement;
    // context.fillText('(Color Coded)',legendX-3,legendY+2);
    // legendY+=legendYincrement;

    // var label=addLabel('Label #0',cw/2,ch/2,fontSize,fontFace,dotRadius);
    // drawLabel(label);

    // thePlot.mousedown(function (e) { handleMouseDown(e); });
}


function reOffset() {
    var BB = document.getElementsByClassName("flot-overlay")[0].getBoundingClientRect();
    offsetX = BB.left;
    offsetY = BB.top;
}


//
function addLabel(text, dotX, dotY, fontsize, fontface, dotRadius) {
    var font = fontsize + 'px ' + fontface;
    context.font = font;
    var w = context.measureText(text).width;
    var h = fontsize * 1.286;
    var label = {
        id: nextId++,
        text: text,
        x: dotX - w / 2,
        y: dotY - dotRadius - h,
        w: w,
        h: h,
        offsetY: 0,
        font: font,
        isColliding: false,
        dotRadius: dotRadius,
        dotX: dotX,
        dotY: dotY,
    };
    labels.push(label);

    // try to position this new label in a non-colliding position
    var positions = [{
            x: dotX - w / 2,
            y: dotY - dotRadius - h
        }, // N
        {
            x: dotX + dotRadius,
            y: dotY - h / 2
        }, // E
        {
            x: dotX - w / 2,
            y: dotY + dotRadius
        }, // S
        {
            x: dotX - dotRadius - w,
            y: dotY - h / 2
        }, // W
    ];
    for (var i = 0; i < positions.length; i++) {
        var p = positions[i];
        label.x = p.x;
        label.y = p.y;
        label.isColliding = thisLabelCollides(label);
        if (!label.isColliding) {
            break;
        }
    }

    //
    return (label);
}

function handleMouseDown(e) {
    // tell the browser actions_local

    var x = parseInt(e.clientX - offsetX);
    var y = parseInt(e.clientY - offsetY);

    console.log(x + ", " + y);
    var label = addLabel('Label #' + nextId, x, y, fontSize, fontFace, dotRadius)

    drawLabel(label);
}

//
function drawLabel(label) {
    context.textAlign = 'left';
    context.textBaseline = 'top';
    if (label.isColliding) {
        //   legendY+=legendYincrement;
        //   context.beginPath();
        //   context.arc(legendX,legendY,3,0,Math.PI*2);
        //   context.fillStyle=randomColor();
        //   context.fill();
        //   context.font='10px arial';
        //   context.fillText(label.text,legendX+5,legendY-5);
    } else {
        context.font = label.font;
        context.fillStyle = 'black';
        context.fillText(label.text, label.x + 0, label.y);
        //   context.strokeRect(label.x,label.y,label.w,label.h);
    }
    // context.beginPath();
    // context.arc(label.dotX,label.dotY,label.dotRadius,0,Math.PI*2);
    // context.fill();
}

//
function thisLabelCollides(r1) {
    for (var i = 0; i < labels.length; i++) {
        var r2 = labels[i];
        if (r1.id == r2.id || r2.isColliding) {
            continue;
        }
        var collides = (!(
            r1.x > r2.x + r2.w ||
            r1.x + r1.w < r2.x ||
            r1.y > r2.y + r2.h ||
            r1.y + r1.h < r2.y
        ));

        if (collides) {
            // var title = r1.text;
            // if (title.length >= maxLabelString)
            //     r1.text = title.substr(0, maxLabelString) + "...";
            return (true);
        }
    }
    return (false);
}


function getPercentageChange(oldNumber, newNumber) {
    var decreaseValue = oldNumber - newNumber;

    let toRet = (decreaseValue / oldNumber) * 100;
    if (!isFinite(toRet))
        toRet = 0;

    // if(toRet != 0)
    //   toRet *= -1;

    return toRet;
}

function randomColor() {
    return ('#' + Math.floor(Math.random() * 16777215).toString(16));
}


function asyncFunction(item, cb) {
    setTimeout(() => {

        cb();
    }, 100);
}