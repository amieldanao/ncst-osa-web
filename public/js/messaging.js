// const { get } = require("jquery");

(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];
addUserCallback();
// var initial_role;
// var myRole;
function addUserCallback()
{
    let email = $("#user_email").text();

    let query = db.collection('users').where('email', '==', email);

    query.onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {


            if (change.type === "added") {
                let role = change.doc.data().role;
                initial_role = role;
                setRole(role);
                if(role == 2 || role == 1)
                {
                    $("#table_tr th").last().attr('id', 'actions_th');//append('<th id="actions_th" class="role_admin">Actions</th>');
                }
            }

            if (change.type === "modified") {
                setRole(change.doc.data().role);
                $("body").empty();
                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                 setTimeout(() => {
                    location.replace("index.html");
                }, 3000);
            }

            if (change.type === "removed") {
                // console.log("Removed user: ", change.doc.data());
                // window.location = "index.html";
            }
        });
    },
        function(error){
        console.log('retry add user callback');
        addUserCallback();
    });
}

function setRole(newRole)
{
    switch(newRole)
    {
        case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
        case 1 : $(".role_admin").remove();break;
    }
    
    $("#user_role").text("Role: " + roles_string[newRole]);
    $("#user_role").attr("data-done", true);
}

})();




'user strict'


var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);



function Start()
{
    myDefaultWhiteList.span = ['style'];
    sessionStorage.setItem('scrolled', false);
    //page refresh
    window.onbeforeunload = function(e) {
        // Turning off the event
        if($(".progress-bar").length > 0)
        {
            console.log("there's uploading ..." + upload_progress.length);
            e.preventDefault();
        }
        else
        {
            console.log("no uploading process");
        }
    }

    $('.message-input textarea').val(null);
    initVariables();

    addButtonCallbacks();
    setMessageCaseNew();
    
    loadDescriptions();
    // loadCategories();
    // readCategoriesFromDB();
}

var caseID;
var lastMsgDate;
var suspect_complete_name;
var suspect_gender;
var thisReport = null;
var cached_profile_pics = [];
var storageRef;
var files_to_upload = [];
var maxVideoDurationLimit = 30;
var maxFileSize = 20000000;//20mb
var imageCompressFactor = 0.5;

var skipped_files = [];
var upload_input;
var video_elements = [];
var upload_progress = [];

var categories = new Array();
var thisReportData;
var previousReportStatus;
var report_key;
var resolve_dialog;
var actions = [];
var report_listener;
var thisSuspectData;
var invite_dialog;


var status_text = ['Report has been resolved.', 'Report Verified. Processing. Investigation on going ...', 'Reported. Waiting for response ...', 'Pending ...'];
var status_class = ['badge badge-success', 'badge badge-warning', 'badge badge-primary', 'badge badge-info'];

var cached_students = [];
var search_results;

var message_listener;
var chatLimitPerPage = 15;
var current_chat_count = 15;
var total_chat_messages;
var lastVisibleMessage;
var myDefaultWhiteList = $.fn.tooltip.Constructor.Default.whiteList;
var myDateFormat = "YYYYMMDD MMMM D hh:mm:ss A";

var all_descriptions = new Array();
var all_categories_color = new Array();

var totalOffenseCount = 0;
var offenseListener;

var offenseCount = 0;
var suggested_penalties = [];
var suggested_penalty_listener = null;
var this_report_category_names;

$(document).ready(function () 
{
    observer.observe(targetNode, config);
});

function initVariables()
{
    suspect_complete_name = "";

    storageRef = storage.ref();

    myStack = new PNotify.Stack({
        dir1: 'down',
        dir2: 'right',
        firstpos1: 25,
        firstpos2: 25,
        context: document.getElementsByTagName('body'),
        modal: false,
        maxOpen: Infinity
    });
}

function exportReportTree(){
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
         "xmlns:w='urn:schemas-microsoft-com:office:word' "+
         "xmlns='http://www.w3.org/TR/REC-html40'>"+
         "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";

    //replace dates
    var mainSource = document.getElementById("tree_frame").innerHTML.replace("tree_root", "temp_root");

    var myWindow = window.open("", "MsgWindow", "width=200,height=100");
    myWindow.document.write(mainSource);

    var dateStamps = myWindow.document.querySelectorAll('[data-real-date]');
    for (i = 0; i < dateStamps.length; ++i) {
        dateStamps[i].innerText = dateStamps[i].getAttribute("data-real-date");
    }

    mainSource = "<h3>Report History:</h3><br>";
    mainSource += myWindow.document.body.innerHTML;

    mainSource += "<hr>"
    //Add Resolved notes
    if("resolve_date" in thisReportData)
        mainSource += "<p> Resolve Date : " + new Date(moment(thisReportData.resolve_date)).toString().split("GMT")[0] + "</p><br>";
    if("resolve_seminars" in thisReportData)
    {
        var sems = "";
        if(thisReportData.resolve_seminars != null)
            sems = thisReportData.resolve_seminars.toString()
        mainSource += "<p> Resolve Actions : " + sems + "</p><br>";
    }
    if("resolve_description" in thisReportData){
        var semsDescript = "";
        if(thisReportData.resolve_description != null)
        semsDescript = thisReportData.resolve_description.toString()
        mainSource += "<p> Resolve Description : " + semsDescript + "</p><br>";
    }

    var sourceHTML = header+mainSource+footer;
    
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = myWindow.document.createElement("a");
    myWindow.document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = thisReportData.reportId + ' Report Data.doc';
    fileDownload.click();
    myWindow.document.body.removeChild(fileDownload);
    myWindow.close();
}

function getSuggestedPenalties()
{
    suggested_penalties = [];
   
    if(suggested_penalty_listener != null)
        suggested_penalty_listener();

    suggested_penalty_listener = db.collection("Penalties").where("offense", "==", offenseCount)
    .onSnapshot(function(snapshot) {
        let requests = snapshot.docChanges().map(function(change) {
            var key = change.doc.id;
            var data = change.doc.data();
            if(change.type === "removed")
            {
                if(key in suggested_penalties)
                    delete suggested_penalties[key];
            }
            else
            {
                var found = data.categories.some(category_name=> this_report_category_names.includes(category_name));

                if(found){
                    suggested_penalties[key] = data.title;
                }
                else
                {
                    if(key in suggested_penalties)
                        delete suggested_penalties[key];
                }
            }
            
            return new Promise((resolve) => {
                asyncFunction(change, resolve);
            });

        });

        Promise.all(requests).then(() => {
            refreshSuggestionPenaltyOptions();
        });

    },
    function(error) {
        showNotifError("Error", error.message);
        // console.log("Error getting documents: ", error);
    });
}

function refreshSuggestionPenaltyOptions()
{
    if($('#penalty_select_field').length > 0)
    {
        $('#penalty_select_field').empty();
        Object.keys(suggested_penalties).forEach(function(e, i){
            $('#penalty_select_field').append('<option value="'+e+'">'+suggested_penalties[e]+'</option>');
        });

        let select = $('#penalty_select_field');
        select.select2({dropdownParent: penalty_edit_dialog, width: 'resolve', theme: "classic"});
    }
}

function savePenalty(penalty_id_to_edit, selected_penalty, sDate, eDate)
{
    showLoading(true, "Saving Penalty...");
    var ref = db.collection("reports").doc(report_key).collection("penalties").doc();
    if(penalty_id_to_edit != undefined)
    {
        ref = db.collection("reports").doc(report_key).collection("penalties").doc(penalty_id_to_edit);
    }

    var penaltyObj = 
    {
        title : suggested_penalties[selected_penalty],
        start_date : firebase.firestore.Timestamp.fromDate(sDate),
        end_date : firebase.firestore.Timestamp.fromDate(eDate),
        penalty_key : selected_penalty
    };

    ref.set(penaltyObj, {merge:true})
    .then(function(){

        var prefix = ((penalty_id_to_edit == undefined)?"Added" : "Modified");
        var newProcessData = {
            name: "Penalty " + prefix + ": " + penaltyObj.title,
            message: "Penalty " + prefix + ": " + penaltyObj.title + "<br>"
            + "Start Date : " + sDate.toString().split("GMT")[0] + "<br>"
            + "End Date : " + eDate.toString().split("GMT")[0] + "<br>"
            + "Duration : " + DurationAgo.inWords(sDate.getTime(), eDate),
            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
            type : "penalty"
        };

        saveProcessAuto(newProcessData, function(){
            showLoading(false);
            showNotifSuccess("Saved", "Penalty Saved!");
            if(penalty_edit_dialog != null)
                penalty_edit_dialog.modal('hide');
        });
        
    })
    .catch(function(){
        showLoading(false);
        showNotifError("Error", error.message);
    });
}

function refreshPenaltyDurationText()
{
    var sDate = new Date(moment($("#sDate").val()));
    var eDate = new Date(moment($("#eDate").val()));

    if(sDate < eDate)
        $("#penalty_duration").text("Duration : " + DurationAgo.inWords(sDate.getTime(), eDate));
    else
        $("#penalty_duration").text("Invalid! Start Date must come before End date!");
}

function showPromptDeletePenalty(elem)
{
    var penalty_id_to_edit = $(elem).attr("data-key");
    if(penalty_id_to_edit == undefined || penalty_id_to_edit == null)
        return;

    var penaltyTitle = cached_penalties[penalty_id_to_edit].title;
    
    bootbox.confirm({
        size:"small",
        centerVertical:true,
        message: "Are you sure you want to remove this penalty?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if(result)
            {
                showLoading(true, "Removing Penalty...");
                db.collection("reports").doc(report_key).collection("penalties").doc(penalty_id_to_edit)
                .delete()
                .then(function(){

                    
                    var newProcessData = {
                        name: "Penalty Removed : " + penaltyTitle,
                        message: "Penalty Removed : " + penaltyTitle,
                        timestamp: firebase.firestore.FieldValue.serverTimestamp()
                    };

                    saveProcessAuto(newProcessData, function(){
                        showLoading(false);
                        showNotifSuccess("Penalty Removed");
                    });
                    
                })
                .catch(function(error){
                    showLoading(false);
                    showNotifError("Error", error.message);
                });
            }
        }
    });    
    
}

function saveSuspectName(first_name_val, last_name_val)
{
    showLoading(true, "Saving...");
    var newlyIdentified = (thisReportData.suspect.trim().length == 0);
    var previousName = thisReportData.suspect + " " + thisReportData.suspect_surname;
    var previousDescriptions = "";
    var nameChanged = false;

    if(previousName.toLocaleLowerCase() != first_name_val.toLocaleLowerCase() + " " + last_name_val.toLocaleLowerCase())
    {
        nameChanged = true;
    }

    if(newlyIdentified){
        previousDescriptions = thisReportData.descriptions.toString();
        nameChanged = true;
    }

    
    if(nameChanged)
    {
        var newData = {
            suspect : first_name_val.toLowerCase(),
            suspect_surname : last_name_val.toLowerCase(),
            descriptions : new Array()
        };
    
        db.collection("reports").doc(report_key).set(newData, {merge:true})
        .then(function(){
            var prefix = (newlyIdentified)? "Suspect Identified as " : "Suspect name edited to";
            var suffix = (newlyIdentified)? "previously describe as " + previousDescriptions : " from previous name " + previousName;
            var newProcessData = {
                name: prefix + toTitleCase(first_name_val + " " + last_name_val),
                message: prefix + toTitleCase(first_name_val + " " + last_name_val) + "<br>" + suffix,
                timestamp: firebase.firestore.FieldValue.serverTimestamp()
            };
    
            saveProcessAuto(newProcessData, function(){
                showLoading(false);
                if(suspect_edit_dialog != null)
                {
                    suspect_edit_dialog.modal("hide");
                }
            });
        })
        .catch(function(error){
            showLoading(false);
            showNotifError("Error", error.message);
        });
    }
    else
    {
        showLoading(false);
    }

    
}

var suspect_edit_dialog = null;
function showSuspectNameEdit()
{

    var note = "";
    if(thisReportData.suspect.trim().length == 0)
        note = "<small>Note : Editing suspect name will clear this report suspect descriptions!</small>";
    suspect_edit_dialog = bootbox.dialog(
    {
        size:"small",
        centerVertical : true,
        title: '<i class="fas fa-user-edit"></i> Suspect Edit', 
        message:
        `
        <div class="row">

            <div class="col-12">
                `+note+`
                <label for="suspect_first_name_field" style="width:100%;">Suspect First Name:<input id="suspect_first_name_field" type="text"><br>
                <label for="suspect_last_name_field" style="width:100%;">Suspect Last Name:<input id="suspect_last_name_field" type="text"><br>
                
            </div>

        </div>
        `,
        buttons: {
            Save: {
                label: 'Save',
                className: 'btn-success',
                callback: function(){
                    
                    var first_name_val = $("#suspect_first_name_field").val();
                    var last_name_val = $("#suspect_last_name_field").val();

                    if (first_name_val == "" || last_name_val == "") {
                        showNotifError("Invalid", "Please type in suspect name");
                        return false;
                    }

                    if (!/^[a-zA-Z]*$/g.test(first_name_val) || !/^[a-zA-Z]*$/g.test(last_name_val)) {
                        showNotifError("Invalid", "Invalid characters are not allowed");
                        return false;
                    }
                    
                    saveSuspectName(first_name_val, last_name_val);

                    return false;           
                }
            },
            Cancel: {
                label: 'Close',
                className: 'btn-default',
                callback: function(){
                    
                    return true;           
                }
            }
        }
    });

    suspect_edit_dialog.on("shown.bs.modal", function(){
        if(thisReportData.suspect.trim().length > 0)
            $("#suspect_first_name_field").val(thisReportData.suspect);

        if(thisReportData.suspect_surname.trim().length > 0)
            $("#suspect_last_name_field").val(thisReportData.suspect_surname);
    });
}

var penalty_edit_dialog;
function showPromptEditPenalty(elem){
    var penalty_id_to_edit;
    var isNew = (elem == undefined);
    
    if(isNew == false)
        penalty_id_to_edit = $(elem).attr("data-key");

    var penaltySelectOptions = '';

    Object.keys(suggested_penalties).forEach(function(e, i){
        penaltySelectOptions += '<option value="'+e+'">'+suggested_penalties[e]+'</option>';
    });

    penalty_edit_dialog = bootbox.dialog(
    {
        centerVertical : true,
        title: '<i class="far fa-flag"></i> ' + ((isNew)?"Add Penalty" : "Edit Penalty"), 
        message:
        `
        <div class="row">

            <div class="col-12">

                <label for="penalty_select_field" style="width:100%;">Suggested Penalty:<select id="penalty_select_field" style="width: 100%">
                `+penaltySelectOptions+
                `</select></label><br>
                <label for="sDate">Start Date:</label><div class="input-group"><input id="sDate" class="form-control" size="16" type="text" value="" r><div class="input-group-append"><span class="input-group-text"><i class="far fa-calendar-alt"></i></span></div></div><br>
                <label for="eDate">End Date:</label><div class="input-group"><input id="eDate" class="form-control" size="16" type="text" value="" ><div class="input-group-append"><span class="input-group-text"><i class="far fa-calendar-alt"></i></span></div></div></form>
                <h3 id="penalty_duration"> Duration : </h3>
            </div>

        </div>
        `,
        buttons: {
            Save: {
                label: 'Save',
                className: 'btn-success',
                callback: function(){
                    
                    //validate penalty edit form
                    //prevent blank penalty
                    var selected_penalty = $("#penalty_select_field").val();
                    if(selected_penalty == null || selected_penalty.length == 0)
                    {
                        showNotifError("Blank penalty", "Please select a penalty!");
                        return false;
                    }

                    //prevent invalid start end date
                    var sDate = new Date(moment($("#sDate").val()));
                    var eDate = new Date(moment($("#eDate").val()));

                    if(sDate < eDate)
                    {
                        savePenalty(penalty_id_to_edit, selected_penalty, sDate, eDate);
                    }
                    else
                    {
                        showNotifError("Invalid date", "Start date must come before End date");
                        return false;
                    }

                    

                    return false;           
                }
            },
            Cancel: {
                label: 'Close',
                className: 'btn-default',
                callback: function(){
                    
                    return true;           
                }
            }
        }
    });

    penalty_edit_dialog.on("shown.bs.modal", function(){

        let start_date_picker = $('#sDate');
        start_date_picker.datetimepicker({autoclose:true, disableTouchKeyboard: true, dateFormat: 'MM-dd-yyyy HH:ii', forceParse:true, fontAwesome : true})
        .on('changeDate', function(ev){
            refreshPenaltyDurationText();
        });

        let end_date_picker = $('#eDate');
        end_date_picker.datetimepicker({autoclose:true, disableTouchKeyboard: true, dateFormat: 'MM-dd-yyyy HH:ii', forceParse:true, fontAwesome : true})
        .on('changeDate', function(ev){
            refreshPenaltyDurationText();
        });

        

        let select = $('#penalty_select_field');
        select.select2({dropdownParent: penalty_edit_dialog, width: 'resolve', theme: "classic",
            "language": {
                "noResults": function(){
                    return "No penalty suggestion for this offense no. matching this report categories";
                }
            }
        });

        if(isNew == false)
        {
            if(penalty_id_to_edit in cached_penalties){
                var penalty_obj = cached_penalties[penalty_id_to_edit];

                // $("#penalty_id_field").val(penalty_obj.title);
                
                start_date_picker.val(moment(penalty_obj.start_date.toDate()).format("YYYY-MM-DD HH:mm"));
                end_date_picker.val(moment(penalty_obj.end_date.toDate()).format("YYYY-MM-DD HH:mm")); 
                
                var penalty_orig_key = $(elem).attr("data-penalty-key");
                //Add selected penalty if it was removed from Penalty page OR this Offense has changed
                if($("#penalty_select_field").find('option[value="'+penalty_orig_key+'"]').length == 0)
                {
                    $("#penalty_select_field").append('<option value="'+penalty_id_to_edit+'">'+cached_penalties[penalty_id_to_edit].title+'</option>');
                    select.select2({dropdownParent: penalty_edit_dialog, width: 'resolve', theme: "classic"});
                    select.val(penalty_id_to_edit).trigger('change');
                }
                else
                {
                    select.val(penalty_orig_key).trigger('change');
                }
            }
        }
        else
        {
            start_date_picker.datetimepicker('setDate', getCurrentDateServerOffset());
            end_date_picker.datetimepicker('setDate', addDays(getCurrentDateServerOffset(), 7));
        }

        refreshPenaltyDurationText();

    });
}

function fillPenaltyTable(){
    if(penalty_listener != null)
        penalty_listener();

    cached_penalties = [];
    penalty_listener = db.collection("reports").doc(report_key).collection("penalties")
    .onSnapshot(function(snapshot) {

        if(snapshot.docs.length == 0)
        {
            $(".penalty_rows").remove();
            cached_penalties = [];
            return;
        }

        let requests = snapshot.docChanges().map(function(change) {
            var key = change.doc.id;
            var data = change.doc.data();
            if(change.type === "removed")
            {
                $("#" + key).remove();
                if(key in cached_penalties)
                    delete cached_penalties[key];

                console.log("Removed " + key);
            }
            else
            {
                var tds = 
                    '<td>' + data.title + '</td>' +
                    '<td>' + data.start_date.toDate().toString().split("GMT")[0] + '</td>' +
                    '<td>' + data.end_date.toDate().toString().split("GMT")[0] + '</td>' +
                    '<td><a data-penalty-key="'+data.penalty_key+'" data-key="'+key+'" href="#" onclick="showPromptEditPenalty(this);" class="on-default edit-row mx-2"><i class="fa fa-pencil"></i></a><a data-key="'+change.doc.id+'" href="#" onclick="showPromptDeletePenalty(this);" class="on-default remove-row mx-2"><i class="fa fa-trash-o"></i></a></td>';

                
                if($("#" + key).length == 0)
                {
                    var newRow = `
                    <tr id="`+key+`" class="penalty_rows">
                    `+tds+`
                    </tr>`;
                    $("#penalty_table > tbody").append(newRow);
                }
                else{
                    $("#" + key).empty();
                    $("#" + key).append(tds);
                }                

                cached_penalties[key] = data;
            }

            return new Promise((resolve) => {
                asyncFunction(change, resolve);
            });

        });

        Promise.all(requests).then(() => {
            
        });
    },

    function(error){
        showNotifError("Error", error.message);
    });
    
}

var cached_penalties = [];
var penalty_listener = null;
var penalty_dialog;
// var penalty_table;
function showEditPenalties()
{
    var dialogTitle = '<i class="far fa-flag"></i> Edit Penalties';

    var myButtons = 
    {
        Cancel: {
            label: 'Close',
            className: 'btn-default',
            callback: function(){
                
                return true;           
            }
        }
    };

    penalty_dialog = bootbox.dialog(
    {
        centerVertical : true,
        title: dialogTitle , 
        message:
        `
        <div class="row">

            <div class="col-12">

                <button onclick="showPromptEditPenalty();" class="dt-button btn btn-primary" type="button"><span>Add <i class="fa fa-plus" aria-hidden="true"></i></span></button>
                <table id="penalty_table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr id="table_tr">
                            <th>Title</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

            </div>

        </div>
        `,
        buttons: myButtons
    });

    

    penalty_dialog.on("shown.bs.modal", function(){
        fillPenaltyTable();
    });
}

function saveProcessAuto(newProcessData, callback)
{    
    db.collection("reports").doc(report_key).collection("Process").doc().set(newProcessData, {merge:true})
    .then(function(){
        // process_dialog.modal('hide');
        showLoading(false);
        if(callback)
            callback();
    })
    .catch(function(error){
        showLoading(false);
        showNotifError("Error", error.message);      
    });
}

function saveProcess(process_id_to_save, newProcessData)
{
    showLoading(true, "Saving Branch...");
    var ref = db.collection("reports").doc(report_key).collection("Process");
    if(process_id_to_save == undefined)
        process_id_to_save = ref.doc().id;

    
    ref.doc(process_id_to_save).set(newProcessData, {merge:true})
    .then(function(){
        process_dialog.modal('hide');
        showLoading(false);
        showNotifSuccess("Branch saved", newProcessData.name);
    })
    .catch(function(error){
        showLoading(false);
        showNotifError("Error", error.message);      
    });
}

function deleteProcess(process_id_to_delete)
{
    showLoading(true, "Deleting...");
    var name = cached_process_data[process_id_to_delete].name;
    db.collection("reports").doc(report_key).collection("Process").doc(process_id_to_delete).delete()
    .then(function(){
        showLoading(false);
        showNotifSuccess("Deleted", 'Branch : "' + name + '" deleted!');
        process_dialog.modal('hide');
    })
    .catch(function(error){
        showNotifError("Error", error.message);
    });
}

var process_dialog;
function showEditProcess(process_id_to_edit)
{
    var dialogTitle = '<i class="fas fa-stream"></i> ' + ((process_id_to_edit == undefined)? 'Add Branch' : 'Edit Branch');
    var buttonText = ((process_id_to_edit == undefined)? 'Add' : 'Save') + ' <i class="fas fa-check"></i>';
    var processDate = "";
    if(process_id_to_edit != undefined)
        processDate = cached_process_data[process_id_to_edit].timestamp.toDate();

    var myButtons = 
    {
        Resolve: {
            label: buttonText,
            className: 'btn-success bootbox-confirm-class',
            callback: function(){
                
                //look for invalid data in form
                var name_field_val = $("#branch_name_field").val();
                if(name_field_val.includes("_"))
                {
                    showNotifError("Invalid character in name field", '"_" are not allowed!');
                    return false;
                }

                if(name_field_val.trim().length == 0)
                {
                    showNotifError("Blank name field", 'blank name are not allowed!');
                    return false;
                }

                if ($('#process_summernote').summernote('isEmpty'))
                {
                    showNotifError("Blank message field", 'blank message are not allowed!');
                    return false;
                }

                //check conflict process name
                if(process_id_to_edit != name_field_val && (name_field_val in cached_process_data))
                {
                    showNotifError("Conflict name", 'Branch name "'+name_field_val+'" already exist in Report Tree!');
                    return false;
                }

                var newProcessData = {
                    name: name_field_val,
                    message: $('#process_summernote').summernote('code'),
                    timestamp:firebase.firestore.FieldValue.serverTimestamp()
                };

                saveProcess(process_id_to_edit, newProcessData);

                return false;
            }
        }
    };

    if(process_id_to_edit != undefined)
        myButtons['Delete'] = {
            label: 'Delete',
            className: 'btn-danger',
            callback: function(){
                bootbox.confirm(
                    {
                        message:'Are you sure to delete this branch?\n' + cached_process_data[process_id_to_edit].name , 
                        size:"small",
                        centerVertical:true,
                        callback:function(result){ 
                            // console.log('This was logged in the callback: ' + result); 
                            if(result == true)
                            {
                                deleteProcess(process_id_to_edit);
                            }
                        }
                    });

                return false;
            }
        };

    myButtons['Cancel'] = {
        label: 'Cancel',
        className: 'btn-default',
        callback: function(){
            
            return true;           
        }
    };

    process_dialog = bootbox.dialog(
    {
        centerVertical : true,
        title: dialogTitle , 
        message:
        `
        <div class="row">

            <div class="col-12">
                <label for="branch_name_field">Name:</label>
                <input type="text" class="form-control" id="branch_name_field">

                
                <label for="process_summernote">
                Description:
                </label>
                <textarea id="process_summernote" style="width:100%; border-radius:5px; "></textarea><br>
                
                <p>`+processDate+`</p>

            </div>

        </div>
        `,
        buttons: myButtons
    });
    
    $(".bootbox-confirm-class").attr('id', 'process_dialog_button_ok');

    $('#process_summernote').summernote({
        height: '200px',
        toolbar: defaultToolbar
    });

    if(process_id_to_edit != undefined)
    {
        $("#branch_name_field").val(cached_process_data[process_id_to_edit].name);
        $('#process_summernote').summernote('code', cached_process_data[process_id_to_edit].message);        
    }


    process_dialog.on("shown.bs.modal", function(){
        if(!$('body').hasClass("modal-open"))
        {
            $('body').addClass("modal-open");
        }
    });
}

function refreshTreeTimeAgo()
{
    // console.log("REFRESHING TIME AGO");
    var serverDate = getCurrentDateServerOffset();
    $(".timeAgo").each(function( index ) {
        var id = $(this).attr("data-id");
        // console.log(id);
        if(id in cached_process_data)
        {
            var thisBranchDate = cached_process_data[id].timestamp.toDate();
            $(this).find("small").text(TimeAgo.inWords(thisBranchDate.getTime(), serverDate));
        }
    });
}

function startRefreshTreeTimeAgo()
{
    setInterval(function() {
        refreshTreeTimeAgo();
    }, 60 * 1000);
}

var treeListener = null;
const initial_report_branch_name = "Initial Report";
var cached_process_data = [];

function loadTreeData()
{
    if(treeListener != null){
        treeListener();        
    }

    cached_process_data = [];
    setTimeout(function(){
        startRefreshTreeTimeAgo();
    }, (60 * 1000) - (getCurrentDateServerOffset().getSeconds() * 1000));

    try
    {
        treeListener = db.collection("reports").doc(report_key).collection("Process").orderBy("timestamp")
        .onSnapshot(function(snapshot) {

            if(snapshot.docs.length == 0)
            {
                addTreeControls();
                console.log("added report tree with zero process");
                return;
            }

            var serverDate = getCurrentDateServerOffset();

            let requests = snapshot.docChanges().map(function(change) {
                var type = change.type;
                if (type === "removed") {
                    // console.log("Removed tree: ", change.doc.data());

                    $("#process_" + change.doc.id).remove();

                    if(change.doc.id in cached_process_data)
                        delete cached_process_data[change.doc.id];
                }
                else {

                    let thisBranchDate = firebase.firestore.Timestamp.now().toDate();

                    if(change.doc.data().timestamp != null) {
                        // now we have the final timestamp value
                        thisBranchDate = change.doc.data().timestamp.toDate();
                    }
                    // console.log("New tree: ", change.doc.data());
                    // var id_name = change.doc.id.split(" ").join("_");
                    let branch_name = change.doc.data().name;
                    let edit_text = '<span class="float-right"><button id="'+change.doc.id+'" onclick="showEditProcess(this.id);" class="btn btn-outline-primary"><i class="far fa-edit"></i></button></span>';

                    if(branch_name == initial_report_branch_name)
                    {
                        edit_text = '';
                        hasInitialBranch = true;
                    }

                    let final_edit_text = edit_text + '  <span data-id="'+change.doc.id+'" class="float-right timeAgo"><small data-real-date="' + thisBranchDate.toString().split("GMT")[0] + '">' + TimeAgo.inWords(thisBranchDate.getTime(), serverDate) + '</small></span>';
                    let endHTML = 
                        '<span class="caret">'+branch_name+'</span>'+
                        final_edit_text +
                        '<ul class="nested">'+
                            '<li>'+
                                change.doc.data().message+
                            '</li>'+
                        '</ul>';


                    if(type === "added")
                    {
                        if($("#process_" + change.doc.id).length == 0)
                        {
                            console.log("adding " + final_edit_text);
                            $("#process_container").append(
                            
                                '<li id="process_'+change.doc.id+'" data-date="' + thisBranchDate + '">'                             
                                + endHTML +
                                '</li>'                        
                            );
                            console.log($("#process_"+change.doc.id));
                        }
                    }

                    if(type === "modified")
                    {
                        var modifiedElem = $("#process_" + change.doc.id);
                        modifiedElem.empty();
                        modifiedElem.append(endHTML);
                        // .find(".caret").text(branch_name);
                        // $("#process_" + change.doc.id).find(".caret").text(branch_name);
                        // $("#process_" + change.doc.id).find("ul").find("li").html(change.doc.data().message);
                        console.log("modified");
                        console.log($("#process_" + change.doc.id));
                    }
                    
                    cached_process_data[change.doc.id] = change.doc.data();
                }
                
                

                return new Promise((resolve) => {
                    asyncFunction(change, resolve);
                });
            });

            Promise.all(requests).then(() => {            
                //Done listing tree
                addTreeControls();
            });

            
        },
        function(error){
            console.error(error.message);
        });

    }
    catch(error)
    {
        console.error(error.message);
    }
}

function getThisReportCategoryNames()
{
    var toReturn = new Array();
    var i;
    for(i=0; i<thisReportData.categories.length; i++)
    {
        toReturn.push(thisReportData.categories[i].name);
    }

    return toReturn;
}

var hasInitialBranch = false;
// function hasInitialBranch()
// {
//     for (var key in cached_process_data) {
//         // check if the property/key is defined in the object itself, not in parent
//         if(cached_process_data[key].name == initial_report_branch_name)
//             return true;
//     }

//     return false;
// }

function addTreeControls()
{
    // if(thisReportData.status == 0){
    //     $("#resolve_export_button").css("display", "inline-block");
    // }
    // else{
    //     $("#resolve_export_button").css("display", "none");
    // }

    $("#btn_add_process").css("display", "block");
    $("#root_report_li").text(caseID);

    //For Debugging, manual add initial report branch
    if(hasInitialBranch == false)
    {
        console.error("Doesn't have initial branch!");
        //get the first message
        db.collection("reports").doc(report_key).collection("messages").orderBy("date").limit(1)
        .get()
        .then(function(querySnapshot) {
            var msg = "Initial Report auto generated by the system";

            querySnapshot.forEach(function(doc) {                
                msg = doc.data().message;
            });

            var initialReporter = cached_students[Object.keys(cached_students)[0]];
            var thisReportDateSplitted = thisReportData.postedAt.split(" ");
            var thisReportDate = thisReportDateSplitted[0] + " " + thisReportDateSplitted[1] + " " + thisReportDateSplitted[2] + " " + thisReportDateSplitted[3];
            var thisReportTime = thisReportDateSplitted[4] + " " + thisReportDateSplitted[5];
            
            var initialObj = 
            {
                victims: toTitleCase(initialReporter.last_name) +`, ` + toTitleCase(initialReporter.first_name),
                report_date : thisReportDate,
                report_time : thisReportTime,
                report_categories : getThisReportCategoryNames().toString(),
                message : msg,
                suspect : ((thisReportData.suspect.length == 0)? "Unknown" : toTitleCase(thisReportData.suspect) + " " + toTitleCase(thisReportData.suspect_surname))
            }
            
            var thisReportDateObject = new Date(moment(thisReportData.postedAt).format('ddd MMMM DD YYYY hh:mm:ss a'));
            var newData =
            {
                name: initial_report_branch_name,
                message: generateInitialReport(initialObj),
                timestamp: firebase.firestore.Timestamp.fromDate(thisReportDateObject)
            };

            db.collection("reports").doc(report_key).collection("Process").doc().set(newData, {merge:true});
        })
        .catch(function(error) {
            console.log("Error getting documents: ", error);
        });
    }
}

function generateInitialReport(initialObj)
{
    var suspect_text = "";
    if(initialObj.suspect != undefined)
    {
        suspect_text = '<p><b>Suspect:</b> ' + initialObj.suspect + '</p>';
    }
    else if(initialObj.descriptions != undefined)
    {
        suspect_text = '<p><b>Description:</b> ' + initialObj.descriptions + '</p>';
    }

    var msg = 
    `
    <p><b>Victim:</b> `+ initialObj.victims + `</p>
    ` + suspect_text + `
    <p><b>Report Category:</b> ` + initialObj.report_categories + `</p>
    <br>
    <p><b>Date:</b> `+initialObj.report_date+`</p>
    <p><b>Time:</b> `+initialObj.report_time+`</p>
    <br>                                                       
    <p><b>Message:</b> `+ initialObj.message+`</p>
    `;

    return msg;
}

var available_merge_dialog;
var suspect_reports_results;
var selected_report_to_merge;
var merge_new_report_id_listener;
var timerToNextDay;
// var previouse_selected_report_to_merge;// before typing new suspect name

function refreshNextDayReportCounts()
{
    db.collection("TodaysReport").doc("current")
    .get().then(function(doc) {
        if(doc.exists == false)
        {
            db.collection("TodaysReport").doc("current").set({
                reportCount : 0, 
                timestamp : firebase.firestore.serverTimestamp()
            })
            var serverDateNow = getCurrentDateServerOffset();
            let new_potential_merge_report_id1 = "CASE#" + serverDateNow.getFullYear() + (serverDateNow.getMonth()+1) + serverDateNow.getDate() + "_" + pad(1, 3);
            $("#potential_merge_report_id").text(new_potential_merge_report_id1);
        }
        else
        {
            fetchServerTime(function(){
                var timestamp = doc.data().timestamp.toDate();
                var count = doc.data().reportCount;
                var today = getCurrentDateServerOffset();
                // reset report count if report timestamp is not from today
                if(timestamp.getDate() != today.getDate()){
                    db.collection("TodaysReport").doc("current").set({
                        reportCount : 0, 
                        timestamp : firebase.firestore.serverTimestamp()
                    });

                    var serverDateNow2 = getCurrentDateServerOffset();
                    let new_potential_merge_report_id2 = "CASE#" + serverDateNow2.getFullYear() + (serverDateNow2.getMonth()+1) + serverDateNow2.getDate() + "_" + pad(1, 3);
                    $("#potential_merge_report_id").text(new_potential_merge_report_id2);
                }
                else
                {                
                    let new_potential_merge_report_id3 = "CASE#" + timestamp.getFullYear() + (timestamp.getMonth()+1)  + timestamp.getDate() + "_" + pad(count+1, 3);
                    $("#potential_merge_report_id").text(new_potential_merge_report_id3);
                }
            });
            
        }

        
    });
}

function selectReportToMerge(selected){
    $("#merge_summary").empty();

    var now = getCurrentDateServerOffset();
    var nextDay = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1, 0, 0, 10, 0);
    var diff = nextDay - now;
    console.log(diff);
    window.clearTimeout(timerToNextDay);
    //var millisecBeforeRedirect = 10000; 
    timerToNextDay = window.setTimeout(function(){
        // alert('Hello!');
        refreshNextDayReportCounts();
    }, diff);



    var tr = $(selected).parent().parent();

    selected_report_to_merge = suspect_reports_results[tr.attr("data-index")];
    let potential_merge_report_id = "";
    //fetch potential report id
    if(merge_new_report_id_listener != undefined)
        merge_new_report_id_listener();
        
    merge_new_report_id_listener = db.collection("TodaysReport").doc("current")
    .onSnapshot(function(doc) {
        if(doc.exists == false)
        {
            db.collection("TodaysReport").doc("current").set({
                reportCount : 0, 
                timestamp : firebase.firestore.serverTimestamp()
            })
            var serverDateNow = getCurrentDateServerOffset();
            potential_merge_report_id = "CASE#" + serverDateNow.getFullYear() + (serverDateNow.getMonth()+1) + serverDateNow.getDate() + "_" + pad(count+1, 3);

        }
        else
        {
            var timestamp = doc.data().timestamp.toDate();
            var count = doc.data().reportCount;
            
            potential_merge_report_id = "CASE#" + timestamp.getFullYear() + (timestamp.getMonth()+1)  + timestamp.getDate() + "_" + pad(count+1, 3);
            
        }

        $("#potential_merge_report_id").text(potential_merge_report_id);
    });

    

    $("#merge_summary").html("<h4>" + thisReportData.reportId + " and " + selected_report_to_merge.reportId + " will be merged. </h4>" + 
    '<br><h1><i class="fas fa-arrow-alt-circle-down"></i></h1>' + 
    '<br><h4 id="potential_merge_report_id">'+potential_merge_report_id+'</h4>' + 
    "<br> Suspect : " + toTitleCase(selected_report_to_merge.suspect + " " + selected_report_to_merge.suspect_surname) + 
    '<br>');

    // $("#merge_dialog_button_ok").html('Merge <i class="fas fa-check>');

    available_merge_dialog.remove();
}



function fillAvailableMergeReports()
{
    showLoading(false);
    let title = "";
    let message = "...";
    if(suspect_reports_results.length > 0)
    {
        title = suspect_reports_results.length + " result"+((suspect_reports_results.length > 1)?"s":"")+" found. With similar suspect and categories.";
        let tds = '';
        suspect_reports_results.forEach((report, index) =>{
            let thisReportCategories = new Array();
            report.categories.forEach((value, key, map) => {
                let thisCategoryName = toTitleCase(value["name"]);
                thisReportCategories.push(thisCategoryName);
            });

            var final_status_text = "Invalid Report";
            var final_status_class = "badge badge-danger";

            if(report.status > -1){
                final_status_text = status_text[report.status];
                final_status_class = status_class[report.status];
            }

            tds += '<tr data-id="'+report["key"]+'" data-index="'+index+'">';
            tds += '<td>' + report.reportId + '</td>';
            tds += '<td>' + thisReportCategories.toString() + '</td>';
            // tds += '<td>' + report.senders + '</td>';
            tds += '<td>' + report.suspect + " " + report.suspect_surname + '</td>';
            tds += '<td>' + '<h5><span data-status="'+report.status+'" class="label '+final_status_class+'">'+final_status_text+'</span></h5>';
            tds += '<td><button onclick="selectReportToMerge(this);" type="button" class="btn btn-primary">Select</button></td>';

            tds += '</tr>';
        });

        message = 
        `
        <table id="merge_results_table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr id="table_tr">
                            <th>Case#</th>
                            <th>Categories</th>
                            <th>Suspect Name</th>
                            <th>Status</th>
                            <th>Merge</th>
                        </tr>
                    </thead>
                    <tbody>
                    `+tds+
                    `</tbody>
                </table>`;
    }
    else
    {
        title = "No existing reports available to be merge with similar suspect and categories. :(";
    }

    available_merge_dialog = bootbox.dialog(
    {
        title: title,
        centerVertical: true,
        scrollable: true,
        message: message
    });
}



function searchSuspectStudent()
{
    showLoading(true);
    var input_first_name = $("#search_suspect_first_name").val().trim().toLowerCase();
    var input_last_name = $("#search_suspect_last_name").val().trim().toLowerCase();
        
    var query = db.collection("reports").where("suspect", "==", input_first_name).where("suspect_surname", "==", input_last_name).where("gender", "==", thisReportData.gender).where("status", ">", 0).orderBy("status").orderBy("reportId");

    if(input_last_name.length == 0 && input_first_name.length > 0)
        query = db.collection("reports").where("suspect", "==", input_first_name).where("gender", "==", thisReportData.gender).where("status", ">", 0).orderBy("status").orderBy("reportId");
    else if (input_first_name.length == 0 && input_last_name.length > 0)
        query = db.collection("reports").where("suspect_surname", "==", input_last_name).where("gender", "==", thisReportData.gender).where("status", ">", 0).orderBy("status").orderBy("reportId");
    else if (input_first_name.length == 0 && input_last_name.length == 0){
        showLoading(false);
        bootbox.alert("Please fill up suspect first name or last name!");        
        return;
    }

    suspect_reports_results = new Array();

    query.get()
    .then(function(querySnapshot) {

        let requests = querySnapshot.docs.map((report_data) => {
                
            // actions[action.data().title] = action.data();
            //Limit by same category
            if(thisReportData.categories.length == report_data.data().categories.length && report_data.data().reportId != thisReportData.reportId)
            {
                var thisReportCategories = new Array();
                thisReportData.categories.forEach((item, index) => {
                    thisReportCategories.push(item["name"]);
                });

                var otherReportCategories = new Array();
                report_data.data().categories.forEach((item, index) => {
                    otherReportCategories.push(item["name"]);
                });

                var hasDifferentCategory = false;
                for(var i = 0; i < otherReportCategories.length; i++){
                    if(thisReportCategories.indexOf(otherReportCategories[i]) == -1){
                        hasDifferentCategory = true;
                        break;
                    }
                }

                if(hasDifferentCategory == false)
                {
                    var newData = report_data.data();
                    // newData["key"] = report_data.id;

                    suspect_reports_results.push(newData);
                }
            }

            return new Promise((resolve) => {
              asyncFunction(report_data, resolve);
            });
        });

        Promise.all(requests).then(() => {                    
            // Search by name
            fillAvailableMergeReports();
        }); 

    })
    .catch(function(error) {
        showLoading(false);
        bootbox.alert(error.message);        
    });
}

var merge_report_dialog;

function showMergeReport()
{
    // let thisReportCategories = "";
    // let cats = $('.report_category');
    // cats.each((i, elem) => {
    //     thisReportCategories += $(elem).text() + ((i < cats.length-1)? ", " : "");
    // });

    merge_report_dialog = bootbox.dialog(
    {
        centerVertical : true,
        title: '<i class="far fa-object-group"></i> Merge Report', 
        message:
        `
        <div class="row">

            <div class="col-12">
                <div class="input-group mb-3" style="width:100%">
                    <input type="text" class="form-control" id="search_suspect_first_name" placeholder="Search for suspect first name" aria-describedby="basic-addon2">
                    <input type="text" class="form-control" id="search_suspect_last_name" placeholder="Search for suspect last name" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button" onclick="searchSuspectStudent();">Search</button>
                    </div>

                    
                </div>
                <br>
                <div id="merge_summary" class="merge_box">
                    <h4>No report selected to be merge. Begin by searching for suspect name</h4>
                </div>

            </div>

        </div>
        `,
        buttons: {
            Resolve: {
                label: 'Merge <i class="fas fa-check"></i>',
                className: 'btn-success bootbox-confirm-class',
                callback: function(){
                    var potential_new_report_case_num = $("#potential_merge_report_id").text();

                    if(selected_report_to_merge != null && potential_new_report_case_num.trim().length > 0)
                    {
                        showLoading(true);
                        var newDoc = db.collection("reports").doc();

                        let newData = 
                        {
                            categories : thisReportData.categories,
                            descriptions : new Array(),
                            gender : thisReportData.gender,
                            key : newDoc.id,
                            postedAt : moment(getCurrentDateServerOffset()).format("ddd MMMM D YYYY hh:mm:ss A"),
                            reportId : potential_new_report_case_num,
                            status : 2,
                            suspect : thisReportData.suspect,
                            suspect_surname : thisReportData.suspect_surname,                            
                            senders : merge_array(thisReportData.senders, selected_report_to_merge.senders)
                        }

                        newDoc
                        .set(newData, {merge:true})
                        .then(function(){
                            sessionStorage.setItem("selectedCase", potential_new_report_case_num);
                            db.collection("TodaysReport").doc("current").set({
                                reportCount : firebase.firestore.FieldValue.increment(1), 
                                timestamp : firebase.firestore.FieldValue.serverTimestamp()
                            }
                            ,{merge:true})
                            .then(function(){
                                var newProcessData = {
                                    name: "Report Merged to " + potential_new_report_case_num,
                                    message: "Report merged to " + potential_new_report_case_num,
                                    timestamp: firebase.firestore.FieldValue.serverTimestamp()
                                };
        
                                saveProcessAuto(newProcessData, function(){
                                    window.location.reload();
                                });
                                
                            })
                            //now delete this report
                            // db.collection("reports").doc(thisReportData.key).delete().then(function() {
                            //     // console.log("Document successfully deleted!");
                            //     window.location.reload();
                            // }).catch(function(error) {
                            //     console.error("Error removing document: ", error);
                            // });
                            
                        })
                        .catch(function(error){
                            showLoading(false);
                            bootbox.alert(error.message);
                        });
                    }
                    return false;
                }
            },
            Cancel:{
                label: 'Cancel',
                className: 'btn-default',
                callback: function(){
                    selected_report_to_merge = null;
                    return true;           
                }
            }
        }
    });

    $(".bootbox-confirm-class").attr('id',  'merge_dialog_button_ok');

    merge_report_dialog.on("shown.bs.modal", function(){
        if(!$('body').hasClass("modal-open"))
        {
            $('body').addClass("modal-open");
        }
    });
}


function merge_array(array1, array2) {
    var result_array = [];
    var arr = array1.concat(array2);
    var len = arr.length;
    var assoc = {};

    while(len--) {
        var item = arr[len];

        if(!assoc[item]) 
        { 
            result_array.unshift(item);
            assoc[item] = true;
        }
    }

    return result_array;
}

function resortLoadMore()
{
    console.log('resortLoadMore');
    $("#load_more_button").show(); 
}

function scrollToBottomChat()
{
    setTimeout(() => {  

        var scrollHeight = $("#chat-messages")[0].scrollHeight;
        var objDiv = document.getElementById("chat-messages");

        let scrolled = sessionStorage.getItem('scrolled');
        if(scrolled == "false" || scrolled == false)
        {
            objDiv.scrollTop = scrollHeight;
        }
        else
        {
            objDiv.scrollTop = 0;
        }

    }, 100);
}

function appendInitialMessage(senderObj)
{
    var msg = senderObj._final_message;
        
    let realName = senderObj._sender_name;

    if(senderObj._sender_id in cached_students)
        realName = toTitleCase(cached_students[senderObj._sender_id].first_name) + " " + toTitleCase(cached_students[senderObj._sender_id].last_name);
    else (!thisReportData.senders.includes(senderObj._sender_id))
    {
        fetchIndividualSenderInfo(senderObj._sender_id);
    }

    let nameStyle = 'left';

    if($('#message_ul li[data-key="'+senderObj._key+'"]').length > 0)
    {
        $('#message_ul li[data-key="'+senderObj._key+'"]').remove();
    }

    let li = document.createElement("LI");
    li.setAttribute('data-key', senderObj._key);
    li.setAttribute('data-msg_date', senderObj._msg_date);
    li.classList.add(senderObj._senderClass);
    li.classList.add('message_entry');

    let h6 = document.createElement("H6");
    h6.classList.add('realname-'+senderObj._sender_user_id);
    h6.style.textAlign = nameStyle;
    h6.appendChild(document.createTextNode(realName));
    li.appendChild(h6);

    let newImg = document.createElement("IMG");
    newImg.setAttribute('data-id', senderObj._sender_user_id);
    newImg.classList.add('message_sender_pics');
    if((senderObj._sender_user_id in cached_profile_pics))
        newImg.src = cached_profile_pics[senderObj._sender_user_id];
    else
        newImg.src = senderObj._senderPic;
    li.appendChild(newImg);

    // let p = document.createElement("P");
    // let newMsg = createTextLinks_(msg);

    document.getElementById('message_ul').appendChild(li);

    $('#message_ul li[data-key="'+senderObj._key+'"]').append('<div class="start_message mt-2">' + msg + '</div>');
    console.log("Added div");

    resortLi();
    scrollToBottomChat();

    lastMsgDate = senderObj._msg_date;
}

function processMessage(MESSAGE, MESSAGE_KEY)
{
    // Add only if not already in chatbox
    if($('li[data-key="'+MESSAGE_KEY+'"]').length > 0)
        return;

    var final_message = MESSAGE.message;
    var sender_id = MESSAGE.user;
    
    var msg_date = moment(getCurrentDateServerOffset()).format("YYYYMMDD MMMM D hh:mm:ss A");

    if(MESSAGE.date != null)
        msg_date = moment(MESSAGE.date.toDate()).format("YYYYMMDD MMMM D hh:mm:ss A");
    console.log(msg_date);
    var senderIsAdmin = MESSAGE.is_admin;
    var is_media = MESSAGE.is_media;

    // if(MESSAGE.user_id == "admin")
    //     senderIsAdmin = true;
    var senderPic = "images/noimage.png";
    var senderClass = "sent";

    var sender_is_me = sender_id == firebase.auth().currentUser.email;

    if(sender_is_me)
        senderClass = "replies";

    if(senderIsAdmin)
    {
        senderPic = "images/ncst_logo_256px.png";
    }
    else
    {
        if((MESSAGE.user_id in cached_profile_pics))
        {
            senderPic = cached_profile_pics[MESSAGE.user_id];
        }        
        else
        {
            getSetProfileImage(MESSAGE.user_id);
        }
    }

    var splitted_date = msg_date.split(" ");
    var year = msg_date.substr(0, 4);
    var month = msg_date.substr(4, 2);
    var day = msg_date.substr(6, 2);
    var time = splitted_date[3] + ":00 " + splitted_date[splitted_date.length-1];
    var time24 = moment(time, "h:mm:ss A").format("HH:mm:ss");
    var formatted_date = year + "-" + month + "-" + day + "T" + time24;
    //~ console.log(msg_date + " compare to " + formatted_date);

    //determine if date needs to be added
    if(lastMsgDate == null || (lastMsgDate != null && msg_date != lastMsgDate))
    {
        //we don't want to add duplicate date entry
        if($(".messages ul p[data-msg_date='"+formatted_date+"']").length > 0)
        {
            $(".messages ul p[data-msg_date='"+formatted_date+"']").remove();
        }

        var splittedDate = msg_date.split(" ");
        var msg_to_display = splittedDate[1] + " " + splittedDate[2] + " " + splittedDate[3] + " " + splittedDate[4];
        $('.messages ul').append('<p class="message_entry" data-msg_date="'+formatted_date+'" style="text-align: center;max-width: 100%; width: 100%;">'+msg_to_display+'</p>');
            
        
    }

    var isInitial = false;
    if("initial" in MESSAGE)
        isInitial = MESSAGE.initial;
        
    if(is_media == false || isInitial)
    {
        var senderObject = {
            _senderClass : senderClass,
            _sender_id : sender_id,
            _sender_name : senderIsAdmin? $('#user_email').attr('data-myname') : MESSAGE.user,
            _sender_user_id : MESSAGE.user_id,
            _senderPic : senderPic,
            _final_message : final_message,
            _msg_date : formatted_date,
            _is_admin : senderIsAdmin,                
            _index : 0,
            _key : MESSAGE_KEY
        };

        if(isInitial){
            appendInitialMessage(senderObject);
        }
        else{
            //we will try to see if this is an image link
            //we need to split the string into spaces, and test each splitted string

            var splitted_message = MESSAGE.message.match(/\S+/g) || [];
            if(splitted_message.length > 0)
            {
                senderObject["_message_splitted"] = splitted_message;

                for (let s = 0; s < splitted_message.length; s++) {
                    const url_test = splitted_message[s];
                    senderObject._index = s;
                    testImage(url_test, record, senderObject);
                }
            }
        }
    }
    else
    {
        
        var tangRef = storageRef.child('reports/'+caseID+'/' + MESSAGE.file_name);

        let realName = sender_id;

        if(realName in cached_students)
            realName = toTitleCase(cached_students[realName].first_name) + " " + toTitleCase(cached_students[realName].last_name);
        else (!thisReportData.senders.includes(realName))
        {
            fetchIndividualSenderInfo(realName);
        }

        let nameStyle = 'left';

        if(sender_is_me)
            nameStyle = 'right';

        $('<li data-key="'+MESSAGE_KEY+'" data-msg_date="'+formatted_date+'" id="'+MESSAGE.file_name+'" class="'+senderClass+' message_entry"><h6 class="realname-'+MESSAGE.user_id+'" style="text-align:'+nameStyle+'">'+(sender_is_me?'' : realName)+'</h6><img data-id="'+MESSAGE.user_id+'" class="message_sender_pics" src="'+senderPic+'" alt="" /></li>').appendTo($('.messages ul'));

        

        tangRef.getMetadata().then(function(metadata) {
            // Metadata now contains the metadata for 'images/forest.jpg'
            var theMetadata = metadata.contentType.split("/")[0];
          
            //we get the thumbnail if this is a video
            if(theMetadata == "video")
            {
                var theThumbnailFileName = MESSAGE.file_name.split(".")[0] + ".jpg";
                var thumbnailRef = storageRef.child('video_thumbnails/'+caseID+'/' + theThumbnailFileName);
                thumbnailRef.getDownloadURL().then(function(url) {
                    
                    // img_urls.push(url);
                    // var index = img_urls.length;
        
                    var newLi = document.getElementById(MESSAGE.file_name);
                    var div_wrapper = document.createElement("div");
                    div_wrapper.className = "video_wrapper_" + senderClass;
                    var a = document.createElement("a");
                    a.href = MESSAGE.message;
                    a.setAttribute('data-fancybox', 'gallery');
                    var img = document.createElement("img");
                    img.className = 'message_image';
                    img.src = url;
                    img.style.width = "100%";
                    img.style.height = "100%";
                    a.appendChild(img);

                    var div_playImg = document.createElement("div");
                    div_playImg.className = "play_video_icon";
                    if(sender_is_me)
                        div_playImg.style.left = "50%";
                    else
                        div_playImg.style.right = "50%";
                    a.appendChild(div_playImg);

                    div_wrapper.appendChild(a);
                    newLi.appendChild(div_wrapper);

                    resortLi();
                    scrollToBottomChat();
                    

                    lastMsgDate = msg_date;
                  }).catch(function(error) {
              });

            }
            else
            {

                var newLi = document.getElementById(MESSAGE.file_name);                    
                var a = document.createElement("a");
                a.href = MESSAGE.message;
                a.setAttribute('data-fancybox', 'gallery');
                var img = document.createElement("img");
                img.className = 'message_image';
                img.src = MESSAGE.message;                    
                a.appendChild(img);                    
                newLi.appendChild(a);

                resortLi();
                //$('<a data-fancybox="gallery" href="'+data.val().message+'"><img class="message_image" src="'+data.val().message+'" alt=""/></a>').appendTo($("#" + data.val().file_name));
                
                
                scrollToBottomChat();

                lastMsgDate = msg_date;
            }


            
            //console.log(theMetadata);
            // console.log(url);

        }).catch(function(error) {
            // Uh-oh, an error occurred!
            console.error(error.message);
            if(error.code == "storage/object-not-found")
            {
                var newLi = document.getElementById(MESSAGE.file_name);
                var errorText = document.createElement("p");
                errorText.innerText = "File not found";
                errorText.style = "color:red;";
                newLi.appendChild(errorText);
            }
        });
    }

    // $("a.grouped_elements").fancybox({
    //     'transitionIn'  :   'elastic',
    //     'transitionOut' :   'elastic',
    //     'speedIn'       :   600, 
    //     'speedOut'      :   200, 
    //     'overlayShow'   :   false
    // });
}

function addMessageListener()
{
    // if(message_listener != null)
    //     message_listener();
    fetchServerTime(function(){
        try
        {
            let query = db.collection("reports").doc(report_key).collection('messages').orderBy("date", "desc").limit(chatLimitPerPage);

            if(lastVisibleMessage != null)
                query = db.collection("reports").doc(report_key).collection('messages').orderBy("date", "desc").startAfter(lastVisibleMessage).limit(chatLimitPerPage);
            // else
            //     sessionStorage.setItem('scroll', 'bottom');
            total_chat_messages = 0;

            // message_listener = 
            query.onSnapshot(function(snapshot) {
                

                if(snapshot.size == 0 && sessionStorage.getItem('scrolled') == false)
                {
                    $('#message_ul').find('*').not('button').remove();
                    $('#load_more_button').css('display', 'none');
                    return;
                }

                let requests = snapshot.docChanges().map((change) => {
                    if (change.type === "added") {
                        
                        lastVisibleMessage = change.doc;
                        // console.log("last", lastVisibleMessage);

                        let thisMessageData = change.doc.data();

                        // console.log(thisMessageData.msg_date.toDate());

                        processMessage(thisMessageData, change.doc.id);

                        console.log("New message added");
                        // total_chat_messages ++;
                    }

                    if (change.type === "modified") {
                        // console.log("modified", change.doc.id);
                    }

                    if (change.type === "removed") {
                    }
                
                    return new Promise((resolve) => {
                      asyncFunction(change, resolve);
                    });
                });

                Promise.all(requests).then(() => {                    
                    // Search by name
                    resortLoadMore();
                }); 

            }
            ,function(error){
                setTimeout(() => {
                    console.log("retrying addMessageListener...");
                    addMessageListener();
                }, 3000);
            });
        }
        catch(error)
        {
            console.log(error.message);
            setTimeout(() => {
                console.log("retrying addMessageListener...");
                addMessageListener();
            }, 3000);
        }
    }); 
}

function getLastOffenseResolve()
{
    $('#offense_count').popover({
        container: 'body',
        content : "No Previous offense",
        html : true
    });

    db.collection("offense").doc(thisReportData.suspect.toLowerCase() + "-" + thisReportData.suspect_surname.toLowerCase() + ":" + thisReportData.gender)
    .collection("myResolves")
    .orderBy("dateSolved").limit(1)
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {

            // doc.data() is never undefined for query doc snapshots
            // console.log(doc.id, " => ", doc.data());
            // alert(doc.id);
            $('#offense_count').popover('dispose');

            let cats = new Array();

            if(doc.data().categories != undefined)
            {
                for(let i=0; i< doc.data().categories.length; i++)
                {
                    cats.push(doc.data().categories[i].name);
                }
            }

            var correctDate = firebase.firestore.Timestamp.now().toDate();

            if(doc.data().dateSolved != null)
                correctDate = doc.data().dateSolved.toDate();
            $('#offense_count').popover({
                container: 'body',
                content : doc.id + "<br><small>" + correctDate.toString().split("GMT")[0] + "<br>" + cats  +"</small>",
                html : true
            });
            

        });
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}

function fetchOffenseCount()
{
    if(offenseListener != null)
        offenseListener();
    
    offenseListener = db.collection("offense").doc(thisReportData.suspect.toLowerCase() + "-" + thisReportData.suspect_surname.toLowerCase() + ":" + thisReportData.gender)
    
    .onSnapshot(function(doc) {
        
        if(!doc.exists)
        {
            $("#offense_count").html("No previous offense");
            $("#offense_count").show();
            getSuggestedPenalties();
            return;
        }
        // console.log(doc.data());
        totalOffenseCount = doc.data().count;//doc.data().count;

        offenseCount = Math.floor(doc.data().count / 3);
        let warningCount = doc.data().count % 3;

        if(totalOffenseCount > 0)
        {
            let offenseText = "";
            let warningText = "";

            if(offenseCount == 1)
                offenseText = "1st Offense";
            if(offenseCount == 2)
                offenseText = "2nd Offense";
            if(offenseCount == 3)
                offenseText = "3rd Offense";
            if(offenseCount > 3)
                offenseText =  offenseCount + "th Offense";

            if(warningCount == 1)
                warningText = "1st Warning";
            if(warningCount == 2)
                warningText = "2nd Warning";
            if(warningCount == 3)
                warningText = "3rd Warning";

            
            $("#offense_count").html(offenseText + (offenseCount >= 1 ? "<br>" : "") + warningText);
            $("#offense_count").show();
        }
        else
        {
            $("#offense_count").hide();
        }

        getLastOffenseResolve();
        getSuggestedPenalties();
    });
}

function setNoPhoto()
{
    let filename = 'profile_pic_placeholder.png';
    if(thisReportData != null)
    {
        filename = thisReportData.gender + ".png";
    }

    filename = "images/" + filename;

    document.getElementById('suspect_photo').src = filename;

    console.log("setNoPhoto");
}

function tryFetchSuspectPic()
{
    if(thisReportData.suspect.trim().length > 0)
    {
        try{

            // Get suspect data first
            db.collection("students").where('first_name', '==', thisReportData.suspect.toLowerCase()).where('last_name', '==', thisReportData.suspect_surname.toLowerCase())
            .get()
            .then(function(querySnapshot) {
                
                // console.log(thisReportData.suspect.toLowerCase() + " " + thisReportData.suspect_surname.toLowerCase());
                // console.log(querySnapshot.docs[0].data());
                if(querySnapshot.docs.length > 0)
                {
                    console.log("tryFetchSuspectPic");
                    var picRef = storageRef.child('profileImages/'+querySnapshot.docs[0].data().ID+'.jpeg');

                    // Get the download URL
                    picRef.getDownloadURL().then(function(url) {
                      // Insert url into an <img> tag to "download"
                      $("#suspect_photo").get(0).src = url;
                      console.log(url);
                    }).catch(function(error) {
                        setNoPhoto();
                        console.log(error.message);
                    });
                }
                else
                {
                    console.log(querySnapshot.docs);
                }
            })
            .catch(function(error){
                setNoPhoto();
                console.log(error.message);
            });

            

        }
        catch(error)
        {
            console.log(error.message);
            setTimeout(() => {
                console.log("retrying tryFetchSuspectPic...");
                tryFetchSuspectPic();
            }, 3000);
        }
    }
}

function saveResolve(seminar_array, description, resolve_date)
{
    showLoading(true, "Saving...");
    var thisPreviousReportStatus = thisReportData.status;
    try
    {
        console.log(seminar_array);
        // var seminar_array = new Array();
        // if(seminars.length > 0)
        //     seminar_array = seminars.split(",");

        //format seminars strings
        for(let i=0; i < seminar_array.length; i++)
        {
            seminar_array[i] = toTitleCase(seminar_array[i].replace(/\s+/g, ' '));
        }

        let resolveData = 
        {
            resolve_seminars : seminar_array,
            resolve_description : description,
            resolve_date : resolve_date,
            status : 0
        }

        db.collection('reports').doc(report_key).set(resolveData, {merge:true}).then(function(){
            
            var newProcessData = {
                name: "Report resolved ",
                message: "Report resolved : <br>"
                + "<p> Resolve Date : " + new Date(moment(resolveData.resolve_date)).toString().split("GMT")[0] + "</p><br>"
                + "<p> Resolve Actions : " + resolveData.resolve_seminars.toString() + "</p><br>"
                + "<p> Resolve Description : " + resolveData.resolve_description + "</p><br>",
                timestamp: firebase.firestore.FieldValue.serverTimestamp()
            };
    
            saveProcessAuto(newProcessData, function(){
                showNotifSuccess("Saved", "Report resolved successfully");
                resolve_dialog.modal("hide");
                showLoading(false);
            });

            

            console.log("has reportId " + thisReportData.reportId);
            // Resolve notifications
            db.collection('notifications').where('reportId', '==', thisReportData.reportId)
            .get()
            .then(function(querySnapshot) {

                querySnapshot.forEach(function(doc) {
                    // doc.data() is never undefined for query doc snapshots
                    // console.log(doc.id, " => ", doc.data());
                    // console.log(doc);

                    // dont receive any more notifications
                    db.collection('notifications').doc(doc.id).set({resolved: true}, {merge:true})
                    .then(function(){

                        
                    });

                    

                });
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
            });

            // Now send resolve notifications 
            let recipients = new Array();

            Object.keys(cached_students).forEach((key) => {
                recipients.push(cached_students[key].ID);
            });

            let newNotifData = {
                title: thisReportData.reportId + " has been resolved!", 
                dateSent: firebase.firestore.FieldValue.serverTimestamp(), 
                recipients : recipients, 
                message : thisReportData.reportId + " has been resolved!<br> Thank you for your participation in solving this case.", 
                reportId: thisReportData.reportId,
                resolved : false
            };

            db.collection('notifications').doc().set(newNotifData)
            .then(function() {
                // showLoading(false);
                // showNotifSuccess("Sent", "Invitation successfully sent!");
                // invite_dialog.modal('hide');

                // location.reload();
            })
            .catch(function(error) {
                showLoading(false);
                showNotifError("Error", error.message);
            });


            if(thisReportData.suspect.trim().length > 0 && thisPreviousReportStatus != 0)
            {
                var offense_doc_key = thisReportData.suspect.toLowerCase() + "-" + thisReportData.suspect_surname.toLowerCase() + ":" + thisReportData.gender;
                console.log(offense_doc_key);
                
                db.collection("offense").doc(offense_doc_key)
                .get()
                .then(function(querySnapshot1){

                    
                    db.collection("offense").doc(offense_doc_key).set({count:firebase.firestore.FieldValue.increment(2)}, { merge: true })
                    .then(function(){
                        // console.log("updated offense back " + thisReportData.reportId);
                    
                        console.log("1");
                        var currentCount = querySnapshot1.data().count;
                        // if(currentCount >= 9)
                        // {
                            console.log("2");
                            let solved_data = {
                                dateSolved : firebase.firestore.FieldValue.serverTimestamp(),
                                resolve_seminars : seminar_array,
                                categories : thisReportData.categories
                            };
                                
                            db.collection("offense").doc(offense_doc_key)
                            .collection("myResolves").doc(thisReportData.reportId)
                            .set(solved_data, {merge:true})
                            .then(function(){
                                // console.log("3");
                                // let newOffenseCount = {count : Math.max(0, currentCount - 9)};
                                // //reset offense count
                                // db.collection("offense").doc(offense_doc_key).set(newOffenseCount)
                                // .then(function(){
                                //     console.log("updated offense count of " + offense_doc_key);
                                // })
                                // .catch(function(error){
                                //     console.error(error.message);
                                // });
                            })
                            .catch(function(error){
                                console.error(error.message);
                            });
                        // }
                    })
                    .catch(function(error){
                        console.error(error.message);
                    });
                    
                })
                .catch(function(error){
                    console.error(error.message);
                });
            }
        })
        .catch(function(error){
            showLoading(false);
            bootbox.alert(error.message);
        });
    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(error.message);
    }
}

function addButtonCallbacks()
{
    document.getElementById('suspect_photo').addEventListener("error", setNoPhoto);

    $("#identify_button").on('click', function(){
        showSuspectNameEdit();
    });

    $("#resolve_export_button").on('click', function(){
        exportReportTree();
    });

    $("#penalty_button").on('click', function(){
        showEditPenalties();
    });

    $("#btn_add_process").on('click', function(){
        showEditProcess();
    });

    $('#tree_root').on('click', '.caret', function (event) {
        $(this).get(0).parentElement.querySelector(".nested").classList.toggle("active");
        $(this).get(0).classList.toggle("caret-down");
    });

    $("#load_more_button").on('click', function()
    {
        // current_chat_count += (total_chat_messages + chatLimitPerPage);
        addMessageListener();
        sessionStorage.setItem('scrolled', true);
    });

    $("#edit_report_button").on('click', function(){

        clickEditReportStatus();
        // });
    });

    $("#split_report_button").on('click', function(){

        showSplitReport();
        // });
    });

    $("#merge_report_button").on('click', function(){

        showMergeReport();
        // });
    });

    $('#reporters_button').popover({
        container: 'body',
        html:true,
        placement:'auto',
        trigger:'click hover',
        // trigger:'focus',
        content: function() {
            var message = '<div id="senders_div" class="row px-4" style="overflow:scroll !important;">';

            thisReportData.senders.forEach((sender) => {
                let realName = sender;
                let imgTag = '';
                let img_url = 'images/profile_pic_placeholder.png';

                if((sender in cached_students)){
                    realName = cached_students[sender].first_name + ' ' + cached_students[sender].last_name;
                    img_url = 'images/'+cached_students[sender].gender+ '.png';

                    if((cached_students[sender].ID in cached_profile_pics))
                        img_url = cached_profile_pics[cached_students[sender].ID];
                }                
                
                

                imgTag = '<img class="mini_pic" id="'+cached_students[sender].ID+'" src="'+img_url+'"></img>';
                message += '<h4 class="m-1"><span class="badge badge-pill badge-info">' + imgTag +' ' + realName + '</span></h4>';
            });

            message += "</div>";

            return message;
        }
    });


    //upload input
    upload_input = document.getElementById("file_upload");
    upload_input.onchange = function(e) {

        try
        {
            files_to_upload = [];
            skipped_files = [];
            video_elements = [];
            upload_progress = [];

            for (let index = 0; index < e.target.files.length; index++) {
                const element = e.target.files[index];

                // console.log()
                
                var mimeType = element.type.split("/")[0];
                if(mimeType == "image" || mimeType == "video")
                {
                    if(element.size > maxFileSize)
                    {
                        skipped_files.push(element);
                    }
                    else
                    {
                        if(mimeType == "image"){
                            files_to_upload.push(element);
                            refreshFileCounter()
                        }
                        else{
                            validateVideoFile(element);
                        }                        
                    }
                    console.log(element);
                }
                else
                {
                    skipped_files.push(element);
                }

                if(index == e.target.files.length-1)
                {
                    showSkippedFiles();
                }
            }
        }
        catch(error)
        {
            console.error(error.message);
        }
    };

    $(document).on("keydown", "form", function(event) { 
        return event.key != "Enter";
    });

    $("#notif_suspect").on('click', function(){

        showInvitePrompt();
    });

    // $(".messages").animate({ scrollTop: $(document).height() }, "fast");
    

    $('#send_message_button').on('click', function () {
        sendNewMessage();
    });
}

function loadDescriptions()
{
    db.collection('descriptions').get().then(function(querySnapshot) {


        let requests = querySnapshot.docs.map((description) => {

            all_descriptions[description.id] = description.data().opposite;

            return new Promise((resolve) => {
                asyncFunction(description, resolve);
            });
        });

        Promise.all(requests).then(() => {
            // $("#split_report_button").css('display', 'block');
            loadCategories();

        });

    });
}

function loadCategories() {

    db.collection('categories').get().then(function(querySnapshot) {


        let requests = querySnapshot.docs.map((category) => {

            categories.push(category.data().name.toString());
            all_categories_color[category.data().name.toString()] = category.data().color;

            return new Promise((resolve) => {
                asyncFunction(category, resolve);
            });
        });

        Promise.all(requests).then(() => {
            

        });
    });
}

var last_valid_selection = null;
var split_report_dialog;

function showSplitReport()
{
    let thisReportCategories = "";
    let cats = $('.report_category');
    cats.each((i, elem) => {
        thisReportCategories += $(elem).text() + ((i < cats.length-1)? ", " : "");
    });

    split_report_dialog = bootbox.dialog(
    {
        centerVertical : true,
        title: '<i class="fas fa-exchange-alt"></i> Split Report', 
        message:
        `
        <div class="row">

            <div class="col-5">
                <h3>Current</h3>

                <div class="row px-2">
                    <select id="students_current_select" multiple="true" class="col-12">


                    </select>
                </div>

            </div>

            <div class="col">

                <h3 style="color: transparent;">.</h3>

                <button class="btn btn-outline-secondary" id="moveLeft">
                    <i class="fas fa-arrow-circle-left" aria-hidden="true"></i>
                </button>

                <button class="btn btn-outline-secondary" id="moveRight">
                    <i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                </button>

            </div>

            <div class="col-6">
                <h3>New</h3>

                <div class="row px-2">
                    <select id="students_new_select" multiple="true" class="col-12">


                    </select>
                </div>
            </div>

            <div class="row col-12">
                <div class="col-5">
                    <p>Categories : `+
                        thisReportCategories
                    +`</p>
                    <hr>
                    <p>
                    Descriptions : 
                    `+
                        thisReportData.descriptions.toString()
                    +`</p><hr>
                    <p>
                    `+ $("#suspect_name").text()+`
                    </p><hr>
                    <p> 
                    `+$("#suspect_gender").text()+`
                    </p>

                </div>

                <div class="col-1">
                </div>

                <div class="col-4">

                    <div class="row col-12">
                        <div class="input-group float-right p-2" style="width:auto;">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="new_categories_select">New categories</label>
                            </div>
                            <select class="selectpicker" id="new_categories_select" multiple="true" data-size="auto">
                            </select>
                        </div>
                    </div>

                    Known Suspect :<input id="toggle_known_suspect" type="checkbox" data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No"><br>

                    <div id="known_suspect_panel" class="12">
                        <h5>Suspect (Optional)</h5>
                        <div class="row col-12">
                            <div class="input-group float-right p-2" style="width:auto;">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="suspect_first_name">First Name</label>
                                </div>
                                <input type="text" id="suspect_first_name" class="inputNamesOnly"/>
                            </div>
                        </div>

                        <div class="row col-12">
                            <div class="input-group float-right p-2" style="width:auto;">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="suspect_last_name">Last Name</label>
                                </div>
                                <input type="text" id="suspect_last_name" class="inputNamesOnly"/>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group p-2">
                            <label for="gender" class="control-label col-auto">Gender</label>
                            <div class="col-auto">
                               <select class="form-control" id="new_gender">
                                  <option value="male">Male</option>
                                  <option value="female">Female</option>
                               </select>
                            </div>
                         </div>
                    </div>

                    <div class="col-12" id="suspect_describe_panel" style="display:none;">
                        <div class="row p-2">
                            <h5>New Descriptions</h5>
                        </div>
                        <select id="new_descriptions_select" multiple="true" style="width: 100%; height:80%">
                        </select>



                    </div>

                </div>

            </div>

        </div>
        `,
        buttons: {
            Resolve: {
                label: 'Ok <i class="fas fa-check"></i>',
                className: 'btn-success',
                callback: function(){
                    // check if new students are not empty
                    if($("#students_new_select").children().length > 0)
                    {
                        // check if there's new category selected                        
                        if($('#new_categories_select').val().length > 0)
                        {
                            //check if there's at least one description or a suspect name supplied
                            if($("#new_descriptions_select").val().length > 0 || ($("#suspect_first_name").val().trim().length > 0 && $("#suspect_last_name").val().trim().length > 0))
                            {
                                showLoading(true, "Saving...");

                                var properCategories = new Array();
                                $("#new_categories_select").val().forEach((val) => {
                                    properCategories.push({color : all_categories_color[val], name : val});
                                });

                                // update current report
                                var modified_senders = thisReportData.senders.filter(function(value, index, arr){ return !$("#students_new_select").val().includes(value);});

                                console.log(modified_senders);

                                db.collection("reports").doc(thisReportData.key)
                                .update({senders : modified_senders})
                                .then(function() {
                                    // console.log("Document successfully updated!");
                                    // create a new report

                                    fetchServerTime(function(){
                                        var nowDate = getCurrentDateServerOffset();
                                        var reportCountRef = db.collection("TodaysReport").doc("current");

                                        // get current report index
                                        reportCountRef
                                        .get().then(function(doc) {
                                            var current_today_report_count = 1;

                                            if (doc.exists) {
                                                let todaysReportDate = doc.data().timestamp.toDate();

                                                if(sameDay(nowDate, todaysReportDate))
                                                {
                                                    current_today_report_count = parseInt(doc.data().reportCount) + 1;
                                                }
                                                else
                                                {
                                                    reportCountRef.update({
                                                        reportCount : 0,
                                                        timestamp : firebase.firestore.FieldValue.serverTimestamp()
                                                    });
                                                }                                                
                                            }

                                            reportCountRef.set({reportCount : current_today_report_count, timestamp : firebase.firestore.FieldValue.serverTimestamp()}, { merge: true })
                                            .then(function() {
                                                // console.log("Document successfully written!");
                                                let format = "E MMMM dd YYYY hh:mm:ss a";
                                                let nowDateMoment = moment(nowDate, "YYYY-MM-DD HH:mm:ss");
                                                let appendReportID = pad(current_today_report_count, 3);
                                                let year = nowDate.getFullYear();
                                                let month = nowDate.getMonth() + 1;
                                                let day = nowDate.getDate();

                                                let properDay = (day < 10? "0" : "") + day;
                                                let preNewReportID = "CASE#" + year + month + properDay;

                                                var newReportID = preNewReportID + "_" + appendReportID;

                                                let postedAt = nowDateMoment.format("ddd") + " " + nowDateMoment.format("MMMM") + " " + properDay + " " + year + " " + nowDateMoment.format('hh:mm:ss A');

                                                var newReport = 
                                                {
                                                    reportId : newReportID,
                                                    postedAt : postedAt,
                                                    categories : properCategories,
                                                    descriptions : $("#new_descriptions_select").val(),
                                                    senders : $("#students_new_select").val(),
                                                    suspect : $("#suspect_first_name").val().toLowerCase(),
                                                    suspect_surname : $("#suspect_last_name").val().toLowerCase(),
                                                    gender :  $("#new_gender").val(),
                                                    status : 3
                                                }

                                                if($("#suspect_first_name").val().trim().length > 0 && $("#suspect_last_name").val().trim().length > 0)
                                                {
                                                    newReport.descriptions = new Array();
                                                }
                                                else
                                                {
                                                    newReport.suspect = "";
                                                    newReport.suspect_surname = "";
                                                }

                                                var ref = db.collection("reports").doc();
                                                ref
                                                .set(newReport)
                                                .then(function(){
                                                    var realSendersName = "";
                                                    var splittedSenders = $("#students_new_select").val();
                                                    for(var z=0; z<splittedSenders.length; z++)
                                                    {
                                                        let thisStudent = cached_students[splittedSenders[z].trim()];
                                                        realSendersName += thisStudent.first_name + " " + thisStudent.last_name + ((z == splittedSenders.length-1)? "" : ", ");
                                                    }

                                                    // save initial report of splitted report
                                                    var thisReportDateSplitted = newReport.postedAt.split(" ");
                                                    var initialObj = 
                                                    {
                                                        victims: realSendersName,
                                                        report_date : thisReportDateSplitted[0] + " " + thisReportDateSplitted[1] + " " + thisReportDateSplitted[2] + " " + thisReportDateSplitted[3],
                                                        report_time : thisReportDateSplitted[4] + " " + thisReportDateSplitted[5],
                                                        report_categories : $("#new_categories_select").val().toString(),
                                                        message : "This Report has been created by splitting report from : " + thisReportData.reportId,                                                        
                                                    }

                                                    var hasSuspect = $("#suspect_first_name").val().trim().length > 0;

                                                    if(hasSuspect)
                                                        initialObj["suspect"] = toTitleCase(newReport.suspect + " " + newReport.suspect_surname);
                                                    else
                                                        initialObj["descriptions"] = newReport.descriptions.toString();

                                                    var thisReportDateObject = new Date(moment(newReport.postedAt).format('ddd MMMM DD YYYY hh:mm:ss a'));
                                                    var newInitialReportData =
                                                    {
                                                        name: initial_report_branch_name,
                                                        message: generateInitialReport(initialObj),
                                                        timestamp: firebase.firestore.Timestamp.fromDate(thisReportDateObject)
                                                    };


                                                    db.collection("reports").doc(ref.id).collection("Process").doc().set(newInitialReportData, {merge:true})
                                                    .then(function(){
                                                        

                                                        var endHTML = "Descriptions : " + $("#new_descriptions_select").val() + "<br>";
                                                        if(hasSuspect)
                                                            endHTML = "Suspec First Name : " + toTitleCase($("#suspect_first_name").val().toLowerCase()) + "<br>" + "Suspect Last Name : " + toTitleCase($("#suspect_last_name").val().toLowerCase());
                                                        var newProcessData = {
                                                            name: "Report splitted to " + newReportID,
                                                            message: "Report splitted to " + newReportID + "<br>" + 
                                                            "Senders : " + realSendersName + "<br>" + 
                                                            endHTML,
                                                            timestamp: firebase.firestore.FieldValue.serverTimestamp()
                                                        };
                                
                                                        saveProcessAuto(newProcessData, function(){
                                                            showNotifSuccess("Report " + newReportID + " has been created successfully");
                                                        
                                                            showLoading(false);
                                                            split_report_dialog.remove();
                                                            $('.modal-backdrop').remove();
                                                        });
                                                    })
                                                    .catch(function(error){
                                                        showNotifError("Error", error.message);
                                                    });

                                                    
                                                })
                                                .catch(function(error) {
                                                    console.error("Error writing document: ", error);
                                                    showLoading(false);
                                                    bootbox.alert(error.message);
                                                });

                                            })
                                            .catch(function(error) {
                                                console.error("Error writing document: ", error);
                                                showLoading(false);
                                                bootbox.alert(error.message);
                                            });

                                        }).catch(function(error) {
                                            console.log("Error getting document:", error);
                                            showLoading(false);
                                            bootbox.alert(error.message);
                                        });

                                    });
                                })
                                .catch(function(error) {

                                    showLoading(false);
                                    bootbox.alert(error.message);
                                    // The document probably doesn't exist.
                                    // console.error("Error updating document: ", error);
                                });

                                
                            }
                            else
                            {
                                bootbox.alert({
                                    message: "Please select at least one description, or provide the suspect's first and last name",
                                    centerVertical: true
                                });
                            }
                        }
                        else
                        {
                            bootbox.alert({
                                message: "Please select at least one category for the new report.",
                                centerVertical: true
                            });
                        }
                    }
                    else
                    {
                        bootbox.alert({
                            message: "Please select at least one student, to be move along with new report.",
                            centerVertical: true
                        });
                    }
                    

                    return false;
                }
            },
            Cancel:{
                label: 'Cancel',
                className: 'btn-default',
                callback: function(){
                     return true;           
                }
            }
        }
    });

    split_report_dialog.on("shown.bs.modal", function(){
        if(!$('body').hasClass("modal-open"))
        {
            $('body').addClass("modal-open");
        }
    });

    let current_students_options = "";

    thisReportData.senders.forEach((val)=>{
        current_students_options += '<option value="'+val+'">'+cached_students[val].first_name + ' ' + cached_students[val].last_name +'</option>';
    });

    $("#students_current_select").append(current_students_options);


    $("#moveLeft").on('click', function(){
        let selected = $('#students_new_select').val();

        selected.forEach((val)=>{
            $('#students_new_select option[value="'+val+'"]').detach().appendTo("#students_current_select");
        });
    });

    $("#moveRight").on('click', function(){
        let selected = $('#students_current_select').val();

        if(selected.length < $('#students_current_select').children().length)
        {
            selected.forEach((val)=>{
                $('#students_current_select option[value="'+val+'"]').detach().appendTo("#students_new_select");
            });            
        }
        else
        {
            bootbox.alert({
                message:"You cannot remove everyone from this current report!",
                centerVertical:true
            });
        }
    });

    $('#students_current_select').change(function(event) {

        if ($(this).val().length > $('#students_current_select').val().length-1) {

          $(this).val(last_valid_selection);
        } else {
          last_valid_selection = $(this).val();
        }
    });

    $('#students_current_select option').mousedown(function(e) {
        e.preventDefault();
        $(this).toggleClass('selected');
      
        $(this).prop('selected', !$(this).prop('selected'));
        return false;
    });

    let category_selections = "";
    categories.forEach((val)=>{
        category_selections += '<option value="'+val+'">' +val+ '</option>';
    });


    $('#new_categories_select').append(category_selections);
    $('#new_categories_select').selectpicker();

    let description_selections = "";

    Object.keys(all_descriptions).forEach((description)=>{
        let selected = "";
        if(thisReportData.descriptions.includes(description))
            selected = 'selected="selected"';
        description_selections += '<option value="'+description+'" '+selected+'>' +description+ '</option>';
    });

    $("#new_descriptions_select").append(description_selections);

    $('#new_descriptions_select option').mousedown(function(e) {
        e.preventDefault();
        $(this).toggleClass('selected');
      
        $(this).prop('selected', !$(this).prop('selected'));

        if(this.selected == true)
        {
            all_descriptions[this.value].forEach((val) => {
                let dom = $('#new_descriptions_select option[value="' + val + '"]');
                if(dom.length > 0)
                    dom.get(0).selected = false;
            });
        }
        return false;
    });

    $('#new_descriptions_select option').on('click onmouseup', function(event) {
        event.preventDefault();
        console.log(this.selected);

        if(this.selected == true)
        {
            all_descriptions[this.value].forEach((val) => {
                let dom = $('#new_descriptions_select option[value="' + val + '"]');
                if(dom.length > 0)
                    dom.get(0).selected = false;
            });
            
            this.selected = true;
            return true;
        }
        else
        {
            this.selected = false;
            $('#new_descriptions_select option').focus();
            return false;
        }
    });

    $("#new_descriptions_select").change(function(event){
        let selected = $(this).val();
        for(let i=0; i<selected.length; i++) 
        {
            let value = selected[i];
            all_descriptions[value].forEach((val) => {
                $('#new_descriptions_select option[value="' + val + '"]').get(0).selected = false;
            });
        };
    });

    $('#toggle_known_suspect').bootstrapToggle();

    $('#toggle_known_suspect').bootstrapToggle((thisReportData.descriptions.length == 0)? 'on' : 'off');

    toggleKnownSuspect((thisReportData.descriptions.length == 0));

    $('#toggle_known_suspect').change(function() {
        toggleKnownSuspect($(this).prop('checked'));
    });

    $("#new_gender").val(thisReportData.gender);

    // object.onmouseup = function(){myScript};

}

function toggleKnownSuspect(show)
{
    if (show)
    {
        $("#known_suspect_panel").show();
        $("#suspect_describe_panel").hide();
    }
    else
    {
        $("#known_suspect_panel").hide();
        $("#suspect_describe_panel").show();
    }
}


function sendNewMessage() {

    // fetchServerTime(function(){
        
        let message = $(".message-input textarea").val();
        //we only allow blank message if we are sending an attachment
        if ($.trim(message) == '' && files_to_upload.length == 0) {
            return false;
        }

        if($.trim(message) == ''){
            uploadFiles();
        }
        else
        {
            // let timestamp = new firebasefirestore.Timestamp();
            // var currentDate = moment(timestamp.toDate()).format("YYYYMMDD MMMM D hh:mm:ss A");
            let username = $("footer").attr('data-username');

            var newMessageData = {
                message: message,
                date: firebase.firestore.FieldValue.serverTimestamp(),
                user: firebase.auth().currentUser.email,
                user_id : $("#user_email").attr('data-myName'),
                is_media : false,
                is_admin : true,
                file_name : "",
                key : report_key,
                reportId : thisReportData.reportId
            };

            try{
                db.collection('reports').doc(report_key).collection('messages').doc().set(newMessageData)
                .then(function(){
                    $('.message-input textarea').val(null);
                    if(files_to_upload.length > 0)
                    {
                        uploadFiles();
                    }

                    // make this report accepted because we replied
                    if(thisReportData.status == 3){
                        updateThisReportData(report_key, {status:2});
                    }

                    sessionStorage.setItem('scrolled', false);
                    scrollToBottomChat();

                }).catch(function(error){
                    showNotifError('Failed to send message', error.message);
                });
            }
            catch(error)
            {
                showNotifError('Failed to send message', error.message);
            }
        }
    // });
};




function searchStudent()
{
    let searchValue = $("#search_field").val().trim().toLowerCase();

    if(searchValue.length == 0)
    {
        return;
    }

    showLoading(true, 'searching...');
    $('#search_result_hint').hide();

    search_results = new Array();

    try
    {
        if(searchValue.includes('*'))
        {
            searchStudentByName();
            return;            
        }

        // Search by ID
        db.collection('students').where('ID', '==', searchValue).get()
        .then(function(querySnapshot){

            if(querySnapshot.size > 0)
            {
                let requests = querySnapshot.docs.map((student_data) => {
                
                    // actions[action.data().title] = action.data();
                    search_results.push(student_data.data());
                    cached_students[student_data.data().email] = student_data.data();

                    return new Promise((resolve) => {
                      asyncFunction(student_data, resolve);
                    });
                });

                Promise.all(requests).then(() => {                    
                    // Search by name
                    searchStudentByName();
                });                
            }
            else
            {
                searchStudentByName();
            }

        }).catch(function(error){
            searchStudentByName();
        });
    }
    catch(error)
    {
        // showLoading(false);
        searchStudentByName();
        // $('#search_result_hint').text(error.message);
        // $('#search_result_hint').show();
    }
}

function searchStudentByName()
{
    let searchValue = $("#search_field").val().trim().toLowerCase();

    if(searchValue.includes('*'))
    {
        try
        {
            let splitted_name = searchValue.split('*');
            let f_name = splitted_name[0].trim();
            let l_name = splitted_name[1].trim();

            // Search by ID
            db.collection('students').where('first_name', '==', f_name).where('last_name', '==', l_name).get()
            .then(function(querySnapshot){

                if(querySnapshot.size > 0)
                {
                    let requests = querySnapshot.docs.map((student_data) => {
                    
                        // actions[action.data().title] = action.data();
                        search_results.push(student_data.data());


                        return new Promise((resolve) => {
                          asyncFunction(student_data, resolve);
                        });
                    });

                    Promise.all(requests).then(() => {                    
                        // Search by name
                        showSearchResults();
                    });                
                }
                else
                {
                    showSearchResults();
                }

            }).catch(function(error){
                showSearchResults(error.message);
            });
        }
        catch(error)
        {
            showSearchResults(error.message);
        }
    }
    else
    {
        try
        {
            // Search by first name
            db.collection('students').where('first_name', '==', searchValue).get()
            .then(function(querySnapshot){

                if(querySnapshot.size > 0)
                {
                    let requests = querySnapshot.docs.map((student_data) => {
                    
                        // actions[action.data().title] = action.data();
                        search_results.push(student_data.data());


                        return new Promise((resolve) => {
                          asyncFunction(student_data, resolve);
                        });
                    });

                    Promise.all(requests).then(() => {
                        showSearchResults();
                    });                
                }
                else
                {
                    // Search by student surname.
                    db.collection('students').where('last_name', '==', searchValue).get()
                    .then(function(querySnapshot){

                        if(querySnapshot.size > 0)
                        {
                            let requests = querySnapshot.docs.map((student_data) => {
                            
                                // actions[action.data().title] = action.data();
                                search_results.push(student_data.data());


                                return new Promise((resolve) => {
                                  asyncFunction(student_data, resolve);
                                });
                            });

                            Promise.all(requests).then(() => {
                                showSearchResults();
                            });                
                        }
                        else
                        {
                            showSearchResults();
                        }

                    }).catch(function(error){
                        showSearchResults(error.message);
                    });

                }

            }).catch(function(error){
                showSearchResults(error.message);
            });
        }
        catch(error)
        {
            showSearchResults(error.message);
        }
    }
}

function addrecipient(elem)
{
    let student_id = $(elem).parent().parent().attr("data-id");
    console.log("this id " + student_id);

    // let student_data = getStudentFromSearchResult(student_id);

    var res = search_results.filter(obj => {
      return obj.ID === student_id
    });

    let student_data;

    if(res.length > 0)
        student_data = res[0];
    // console.log('adding recipient');

    if(student_data != undefined)
    {

        // Don't add multiple same student
        if($('#recipients_container h4[data-email="'+student_data.email+'"]').length == 0)
        {
            let toAppend =
            '<h4 class="m-1" data-email="'+student_data.email+'"><span class="badge badge-pill badge-success">'+ toTitleCase(student_data.first_name) +' '+ toTitleCase(student_data.last_name) +'   <i style="cursor:pointer;" onclick="$(this).parent().parent().remove();" class="fa fa-times"></i> </span></h4>';
            $("#recipients_container").append(toAppend);
        }

        $(elem).parent().parent().remove();

        console.log('added recipient');
    }
}

function getStudentFromSearchResult(student_id)
{
    let requests = search_results.map((item) => {
        console.log(item.ID);
        console.log(item.ID == student_id);
        if(item.ID == student_id)
            return item;

        return new Promise((resolve) => {
          asyncFunction(item, resolve);
        });
    });

    Promise.all(requests).then(() => {
        return null;
    });



    // return null;
}

function showSearchResults(title)
{
    showLoading(false);
    let message = "...";
    if(title == undefined && search_results.length > 0)
    {
        title = search_results.length + " result"+((search_results.length > 1)?"s":"")+" found.";
        let tds = '';
        search_results.forEach((student, index) =>{
            tds += '<tr data-id="'+student.ID+'">';
            tds += '<td>' + student.ID + '</td>';
            tds += '<td>' + student.email + '</td>';
            tds += '<td>' + toTitleCase(student.first_name) + '</td>';
            tds += '<td>' + toTitleCase(student.last_name) + '</td>';
            tds += '<td>' + student.contact_number + '</td>';
            tds += '<td>' + student.status + '</td>';
            tds += '<td>' + student.gender + '</td>';
            tds += '<td>' + student.address + '</td>';
            tds += '<td>' + student.course + '</td>';
            tds += '<td>' + student.birthday + '</td>';
            tds += '<td><button onclick="addrecipient(this);" type="button" class="btn btn-primary"> + <i class="fa fa-paper-plane" aria-hidden="true"></i></button></td>';

            tds += '</tr>';
        });

        message = 
        `<table id="results_table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr id="table_tr">
                            <th>ID</th>
                            <th>E-mail</th>                                                                    
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Contact number</th>
                            <th>Status</th>                                                                     
                            <th>Gender</th>
                            <th>Address</th>
                            <th>Course</th>
                            <th>Birthday</th>
                            <th>Add recipient</th>
                        </tr>
                    </thead>
                    <tbody>
                    `+tds+
                    `</tbody>
                </table>`;

    }
    else
    {
        title = "No results found :(";
    }


    bootbox.dialog(
    {
        title: title,
        centerVertical: true,
        scrollable: true,
        message: message,
         buttons: {
            Done: {
                label: 'Done',
                className: 'btn-success',
                callback: function(){
                        return true;
                }
            }
        }
    });
}

function sendInviteNotification(newNotifData)
{
    // console.log(newNotifData);
    showLoading(true, "Sending invitation...");

    try
    {
        db.collection('notifications').doc().set(newNotifData)
        .then(function() {

            let recipients = new Array();
            
            newNotifData.recipients.forEach((elem) => {
                if(elem in cached_students)
                recipients.push(cached_students[elem].first_name + " " + cached_students[elem].last_name);
            });

            var newProcessData = {
                name: "Invite notification sent",
                message: 
                "Title : " + newNotifData.title + "<br>"
                + "Sent to : " + recipients.toString() + "<br>"
                + newNotifData.message + "<br>",
                timestamp: firebase.firestore.FieldValue.serverTimestamp()
            };

            saveProcessAuto(newProcessData, function(){
                showLoading(false);
                showNotifSuccess("Sent", "Invitation successfully sent!");
                invite_dialog.modal('hide');

                location.reload();
            });


            
        })
        .catch(function(error) {
            showLoading(false);
            showNotifError("Error", error.message);
        });
    }
    catch(error)
    {
        showLoading(false);
        showNotifError("Error", error.message);
    }
}

function showInvitePrompt()
{
    invite_dialog = bootbox.dialog(
    {
        centerVertical : true,
        title: '<i class="fas fa-bullhorn" aria-hidden="true"></i> Send an invite notification', 
        message:


        '<form id="infos" action="" >'+ 
            '<small style="color:red;" >Note: when searching for student\'s full name, separate the first name and surname by an asterisk * for better results.</small>'+
            '<div class="input-group mb-3" style="width:100%">'+
                
              '<input type="text" class="form-control" id="search_field" placeholder="Search for student\'name/ surname / ID" aria-describedby="basic-addon2">'+
              '<div class="input-group-append">'+
                '<button class="btn btn-primary" type="button" onclick="searchStudent();">Search</button>'+
              '</div>'+
            '</div>'+
            // '<small style="display:none; color:red;" id="search_result_hint">No search result</small>'+


            '<label>Recipients:</label>'+
            '<br>'+
            '<div class="row" id="recipients_container" style="background-color:gray; border-radius:4px;  min-height:20px;">'+

            '</div>'+
            '<button type="button" class="btn btn-outline-secondary float-right" onclick="$(this).prev().empty();">Clear recipients</button><hr>'+

            'Title:<input type="text" id="notif_title" style="width:100%; border-radius:5px; "/><br>'+
            'Message:<textarea id="summernote" style="width:100%; border-radius:5px; "></textarea><br>'+

        '</form>'
        ,
        buttons: {
            Resolve: {
                label: 'Send  <i class="fa fa-paper-plane"></i>',
                className: 'btn-success',
                callback: function(){
                    
                    let message = $('#summernote').summernote('code');

                    let recipients_container = document.getElementById('recipients_container');
                    // Check if message is blank // Check if has recipients.
                    if (!$('#summernote').summernote('isEmpty') && recipients_container.childElementCount > 0 && $("#notif_title").val().trim().length > 0){
                        

                        // let thisDate = new Date();
                        // let thisDate = moment().format("YYYYMMDD MMMM D hh:mm:ss A");
                        // fetchServerTime(function(){
                            let recipientsNodes = [...recipients_container.children];
                            let recipients = new Array();
                            
                            recipientsNodes.forEach((elem) => {
                                recipients.push($(elem).attr('data-email'));
                            });

                            // let thisDate = getCurrentDateServerOffset().format("YYYYMMDD MMMM D hh:mm:ss A");
                            var newNotifData = 
                            {
                                title: $("#notif_title").val(), 
                                dateSent: firebase.firestore.FieldValue.serverTimestamp(), 
                                recipients : recipients, 
                                message : message, 
                                reportId: thisReportData.reportId, 
                                resolved : thisReportData.status == 0
                            };
                            sendInviteNotification(newNotifData);
                        // });

                        
                    }
                    else
                    {
                        showNotifError("Required field", "Please fill up message and select at least one recipient!");
                    }


                    return false;


                }
            },
            Cancel:{
                label: 'Cancel',
                className: 'btn-default',
                callback: function(){
                     return true;           
                }
            }
        }
    });

    $('#summernote').summernote({
        height: '200px',
        toolbar: defaultToolbar
    });

}

function fetchActions() {
    showLoading(true, "Loading...");

    try{
        db.collection('actions').get().then(function(querySnapshot){
            // console.log(querySnapshot);
            //updateReportCountToolTip();

            let requests = querySnapshot.docs.map((action) => {
                
                // var hasCategory = action.data().categories.some(cat=> .indexOf(cat) >= 0)
                
                Object.keys(thisReportData.categories).forEach((e) => {
                    if(action.data().categories.includes(thisReportData.categories[e].name))
                        actions[action.data().title] = action.data();
                });
                // action.data().categories.includes(thisReportData.categories)
                // if(hasCategory)
                //     actions[action.data().title] = action.data();

                return new Promise((resolve) => {
                  asyncFunction(action, resolve);
                });
            });

            Promise.all(requests).then(() => {
                // setMessageCaseNew();
                showLoading(false);
            });

        }, function(error){
            setTimeout(() => {
                console.log("retrying fetchActions...");
                fetchActions();
            }, 3000);
        });
    }
    catch(error)
    {
        setTimeout(() => {
            console.log("retrying fetchActions...");
            fetchActions();
        }, 3000);
    }
}

function clickEditReportStatus()
{
    var actionSelectionsOptions = '';
    // var selected_category_from_data = '';

    Object.keys(actions).forEach(function(e, i){
        actionSelectionsOptions += '<option value="'+toTitleCase(e)+'">'+actions[e].title+'</option>';
        // if(_category == actions[e].title)
        // {
        //     selected_category_from_data = e;
        // }
    });

    // actionSelectionsOptions += '<option value="-1">Invalidate</option>';

    resolve_dialog = bootbox.dialog(
    {
        centerVertical : true,
        title: "Change Report Status", 
        message:


        '<form id="infos" action="">'+ 
            '<label for="status_select" style="width:100%;">Report Status:<select id="status_select" style="width: 100%">'+
                '<option value="3">Pending ...</option>'+
                '<option value="2">Reported. Waiting for response ...</option>'+
                '<option value="1">Report Verified. Processing. Investigation on going ...</option>'+
                '<option value="0">Report has been resolved.</option>'+
                '<option value="-1">Invalidate</option>'+
                '</select></label><br><hr>'+

            '<div id="resolve_div" style="display:none;">'+
                '<label for="seminar_to_attended" style="width:100%;">Resolve action :<select id="seminar_to_attended" style="width: 100%" multiple="multiple">'+
                actionSelectionsOptions+
                '</select></label><br>'+
                'Description:<textarea id="summernote" style="width:100%; border-radius:5px; "></textarea><br>'+
                '<label for="resolve_date">Resolve Date:</label><div class="input-group"><input id="resolve_date" class="form-control" size="16" type="text" value="" ><div class="input-group-append"><span class="input-group-text"><i class="far fa-calendar-alt"></i></span></div></div><br>'+
            '</div>'+
        '</form>'
        ,
        buttons: {
            Resolve: {
                label: 'Save',
                className: 'btn-success',
                callback: function(){
                        let newStatus = parseInt($("#infos #status_select").val());
                        

                        if(newStatus != 0)
                        {
                            let newReportData =
                            {
                                resolve_seminars : new Array(),
                                resolve_description : "",
                                resolve_date : "",
                                status : newStatus
                            }

                            updateThisReportData(report_key, newReportData, true);
                        }                        
                        else
                        {
                            let description = $('#summernote').summernote('code');

                            if($("#infos #seminar_to_attended").val().length == 0)
                            {
                                showNotifError("Field required", "Please specify atleast one resolve seminar");  
                                return false;
                            }

                            if (!$('#summernote').summernote('isEmpty')){
                                // bootbox.alert('You typed ' + description);
                                saveResolve($("#infos #seminar_to_attended").val(), description, $('#infos #resolve_date').val())
                            }
                            else
                            {
                                showNotifError("Field required", "Please fill out description");                                
                            }
                        }

                        return false;


                }
            },
            Cancel:{
                label: 'Cancel',
                className: 'btn-default',
                callback: function(){
                                    
                }
            }
        }
    });


    $("#infos #status_select").val(thisReportData.status);
    if(thisReportData.status == 0)
        $('#infos #resolve_div').show();

    document.getElementById('status_select').addEventListener('change', function(){
        if(this.value == 0)
        {
            $('#infos #resolve_div').show();
        }
        else
        {
            $('#infos #resolve_div').hide();
        }
    });

    resolve_dialog.on('shown.bs.modal', function(e){

        
        $('#summernote').summernote({
            height: '200px',
            toolbar: defaultToolbar
        });

        let select = $('#seminar_to_attended');
        select.select2({dropdownParent: resolve_dialog, width: 'resolve', theme: "classic"});
        // select.val(selected_categories_from_data).trigger('change');

        
        let resolve_date_picker = $('#infos #resolve_date');
        resolve_date_picker.datetimepicker({autoclose:true, disableTouchKeyboard: true, dateFormat: 'MM-dd-yyyy HH:ii', forceParse:true});


        resolve_date_picker.datetimepicker('setDate', new Date());

        // Set resolved datas.
        if(thisReportData.status == 0)
        {
            $('#summernote').summernote('code', thisReportData.resolve_description);
            select.val(thisReportData.resolve_seminars).trigger('change');
            resolve_date_picker.val(moment(thisReportData.resolve_date).format("YYYY-MM-DD HH:mm"));
        }


        addPastePrevent($('#infos #resolve_date'));

    });
}


function refreshReportUI()
{
    let final_status_text = "Invalid Report";
    let final_status_class = "badge badge-danger";

    if(thisReportData.status > -1){
        final_status_text = status_text[thisReportData.status];
        final_status_class = status_class[thisReportData.status];
    }

    var realReportName = thisReportData.reportId;
    if(thisReportData.status == 3 || thisReportData.status == 2)
    {
        realReportName = "Report#" + thisReportData.reportId.split("#")[1];
    }

    $("#page-header > h1").fadeOut(1000, function() {
        $(this).html('<h2>'+realReportName+'  <span class="'+final_status_class+'">' + final_status_text + ' </span></h2>').fadeIn(1000);
    });

    $("#categories_container").empty();

    // Add Categories.
    this_report_category_names = new Array();
    thisReportData.categories.forEach((item, index) => {
        let toAppend = '<h3><span class="report_category badge badge-default m-2" style="background-color:'+item['color']+'; color:'+invertColor(item['color'], true)+';">' + capitalizeFLetter(item['name']) + '</span></h3>';
        this_report_category_names.push(item['name']);
        $("#categories_container").append(toAppend);
    });

    // Set Suspect name.
    $("#suspect_name").text("Suspect : " + toTitleCase(thisReportData.suspect) + " " + toTitleCase(thisReportData.suspect_surname));
    // fadeInOutElement("#suspect_name", "Suspect : " + toTitleCase(thisReportData.suspect) + " " + toTitleCase(thisReportData.suspect_surname));

    // Set Report Date.
    // $("#categories_container").append('<hr style="color:white !important;">');
    $("#report_date_text").remove();
    $("#report_card_body").prepend('<div class="row" id="report_date_text"><small  style="font-style: italic; font-weight:bolder; color:black; padding-left: 10px" class="m-2">Report Date : '+thisReportData.postedAt+'</small></div>');

    // Set senders.


    // Set gender.
    fadeInOutElement("#suspect_gender", "Gender : " + capitalizeFLetter(thisReportData.gender));

    // Set Descriptions
    $("#descriptions_container").empty();
    thisReportData.descriptions.forEach((item, index) => {
        let toAppend = '<h3><span class="badge badge-default m-1" style="background-color:white; color:black;">' + capitalizeFLetter(item) + '</span></h3>';

        $("#descriptions_container").append(toAppend);
    });
}

function SendReopenNotif()
{
    console.log("Sending re-open notifications");
    db.collection('notifications').where('reportId', '==', thisReportData.reportId)
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            // console.log(doc.id, " => ", doc.data());
            console.log(doc);

            let newNotifData = {
                title: thisReportData.reportId + " has been re-opened!", 
                dateSent: firebase.firestore.FieldValue.serverTimestamp(), 
                message : thisReportData.reportId + " has been re-opened!<br> You can now continue in helping to solve this case.", 
                reportId: thisReportData.reportId,
                resolved : false
            };

            db.collection('notifications').doc(doc.id).set(newNotifData, {merge:true})
            .then(function(){
                console.log("Updated notification " )

            });

        });
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}

function updateThisReportData(key, data, fromForm)
{
    var thisPreviousReportStatus = thisReportData.status;
    var thisCurrentSuspect = thisReportData.suspect;
    var thisCurrentSuspectSurname = thisReportData.suspect_surname;
    var thisCurrentSuspectGender = thisReportData.gender;

    if(thisPreviousReportStatus == data.status)
    {
        showLoading(false);
        return;
    }

    try
    {
        db.collection('reports').doc(key).set(data, {merge:true}).then(function(){
            if(fromForm)
            {
                //re-add previous solved cases
                
                if(thisCurrentSuspect != null && thisCurrentSuspect.trim().length > 0 ){
                    var offense_doc_key = thisCurrentSuspect.toLowerCase() + "-" + thisCurrentSuspectSurname.toLowerCase() + ":" + thisCurrentSuspectGender;

                    //this report is now verified and ongoing suspect is guilty add offense count by 1
                    if((thisPreviousReportStatus == -1 || thisPreviousReportStatus == 3 || thisPreviousReportStatus == 2) && data.status == 1)
                    {
                        db.collection("offense").doc(offense_doc_key).set({count:firebase.firestore.FieldValue.increment(1)}, {merge:true})
                        .then(function(){
                            showLoading(false);
                            if(resolve_dialog != null)
                                resolve_dialog.modal('hide');
                            showNotifSuccess("Saved", "Report status saved successfully");
                        }).catch(function(error){
                            showLoading(false);
                            console.error(error.message);
                        });
                    }

                    //FROM RESOLVE TO OTHER STATUS
                    if(thisPreviousReportStatus == 0)
                    {
                        var to_add = -2;
                        if(data.status == 3)
                            to_add = -3;

                        db.collection("offense").doc(offense_doc_key).set({count:firebase.firestore.FieldValue.increment(to_add)}, {merge:true})
                            .then(function(){
                                console.log("updated offense back " + thisReportData.reportId);
                                showLoading(false);
                                if(resolve_dialog != null)
                                    resolve_dialog.modal('hide');
                                showNotifSuccess("Saved", "Report status saved successfully");
                            })
                            .catch(function(error){
                                console.error(error.message);
                            });
                        
                            
                        db.collection("offense").doc(offense_doc_key)
                        .collection("myResolves").doc(thisReportData.reportId)
                        .delete()
                        .then(function(){
                            console.log("Resolve offense removed!");
                        })
                        .catch(function(error){
                            console.error(error.message);
                        });
                        // .get()
                        // .then(function(querySnapshot){
                        //     if(querySnapshot.exists)
                        //     {
                                
                        //     }                    
                        // })
                        // .catch(function(error){
                        //     console.error(error.message);
                        // });
                    }
                }
            }
            // else
            // {
            showLoading(false);
            if(resolve_dialog != null)
                resolve_dialog.modal('hide');
            showNotifSuccess("Saved", "Report status saved successfully");
            // }

            console.log("has reportId " + thisReportData.reportId);

        }
            ,function(error){
                setTimeout(() => {
                console.log("retrying updateThisReportData...");
                updateThisReportData(key, data, fromForm)
            }, 3000);
        });
    }
    catch(error)
    {
        setTimeout(() => {
            console.log("retrying updateThisReportData...");
            updateThisReportData(key, data, fromForm)
        }, 3000);
    }
}

function fetchSendersInfo()
{
    thisReportData.senders.forEach((sender) => {
        // console.log(sender + " is not yet fetched");
        if(!(sender in cached_students))
        {
            try
            {
                console.log('fetchSendersInfo');

                db.collection('students').where('email', '==', sender)
                .onSnapshot(function(querySnapshot) {
                    console.log(querySnapshot);
                    let thisData = querySnapshot.docs[0].data();
                    cached_students[sender] = thisData;

                    $(".realname-" + thisData.ID).text(toTitleCase(thisData.first_name) + " " + toTitleCase(thisData.last_name));
                    console.log(sender + ' stored info');

                    getSetProfileImage(thisData.ID);
                })
                .catch(function(error){
                    setTimeout(() => {
                        console.log("retrying fetchSendersInfo...");
                        fetchSendersInfo();
                    }, 3000);
                });
            }
            catch(error)
            {
                setTimeout(() => {
                    console.log("retrying fetchSendersInfo...");
                    fetchSendersInfo();
                }, 3000);
            }
        }
        else
        {
            let thisData = cached_students[sender];
            $(".realname-" + thisData.ID).text(toTitleCase(thisData.first_name) + " " + toTitleCase(thisData.last_name));
        }
    });
}

function fetchIndividualSenderInfo(senderEmail)
{
    db.collection('students').where('email', '==', senderEmail)
    .get().then(function(querySnapshot) {
        // console.log(querySnapshot);
        let thisData = querySnapshot.docs[0].data();
        cached_students[senderEmail] = thisData;

        $(".realname-" + thisData.ID).text(toTitleCase(thisData.first_name) + " " + toTitleCase(thisData.last_name));
        console.log(senderEmail + ' stored info');

        getSetProfileImage(thisData.ID);
    })
    .catch(function(error){
        setTimeout(() => {
            console.log("retrying fetchSendersInfo...");
            fetchSendersInfo();
        }, 3000);
    });
}

function setMessageCaseNew()
{
    showLoading(true, "Loading Report...");
    caseID = sessionStorage.getItem('selectedCase');
    // sessionStorage.setItem();

    if(caseID.trim().length == 0)
    {
        bootbox.alert("Report doesn't exists anymore!");
        setTimeout(() => {
            sessionStorage.setItem('selectedCase', '');
            window.location.replace("reports.html");
        }, 3000);
        return;
    }

    // Stop listening to changes
    if(report_listener != null)
        report_listener();

    try
    {
        cached_students = [];

        report_listener = db.collection("reports").where('reportId', '==', caseID)
        .onSnapshot(function(snapshot) {
            if(snapshot.size == 0)
            {
                bootbox.alert("Report doesn't exists anymore!");
                setTimeout(() => {
                    sessionStorage.setItem('selectedCase', '');
                    window.location.replace("reports.html");
                }, 3000);
            }

            snapshot.docChanges().forEach(function(change) {
                if (change.type === "added") {

                    report_key = change.doc.id;

                    addMessageListener();
                    

                    thisReportData = change.doc.data();

                    thisReportData["key"] = report_key;

                    // Update this report status if it still pending.
                    

                    console.log('report added');

                    tryFetchSuspectPic();
                    refreshReportUI();

                    // get senders info
                    fetchSendersInfo();

                    
                    if(thisReportData.suspect.trim().length > 0)
                    {
                        fetchOffenseCount();
                        $("#penalty_button").css("display", "inline-block");
                    }
                    else
                    {
                        $("#penalty_button").css("display", "none");
                    }

                    //dont allow reply when status is resolved
                    if(thisReportData.status <= 0)
                    {
                        $(".message-input").css("display", "none");
                    }
                    else
                    {
                        $(".message-input").css("display", "block");
                    }

                    

                    showLoading(false);
                }

                if (change.type === "modified") {
                    console.log('report modified');
                    previousReportStatus = thisReportData.status;

                    thisReportData = change.doc.data();

                    // var newProcessName = "Report status changed to " + ((thisReportData.status > -1)? status_text[thisReportData.status] : " Invalidated");
                    // This report has re-open
                    if(previousReportStatus == 0 && thisReportData.status > 0)
                    {
                        SendReopenNotif();
                        // newProcessName = "Report has been re-opened.";
                    }

                    if(thisReportData.status == 0)
                    {
                        getLastOffenseResolve();
                    }

                    if(thisReportData.status == -1)
                    {                        
                        $("#split_report_button").css("display", "none");
                        $("#merge_report_button").css("display", "none");
                    }
                    
                    //dont allow reply when status is resolved
                    if(thisReportData.status <= 0)
                    {
                        $(".message-input").css("display", "none");
                    }
                    else
                    {
                        $(".message-input").css("display", "block");
                    }


                    // var realReportName = thisReportData.reportId;
                    // if(thisReportData.status == 3 || thisReportData.status == 2)
                    // {
                    //     realReportName = "Report#" + thisReportData.reportId.split("#")[1];
                    // }

                    // $("#page-header > h1").fadeOut(1000, function() {
                    //     $(this).html('<h2>'+realReportName+'  <span class="'+final_status_class+'">' + final_status_text + ' </span></h2>').fadeIn(1000);
                    // });

                    // if(thisReportData.status == 0)
                    // {
                    //     $("#resolve_export_button").css("display", "inline-block");
                    // }
                    // else
                    // {
                    //     $("#resolve_export_button").css("display", "none");
                    // }


                    tryFetchSuspectPic();

                    fetchSendersInfo();

                    refreshReportUI();

                    if(thisReportData.suspect.trim().length > 0)
                    {
                        fetchOffenseCount();
                    }
                }

                if (change.type === "removed") {
                    bootbox.alert("Report doesn't exists anymore!");
                    setTimeout(() => {
                        sessionStorage.setItem('selectedCase', '');
                        window.location.replace("reports.html");
                    }, 3000);
                }
            });

            if(thisReportData.status > -1)
            {
                $("#split_report_button").css('display', 'inline-block');
                $("#merge_report_button").css('display', 'inline-block');
            }

            fetchActions();
            loadTreeData();
        }
        ,function(error){
            setTimeout(() => {
                console.log("retrying setMessageCaseNew...");
                setMessageCaseNew();
            }, 3000);
        });

    }
    catch(error)
    {
        console.log(error);
        setTimeout(() => {
            console.log("retrying setMessageCaseNew...");
            setMessageCaseNew();
        }, 3000);
    }
}




$.extend({
    jYoutube: function (url, size) {
        if (url === null) { return ""; }

        size = (size === null) ? "big" : size;
        var vid;
        var results;

        results = url.match("[\?&]v=([^&#]*)");

        vid = (results === null) ? url : results[1];

        if(url.includes('https://youtu.be/'))
            vid = url.split("https://youtu.be/")[1];

        // if (size == "small") {
        //     return "http://img.youtube.com/vi/" + vid + "/2.jpg";
        // } else {
            return "http://img.youtube.com/vi/" + vid + "/0.jpg";
        // }
    }
});

function createTextLinks_(text) {

  return (text || "").replace(
    /([^\S]|^)(((https?\:\/\/)|(www\.))(\S+))/gi,
    function(match, space, url){
      var hyperlink = url;
      if (!hyperlink.match('^https?:\/\/')) {
        hyperlink = 'http://' + hyperlink;
      }
      return space + '<a href="' + hyperlink + '">' + url + '</a>';
    }
  );
};


function record(url, result, img, senderObj) {    
    var myLi = $("#message_ul li[data-key='"+senderObj._key+"']");
    
    var message_exists = myLi.length > 0;

    let sender_is_me = senderObj._sender_id == firebase.auth().currentUser.email;
    
    if(result == "success")
    {
        //check if this message contains not only an image
        
        if(message_exists)
        {
			if(senderObj._message_splitted.length > 1)
			{
				//we will only show first image on the message

				var p_with_link = '<a href="'+url+'">' + url + '</a>';
				//var element = $("#message_ul li[data-key='"+senderObj._key+"'] p");
				var element = myLi.find("p");
				console.log(senderObj._key);
				console.log(url);
				console.log(element);
				var currString = senderObj._final_message;
				var img_link = url;
				if(element.length > 0){
					currString = element.text();
					if(element.children('img').length > 0)
						img_link = element.children('img')[0].attr('src');
				}
				
				myLi.children().not(':first-child').remove();
				var newAppend = '<p>' + currString.replace(url, p_with_link) + '<a data-fancybox="gallery" href="'+img_link+'"><img style="width:100%;" src="'+img_link+'" alt=""/></a></p>';
				
				myLi.append(newAppend);
			}
		}
        else
        {
			var msg = '<p>' + senderObj._final_message + '</p>';
			//this is to make sure we create a link, if the message only contains a link (no space)
			
			if(senderObj._message_splitted.length == 1)
				msg = '<a data-fancybox="gallery" href="'+url+'"><img class="message_image" src="'+url+'" alt=""/></a>';
			
			$('<li data-key="'+senderObj._key+'" data-msg_date="'+senderObj._msg_date+'" class="'+senderObj._senderClass+' message_entry"><img data-id="'+senderObj._sender_user_id+'" class="sender-'+senderObj._sender_user_id+' message_sender_pics" src="'+senderObj._senderPic+'" alt="" />'+msg+'</li>').appendTo($('.messages ul'));
		
            if(senderObj._sender_user_id in cached_profile_pics)
                $('img[data-id="'+senderObj._sender_user_id+'"]').attr('src', cached_profile_pics[senderObj._sender_user_id]);
        }
        scrollToBottomChat();
        resortLi();
        
        lastMsgDate = senderObj._msg_date;
    }
    else
    {
        //check if youtube url
        if(url.includes('www.youtube.com') || url.includes("https://youtu.be"))
        {
			var thumbnail_link = $.jYoutube(url, 'big');
			var stylePlayIcon = "right:50%";
			if(sender_is_me)
				stylePlayIcon = "left:50%";
			
			if(message_exists)
			{				
				if(senderObj._message_splitted.length > 1)
				{					
					//we will only show first video thumbnail on the message

					var p_with_link = '<a href="'+url+'">' + url + '</a>';
					//var element = $("#message_ul li[data-key='"+senderObj._key+"'] p");
					var element = myLi.find("p");
					console.log(senderObj._key);
					console.log(url);
					console.log(element);
					var currString = senderObj._final_message;
					var img_link = thumbnail_link;
					if(element.length > 0){
						currString = element.text();
						if(element.children('div').length > 0)
							img_link = element.find('div').find('a').find('img').attr('src');
					}
					
					//~ myLi.children().not(':first-child').empty();
					myLi.children().not(':first-child').remove();
					//~ var newAppend = '<p>' + currString.replace(url, p_with_link) + 
					//~ '<div class="video_wrapper_'+senderObj._senderClass+'"><a data-fancybox="gallery" href="'+url+'"><img class="message_image" style="width:100%; height:100%;" src="'+thumbnail_link+'" alt=""/><div class="play_video_icon" style="'+stylePlayIcon+'"></div></a></div>'+
					//~ '</p>';
					var newAppend = '<p>' + currString.replace(url, p_with_link) + '<a data-fancybox="gallery" href="'+url+'"><img style="width:100%;" src="'+img_link+'" alt=""/></a></p>';
					
					myLi.append(newAppend);
				}
			}
			else
			{
				var msg = '<p>' + senderObj._final_message + '</p>';
				//this is to make sure we create a link, if the message only contains a link (no space)
				
				if(senderObj._message_splitted.length == 1){
					// msg = '<a data-fancybox="gallery" href="'+url+'"><img class="message_image" src="'+thumbnail_link+'" alt=""/></a>';
					msg = '<div class="video_wrapper_'+senderObj._senderClass+'"><a data-fancybox="gallery" href="'+url+'"><img class="message_image" style="width:100%; height:100%;" src="'+thumbnail_link+'" alt=""/><div class="play_video_icon" style="'+stylePlayIcon+'"></div></a></div>';
                }
				
				
				$('<li data-key="'+senderObj._key+'" data-msg_date="'+senderObj._msg_date+'" class="'+senderObj._senderClass+' message_entry"><h6 class="'+(sender_is_me?'':'realname-'+senderObj._sender_user_id)+'" >'+(sender_is_me?'':senderObj._sender_name)+'</h6><img data-id="'+senderObj._sender_user_id+'" class="message_sender_pics" src="'+senderObj._senderPic+'" alt="" />'+ msg
				+'</li>').appendTo($('.messages ul'));

                if(senderObj._sender_user_id in cached_profile_pics)
                    $('img[data-id="'+senderObj._sender_user_id+'"]').attr('src', cached_profile_pics[senderObj._sender_user_id]);

			}
			
			scrollToBottomChat();
			resortLi();
			
			lastMsgDate = senderObj._msg_date;
        }
        else
        {
			//this will prevent multiple append of same message (it happened because we are checking message string for each word separated by white space)			
			
			var isValidUrl = validURL(url);
			
			var p_with_link = url;
			if (isValidUrl){
				p_with_link = '<a href="'+url+'">' + url + '</a>';
			}
			
			// if(message_exists)
			// {
			// 	var element = myLi.find("p");//$("#message_ul li[data-key='"+senderObj._key+"'] p");
			// 	var currString = senderObj._final_message;//
			// 	console.log(senderObj._key);
			// 	console.log(url);
			// 	console.log(element);
			// 	var theString = url;
			// 	if(element.length > 0)
			// 	{					
			// 		theString = element.text();
			// 		if(isValidUrl)
			// 			theString = currString.replace(url, p_with_link);
			// 	}

			// 	myLi.children().not(':first-child').remove();
				
			// 	$(myLi).append('<p>'+theString+'</p>');
			// }
			// else
			// {
				var msg = senderObj._final_message;
				//this is to make sure we create a link, if the message only contains a link (no space)
				
				if(senderObj._message_splitted.length == 1 && isValidUrl)
					msg = '<a href="'+senderObj._final_message+'">' + senderObj._final_message + '</a>';
					
                let realName = senderObj._sender_name;

                if(senderObj._sender_id in cached_students)
                    realName = toTitleCase(cached_students[senderObj._sender_id].first_name) + " " + toTitleCase(cached_students[senderObj._sender_id].last_name);
                else (!thisReportData.senders.includes(senderObj._sender_id))
                {
                    fetchIndividualSenderInfo(senderObj._sender_id);
                }
                let nameStyle = 'left';

                

                if(sender_is_me)
                    nameStyle = 'right';

                if($('#message_ul li[data-key="'+senderObj._key+'"]').length > 0)
                {
                    $('#message_ul li[data-key="'+senderObj._key+'"]').remove();
                }

                let li = document.createElement("LI");
                li.setAttribute('data-key', senderObj._key);
                li.setAttribute('data-msg_date', senderObj._msg_date);
                li.classList.add(senderObj._senderClass);
                li.classList.add('message_entry');

                let h6 = document.createElement("H6");
                h6.classList.add('realname-'+senderObj._sender_user_id);
                h6.style.textAlign = nameStyle;
                if(sender_is_me == false)
                    h6.appendChild(document.createTextNode(realName));
                li.appendChild(h6);

                if(sender_is_me == false)
                {
                    let newImg = document.createElement("IMG");
                    newImg.setAttribute('data-id', senderObj._sender_user_id);
                    newImg.classList.add('message_sender_pics');
                    if((senderObj._sender_user_id in cached_profile_pics))
                        newImg.src = cached_profile_pics[senderObj._sender_user_id];
                    else
                        newImg.src = senderObj._senderPic;
                    li.appendChild(newImg);
                }

                let p = document.createElement("P");
                let newMsg = createTextLinks_(msg);
                // p.appendChild(document.createTextNode(newMsg));
                // li.appendChild(p);

                document.getElementById('message_ul').appendChild(li);

                $('#message_ul li[data-key="'+senderObj._key+'"]').append('<p>' + newMsg + '</p>');

                // if((senderObj._sender_user_id in cached_profile_pics))
                //     $('img[data-id="'+senderObj._sender_user_id+'"]').attr('src', cached_profile_pics[senderObj._sender_user_id]);
            // }
			
			resortLi();
			scrollToBottomChat();

			lastMsgDate = senderObj._msg_date;
        }
    }
}


function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
}

function resortLi()
{
    $("#message_ul .message_entry").sort(sort_li) // sort elements
            .appendTo('#message_ul'); // append again to the list

    //remove duplicate
    var seen = {};
    $('p.message_entry').each(function() {
        var txt = $(this).attr("data-msg_date");
        if (seen[txt])
            $(this).remove();
        else
            seen[txt] = true;
    });
}


// sort function callback
function sort_li(a, b){
    // return ($(b).data('msg_date')) < ($(a).data('msg_date')) ? 1 : -1;    
    return (moment($(b).data("msg_date")).isBefore(moment($(a).data("msg_date")))? 1 : -1);
}



function testImage(url, callback, senderObj, timeout) {
    timeout = timeout || 5000;
    var timedOut = false, timer;
    var img = new Image();
    img.onerror = img.onabort = function() {
        if (!timedOut) {
            clearTimeout(timer);
            callback(url, "error", null, senderObj);
        }
    };
    img.onload = function() {
        if (!timedOut) {
            clearTimeout(timer);
            callback(url, "success", img, senderObj);
        }
    };
    
    img.src = url;
    timer = setTimeout(function() {
        timedOut = true;
        callback(url, "timeout", null, senderObj);
    }, timeout); 
}





function getSetProfileImage(userID)
{    

    var storage = firebase.storage();

    //var storageRef = storage.ref('images/stars.jpg');
    
    storage.ref("profileImages").child(userID + '.jpeg').getDownloadURL().then(function(url) {
        // `url` is the download URL for 'images/stars.jpg'
      
        cached_profile_pics[userID] = url;

        $('img[data-id="'+userID+'"]').attr('src', url);


      }).catch(function(error) {
        // Handle any errors
      });
}



function uploadFiles()
{
    //clear input files
    upload_input.value = "";
    refreshFileCounter();
    const cloneFilesToUpload = [...files_to_upload];
    files_to_upload = [];

    for (let index = 0; index < cloneFilesToUpload.length; index++) {
        const thisFile = cloneFilesToUpload[index];
        
        var metadata = {
            contentType: thisFile.type
        };

        if(thisFile.type.includes("video"))
        {
            uploadVideo(thisFile, metadata);            
        }
        else
        {
            //compress if image
            //we dont want to compress gif images
            if(metadata.contentType.includes("gif"))
            {
                // console.log(file.name + " is a gif file!");
                continueUploadImage(thisFile, metadata);
            }
            else
            {
                tryUploadImage(thisFile, metadata);
            }
        }

    }

    upload_input.value = "";
}

function tryUploadImage(file, metadata)
{
    const fileName = file.name;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = event => {
        const img = new Image();
        img.src = event.target.result;
        img.onload = () => {
            //alert(img.width + "x" + img.height);

            const width = Math.min(1280, img.width);
            const scaleFactor = width / img.width;
            

            const elem = document.createElement('canvas');
            elem.width = width;
            elem.height = img.height * scaleFactor;

            const ctx = elem.getContext('2d');
            // img.width and img.height will contain the original dimensions
            // ctx.drawImage(img, 0, 0, width, height);
            ctx.drawImage(img, 0, 0, width, img.height * scaleFactor);

            ctx.canvas.toBlob((blob) => {
                const newFile = new File([blob], fileName, {
                    type: metadata.contentType,
                    lastModified: Date.now()
                });

                console.log(newFile);
                continueUploadImage(newFile, metadata);
            }, metadata.contentType, imageCompressFactor);

        },
        reader.onerror = (error) => {
            skipped_files.push(file);
            showSkippedFiles();
        };
    };    
}

//***IMAGE FILES *////
function continueUploadImage(file, metadata)
{
    var fileExtension = file.name.split(".")[1];
    var newFileName = uuidv4() + "." + fileExtension;
    
    var uploadTask = storageRef.child('reports/'+ caseID + "/" + newFileName).put(file, metadata);

    upload_progress[file.name] = createProgress(file.name);


    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
        function (snapshot) {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

            updateProgressBar(file.name, progress);

            // console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                    console.log('Upload is paused');
                    break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                    console.log('Upload is running');
                    break;
            }
        }, function (error) {

            // A full list of error codes is available at
            // https://firebase.google.com/docs/storage/web/handle-errors
            switch (error.code) {
                case 'storage/unauthorized':
                    // User doesn't have permission to access the object
                    PNotify.error({
                        title: error.code,
                        text: file.name
                    });
                    break;

                case 'storage/canceled':
                    // User canceled the upload
                    PNotify.notice({
                        title: error.code,
                        text: file.name
                    });
                    break;

                case 'storage/unknown':
                    // Unknown error occurred, inspect error.serverResponse
                    PNotify.error({
                        title: error.code,
                        text: file.name
                    });
                    break;
            }
        }, function () {
            //image upload success

            uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                // var currentDate = moment().format('YYYYMMDD MMMM D hh:mm:ss A');

                var newMessage = 
                {
                    message : downloadURL,
                    date : firebase.firestore.FieldValue.serverTimestamp(),
                    is_media : true,
                    is_admin : true,
                    user : firebase.auth().currentUser.email,
                    user_id : $("#user_email").attr('data-myName'),
                    file_name : newFileName
                }

                db.collection('reports').doc(report_key).collection('messages').doc()
                .set(newMessage).then(function(){
                    $('.message-input textarea').val(null);
                });
        
                // rootRef.child("reports/" + caseID + "/thread")
                //     .push(newMessage)
                //     .then((snap) => {
                //         $('.message-input textarea').val(null);
                // });
            });

        });

}


//***VIDEO FILES *////
function uploadVideo(file, metadata)
{
    var fileExtension = file.name.split(".")[1];
    var newFileName = uuidv4() + "." + fileExtension;

    // Upload file and metadata to the object 'images/mountains.jpg'
    var uploadTask = storageRef.child('reports/'+ caseID + "/" + newFileName).put(file, metadata);

    upload_progress[file.name] = createProgress(file.name);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
        function (snapshot) {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

            updateProgressBar(file.name, progress);

            // console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                    console.log('Upload is paused');
                    break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                    console.log('Upload is running');
                    break;
            }
        }, function (error) {

            // A full list of error codes is available at
            // https://firebase.google.com/docs/storage/web/handle-errors
            switch (error.code) {
                case 'storage/unauthorized':
                    // User doesn't have permission to access the object
                    PNotify.error({
                        title: error.code,
                        text: file.name
                    });
                    break;

                case 'storage/canceled':
                    // User canceled the upload
                    PNotify.notice({
                        title: error.code,
                        text: file.name
                    });
                    break;

                case 'storage/unknown':
                    // Unknown error occurred, inspect error.serverResponse
                    PNotify.error({
                        title: error.code,
                        text: file.name
                    });
                    break;
            }
        }, function () {
            // Upload completed successfully, now we can get the download URL
            // uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
            //     console.log('File available at', downloadURL);
            // });

            //create and upload video thumbnail\
            var thumbnail_metadata = "image/jpeg";

            // console.log(dataUrl);
            var canvas = document.createElement("canvas");
            var video = video_elements[file.name];
            var scale_factor = 0.5;
            var w = video.videoWidth * scale_factor;
            var h = video.videoHeight * scale_factor;
            canvas.width  = w;
	        canvas.height = h;
            console.log(w + "x" + h);
            canvas.getContext('2d').drawImage(video, 0, 0, w, h);

            var dataUrl = canvas.toDataURL(thumbnail_metadata, 0.8);

            console.log(dataUrl);
            var thumbnailFilename = newFileName.split(".")[0] + ".jpg";

            storageRef.child('video_thumbnails/'+ caseID + "/" + thumbnailFilename)
            .putString(dataUrl, 'data_url').then(function(snapshot) {
                console.log('Uploaded a base64 thumbnail!');
            });


            

            uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                var currentDate = moment().format('YYYYMMDD MMMM D hh:mm:ss A');

                var newMessage = 
                {
                    message : downloadURL,
                    date : currentDate,
                    is_media : true,
                    user : username,
                    user_id : "admin",
                    is_admin : true,
                    file_name : newFileName
                }
        
                rootRef.child("reports/" + caseID+"/thread")
                    .push(newMessage)
                    .then((snap) => {
                        $('.message-input textarea').val(null);
                });
            });

        });
}


function updateProgressBar(name, curValue)
{
    if (curValue >= 100) {
        // Clean up the interval.
        // window.clearInterval(timer);
        upload_progress[name].close();
        PNotify.success({
            title: 'File uploaded!',
            text: name
        });
        delete upload_progress[name];

        return;
    }

    var progress = upload_progress[name].refs.elem.querySelector('.progress-bar');

    progress.style.width = (curValue + '%');
    progress.attributes['aria-valuenow'].value = curValue;
}

function createProgress(p_title) {
    
    if (typeof window.stackContextModeless === 'undefined') {
        window.stackContextModeless = new PNotify.Stack({
            dir1: 'down',
            dir2: 'left',
            firstpos1: 25,
            firstpos2: 25,
            context: $("body").get(0),
            modal: false,
            maxOpen: Infinity
        });
      }

    // Make a loader.
    var notice = PNotify.notice({
        title: 'Uploading...',
        text: '<p style="overflow:scroll;">' + p_title + '</p>'+
            '<div class="progress">' +
            '  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>' +
            '</div>',
        textTrusted: true,
        icon: 'fas fa-spinner fa-pulse',
        hide: false,
        destroy: true,
        closer: false,
        sticker: false,
        stack: window.stackContextModeless
    });

    return notice;
}



function validateVideoFile(file) {

    try
    {
        var video = document.createElement('video');

        video.onerror = function(){

            skipped_files.push(file);
            let str = file.name + '<br> Cannot upload media. No decoders for requested formats: ' + file.type;
            showStackBottomRight(str);
        };

        video.preload = 'metadata';
        video.crossOrigin = "anonymous";

        video.onloadedmetadata = function() {

            window.URL.revokeObjectURL(video.src);
            console.log("this video file size : " + file.size);

            if (video.duration < 1) {

                console.log("Invalid Video! video duration is less than 1 second");
                skipped_files.push(file);
                showSkippedFiles();
                return;
            }
            else if(video.duration > maxVideoDurationLimit)
            {
                console.log("Invalid Video! video duration exceeded maximum duration limit : " + video.duration);
                skipped_files.push(file);
                showSkippedFiles();
                return;
            }
            // else if(file.size > maxFileSize)
            // {
            //     console.log("Invalid Video! video file size exceeded maximum file size limit : " + file.size + " bytes");
            //     return;
            // }

            video_elements[file.name] = video;
            files_to_upload.push(file);
            refreshFileCounter();
            //methodToCallIfValid();
            //return true;
        }

        video.src = URL.createObjectURL(file);
    }
    catch(error)
    {
        console.error(error.message);
    }
}

function clear_upload_files()
{
    upload_input.value = "";
    files_to_upload = [];
    skipped_files = [];
    video_elements = [];
    upload_progress = [];
    refreshFileCounter();
}

function refreshFileCounter()
{
    if(upload_input.value.length > 0)
    {
        // $("#total_files_label").get(0).innerHTML = files_to_upload.length + " file" + ((files_to_upload.length > 1)?"s" : "") + '<br><i class="fa fa-paperclip attachment" aria-hidden="true">';
        $("#file_count_span").get(0).innerHTML = files_to_upload.length + ' file' + ((files_to_upload.length > 1)?"s" : "") + ' ready to be send...' + '<button type="button" onclick="clear_upload_files();"><i class="fas fa-times-circle"></i></button>';
        $("#file_count_span").show();
    }
    else
    {
        $("#file_count_span").hide();
        // $("#total_files_label").get(0).innerHTML = '<br><i class="fa fa-paperclip attachment" aria-hidden="true">';
    }
}

function showSkippedFiles()
{
    console.log("trying to show skipped files");
    if(skipped_files.length > 0)
    {
        //destroy existing alert
        if($(".ui-pnotify ").length > 0){
            $(".ui-pnotify ").remove();
            console.log("removed existing alert!");
        }

        var str = '';
        for (let index = 0; index < skipped_files.length; index++) {
            const element = skipped_files[index];
            
            str += '<p style="overflow:scroll;">' + element.name + ' (' + humanFileSize(element.size, true, 0) + ')</p> <br>';
            if(index == skipped_files.length-1)
            {
                str += "<br><em><i>Files are invalid due to file size limit("+humanFileSize(maxFileSize, true, 0)+"), video duration limit or invalid file type. </i></em>";

                showStackBottomRight(str);

                // notify('Some files will not be uploaded :', str, 'warning');
            }
        }
    }
}

function showStackBottomRight(str) {
    if (typeof window.stackBottomRight === 'undefined') {
        window.stackBottomRight = new PNotify.Stack({
            dir1: 'up',
            dir2: 'left',
            firstpos1: 25,
            firstpos2: 25
        });
    }

    const opts = {
        stack: window.stackBottomRight,
        textTrusted : true,
        delay: 10000
    };

    opts.title = 'Some files will not be uploaded';
    opts.text = str;
    opts.type = 'notice';

    PNotify.alert(opts);
}



function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}


function humanFileSize(bytes, si=false, dp=1) {
  const thresh = si ? 1000 : 1024;

  if (Math.abs(bytes) < thresh) {
    return bytes + ' B';
  }

  const units = si 
    ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] 
    : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
  let u = -1;
  const r = 10**dp;

  do {
    bytes /= thresh;
    ++u;
  } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);


  return bytes.toFixed(dp) + ' ' + units[u];
}


function asyncFunction (item, cb) {
  setTimeout(() => {

    cb();
}, 100);
}