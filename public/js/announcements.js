(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];
addUserCallback();
// var initial_role;
// var myRole;
function addUserCallback()
{
    let email = $("#user_email").text();

    let query = db.collection('users').where('email', '==', email);

    query.onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {


            if (change.type === "added") {
                let role = change.doc.data().role;
                initial_role = role;
                setRole(role);
                if(role == 2 || role == 1)
                {
                    $("#table_tr th").last().attr('id', 'actions_th');//append('<th id="actions_th" class="role_admin">Actions</th>');
                }
            }

            if (change.type === "modified") {
                setRole(change.doc.data().role);
                $("body").empty();
                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                 setTimeout(() => {
                    location.replace("index.html");
                }, 3000);
            }

            if (change.type === "removed") {
                // console.log("Removed user: ", change.doc.data());
                // window.location = "index.html";
            }
        });
    },
        function(error){
        console.log('retry add user callback');
        addUserCallback();
    });
}

function setRole(newRole)
{
    switch(newRole)
    {
        case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
        case 1 : $(".role_admin").remove();break;
    }
    
    $("#user_role").text("Role: " + roles_string[newRole]);
    $("#user_role").attr("data-done", true);
}

})();


var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

'user strict'
// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);

function Start()
{
    sessionStorage.setItem("toEdit", '');
    sessionStorage.setItem('reportImageHeader', '');
    initTable();
    // addDBListener();
}

$(document).ready( function () {
    observer.observe(targetNode, config);
//    initButtonCallbacks();
});



var announcements_table;
var storedContent = {};
var cp;
var edit_dialog;

function initTable()
{
    // let myColumns =
    // [
    //     {"sClass": "col_section" },
    //     {"sClass": "long_columns col_content" },
    //     {"sClass": "actions_buttons" }           
    // ]

    let btns = [];
    if(document.getElementById("actions_th") != null)
    {
        console.log("has action column!");
        // myColumns.push({"sClass": "actions_buttons" });
        btns = [
            {
                text: 'Add <i class="fa fa-plus"></i>',
                action: function ( e, dt, node, config ) {
                    showPromptEdit();
                },
                "className": 'btn btn-primary'
            }
        ]
    }

    announcements_table = $('#announcements_table').DataTable({
        // "scrollX": true,
        // "autoWidth": true,
        dom: 'Bfrltip',
        "scrollX": true,
        buttons: btns,
        aoColumns: [
            {"sClass": "long_columns col_title" },
            {"sClass": "long_columns col_content" },
            {"sClass": "col_start_date" },
            {"sClass": "col_end_date" },
            {"sClass": "col_active" },
            {"sClass": "actions_buttons" }
        ],
        "columnDefs": [ {
            "targets": 5,
            "orderable": false
        }],
        initComplete: function(settings, json) {
            // alert( 'DataTables has finished its initialisation.' );
            addDBListener();
        }
    });
}

function addDBListener()
{
    showLoading(true, "Loading announcements...");
    try {
          
        db.collection("announcements")
        .onSnapshot(function(snapshot) { 
            let requests = snapshot.docChanges().map((change) => {
                
                var d = {title: "", content : "", start_date : "", end_date : "", active: true};
                if('title' in change.doc.data())
                    d['title'] = change.doc.data().title;
                
                if('content' in change.doc.data())
                    d['content'] = change.doc.data().content;
                
                if('start_date' in change.doc.data())
                    d['start_date'] = change.doc.data().start_date.toDate();
                
                if('end_date' in change.doc.data())
                    d['end_date'] = change.doc.data().end_date.toDate();
                
                if('active' in change.doc.data())
                    d['active'] = (change.doc.data().active?'on':'off');
                

                let newData = 
                [
                    d.title,
                    getTextFromHTML(d.content),
                    d.start_date.toString().split("GMT")[0],
                    d.end_date.toString().split("GMT")[0],
                    d.active
                ]

                if(document.getElementById("actions_th") != null)
                    newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickEdit(this);" class="on-default edit-row mx-2"><i class="fa fa-pencil"></i></a><a data-key="'+change.doc.id+'" href="#" onclick="showPromptDelete(this);" class="on-default remove-row mx-2"><i class="fa fa-trash-o"></i></a>');
                else
                    newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickView(this);" class="on-default edit-row mx-2"><i class="fas fa-eye"></i></a>');

                if (change.type === "added") {                    
                    

                    let newRow = announcements_table.row.add(newData).node().id = change.doc.id;

                    announcements_table.draw( false );
                    
                    storedContent[change.doc.id] = d.content;

                    let splitted_sDate = d.start_date.toString().split(" ");
                    let splitted_eDate = d.end_date.toString().split(" ");
                    
                    $("#"+change.doc.id).find(".col_start_date").text(splitted_sDate[0] + " " + splitted_sDate[1] + " " + splitted_sDate[2]);
                    $("#"+change.doc.id).find(".col_end_date").text(splitted_eDate[0] + " " + splitted_eDate[1] + " " + splitted_eDate[2]);
                }
                
                if (change.type === "modified") {
                    // console.log("Modified city: ", change.doc.data());

                    // location.reload();

                    let key = change.doc.id;

                    var tr = $('.remove-row[data-key="'+key+'"]').parent().parent();

                    announcements_table.row($("#" + key)).data(newData).draw(false);

                    let splitted_sDate = d.start_date.toString().split(" ");
                    let splitted_eDate = d.end_date.toString().split(" ");
                    
                    $("#"+change.doc.id).find(".col_start_date").text(splitted_sDate[0] + " " + splitted_sDate[1] + " " + splitted_sDate[2]);
                    $("#"+change.doc.id).find(".col_end_date").text(splitted_eDate[0] + " " + splitted_eDate[1] + " " + splitted_eDate[2]);

                    // tr.find(".col_title").text(change.doc.data().title);
            
                    // tr.find(".col_content").text(getTextFromHTML(change.doc.data().content));
                    
                    // tr.find(".col_active").text(change.doc.data().active? 'on' : 'off');

                    // let splitted_sDate = change.doc.data().start_date.toDate().toString().split(" ");
                    // let splitted_eDate = change.doc.data().end_date.toDate().toString().split(" ");

                    // tr.find(".col_start_date").text(splitted_sDate[0] + " " + splitted_sDate[1] + " " + splitted_sDate[2]);
                    
                    // tr.find(".col_end_date").text(splitted_eDate[0] + " " + splitted_eDate[1] + " " + splitted_eDate[2]);

                    
                    
                }
                if (change.type === "removed") {
                    // $('tr[data-key="'+change.doc.id+'"]').parent().parent().remove();
                    announcements_table.row('#'+change.doc.id).remove().draw();
                    
                    // announcements_table.draw();
                }
                
                return new Promise((resolve) => {
                  asyncFunction(change, resolve);
                });
            })

            Promise.all(requests).then(() => {showLoading(false);});
        });
    }
    catch(err) {
      bootbox.alert(err.message);
    }
}

function clickView(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    var section = tr.find(".col_title").text();
    var content = storedContent[key];//$(elem).parent().parent().find(".col_content").val();
    var effectiveDate = tr.find(".col_start_date").text() + "   to   " + tr.find(".col_end_date").text();
    

    bootbox.alert(
    {
        title: section,
        message : content + "<br><hr>Date Effective: <br>" + effectiveDate,
        centerVertical : true,
        scrollable : true
    });
}


// End upload preview image

function showPromptEdit(_title, _content, _sDate, _eDate, _active)
{
    sessionStorage.setItem('reportImageHeader', '');
    edit_dialog = bootbox.dialog({
        title: 'Manage Announcement',
        message: '<form id="infos" action="">Active:'+
        '<label class="switch"><input type="checkbox" id="active_checkbox" class="col_active" checked="false"><span class="slider round"></span></label><br>'+
        'Title:<input style="width:100%;" class="col_title" type="text"><br/>'+
        // '<div class="row"><div class="col-12 mx-auto">'+
        'Header image:<input type="file" id="image_file_input" accept="image/x-png,image/gif,image/jpeg"/><button class="btn btn-outline-secondary" type="button" id="reset_header_image">Reset image</button><br> <img id="announcement_image" src="images/ncst_logo_256px.png"/><br>'+
        'Content:<textarea id="summernote" style="width:100%; border-radius:5px; "></textarea><br>'+
        // '<div class="input-append date">Start Date:<input class="col_start_date" size="16" type="text" value="" readonly="true"><span class="add-on"><i class="fa fas-calender"></i></span></div><br>'+
        // '<div class="input-append date">End Date:<input class="col_end_date" size="16" type="text" value="" readonly="true"><span class="add-on"><i class="fa fas-calender"></i></span></div></form>',
        

        '<label for="sDate">Start Date:</label><div class="input-group date"><input id="sDate" class="form-control"><div class="input-group-append"><span class="input-group-text"><i class="far fa-calendar-alt"></i></span></div></div><br>'+
        '<label for="eDate">End Date:</label><div class="input-group date"><input id="eDate" class="form-control" ><div class="input-group-append"><span class="input-group-text"><i class="far fa-calendar-alt"></i></span></div></div></form>',
        // centerVertical: true,
        // className : "topmost-modal modal-dialog-centered",
        // size : 'small',
        buttons: {
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function(){
                    sessionStorage.setItem("toEdit", '');
                }
            },
            ok: {
                label: 'Save <i class="fa fa-save"></i>',
                className: 'btn-success',
                callback: function(){

                    console.log('Custom OK clicked');
                    var title = $('#infos .col_title').val();
                    if(title == undefined || title.trim().length == 0)
                    {
                        bootbox.alert({
                            message:"Title can't be blank!",
                            centerVertical: true,
                            className : "topmost-modal",
                            size : 'small'
                        });
                        
                    }

                    // var markupStr = $('#mail_body').summernote('code');
        
                    var content = $('#infos #summernote').summernote('code');
                    // $('#infos #summernote').val(content);

                    // console.log(content);
                    
                    let sDate = $('#sDate').datepicker('getDate');//datetimepicker('getDate');
                    sDate = sDate.setHours(0, 0, 0, 0);
                    // sDate = moment(sDate).format("MMMM DD YYYY HH:mm");
                    let eDate = $('#eDate').datepicker('getDate');//datetimepicker('getDate');
                    eDate = eDate.setHours(23, 59, 59);
                    // eDate = moment(eDate).format("MMMM DD YYYY HH:mm");
                    let active = $('#infos .col_active').prop('checked');


                    
                    saveAnnouncements(title, content, sDate, eDate, active);

                    return false;
                }
            }
        }
    });
    
    $("#infos .col_title").val(_title);

    $("#reset_header_image").on('click', function(){
        $("#image_file_input").get(0).value = null;
        $("#announcement_image").attr('src', "images/ncst_logo_256px.png");    
        sessionStorage.setItem('reportImageHeader', '');    
    });

    // Get header image from firebase if any
    let key = sessionStorage.getItem("toEdit");//$('#infos').attr('data-key');
    
    if(key != undefined && key.length > 0)
    {
        var listRef = firebase.storage().ref().child('announcements/' + key);

        // Find all the prefixes and items.
        listRef.listAll().then(function(res) {

          if(res.items.length > 0)
          {
              let fileRef = res.items[0];
                var dirRef = firebase.storage().ref(fileRef.fullPath);
                dirRef.getDownloadURL().then(function(url) {
                    sessionStorage.setItem('reportImageHeader', url);
                    $("#announcement_image").attr('src', url); 
                });
            }

        }).catch(function(error) {
          // Uh-oh, an error occurred!
          console.error(error.message);
        });        
    }

    


    $("#image_file_input").change(function(){
        var fr = new FileReader();

        fr.onload = function(e) {            
            $("#announcement_image").attr('src', e.target.result);
        }

        fr.readAsDataURL(this.files[0]);
    });


    // $('#infos .col_active').bootstrapToggle();
    

    edit_dialog.on('shown.bs.modal', function(e){
        $("body").addClass('modal-open');
    });

    if(_active != undefined){
        $('#active_checkbox').prop('checked', (_active == 'on'));
    }
    else{
        $('#active_checkbox').prop('checked', true);
    }

    edit_dialog.on('hidden.bs.modal', function(e){
        sessionStorage.setItem('reportImageHeader', '');
    });
    
    $('#infos #summernote').summernote({
        height: '200px',
        toolbar: defaultToolbar
    });
    
    if(_content)
    {
        $('#infos #summernote').summernote('code', _content);
    }

    
    let start_date_picker = $('#sDate');
    $('#sDate').datepicker({format: 'MM-dd-yyyy'});//.datetimepicker({autoclose:true, disableTouchKeyboard: true, dateFormat: 'MM-dd-yyyy HH:ii', forceParse:true});
    
    console.log(_sDate);

    if(_sDate){
        start_date_picker.datepicker('setDate', moment(_sDate).toDate());
    }
    else{
        start_date_picker.datepicker('setDate', new Date());
    }
    
    let end_date_picker = $('#eDate');
    $('#eDate').datepicker({format: 'MM-dd-yyyy'});//.datetimepicker({autoclose:true, disableTouchKeyboard: true, dateFormat: 'MM-dd-yyyy HH:ii', forceParse:true});
    
    if(_eDate){
        // end_date_picker.datepicker('setDate', moment(_eDate).format("YYYY-MM-DD HH:mm").toDate());
        end_date_picker.datepicker('setDate', moment(_eDate).toDate());
    }
    else{
        end_date_picker.datepicker('setDate', new Date());
    }
}

function showPromptDelete(elem)
{
    sessionStorage.setItem("toEdit", $(elem).attr('data-key'));
    
    bootbox.confirm({
        title:$(elem).parent().parent().find(".col_title").text(),
        message: "Are you sure you want to delete this Announcement?",
        size:'small',
        centerVertical: true,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            console.log('This was logged in the callback: ' + result);
            if(result)
            {
                clickRemove();
            }
            else
            {
                sessionStorage.setItem("toEdit", '');
            }
            
        }
    });
}

function saveAnnouncements(_title, _content, _sDate, _eDate, _active)
{
    try
    {
        var key = sessionStorage.getItem("toEdit");//$('#infos').attr('data-key');
        var ref = db.collection("announcements").doc();
        
        if(key == undefined || key.length == 0)
        {
            key = ref.id;
        }
        else
        {
            ref = db.collection("announcements").doc(key);
        }
        
        sessionStorage.setItem("toEdit", "");
        
        showLoading(true, "Saving...");

        let st_date = new Date(_sDate);
        let en_date = new Date(_eDate);

        ref.set({
            title: _title,
            content: _content,
            start_date: firebase.firestore.Timestamp.fromDate(st_date),
            end_date: firebase.firestore.Timestamp.fromDate(en_date),
            active : _active
        }, {merge:true})
        .then(function() {
            
            
            tryUploadHeaderImage(key, _title);
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
            PNotify.error({
              title: 'Error',
              text: error.message
            });
            showLoading(false);
        });
    }
    catch(error)
    {
        PNotify.error({
          title: 'Error',
          text: error.message
        });
        showLoading(false);
    }
}

function continueUploadHeaderImage(key, _title)
{
    let storedImageHeader = sessionStorage.getItem('reportImageHeader');

    if(storedImageHeader != null && storedImageHeader.length > 0)
    {
        // lets see if user changes the file uploaded
        if($("#announcement_image").attr('src') == storedImageHeader && $("#announcement_image").attr('src') != "images/ncst_logo_256px.png")
        {
            showNotifSuccess('Saved', _title + ' saved successfully');
            showLoading(false);
            edit_dialog.modal('hide');
            console.log('skipping replacing of stored header image because user doesn\'t change the image');
            return;
        }
        // else if($("#announcement_image").attr('src') == "images/ncst_logo_256px.png")
        // {

        // }
    }
    
    if($("#image_file_input").get(0).files.length == 0)
    {   
        showNotifSuccess('Saved', _title + ' saved successfully');
        showLoading(false);
        edit_dialog.modal('hide');
        console.log('skipping replacing of stored header image because no file selected');
        return;
    }

    let file = $("#image_file_input").get(0).files[0];
    let extension = file.type.split("/")[1];
    var uploadTask = firebase.storage().ref().child('announcements/' + key + '/header.' + extension).put(file);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
      function(snapshot) {
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log('Upload is ' + progress + '% done');
        switch (snapshot.state) {
          case firebase.storage.TaskState.PAUSED: // or 'paused'
            console.log('Upload is paused');
            break;
          case firebase.storage.TaskState.RUNNING: // or 'running'
            console.log('Upload is running');
            break;
        }
      }, function(error) {

        showNotifError('Error saving', error.message);
        edit_dialog.modal('hide');
      // A full list of error codes is available at
      // https://firebase.google.com/docs/storage/web/handle-errors
      // switch (error.code) {
      //   case 'storage/unauthorized':
      //     // User doesn't have permission to access the object
      //     break;

      //   case 'storage/canceled':
      //     // User canceled the upload
      //     break;


      //   case 'storage/unknown':
      //     // Unknown error occurred, inspect error.serverResponse
      //     break;
      // }
    }, function() {
        showNotifSuccess('Saved', _title + ' saved successfully');
        showLoading(false);
        edit_dialog.modal('hide');

          // // Upload completed successfully, now we can get the download URL
          // uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
          //   console.log('File available at', downloadURL);
          // });
    });
}

function tryUploadHeaderImage(key, _title)
{
    // Delete existing header images if any
    try
    {

        //check if we change the header image or not we dont need to delete the store on
        let storedImageHeader = sessionStorage.getItem('reportImageHeader');

        if(storedImageHeader != null && storedImageHeader.length > 0 && $("#announcement_image").attr('src') != "images/ncst_logo_256px.png")
        {
            console.log('skipping deletion of stored header image');
            continueUploadHeaderImage(key, _title);
        }
        else
        {

            let ref = firebase.storage().ref('announcements/' + key);
                ref.listAll().then(dir => {
                  dir.items.forEach(fileRef => {

                    var dirRef = firebase.storage().ref(fileRef.fullPath);
                    dirRef.getDownloadURL().then(function(url) {
                      var imgRef = firebase.storage().refFromURL(url);
                      imgRef.delete().then(function() {
                        // File deleted successfully 
                        console.log("header image deleted successfully : " + _title);
                        continueUploadHeaderImage(key, _title);
                      }).catch(function(error) {
                        // There has been an error      
                        continueUploadHeaderImage(key, _title);
                      });
                    });
                  });

                  if(dir.items.length == 0)
                  {
                    console.log('empty header images');
                    continueUploadHeaderImage(key, _title);
                  }
            }).catch(error => {
                console.log(error);
                continueUploadHeaderImage(key, _title);
            });

        }
    }
    catch(error)
    {
        console.error(error.message);
        setTimeout(() => {
            console.log('retrying tryUploadHeaderImage');
            tryUploadHeaderImage(key, _title);
        }, 3000);
    }
}

function clickEdit(elem)
{
    var title = $(elem).parent().parent().find(".col_title");
    var content = $(elem).parent().parent().find(".col_content");
    
    var sDate = announcements_table.row($(elem).parent().parent()).data()[2];//$(elem).parent().parent().find(".col_start_date");
    var eDate = announcements_table.row($(elem).parent().parent()).data()[3];//$(elem).parent().parent().find(".col_end_date");
    var active = $(elem).parent().parent().find(".col_active");
    
    var key = $(elem).attr('data-key');

    sessionStorage.setItem("toEdit", $(elem).attr('data-key'));
    
    // showPromptEdit(title.text(), storedContent[key], sDate.get(0).textContent, eDate.get(0).textContent, active.text());
    showPromptEdit(title.text(), storedContent[key], sDate, eDate, active.text());
    
    
}

function clickRemove()
{
//    var key = $(elem).attr('data-key');    
    var key = sessionStorage.getItem("toEdit");
    
    if(key.trim().length > 0)
    {
        showLoading(true, "Deleting...");
        db.collection("announcements").doc(key).delete().then(function() {
            console.log("Document successfully deleted!");
            PNotify.info({
              title: 'Deleted',
              text: 'Announcenment deleted successfully'
            });
            showLoading(false);
            
            // announcements_table.row('tr[data-key="'+key+'"]').remove().draw();
            announcements_table.row('#'+key).remove().draw();
            // announcements_table.draw();

            sessionStorage.setItem('toEdit', '');
            // $('.dataTables_scrollHeadInner, table').css('width', '100%');
            
        }).catch(function(error) {
            console.error("Error removing document: ", error);
            PNotify.error({
              title: 'Error',
              text: error
            });
            showLoading(false);
            sessionStorage.setItem('toEdit', '');
        });
    }
}


function asyncFunction (item, cb) {
  setTimeout(() => {
      
    cb();
  }, 100);
}