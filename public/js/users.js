(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];
addUserCallback();
// var initial_role;
// var myRole;
function addUserCallback()
{
    let email = $("#user_email").text();

    let query = db.collection('users').where('email', '==', email);

    query.onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {


            if (change.type === "added") {
                let role = change.doc.data().role;
                initial_role = role;
                setRole(role);
                if(role == 2)
                {
                    $("#users_table_tr").append('<th id="actions_th" class="role_admin">Actions</th>');
                }
            }

            if (change.type === "modified") {
                setRole(change.doc.data().role);
                $("body").empty();
                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                 setTimeout(() => {
                    location.replace("index.html");
                }, 3000);
            }

            if (change.type === "removed") {
                // console.log("Removed user: ", change.doc.data());
                // window.location = "index.html";
            }
        });
    },
        function(error){
        console.log('retry add user callback');
        addUserCallback();
    });
}

function setRole(newRole)
{
    switch(newRole)
    {
        case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
        case 1 : $(".role_admin").remove();break;
    }
    
    $("#user_role").text("Role: " + roles_string[newRole]);
    $("#user_role").attr("data-done", true);
}

})();





'user strict'


var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);

observer.observe(targetNode, config);

function Start()
{
    initTable();
    addButtonsCallback();
}



$(document).ready(function () 
{   
    // Start observing the target node for configured mutations
    

    sessionStorage.setItem("toEdit", '');
});

var users_table;
var roles_string = ['Viewer', 'Editor', 'Admin'];
var before_edit_email;

// var rootRef;
// var user_to_proccess;
// var orig_email;
var myConfig = {
    apiKey: "AIzaSyAACwDUbQbp_loXHhA8ykF0YNPT8XnaLYY",
    authDomain: "ncst-osa.firebaseapp.com",
    databaseURL: "https://ncst-osa.firebaseio.com",
    projectId: "ncst-osa",
    storageBucket: "ncst-osa.appspot.com",
    messagingSenderId: "18001671248",
    appId: "1:18001671248:web:8d9e8230c7afa6890f8d55"
};

var secondaryApp = firebase.initializeApp(myConfig, "Secondary");

function addButtonsCallback(){
    $('#editUserModal').modal(
    {
        keyboard: false,
        backdrop: 'static',
        show:false
    });

    // document.getElementById("role_select").addEventListener("select", updateRoleHint;

    $("#addToTable").on('click', function(){
        sessionStorage.setItem('toEdit', '');
        $("#editUserForm").get(0).reset();
    });

    $("#editUserForm").keypress(function(event){

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
          if($("#editUserForm").get(0).reportValidity())
            goSubmit(event);
          //alert('You pressed a "enter" key in somewhere');
        }
    });

    $("#submitButton").on('click', function(e){
        if($("#editUserForm").get(0).reportValidity())
          goSubmit(e);
      });

    $("#editUserModal").on('hidden.bs.modal', function(m){
        sessionStorage.setItem('toEdit', '');
        before_edit_email = "";
    });

    $("#editUserModal").on('shown.bs.modal', function(e){
        updateRoleHint();
        var key = sessionStorage.getItem('toEdit');
        var isNew = (key == undefined || key.length == 0);
        if(isNew)
        {
            $("#newUserHelp").show();
        }
        else
        {
            $("#newUserHelp").hide();
        }
    });

    addPastePrevent($("form input"));
}

function updateRoleHint()
{
    console.log("changed");
    let txt = '';
    switch($("#role_select").val())
    {
        case "0" :  txt = 'Viewer are not allowed to edit datas, it is limited for reading datas only'; break;
        case "1" :  txt = 'Editor can read/write datas but it is not allowed to change/add other users'; break;
        case "2" :  txt = 'Admin can add/change/remove users and read/write all datas and can change permissions'; break;
    }

    $("#role_select").next('small').text(txt);
}

function initTable()
{
    // Later, you can stop observing
    observer.disconnect();

    let myColumns =
    [
        { sClass: 'name' },
        { sClass: 'email' },
        { sClass: 'role center' },
        { sClass: 'status center' }        
    ]

    if(document.getElementById("actions_th") != null)
    {
        console.log("has action column!");
        myColumns.push({
            bSortable: false,
            sClass: 'actions_buttons center'
        });
    }

    users_table = $('#users_table').DataTable(
        {
        dom: 'flrtip',
        scrollX: true,
        aoColumns: myColumns,
        "initComplete": function(settings, json) {
            // alert( 'DataTables has finished its initialisation.' );
            addDBListener();
          }
    }
    );

    // addDBListener();
}



function addDBListener()
{
    showLoading(true, "Loading users...");
    try {
          
        db.collection("users")
        .onSnapshot(function(snapshot) { 
            let requests = snapshot.docChanges().map((change) => {
                var key = change.doc.id;


                if (change.type === "added") {
                    
                    var d = {name : "", email: "", role: "", status: ""};
                    if('name' in change.doc.data())
                        d['name'] = change.doc.data().name;

                    if('email' in change.doc.data())
                        d['email'] = change.doc.data().email;
                    
                    if('role' in change.doc.data())
                        d['role'] = change.doc.data().role;

                    if('status' in change.doc.data())
                        d['status'] = change.doc.data().status;

                    let theRoleDefault = 0;
                    if(!isNaN(d.role))
                        theRoleDefault = parseInt(d.role);

                    let newData = 
                    [
                        d.name,
                        d.email,                        
                        roles_string[theRoleDefault],
                        d.status                        
                    ]

                    if(document.getElementById("actions_th") != null){
                        newData.push(getNewActionRow(change.doc.id));
                    }

                    let newRow = users_table.row.add(newData).node().id = change.doc.id;

                    users_table.draw( false );
                }
                
                if (change.type === "modified") {
                    // console.log("Modified city: ", change.doc.data());  
                    let tr = $('.remove-row[data-key="'+key+'"]').parent().parent();

                    let data = change.doc.data();

                    let theRoleDefault = 0;
                    if(!isNaN(data.role))
                        theRoleDefault = parseInt(data.role);

                    let updatedData = [
                        data.name,                            
                        data.email,
                        roles_string[theRoleDefault],
                        data.status                        
                    ];

                    if(document.getElementById("actions_th") != null)
                        updatedData.push(getNewActionRow(key));

                    users_table.row(tr).data(updatedData).draw();                 
                    
                }

                if (change.type === "removed") {
                    let k = change.doc.id;
                    // users_table.row($('tr[data-key="'+k+'"]')).remove().draw();
                    users_table.row($('#'+k)).remove().draw();
                    console.log(k +" has been removed");

                    if(change.doc.data().email == firebase.auth().currentUser.email)
                    {
                        $("body").empty();
                        $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                         setTimeout(() => {
                            location.replace("index.html");
                        }, 3000);
                    }
                    // $('#'+change.doc.id).find(".status").text('deleted');
                }
                
                return new Promise((resolve) => {
                  asyncFunction(change, resolve);
                });
            })

            Promise.all(requests).then(() => {showLoading(false);});
        },
        function(error){

            bootbox.alert({
                centerVertical:true,
                size:'small',
                message:error.message
            }
                );
        });
    }
    catch(err) {
        bootbox.alert(
        {
            message:err.message,
            onHidden: function () {
                UpdateModalScrolling();
            }
        });
    }
}

function getNewActionRow(key)
{
    return  '<a href="#" data-key="'+key+'"  onclick="showPromptEdit(this)" class="on-default edit-row mx-2" ><i class="fa fa-pencil"></i></a>'+
            '<a href="#" data-key="'+key+'"  onclick="showPromptDelete(this)" class="on-default remove-row mx-2" style="color:red;"><i class="fa fa-trash-o"></i></a>';
}

function asyncFunction (item, cb) {
  setTimeout(() => {

    cb();
}, 100);
}

function showPromptEdit(elem)
{
    var key = $(elem).attr('data-key');
    sessionStorage.setItem("toEdit", key);


    var tr = $(elem).parent().parent();

    var myRow = users_table.row(tr);

    // Fill form based on selected student data.    
    $("#field_name").val(myRow.data()[0]);

    // $("#email").attr("readonly", true);
    let e = myRow.data()[1];
    before_edit_email = e;
    $("#email").val(e);

    // console.log(myRow.data()[2]);
    $("#role_select").val(roles_string.indexOf(myRow.data()[2]));
    $("#status").val(myRow.data()[3]);


    $("#editUserModal").modal('show');
}

function showPromptDelete(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    let email = users_table.row(tr).data()[1];
    let role = users_table.row(tr).data()[2];
    // sessionStorage.setItem("toEdit", key);
    console.log(email);

    // Change confirm text if you're deleting yourself
    var confirmMessage = "This will permanently delete a user and can't be undone.<br>Continue?<br><b>"+ email +"</b>";

    if(firebase.auth().currentUser.email == email)
        confirmMessage = "You are deleting your own account, this can't be undone.<br>Continue?<br><b>"+ email +"</b>";

    bootbox.confirm({ 
        size: "large",
        centerVertical : true,
        title: "Are you sure?",
        message : confirmMessage,
        callback: function(result){ 
            if(result)
            {
                showLoading(true, "Deleting...");
                try
                {
                    // Prevent deletion if this is the last admin.
                    let adminCountQuery = db.collection('users').where('role', '==', 2);

                    adminCountQuery.get().then(function(adminQuerySnapshot) {

                        if(adminQuerySnapshot.size > 1 || role != "Admin")
                        {
                            let query = db.collection('users').where('email', '==', email);

                            // Get password from database.
                            query.get().then(function(querySnapshot) {
                                // console.log(querySnapshot.docs[0].data());

                                if(querySnapshot.size > 0)
                                {
                                    let pass = querySnapshot.docs[0].data().password;


                                    secondaryApp.auth().signInWithEmailAndPassword(email, pass)
                                    .then(function(result) {
                                        
                                        var user = secondaryApp.auth().currentUser;

                                        user.delete().then(function() {
                                            // User deleted.
                                            // secondaryApp.auth().signOut();

                                            db.collection("users").doc(key).delete().then(function() {
                                                PNotify.alert({
                                                    title:'Success',
                                                    text : 'User ' + email + " deleted successfully"
                                            });




                                                // console.log("Document successfully deleted!");
                                            }).catch(function(error) {
                                                // console.error("Error removing document: ", error);
                                                bootbox.alert(error);
                                                showLoading(false);
                                            });

                                        }).catch(function(error) {
                                          // An error happened.
                                            bootbox.alert(error.message);
                                            showLoading(false);
                                        });

                                        
                                    })
                                    .catch(error => {
                                        // console.error("Error adding document: ", error);
                                        bootbox.alert(error);
                                        showLoading(false);

                                    });

                                    

                                }
                                else
                                {
                                    showLoading(false);
                                    bootbox.alert('user doesn\'t exists!');
                                }
                            })
                            .catch(error => {
                                // console.error("Error adding document: ", error);
                                bootbox.alert(error.message);
                                showLoading(false);

                            });

                        }
                        else
                        {
                            showLoading(false);
                            bootbox.alert({message:'Not allowed to delete the last admin!', centerVertical:true, size:'small'});
                        }

                    });
                }
                catch(error)
                {
                    showLoading(false);
                    bootbox.alert(error.message);
                }
            }

            sessionStorage.setItem("toEdit", '');
        }
    });
}




function goSubmit(e)
{
    showLoading(true, "Saving...");
    var key = sessionStorage.getItem('toEdit');
    var isNew = (key == undefined || key.length == 0);    
    // Check if ID or Email already exist.
    let email_query = db.collection('users').where('email', '==', $("#email").val().toLowerCase());

    // Toggle new user help
    

    try
    {
        email_query.get().then(function(querySnapshot) {
            // console.log(querySnapshot.docs[0].data());

            if(querySnapshot.size > 0 && (isNew || querySnapshot.docs[0].id != key))
            {
                showLoading(false);
                bootbox.alert({
                    message:"Email already in use!",
                    centerVertical: true,
                    className : "topmost-modal",
                    size : 'small'
                });
                sessionStorage.setItem("toEdit", '');

                return;
            }
            else
            // Form is valid, continue saving
            {

                // Prevent changing of role if this is the last admin.
                let adminCountQuery = db.collection('users').where('role', '==', 2);

                adminCountQuery.get().then(function(adminQuerySnapshot) {

                    if(adminQuerySnapshot.size > 1 || isNew == true || (isNew == false && adminQuerySnapshot.docs[0].id != key) || (adminQuerySnapshot.docs[0].id == key && adminQuerySnapshot.size == 1 && roles_string[parseInt($("#role_select").val())] == "Admin" && $("#status").val() == "Active"))
                    {
                        // We need to check if any admin is already using this email

                        let email2_query = db.collection('students').where('email', '==', $("#email").val().toLowerCase());

                        email2_query.get().then(function(querySnapshot) {

                            if(querySnapshot.size > 0 && (isNew || querySnapshot.docs[0].id != key))
                            {
                                showLoading(false);
                                bootbox.alert({
                                    message:"Email already in use!",
                                    centerVertical: true,
                                    className : "topmost-modal",
                                    size : 'small'
                                });
                                sessionStorage.setItem("toEdit", '');

                                return;
                            }
                            else
                            {
                                var ref = db.collection("users").doc();
            
                                if(key == undefined || key.length == 0)
                                {
                                    key = ref.id;
                                }
                                else
                                {
                                    ref = db.collection("users").doc(key);
                                }
                                
                                sessionStorage.setItem("toEdit", "");

                                let data = 
                                {
                                    name: $("#field_name").val(), 
                                    email: $("#email").val().toLocaleLowerCase(), 
                                    role : parseInt($("#role_select").val()),
                                    status : $("#status").val()
                                };

                                if(isNew)
                                {
                                    secondaryApp.auth().createUserWithEmailAndPassword(data.email, data.email)
                                    .then(function(result) {
                                        
                                        secondaryApp.auth().sendPasswordResetEmail(data.email).then(function() {
                                            PNotify.alert({
                                                title:'New User added successfully.',
                                                text : 'Password reset sent to ' + data.email
                                            });

                                            secondaryApp.auth().signOut();

                                            data['password'] = data.email;
                                            setDataOnDBFromForm(data, ref);
                                        })
                                        .catch(function(error){
                                            PNotify.error({
                                              title: 'Error',
                                              text: error
                                        });
                                            showLoading(false);
                                        });
                                    })
                                    .catch(error => {
                                        // console.error("Error adding document: ", error);
                                        bootbox.alert(error);
                                        showLoading(false);

                                    });
                                }
                                else
                                {
                                    // We should delete old email if a user changed a password
                                    if(before_edit_email != data.email)
                                    {
                                        let query = db.collection('users').where('email', '==', before_edit_email);

                                        // Get password from database.
                                        query.get().then(function(querySnapshot) {
                                            // console.log(querySnapshot.docs[0].data());

                                            if(querySnapshot.size > 0)
                                            {
                                                let pass = querySnapshot.docs[0].data().password;


                                                secondaryApp.auth().signInWithEmailAndPassword(before_edit_email, pass)
                                                .then(function(result) {

                                                    var user = secondaryApp.auth().currentUser;

                                                    user.updateEmail(data.email).then(function() {
                                                        
                                                        // Update successful.
                                                        ref.set(data, {merge:true})
                                                        .then(function(docRef) {
                                                            showNotifSuccess('Saved', capitalizeFLetter(data.name) + ' saved successfully');
                                                            // PNotify.notice({
                                                            //   title: 'Saved',
                                                            //   text: capitalizeFLetter(data.name) + ' saved successfully',
                                                            //   icon: 'fas fa-check-circle'
                                                            // });

                                                            showLoading(false);

                                                            // sessionStorage.setItem("toEdit", '');

                                                            $('#editUserModal').modal('hide');

                                                            //log out if email changed SELF
                                                            if(before_edit_email == firebase.auth().currentUser.email)
                                                            {
                                                                $("body").empty();
                                                                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                                                                 setTimeout(() => {
                                                                    location.replace("index.html");
                                                                }, 3000);
                                                             }
                                                        })
                                                        .catch(function(error) {
                                                            console.error("Error adding document: ", error);
                                                            PNotify.error({
                                                              title: 'Error',
                                                              text: error
                                                          });
                                                            showLoading(false);
                                                        });
                                                        
                                                    }).catch(function(error) {
                                                      // An error happened.
                                                        showLoading(false);
                                                        bootbox.alert(error.message);
                                                        console.log(error.message);

                                                    });

                                                    

                                                                                                        
                                                })
                                                .catch(error => {
                                                    // console.error("Error adding document: ", error);
                                                    showLoading(false);
                                                    bootbox.alert(error.message);
                                                    

                                                });

                                                

                                            }
                                            else
                                            {
                                                showLoading(false);
                                                bootbox.alert('user doesn\'t exists!');
                                            }
                                        })
                                        .catch(error => {
                                            // console.error("Error adding document: ", error);
                                            bootbox.alert(error.message);
                                            showLoading(false);

                                        });
                                    }
                                    else
                                    {
                                        setDataOnDBFromForm(data, ref);
                                    }
                                }
                                
                            }
                        });
                    }
                    else
                    {
                        showLoading(false);
                        bootbox.alert({message:'Not allowed to change the role/status of one remaining admin!', centerVertical:true, size:'small'});
                    }
                });


            }

        });
    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(error.message);        
    }
}

function setDataOnDBFromForm(data, ref)
{
    ref.set(data, {merge:true})
    .then(function(docRef) {
        showNotifSuccess('Saved', capitalizeFLetter(data.name) + ' saved successfully');

        showLoading(false);
        // PNotify.notice({
        //   title: 'Saved',
        //   text: capitalizeFLetter(data.name) + ' saved successfully',
        //   icon: 'fas fa-check-circle',
        //   styling: 'custom_success'
        // });

        // sessionStorage.setItem("toEdit", '');

        $('#editUserModal').modal('hide');

    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
        PNotify.error({
          title: 'Error',
          text: error
      });
        showLoading(false);
    });
}






// function setGenericDialogMessage(title, msg, btnTxt)
// {
//     $('#dialogConfirmButton').off('click');
//     $('#genericDialogTitle').text(title);
//     $('#genericDialogMessage').text(msg);
//     $('#dialogConfirmButton').text(btnTxt);
// }

// function deleteUser(me)
// {
//     var evID = me.closest('tr');
//     user_to_proccess = evID.id.split("row_")[1];


//     $.magnificPopup.open({
//         items: {
//             src: '#dialog',
//             type: 'inline'
//         },
//         preloader: false,
//         modal: true
//     });
// }

// function updateUser()
// {
//     // console.log("trying to save : " + user_id);
//     var editedNewEmail = "";
//     if($('#email').val().length > 0)
//         editedNewEmail = $('#email').val();

//     //check if first name field is not blank
//     if($("#first_name").val().length == 0){
//         $("#first_name_hint").text("First name is required!");
//         showLoading(false);
//         return;
//     }

//     //check if first name field is not blank
//     if($("#last_name").val().length == 0){
//         $("#last_name_hint").text("Last name is required!");
//         showLoading(false);
//         return;
//     }    
    
//     //check if password is valid
//     if(validatePassword($('#password').val()) < 4)
//     {
//         showLoading(false);
//         return;
//     }

//     //check if role is selected
//     if($("#role").val() == "Select account role"){
//         $("#role_hint").text("Please select account role");
//         showLoading(false);
//         return;
//     }

//     //check if status is selected
//     if($("#status").val() == "Select account status"){
//         $("#status_hint").text("Please select account status");
//         showLoading(false);
//         return;
//     }

//     //check if email is valid
//     if(ValidateEmail(editedNewEmail) == false || editedNewEmail.length == 0)
//     {        
//         $("#email_hint").text("The edited email has invalid format!");
//         showLoading(false);
//         return;
//     }
//     else
//     {
//         $("#email_hint").text();
//     }
    

//     rootRef.child("students").orderByChild('email')
//     .equalTo(editedNewEmail)
//     .once('value')
//     .then(function (snapshot) {
//         var value = snapshot.val();
//         if (value) {
//             console.log(value);
//             var modifiedID = Object.keys(value)[0];
//             console.log(user_to_proccess + " == " + modifiedID);
//             if(user_to_proccess != modifiedID){
//                 console.log("The edited email is invalid or already in use!");
//                 $("#email_hint").text("The edited email is invalid or already in use!");
//                 showLoading(false);
//                 return;
//             }
//             else{
//                 continueRegister(user_to_proccess);
//             }
//         }
//         else 
//         {
//             console.log("email available");
//             if(user_to_proccess.length == 0)
//             {
//                 //var pass = $("#student_id").val();
//                 firebase.auth().createUserWithEmailAndPassword(editedNewEmail, 'admin-OSA-2020')
//                 .then(function(result) {
//                 	result.user.updateProfile({	  
//                 	    displayName: $('#first_name').text() + " " + $('#last_name').text()
//                   })
//                   .then(function(){
//                         continueRegister("");  
//                   });
                    
//                 })
//                 .catch(error => {
                    
//                 	switch (error.code) {
//                 		case 'auth/email-already-in-use':
//                             if(orig_email != editedNewEmail){
//                                 console.log("The edited email is invalid or already in use!");
//                                 $("#email_hint").text("The edited email is invalid or already in use!");
//                                 showLoading(false);
//                                 return;
//                             }
//                         // else{
//                         //     continueRegister(user_to_proccess);
//                         // }
            
//                 		break;
//                 		case 'auth/invalid-email':
//                 		// console.log(`Email address ${this.state.email} is invalid.`);
//                 		ShowMessage('Warning!', 'Email : ' + email + ' is invalid', 'warning');
//                         showLoading(false);
//                 		break;
//                 		case 'auth/operation-not-allowed':
//                 		// console.log(`Error during sign up.`);
//                 		ShowMessage('Warning!', 'Error during sign up', 'warning');
//                         showLoading(false);
//                 		break;
//                 		case 'auth/weak-password':
//                 		// console.log('Password is not strong enough. Add additional characters including special characters and numbers.');
//                 		ShowMessage('Warning!', 'Password is not strong enough. Add additional characters including special characters and numbers.', 'warning');
//                         showLoading(false);
//                 		break;
//                 		default:
//                 		// console.log(error.message);
//                 		ShowMessage('Error last', error.message, 'danger');
//                         showLoading(false);
//                 		break;
//                 	}
                    
//                     showLoading(false);
//                 	console.log('error msg = ' + error.code);
//                 });
//             }
//             else{
//                 console.log(orig_email + " == " + editedNewEmail);
//                 if(orig_email != editedNewEmail)
//                 {
//                     //update email on firebase auth
//                     console.log("You changed the email!");
                    
//                     rootRef.child("users/" + user_to_proccess).once('value').then(function (snap){
//                         if(snap.exists()){
//                             firebase.auth()
//                             .signInWithEmailAndPassword(orig_email, snap.child("password").val())
//                             .then(function(userCredential) {
//                                 userCredential.user.updateEmail(editedNewEmail)
//                                 console.log("Log in successfull");
//                             })
//                             .catch(error => {
//                                 console.log(error.code);
//                             });
//                         }
//                     });
                    
//                 }
                
//                 continueRegister(user_to_proccess);
//             }
//         }
//     });
    
// }

// function continueRegister(user_id)
// {
//     var editedNewEmail = $('#email').val();
//     //check if id already exist
//     rootRef.child('users/' + user_id).once('value').then(function(snap){
//         console.log(snap.exists());
//         //allow same id OR new id does not conflict with existing id 
//         // var theStatus = "<span class='label label-success'>Active</span>";

//         // if(user_id.length > 0)
//         //     theStatus = $('#row_'+user_id + ' > .status').html();
//         //OR id remains unedited AND conflict is  OR student is new
//         // if(snap.exists() == false || user_id.length == 0)
//         // {
//             var selectedStatus = $("#status").val();

//             if($("#status").val() == "Verified")
//                 selectedStatus = "Active";

//             var theStatus = "<span class='label label-success'>Active</span>";
//             if(selectedStatus == "Deleted")
//                 theStatus = "<span class='label label-danger'>Deleted</span>";
//             if(selectedStatus == "Suspended")
//                 theStatus = "<span class='label label-warning'>Suspended</span>";

//             //remove old node in database if edited with new student ID
//             var postData = {
//                 email: $('#email').val(),
//                 first_name: $('#first_name').val(),
//                 last_name: $('#last_name').val(),
//                 role: $("#role").val(),
//                 status : $("#status").val(),
//                 password: $('#password').val()
//             };

//             var new_actions_row = generateNewActions();
//             //Update table row only
//             if(user_id.length > 0)
//             {
//                 var row = $("#row_" + user_id);
//                 var temp = users_table.row(row).data();
//                 temp[0] = postData.first_name + " " + postData.last_name;
//                 temp[1] = editedNewEmail;
//                 temp[2] = '<span class="label label-primary">'+postData.role+'</span>';
//                 temp[3] = theStatus;
//                 temp[4] = new_actions_row;
//                 temp[5] = postData.password;
//                 temp[6] = user_id;

//                 users_table.row(row).data(temp).draw();

//                 //Write the new post's data simultaneously in the posts list and the user's post list.
//                 var updates = {};
//                 updates['users/' + user_id] = postData;

//                 rootRef.update(updates);

//                 showLoading(false);
//             }
//             else//if this is new user, add new row to table
//             {
//                 rootRef.child("users")
//                     .push(postData)
//                     .then((snap) => {
//                         const key = snap.key;
//                         var newData = [
//                             $("#first_name").val() + " " + $("#last_name").val(),
//                             $("#email").val(),
//                             '<span class="label label-primary">'+postData.role+'</span>',
//                             theStatus,
//                             new_actions_row,
//                             postData.password,
//                             key
//                         ];
                        
//                         users_table.row.add(newData).draw().node().id = 'row_' + key;

//                         var td = $('#row_'+ key).find(".name");
//                         td.attr('data-fname', postData.first_name);
//                         td.attr('data-lname', postData.last_name);

//                         showLoading(false);
//                 });
//             }
            
//             notify('Request succeed!', postData.email + ' updated successfully', 'success');
//             $.magnificPopup.close();
//             console.log("updated!");
//             user_to_proccess = "";
//         // }
//         // else
//         // {
//         //     $('#dialogConfirmButton').off('click');
            
//         //     setGenericDialogMessage('Invalid', 'The edited student ID is invalid or already exist!', 'Ok');
            
//         //     $.magnificPopup.open({
//         //         items: {
//         //             src: '#genericDialog',
//         //             type: 'inline'
//         //         },
//         //         preloader: false,
//         //         modal: true
//         //     });

//         //     $('#dialogConfirmButton').on('click', function(){
//         //         $.magnificPopup.close();
//         //         //re open edit form dialog after conforming
//         //         $.magnificPopup.open({
//         //             items: {
//         //                 src: '#form',
//         //                 type: 'inline'
//         //             },
//         //             preloader: false,
//         //             modal: true
//         //         });
//         //     });
//         // }
//     });
// }

// function continueRegisterOLD(user_id)
// {
//     var originalEmail = $('#row_' + user_id + ' > .email').text();

//     var emailExists = false;

//     // firebase.auth().createUserWithEmailAndPassword(newEmail, pass)

//     //check if id already exist
//     rootRef.child('users').once('value').then(function(snap){

//         var i = 0;
//         var userCount = snap.numChildren();
//         snap.forEach(function(user){            

//             console.log(user);
            
            
//             if(editedNewEmail == user.child('email').val())
//             {
//                 emailExists = true;                                
//             }
//             else
//             {
//                 i++;
//             }
            
//             if((i+1 >= userCount && emailExists == false) || emailExists && i >= 0)
//             {
//                 i = -1;//this will skip other data if we already found matching email
//                 console.log("original email : " + originalEmail + ", editedNewEmail : " + editedNewEmail);
//                 //OR id remains unedited AND conflict is  OR student is new
//                 //if((emailExists == false && editedNewEmail != originalEmail) || (emailExists == true && editedNewEmail == originalEmail) || (user_id.length == 0 && emailExists == false))
//                 if(emailExists && editedNewEmail != originalEmail)
//                 {
//                     $('#dialogConfirmButton').off('click');
//                     setGenericDialogMessage('Invalid', 'The edited student ID is invalid or already exist!', 'Ok');
//                     $.magnificPopup.open({
//                         items: {
//                             src: '#genericDialog',
//                             type: 'inline'
//                         },
//                         preloader: false,
//                         modal: true
//                     });

//                     $('#dialogConfirmButton').on('click', function(){
//                         $.magnificPopup.close();
//                         //re open edit form dialog after conforming
//                         $.magnificPopup.open({
//                             items: {
//                                 src: '#form',
//                                 type: 'inline'
//                             },
//                             preloader: false,
//                             modal: true
//                         });
//                     });
//                 }
//                 else                
//                 {
//                     console.log("processing for update : " + editedNewEmail);
//                     //var nodeKey = user.key;
//                     //allow same id OR new id does not conflict with any existing id 
//                     var selectedStatus = $("#status").val();

//                     if($("#status").val() == "Verified")
//                         selectedStatus = "Active";

//                     var theStatus = "<span class='label label-success'>Active</span>";
//                     if(selectedStatus == "Deleted")
//                         theStatus = "<span class='label label-danger'>Deleted</span>";
//                     if(selectedStatus == "Suspended")
//                         theStatus = "<span class='label label-warning'>Suspended</span>";

//                     //remove old node in database if edited with new student ID
//                     var postData = {
//                         email: $('#email').val(),
//                         first_name: $('#first_name').val(),
//                         last_name: $('#last_name').val(),
//                         role: $("#role").val(),
//                         status : $("#status").val(),
//                         password: $('#password').val()
//                     };

//                     var new_actions_row = generateNewActions();
//                     //Update table row only
//                     if(user_id.length > 0)
//                     {
//                         var row = $("#row_" + user_id);
//                         var temp = users_table.row(row).data();
//                         temp[0] = postData.first_name + " " + postData.last_name;
//                         temp[1] = editedNewEmail;
//                         temp[2] = '<span class="label label-primary">'+postData.role+'</span>';
//                         temp[3] = theStatus;
//                         temp[4] = new_actions_row;
//                         temp[5] = postData.password;
//                         temp[6] = user_id;

//                         users_table.row(row).data(temp).draw();

//                         //Write the new post's data simultaneously in the posts list and the user's post list.
//                         var updates = {};
//                         updates['users/' + user_id] = postData;

//                         rootRef.update(updates);
//                     }
//                     else//if this is new user, add new row to table
//                     {
//                         rootRef.child("users")
//                             .push(postData)
//                             .then((snap) => {
//                                 const key = snap.key;
//                                 var newData = [
//                                     $("#first_name").val() + " " + $("#last_name").val(),
//                                     $("#email").val(),
//                                     '<span class="label label-primary">'+postData.role+'</span>',
//                                     theStatus,
//                                     new_actions_row,
//                                     postData.password,
//                                     key
//                                 ];
                                
//                                 users_table.row.add(newData).draw().node().id = 'row_' + key;
        
//                                 var td = $('#row_'+ key).find(".name");
//                                 td.attr('data-fname', postData.first_name);
//                                 td.attr('data-lname', postData.last_name);
//                         });
//                     }
                    
//                     notify('Request succeed!', postData.email + ' updated successfully', 'success');
//                     $.magnificPopup.close();
//                     console.log("updated!");
//                     user_to_proccess = "";
//                 }
//             }
//         });
//     });
// }

// function editUser(me)
// {
//     var evID = me.closest('tr');//.attr("val");
//     user_to_proccess = evID.id.split("row_")[1];
//     console.log(user_to_proccess);

    
//     //fill edit user form
//     //for hidden columns
//     var dd = users_table.row(evID).data();
//     //console.log(dd);
//     //user email
//     orig_email = $('#'+evID.id + ' > .email').text();
//     $('#email').val(orig_email);
//     //user password
//     $('#password').val(dd[5]);

//     var full_name = $('#'+ evID.id).find(".name");
//     //user first name
//     $('#first_name').val(full_name.attr("data-fname"));
//     //user last name
//     $('#last_name').val(full_name.attr("data-lname"));
//     //user role
//     $('#role').val($('#'+evID.id + ' > .role > span').text());
//     //user status
//     var statusText = $('#'+evID.id + ' > .status > span').text();
    
//     if(statusText == "Active")
//         $('#status').val("Verified");
//     else
//         $('#status').val(statusText);

    

//     $.magnificPopup.open({
//         items: {
//             src: '#form',
//             type: 'inline'
//         },
//         preloader: false,
//         modal: true
//     });
// }

// function loadUsersFromDB()
// {
//     console.log('start reading all users');
//     showLoading(true);

//     var users = [];

//     rootRef.child('users').once('value').then(function(snap){
//         var count = snap.numChildren();
//         var i = 0;
//         //read all students from db
//         snap.forEach(function(user){
            
//             var data = {
//                 first_name: user.child('first_name').val(),
//                 last_name : user.child('last_name').val(),
//                 password : user.child('password').val(),
//                 role :user.child('role').val(),
//                 status: user.child('status').val(),
//                 email: user.child('email').val(),
//                 ID : user.key
//             };

//             //var status = ()? "<span class='label label-danger'>Deleted</span>": "<span class='label label-danger'>Deleted</span>";
//             var theActions = 
//             '<a href="#" onclick="editUser(this)" class="on-default edit-row"><i class="fa fa-pencil"></i></a>'+
//             '<a href="#" onclick="deleteUser(this)" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>';

//             var theStatus = "<span class='label label-success'>Active</span>";
//             if(data.status == "Deleted")
//                 theStatus = "<span class='label label-danger'>Deleted</span>";
//             if(data.status == "Suspended")
//                 theStatus = "<span class='label label-warning'>Suspended</span>";

//             var newData = [
//                 data.first_name + " " + data.last_name,
//                 data.email,
//                 '<span class="label label-primary">'+data.role+'</span>',
//                 theStatus,
//                 theActions,
//                 data.password,
//                 data.ID
//             ];
    
//             var newRow = users_table.row.add(newData);
//             newRow.node().id = 'row_' + data.ID;
            
//             users[i] = {
//                 id : data.ID,
//                 email : data.email,
//                 fname : data.first_name,
//                 lname : data.last_name
//             }

//             i++;

//             if(i >= count)
//             {
//                 showLoading(false);
//                 console.log('done reading all users');
//                 users_table.draw();

//                 for (let z = 0; z < users.length; z++) {
//                     var element = users[z];
//                     var td = $('#row_'+ element.id).find(".name");
//                     td.attr('data-fname', element.fname);
//                     td.attr('data-lname', element.lname);            
//                 }
//             }
//         });        
//     });
// }

// function generateNewActions()
// {
//     return '<a href="#" onclick="editUser(this)" class="on-default edit-row"><i class="fa fa-pencil"></i></a>'+
//     '<a href="#" onclick="deleteUser(this)" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>';
// }


// function initVariables()
// {
//     rootRef = firebase.database().ref();
//     user_to_proccess = "";

//     $('#form').parent().parent().on("scroll", function() {
//         console.log('scrolling');
// 		$('#date_of_birth').datepicker('place');
// 	});
// }

// function populateRoles()
// {
//     rootRef.child('roles').once('value').then(function(snap){
//         //read all roles from db
//         snap.forEach(function(role){
//             $('#role').append("<option>" + role.val() + "</option>");
//         });
//     });
// }

// function initializeUsersTable(){
//     users_table = $('#users-data').DataTable(
//         {
//         dom: 'flrtip',
//         columnDefs: [
//             {
//                 targets: [5, 6],
//                 className: 'noVis',
//                 visible: false
//             }
//         ],
//         aoColumns: [
//             { sClass: 'name' },
//             { sClass: 'email' },
//             { sClass: 'role center' },
//             { sClass: 'status center' },
//             {
//                 bSortable: false,
//                 sClass: 'actions center',
//                 defaultContent: [
//                     '<a href="#" onclick="editUser(this)" class="on-default edit-row"><i class="fa fa-pencil"></i></a>',
//                     '<a href="#" onclick="deleteUser(this)" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>'
//                 ].join(' ')
//             },
//             { sClass: 'password center' },
//             { sClass: 'ID center' }
//         ]
//     }
//     );

//     $(".dt-buttons").hide();
// }

// function hidePassword(){
// 	$("#password").attr('type', 'password');
// 	$('#passSpan').text('');
// 	$('#passSpan').append('<i class="fas fa-eye-slash"></i>');
// }


// function attachButtonCallbacks()
// {    
//     //allow lowercase only on email
//     $('#email').keyup(function () {
// 		$('#email').val($('#email').val().toLowerCase());
// 	});

//     //last name changed
//     $('#last_name').keyup(function(e){
//         if($("#last_name").val().length == 0)
//             $("#last_name_hint").text("Last name is required!");
//         else
//             $("#last_name_hint").text("");
//     });

//     //first name changed
//     $('#first_name').keyup(function(e){
//         if($("#first_name").val().length == 0)
//             $("#first_name_hint").text("First name is required!");
//         else
//             $("#first_name_hint").text("");
//     });

//     //email changed
//     $('#email').keyup(function(e){
//         var editedNewEmail = $("#email").val();
//         if(ValidateEmail(editedNewEmail) == false || editedNewEmail.length == 0)
//         {   
//             $("#email_hint").text("The edited email is invalid or already in use!");
//         }
//         else
//         {
//             $("#email_hint").text("");
//         }
//     });


//     //status select changed
//     $('#status').on('change', function(e){
//         $("#status_hint").text("");
//     });    

//     //role select changed
//     $('#role').on('change', function(e){
//         $("#role_hint").text("");
//     });

//     //reveal password
//     $("#revealPass").on('click',function() {
// 		var $pwd = $("#password");
// 		if ($pwd.attr('type') === 'password') {
// 			$pwd.attr('type', 'text');
// 			$('#passSpan').text('');			
// 			$('#passSpan').append('<i class="fas fa-eye"></i>');
			
// 			} else {
// 				hidePassword();
// 		}
// 	});

//     //cancels
//     $("#dialogCancel").on('click', function(){$.magnificPopup.close();});
//     //$("#suspendCancel").on('click', function(){$.magnificPopup.close(); user_to_proccess = "";});
//     $("#formCancel").on('click', function(){$.magnificPopup.close(); user_to_proccess = "";});

//     /* SAVE */
//     $("#formSave").on('click',function(e){
//         e.preventDefault();
//         showLoading(true, "Saving...");
//         updateUser(user_to_proccess);
//     });

//     /* ADD */    
//     $("#addToTable").on('click',function(e){
//         e.preventDefault();
//         user_to_proccess = "";
//         //console.log("user_to_proccess : " + user_to_proccess);
//         //clear form
//         $('#first_name').val("");
//         $('#last_name').val("");
//         $('#email').val("");
//         $('#password').val("");
//         $('#role').val("Select account role");
//         $('#status').val("Select account status");
//         validatePassword($('#password').val(""));
//         $.magnificPopup.open({
//             items: {
//                 src: '#form',
//                 type: 'inline'
//             },
//             preloader: false,
//             modal: true,
//             callbacks: {
//             }
//         });
//     });


//     /* DELETE */
//     $('#dialogConfirm').on( 'click', function( e ) {
//         e.preventDefault();
        
//         var updates = {};
//         updates['users/' + user_to_proccess + '/status'] = "Deleted";
//         console.log("deleting : " + user_to_proccess);

//         rootRef.update(updates, function(error){
//             // Callback comes here
//             if(error){
//                 notify('Request failed!', error, 'error');
//                 $.magnificPopup.close();
//                 user_to_proccess = "";
//             }
//             else{
//                 console.log("User Delete Complete!");
                
//                 var row = $('#row_' + user_to_proccess);
//                 row.find('.status').html("<span class='label label-danger'>Deleted</span>");

//                 users_table.row(row).draw();
//                 $.magnificPopup.close();
//                 notify('Request Succeed!', $("#email").val() + ' was deleted successfully!', 'success');
//                 user_to_proccess = "";
//             }
//         });
//     });
// }



// function validatePassword(password) {
    
//     var passTextHint = "Password is required!"
// 	// Do not show anything when the length of password is zero.
// 	if (password.length === 0) {
//         $('#passStrength').attr('class', 'text-danger');
//         passTextHint = "Password is required!";
// 		//$('#passStrength').parent().css('background-color', 'red');
// 		return 0;
// 	}
// 	// Create an array and push all possible values that you want in password
// 	var matchedCase = new Array();
// 	matchedCase.push("[$@$!%*#?&]"); // Special Character
// 	matchedCase.push("[A-Z]");      // Uppercase Alpabates
// 	matchedCase.push("[0-9]");      // Numbers
// 	matchedCase.push("[a-z]");     // Lowercase Alphabates
	
// 	// Check the conditions
// 	var ctr = 0;
// 	for (var i = 0; i < matchedCase.length; i++) {
// 		if (new RegExp(matchedCase[i]).test(password)) {
// 			ctr++;
// 		}
// 	}
	
// 	if(password.length >= 8)
// 	    ctr++;
// 	else
// 	    ctr = 2;
// 	// Display it
// 	var color = "";
// 	var strength = "";
// 	switch (ctr) {
// 		case 0:
// 		case 1:
// 		case 2:
//             strength = 'text-danger';
//             color = "red";
//             if(password.length < 8)
//                 passTextHint = "Password must be atleast 8 characters!";
//             else
//                 passTextHint = "Password must contain atleast 1 special character, 1 Uppercase character and 1 number!";
// 		break;
// 		case 3:
//             strength = "text-warning";
//             color = "orange";
//             if(password.length < 8)
//                 passTextHint = "Password must be atleast 8 characters!";
//             else
//                 passTextHint = "Password must contain atleast 1 special character, 1 Uppercase character and 1 number!";
// 		break;
// 		case 4:
//             strength = 'text-success';
//             color = "green";
// 		break;
// 	}
// 	if(ctr >= 4){
// 		strength = 'text-success';
//         color = "green";
//         passTextHint = "Password is valid";
// 	}
	
//     $('#passStrength').attr('class', strength);
//     $('#passStrength').text(passTextHint);
// 	//$('#passStrength').parent().css('background-color', color);
// 	return ctr;
// }

