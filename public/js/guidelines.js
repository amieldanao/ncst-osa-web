(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];
addUserCallback();
// var initial_role;
// var myRole;
function addUserCallback()
{
    let email = $("#user_email").text();

    let query = db.collection('users').where('email', '==', email);

    query.onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {


            if (change.type === "added") {
                let role = change.doc.data().role;
                initial_role = role;
                setRole(role);
                if(role == 2 || role == 1)
                {
                    $("#table_tr").last().attr('id', 'actions_th');//append('<th id="actions_th" class="role_admin">Actions</th>');
                }
            }

            if (change.type === "modified") {
                setRole(change.doc.data().role);
                $("body").empty();
                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                 setTimeout(() => {
                    location.replace("index.html");
                }, 3000);
            }

            if (change.type === "removed") {
                // console.log("Removed user: ", change.doc.data());
                // window.location = "index.html";
            }
        });
    },
        function(error){
        console.log('retry add user callback');
        addUserCallback();
    });
}

function setRole(newRole)
{
    switch(newRole)
    {
        case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
        case 1 : $(".role_admin").remove();break;
    }
    
    $("#user_role").text("Role: " + roles_string[newRole]);
    $("#user_role").attr("data-done", true);
}

})();





'user strict'


var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);



function Start()
{
    sessionStorage.setItem("toEdit", '');
    initTable();
    // addDBListener();
}


var guideline_table;
var storedContent = {};

$(document).ready( function () {
    observer.observe(targetNode, config);
//    initButtonCallbacks();
});

//function initButtonCallbacks()
//{
//    $("")
//}

function clickView(elem)
{
    var key = $(elem).attr('data-key');
    var section = $(elem).parent().parent().find(".col_section").text();
    var content = storedContent[key];//$(elem).parent().parent().find(".col_content").val();
    

    bootbox.alert(
        {
            title: section,
            message : content,
            centerVertical : true,
            scrollable : true
        });
    
    // showPromptEdit(section.text(), storedContent[key]);
    // sessionStorage.setItem("toEdit", $(elem).attr('data-key'));
}

function clickEdit(elem)
{
    var section = $(elem).parent().parent().find(".col_section");
    var content = $(elem).parent().parent().find(".col_content");
    
//    console.log(section.text());
    var key = $(elem).attr('data-key');
    
    showPromptEdit(section.text(), storedContent[key]);
    sessionStorage.setItem("toEdit", $(elem).attr('data-key'));
}

function clickRemove(elem)
{
//    var section = $(elem).parent().parent().find(".col_section");
//    var content = $(elem).parent().parent().find(".col_content");
    var key = $(elem).attr('data-key');
    
    
    if(key.trim().length > 0)
    {
        showLoading(true, "Deleting...");
        db.collection("guidelines").doc(key).delete().then(function() {
            console.log("Document successfully deleted!");
            PNotify.info({
              title: 'Deleted',
              text: 'Guideline deleted successfully'
            });
            showLoading(false);
            
            
        }).catch(function(error) {
            console.error("Error removing document: ", error);
            PNotify.error({
              title: 'Error',
              text: error
            });
            showLoading(false);
        });
    }
}

function initTable()
{
    let myColumns =
    [
        {"sClass": "col_section" },
        {"sClass": "long_columns col_content" },
        {"sClass": "actions_buttons" }           
    ]

    let btns = [];
    if(document.getElementById("actions_th") != null)
    {
        console.log("has action column!");
        // myColumns.push({"sClass": "actions_buttons" });
        btns = [
                {
                    text: 'Add <i class="fa fa-plus"></i>',
                    action: function ( e, dt, node, config ) {
                        showPromptEdit()
                    },
                    "className": 'btn btn-primary'
                }
            ]
    }

    guideline_table = $('#guidelines_table').DataTable({
        scrollX: true,
        dom: 'Bfrltip',
        buttons: btns,
        aoColumns: myColumns,
        columnDefs: [ {
            "targets": 2,
            "orderable": false
        }],
        initComplete: function(settings, json) {
        // alert( 'DataTables has finished its initialisation.' );
        addDBListener();
      }
    });
    
//    $('#guidelines_table tbody').on( 'click', '.remove-row', function () {
//        var data = guideline_table.row( $(this).parents('tr') ).data();
//        //alert( data[0] +"' remove "+ data[ 5 ] );
//        
//        
//    } );
}



function addDBListener()
{
    showLoading(true, "Loading guidelines...");
    db.collection("guidelines")
    .onSnapshot(function(snapshot) { 
        let requests = snapshot.docChanges().map((change) => {
            
            if (change.type === "added") {
//                console.log("New city: ", change.doc.data());
                
                var d = {section: "", content : ""};
                if('section' in change.doc.data())
                    d['section'] = change.doc.data().section;
                
                if('content' in change.doc.data())
                    d['content'] = change.doc.data().content;
                
                let newData = [
                    d.section,
                    getTextFromHTML(d.content)
                ]

                if(document.getElementById("actions_th") != null)
                    newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickEdit(this);" class="on-default edit-row mx-2"><i class="fa fa-pencil"></i></a><a data-key="'+change.doc.id+'" href="#" onclick="showPromptDelete(this);" class="on-default remove-row mx-2"><i class="fa fa-trash-o"></i></a>');
                else
                    newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickView(this);" class="on-default edit-row mx-2"><i class="fas fa-eye"></i></a>');

                guideline_table.row.add(newData).draw();
                
                storedContent[change.doc.id] = d.content;
            }
            
            if (change.type === "modified") {
                console.log("Modified city: ", change.doc.data());
                
                
            }
            if (change.type === "removed") {
//                console.log("Removed city: ", change.doc.data());
                
                $('*[data-key="'+change.doc.id+'"]').parent().parent().remove();
                
                guideline_table.draw();
            }
            
            return new Promise((resolve) => {
              asyncFunction(change, resolve);
            });
        })

        Promise.all(requests).then(() => {showLoading(false);});
        
        
//        .then(function(){
//            console.log("done!");
//        });
    });
}

function asyncFunction (item, cb) {
  setTimeout(() => {
//    console.log('done with', item);
    cb();
  }, 100);
}



function showPromptEdit(_section, _content)
{
    bootbox.dialog({
        title: 'Manage Guideline',
        message: '<form id="infos" action="">Section:<input style="width:100%;" type="text"><br/>Content:<textarea id="summernote" style="width:100%; height:180px; border-radius:5px; "></textarea></form>',
        
        buttons: {
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function(){
//                    console.log('Custom cancel clicked');
                    sessionStorage.setItem("toEdit", '');
                }
            },
            ok: {
                label: 'Save <i class="fa fa-save"></i>',
                className: 'btn-success',
                callback: function(){
                    console.log('Custom OK clicked');
                    var section = $('#infos input').val();
                    var content = $('#summernote').summernote('code');//$('#infos textarea');

                    if(section == undefined || section.trim().length == 0)
                    {
                        bootbox.alert({
                            message:"Section can't be blank!",
                            centerVertical: true,
                            className : "topmost-modal",
                            size : 'small'
                        });
                        return false;
                    }


//                    console.log(content.val());
                    saveGuideline(section, content);
                }
            }
        }
    
    });
    
    $("#infos input").val(_section);
    
    $('#summernote').summernote({
        height: '200px'
    });
    
    if(_content)
    {
        $('#summernote').summernote('code', _content);
    }
}

function showPromptDelete(elem)
{
    sessionStorage.setItem("toEdit", $(elem).attr('data-key'));
    bootbox.confirm({
        title:$(elem).parent().parent().find(".col_section").text(),
        message: "Are you sure you want to delete this guideline?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            console.log('This was logged in the callback: ' + result);
            if(result)
            {
                clickRemove(elem);
            }
            sessionStorage.setItem("toEdit",'');
        }
    });
}

function saveGuideline(_section, _content)
{
    var key = sessionStorage.getItem("toEdit");//$('#infos').attr('data-key');
    var ref = db.collection("guidelines").doc();
    
    if(key == undefined || key.length == 0)
    {
        key = ref.id;
    }
    else
    {
        ref = db.collection("guidelines").doc(key);
    }
    
    sessionStorage.setItem("toEdit", "");
    
    showLoading(true, "Saving...");
    // Add a new document with a generated id.
    ref.set({
        section: _section,
        content: _content
    })
    .then(function(docRef) {
//        console.log("Document written with ID: ", docRef.id);
//        PNotify.info({
//          title: 'Saved',
//          text: _section + ' saved successfully'
//        });
        
        showNotifSuccess('Saved', _section + ' saved successfully');
        showLoading(false);
        
        $('.remove-row[data-key="'+key+'"]').parent().parent().find(".col_section").text(_section);
        
        $('.remove-row[data-key="'+key+'"]').parent().parent().find(".col_content").text(getTextFromHTML(_content));
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
        PNotify.error({
          title: 'Error',
          text: error
        });
        showLoading(false);
    });
}