(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];


addUserCallback();
// var initial_role;
// var myRole;
function addUserCallback()
{
    let email = $("#user_email").text();

    let query = db.collection('users').where('email', '==', email);

    query.onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {


            if (change.type === "added") {
                let role = change.doc.data().role;
                initial_role = role;
                setRole(role);
                if(role == 2 || role == 1)
                {
                    $("#table_tr th").last().attr('id', 'actions_th');//append('<th id="actions_th" class="role_admin">Actions</th>');
                }
            }

            if (change.type === "modified") {
                setRole(change.doc.data().role);
                $("body").empty();
                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                 setTimeout(() => {
                    location.replace("index.html");
                }, 3000);
            }

            if (change.type === "removed") {
                // console.log("Removed user: ", change.doc.data());
                // window.location = "index.html";
            }
        });
    },
        function(error){
        console.log('retry add user callback');
        addUserCallback();
    });
}

function setRole(newRole)
{
    switch(newRole)
    {
        case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
        case 1 : $(".role_admin").remove();break;
    }
    
    $("#user_role").text("Role: " + roles_string[newRole]);
    $("#user_role").attr("data-done", true);
}

})();





'user strict'


var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);



function Start()
{
    fetchUsedActionColors();
    // loadCategories();
}

$(document).ready( function () {
    observer.observe(targetNode, config);
//    initButtonCallbacks();
});



var action_table;
var categories = [];
var actions_count = 0;
var action_messages = [];
var edit_dialog;
var storedMessages = [];
var used_action_colors = new Array();


function fetchUsedActionColors()
{
    db.collection("usedActionColors")
    .get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            // console.log(doc.id, " => ", doc.data());
            used_action_colors.push(doc.data().color);
        });

        loadCategories();
    });
}

function loadCategories() {
    db.collection('categories').get().then(function(querySnapshot){
        // console.log(querySnapshot);

        let requests = querySnapshot.docs.map((category) => {
            
            categories[category.id] = category.data();
        });

        Promise.all(requests).then(() => {
            initTable();
        });


    });
}

function initTable()
{
    let btns = [];
    if(document.getElementById("actions_th") != null)
    {
        console.log("has action column!");
        // myColumns.push({"sClass": "actions_buttons" });
        btns = [
        {
            text: 'Add <i class="fa fa-plus"></i>',
            action: function ( e, dt, node, config ) {
                showPromptEdit()
            },
            "className": 'btn btn-primary'
        }
        ]
    }

    action_table = $('#action_table').DataTable(
    {
        dom: 'Bfrltip',
        "scrollX": true,
        buttons: btns,
        "aoColumns": [
        {"sClass": "col_title" },
        {"sClass": "col_message" },
        {"sClass": "col_categories" },
        {"sClass": "actions_buttons" }
        ],
        "columnDefs": [ {
            "targets": 3,
            "orderable": false
        }],
        initComplete: function(settings, json) {
            // alert( 'DataTables has finished its initialisation.' );
            addDBListener();
        }
    }
    );

    // addDBListener();
}

function showPromptEdit(_title, _message, _categories)
{
    var isNew = (_title == undefined || _message == undefined || _categories);

    var categorySelectOptions = '';
    var selected_categories_from_data = new Array();

    Object.keys(categories).forEach(function(e, i){
        categorySelectOptions += '<option value="'+e+'">'+categories[e].name+'</option>';
        if(_categories != undefined && _categories.indexOf(categories[e].name) != -1)
        {
            selected_categories_from_data.push(e);
        }
    });

    edit_dialog = bootbox.dialog({
        title: 'Manage Action',
        message: '<form id="infos" action="">'+
        'Title:<input style="width:100%;" class="col_title" type="text"><br/>'+
        '<label for="category_selection" style="width:100%;">Action categories:<select id="category_selection" multiple="multiple" style="width: 100%">'+
        categorySelectOptions+
        '</select></label><br>'+
        'Message:<textarea id="summernote" style="width:100%; border-radius:5px; "></textarea><br>'+'</form>',
        // '<label for="sDate">Start Date:</label><div class="input-group"><input id="sDate" class="col_start_date form-control" size="16" type="text" value="" readonly="true"><div class="input-group-append"><span class="input-group-text"><i class="far fa-calendar-alt"></i></span></div></div><br>'+
        // '<label for="eDate">End Date:</label><div class="input-group"><input id="eDate" class="col_end_date form-control" size="16" type="text" value="" readonly="true"><div class="input-group-append"><span class="input-group-text"><i class="far fa-calendar-alt"></i></span></div></div></form>',

        buttons: {
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function(){
                    sessionStorage.setItem("toEdit", '');
                }
            },
            ok: {
                label: 'Save <i class="fa fa-save"></i>',
                className: 'btn-success',
                callback: function(){

                    console.log('Custom OK clicked');
                    var title = $('#infos .col_title').val();
                    if(title == undefined || title.trim().length == 0)
                    {
                        bootbox.alert({
                            message:"Title can't be blank!",
                            centerVertical: true,
                            className : "topmost-modal",
                            size : 'small',
                            onHidden: function () {
                                UpdateModalScrolling();
                            }
                        });
                        
                        return false;
                    }

                    var message = $('#summernote').summernote('code');

                    let selected_categories = $('#infos #category_selection').find(':selected');

                    if(selected_categories.length == 0)
                    {
                        bootbox.alert({
                            message:"Please select atleast one category",
                            centerVertical: true,
                            className : "topmost-modal",
                            size : 'small',
                            onHidden: function () {
                                UpdateModalScrolling();
                            }
                        });
                        return false;
                    }
                    
                    let color = null;
                        
                    if(isNew){
                        console.log("Saving is new");

                        color = getRandomColor();//$('#infos .col_color').val();

                        while(used_action_colors.indexOf(color) != -1)
                        {
                            color = getRandomColor();
                            console.log(color + " generated again");
                        }
                        console.log(color + " THE COLOR");
                        saveAction(title, message, selected_categories, color);
                    }
                    else
                    {
                        console.log("updating only");

                        saveAction(title, message, selected_categories, color);
                    }
                    
                    
                }
            }
        }
    });
    


    edit_dialog.on('shown.bs.modal', function(e){

        $("#infos .col_title").val(_title);
        
        $('#summernote').summernote({
            height: '200px',
            toolbar: defaultToolbar
        });
        
        if(_message)
        {
            $('#summernote').summernote('code', _message);
        }

        let select = $('#infos #category_selection');
        select.select2({dropdownParent: edit_dialog, width: 'resolve', theme: "classic"});
        select.val(selected_categories_from_data).trigger('change');

        
        // let start_date_picker = $('#infos .col_start_date');
        // start_date_picker.datetimepicker({autoclose:true, disableTouchKeyboard: true, dateFormat: 'MM-dd-yyyy HH:ii', forceParse:true});
        
        // if(_sDate){
        //     start_date_picker.val(moment(_sDate).format("YYYY-MM-DD HH:mm"));
        // }
        // else{
        //     start_date_picker.datetimepicker('setDate', new Date());
        // }
        
        // let end_date_picker = $('#infos .col_end_date');
        // end_date_picker.datetimepicker({autoclose:true, disableTouchKeyboard: true, dateFormat: 'MM-dd-yyyy HH:ii', forceParse:true});
        
        // if(_eDate){
        //     end_date_picker.val(moment(_eDate).format("YYYY-MM-DD HH:mm"));
        // }
        // else{
        //     end_date_picker.datetimepicker('setDate', new Date());
        // }
    });
}

// function UpdateModalScrolling()
// {
//     if($('.modal').length == 0)
//     {
//         $('body').removeClass('modal-open');
//         console.log('no modal');
//     }
//     else
//     {
//         $('body').addClass('modal-open');
//         console.log("there's modal");
//     }
// }

function saveAction(_title, _message, _categories, _color)
{
    console.log("color to save = " + _color);
    var selectCategories = new Array();
    _categories.each(function(i, elem){
        console.log(elem);
        selectCategories.push(elem.innerText);
    });

    var key = sessionStorage.getItem("toEdit");
    var ref = db.collection("actions").doc();
    
    if(key == undefined || key.length == 0)
    {
        key = ref.id;
    }
    else
    {
        ref = db.collection("actions").doc(key);
    }
    
    sessionStorage.setItem("toEdit", "");
    
    showLoading(true, "Saving...");
    try
    {
        var newData = {};
        newData["title"] = _title;
        newData["message"] = _message;
        newData["categories"] = selectCategories;

        if(_color != null)
            newData["color"] = _color;

        ref.set(newData, {merge:true})
        .then(function(docRef) {
            showNotifSuccess('Saved', _title + ' saved successfully');
            showLoading(false);

            if(_color != null){
                db.collection("usedActionColors").add({color:_color});
                used_action_colors.push(_color);
            }
            
            
            
            // $('.remove-row[data-key="'+key+'"]').parent().parent().find(".col_start_date").text(_sDate);
            
            // $('.remove-row[data-key="'+key+'"]').parent().parent().find(".col_end_date").text(_eDate);        
            
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
            PNotify.error({
              title: 'Error',
              text: error
            });
            showLoading(false);
        });
    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(
        {
            message:error.message, 
            onHidden: function () {
                UpdateModalScrolling();
            }
        });
    }
}

function addDBListener()
{
    showLoading(true, "Loading actions...");
    try {
          
        db.collection("actions")
        .onSnapshot(function(snapshot) { 
            
            let requests = snapshot.docChanges().map((change) => {
                
                var d = {title: "", message : "", categories: new Array(), start_date : "", end_date : "", color : ""};
                if('title' in change.doc.data())
                    d['title'] = change.doc.data().title;
                
                if('message' in change.doc.data())
                    d['message'] = change.doc.data().message;

                if('color' in change.doc.data())
                    d['color'] = change.doc.data().color;
                
                if('start_date' in change.doc.data())
                    d['start_date'] = change.doc.data().start_date;
                
                if('end_date' in change.doc.data())
                    d['end_date'] = change.doc.data().end_date;
                
                if('categories' in change.doc.data())
                    d['categories'] = change.doc.data().categories;

                if (change.type === "added") {
                    
                    let newData =
                    [
                        '<span class="badge badge-info" style="background-color:'+d.color+';font-size: 1rem;">' + capitalizeFLetter(d.title) + '</span>',
                        getTextFromHTML(d.message),
                        d.categories
                    ]

                    if(document.getElementById("actions_th") != null)
                        newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickEdit(this);" class="on-default edit-row mx-2"><i class="fa fa-pencil"></i></a><a data-key="'+change.doc.id+'" href="#" onclick="showPromptDelete(this);" class="on-default remove-row mx-2"><i class="fa fa-trash-o"></i></a>');
                    else
                        newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickView(this);" class="on-default edit-row mx-2"><i class="fas fa-eye"></i></a>');

                    let newRow = action_table.row.add(newData).node().id = change.doc.id;

                    action_table.draw( false );

                    $(newRow).find(".col_title > span").css('color', invertColor(d.color, true));
                    
                    storedMessages[change.doc.id] = d.message;
                }
                
                if (change.type === "modified") {
                    // console.log("Modified city: ", change.doc.data());
                    
                    
                    let key = change.doc.id;
                    let tr = $("#"+key);//.parent().parent();
            
                    tr.find(".col_title").html('<span class="badge badge-info" style="background-color:'+d.color+';font-size: 1rem;">' + capitalizeFLetter(d.title) + '</span>');
            
                    tr.find(".col_message").text(getTextFromHTML(d.message));

                    tr.find(".col_categories").text(d.categories);

                    tr.find(".col_title > span").css('color', invertColor(d.color, true));

                    storedMessages[change.doc.id] = d.message;
                }
                if (change.type === "removed") {
                    // $('tr[data-key="'+change.doc.id+'"]').parent().parent().remove();
                    action_table.row('#'+change.doc.id).remove().draw();
                    
                    // announcements_table.draw();
                }
                
                return new Promise((resolve) => {
                  asyncFunction(change, resolve);
                });
            });

            Promise.all(requests).then(() => {showLoading(false);});
        });
    }
    catch(err) {
        bootbox.alert(
        {
            message:err.message,
            onHidden: function () {
                UpdateModalScrolling();
            }
        });
    }
}


function clickView(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    var section = tr.find(".col_title").text();    
    var messages = storedMessages[key];//$(elem).parent().parent().find(".col_content").val();
    var myCategories = tr.find(".col_categories").text();
    // var effectiveDate = tr.find(".col_start_date").text() + "   to   " + tr.find(".col_end_date").text();
    

    bootbox.alert(
    {
        title: section,
        message : messages + "<br><hr>" + myCategories +"<br>",
        centerVertical : true,
        scrollable : true
    });
}

function clickEdit(elem)
{
    var title = $(elem).parent().parent().find(".col_title").text();
    // var message = $(elem).parent().parent().find(".col_message");
    var _categories = $(elem).parent().parent().find(".col_categories").text();
    
    // var sDate = $(elem).parent().parent().find(".col_start_date");
    // var eDate = $(elem).parent().parent().find(".col_end_date");
    
    
    var key = $(elem).attr('data-key');
    
    showPromptEdit(title, storedMessages[key], _categories.split(','));
    
    sessionStorage.setItem("toEdit", $(elem).attr('data-key'));
}

function showPromptDelete(elem)
{
    sessionStorage.setItem("toEdit", $(elem).attr('data-key'));
    
    bootbox.confirm({
        title:$(elem).parent().parent().find(".col_title").text(),
        message: "Are you sure you want to delete this Action?",
        size:'small',
        centerVertical: true,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {

            if(result)
            {
                clickRemove();
            }
            else
            {
                sessionStorage.setItem("toEdit", '');
            }
            
        }
    });
}

function clickRemove()
{   
    var key = sessionStorage.getItem("toEdit");
    
    if(key.trim().length > 0)
    {
        showLoading(true, "Deleting...");

        try
        {
            db.collection("actions").doc(key).delete().then(function() {

                PNotify.info({
                  title: 'Deleted',
                  text: 'Action deleted successfully'
              });

                showLoading(false);

                action_table.row('#'+key).remove().draw();

                sessionStorage.setItem('toEdit', '');
                
            }).catch(function(error) {
                console.error("Error removing document: ", error);
                PNotify.error({
                  title: 'Error',
                  text: error
              });
                showLoading(false);
                sessionStorage.setItem('toEdit', '');
            });
        }
        catch(error)
        {
            bootbox.alert(error.message);
            showLoading(false);
        }
    }
}








// function editAction(me)
// {
//     var evID = me.closest('tr');//.attr("val");
//     action_to_proccess = evID.id;//.split("row_")[1];

//     var category_selected = stringToArray($('#'+evID.id + ' > .categories').text());
//     console.log(category_selected);

//     $("#action_title").val($('#'+evID.id + ' > .title').text());

//     $("#action_categories").select2('val', category_selected);

//     $('#action_editor').summernote('code', action_messages[action_to_proccess]);
    
//     var sDateString = $('#'+evID.id + ' > .date_start').text();
//     sDateString = sDateString.replace(/-/g, "/");
//     // console.log(sDateString);
//     var sDate = new Date();
//     if(sDateString.length > 0)
//         setDate = new Date(sDateString);

//     var eDateString = $('#'+evID.id + ' > .date_end').text();
//     eDateString = eDateString.replace(/-/g, "/");
//     var eDate = new Date();

//     if(eDateString.length > 0)
//         eDate = new Date(eDateString);

//     // console.log(sDate);
    
//     $("#started").datepicker('setDate', sDate);
//     $("#finished").datepicker('setDate', eDate);  
    

//     $.magnificPopup.open({
//         items: {
//             src: '#form',
//             type: 'inline'
//         },
//         preloader: false,
//         modal: true
//     });
// }

// function stringToArray(str)
// {
//     var retVal = [];
//     var splitted = str.split(',');
//     for (let index = 0; index < splitted.length; index++) {
//         const element = splitted[index];
        
//         retVal.push(element);
//     }

//     return retVal;
// }

// function updateAction(action_id)
// {
//     console.log("trying to save : " + action_id);

//     var categories_selected = $('#action_categories').val();
//     var action_message = $(".note-editable").get(0).innerHTML;

//     //check if id already exist
//     var postData = {        
//         title: $("#action_title").val(),
//         message: action_message,
//         categories: arrayToString(categories_selected),
//         startDate: $("#started").val(),
//         endDate: $("#finished").val()
//     };

//     var new_actions_row = generateNewActions();
//     //Update table row only
//     if(action_id.length > 0)
//     {
//         console.log("updating action node");
//         var row = $("#" + action_id);
//         var temp = action_table.row(row).data();
//         temp[0] = postData.category;
//         temp[1] = extractContent(postData.message);        
//         temp[2] = postData.categories;
//         temp[3] = postData.startDate;
//         temp[4] = postData.endDate;
//         temp[5] = new_actions_row;

//         // row.find(".color").css("background-color", postData.color);
//         action_messages[action_id] = postData.message;

//         action_table.row(row).data(temp).draw();

//         //Write the new post's data simultaneously in the posts list and the user's post list.
//         var updates = {};
//         updates['actions/' + action_id] = postData;

//         rootRef.update(updates);
//     }
//     else//if this is new user, add new row to table
//     {
//         console.log("creating new action_id node");
        
//         rootRef.child("actions")
//         .push(postData)
//         .then((snap) => {
//             const key = snap.key;
//             var newData = [
//                 postData.title,
//                 extractContent(postData.message),                
//                 postData.categories,                
//                 postData.startDate,
//                 postData.endDate,
//                 new_actions_row
//             ];

//             action_messages[key] = postData.message;
            
//             action_table.row.add(newData).draw().node().id = 'row_' + key;
//         });

//         //category_count += 1;
//     }
    
//     notify('Request succeed!', 'Action updated successfully', 'success');
//     $.magnificPopup.close();
//     console.log("updated!");
//     action_to_process = "";
// }

// function loadActionsFromDB()
// {
//     showLoading(true);
//     console.log('start reading all Actions');
    
//     rootRef.child('actions').once('value').then(function(snap){
//         actions_count = snap.numChildren();

//         if(actions_count == 0)
//             showLoading(false);
//         var i = 0;
//         //read all students from db
//         snap.forEach(function(action){            
//             var data = {
//                 title: action.child('title').val(),
//                 message : action.child('message').val(),
//                 categories : action.child('categories').val(),
//                 startDate : action.child('startDate').val(),
//                 endDate : action.child('endDate').val()
//             };

//             //var status = ()? "<span class='label label-danger'>Deleted</span>": "<span class='label label-danger'>Deleted</span>";
//             var theActions = generateNewActions();

//             action_messages[action.key] = data.message;

//             var newData = [
//                 data.title,
//                 extractContent(data.message),
//                 data.categories,
//                 data.startDate,
//                 data.endDate,
//                 theActions
//             ];
    
//             var newRow = action_table.row.add(newData).node();
//             newRow.id = action.key;
//             // console.log("This suggestion : " + suggestion.key);
            
//             i++;
//             if(i >= actions_count)
//             {
//                 showLoading(false);
//                 console.log('done reading all actions');
//                 action_table.draw();
//             }
//         });        
//     });
// }

// function extractContent(s) {
//     var span = document.createElement('span');
//     span.innerHTML = s;
//     return span.textContent || span.innerText;
// };

// function generateNewActions()
// {
//     return '<a href="#" onclick="editAction(this)" class="on-default edit-row"><i class="fa fa-pencil"></i></a>'+
//     '<a href="#" onclick="deleteAction(this)" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>';
// }

// function deleteAction(me)
// {
//     var evID = me.closest('tr');
//     action_to_process = evID.id;//.split("row_")[1];

//     $.magnificPopup.open({
//         items: {
//             src: '#dialog',
//             type: 'inline'
//         },
//         preloader: false,
//         modal: true
//     });
// }

// function initVariables()
// {
//     rootRef = firebase.database().ref();
//     action_to_process = "";

//     //get categories and add it as an option
//     rootRef.child("categories").once('value', function(snap){
//         var count = snap.numChildren();

//         if(count == 0)
//         {
//             $('#action_categories').select2({
//                 dropdownParent: $("#form")
//             });
//         }

//         var i = 0;
//         snap.forEach(function(category){
//             $("#action_categories").append('<option value="' + category.key +'">'+category.key+'</option>');
//             i++;

//             if(i >= count)
//             {
//                 $('#action_categories').select2({
//                     dropdownParent: $("#form")
//                 });
//             }
//         });
//     });

    

//     $('#action_editor').summernote({
//         theme: "classic",
//         height: 100
        
//         }
//     );

//     $('#started').datepicker({ format: 'MM-dd-yyyy', autoclose: true, todayHighlight: true });
//     $('#finished').datepicker({ format: 'MM-dd-yyyy', autoclose: true, todayHighlight: true});


//     $('.input-daterange').datepicker().on('changeDate', function(e) {
//         var started = $('#started').datepicker('getDate');
//         var finished = $('#finished').datepicker('getDate');
//         var date;
//         if (started > finished) {
//           if (e.target.id == 'started') {
//             date = new Date(started);
//             date.setDate(started.getDate() - 1);
//             $('#started').datepicker('setDate', date);
//           }
//           if (e.target.id == 'finished') {
//             date = new Date(finished);
//             date.setDate(finished.getDate() + 1);
//             $('#finished').datepicker('setDate', date);
//           }
//         }
//       });
//     // replace(new RegExp('/', 'g'), '-');
// }

// function arrayToString(array)
// {
//     var retVal = "";
//     for (let index = 0; index < array.length; index++) {
//         const element = array[index];
//         retVal += element;
//         if(index != array.length-1)
//             retVal += ",";
//     }

//     return retVal;
// }

// function addButtonCallbacks()
// {
//     //first name changed
//     $('#action_title').keyup(function(e){
//         if($("#action_title").val().length == 0)
//             $("#action_title_hint").text("Title is required!");
//         else
//             $("#action_title_hint").text("");
//     });

//     $('#action_categories').on('change', function (e) {
//         // Do something
//         $("#category_hint").text("");
//         if($("#action_categories").val() == null)        
//             $("#category_hint").text("Category is invalid!");
//     });

//     //cancels
//     $("#dialogCancel").on('click', function(){$.magnificPopup.close();});
//     //$("#suspendCancel").on('click', function(){$.magnificPopup.close(); user_to_proccess = "";});
//     $("#formCancel").on('click', function(){$.magnificPopup.close(); user_to_proccess = "";});

//     /* SAVE */
//     $("#formSave").on('click',function(e){
//         e.preventDefault();

//         $("#action_title_hint").text("");
//         $("#category_hint").text("");
//         $("#action_editor_hint").text("");

//         if($("#action_title").val().length == 0)
//         {
//             console.log("title");
//             $("#action_title_hint").text("Title is invalid!");
//             return;
//         }

//         if ($('#action_editor').summernote('isEmpty'))
//         {
//             //alert('editor content is empty');
//             console.log("message");
//             $("#action_editor_hint").text("Please enter action message");
//             return;
//         }

//         if($("#action_categories").val() == null)
//         {
//             console.log("categories");
//             $("#category_hint").text("Category is invalid!");
//             return;
//         }

//         updateAction(action_to_process);
//     });


//     /* ADD */    
//     $("#addToTable").on('click',function(e){
//         e.preventDefault();
//         action_to_process = "";
        
//         //clear form
//         $('#action_title').removeAttr('readonly');
//         $('#action_title').val("");
//         // $('#action_categories').select2();

//         $("#action_categories").select2("val", "");
//         $('#action_editor').summernote("reset");
        
//         var d = new Date();
//         $('#started').datepicker('setDate', d);
//         $('#finished').datepicker('setDate', d);

//         $.magnificPopup.open({
//             items: {
//                 src: '#form',
//                 type: 'inline'
//             },
//             preloader: false,
//             modal: true,
//             callbacks: {
//             }
//         });
//     });


//     /* DELETE */
//     $('#dialogConfirm').on( 'click', function( e ) {
//         e.preventDefault();
        
//         if(action_to_process.length == 0)
//             return;

//         rootRef.child('actions/' + action_to_process).remove().then(function(error){
//             // Callback comes here
//             if(error){
//                 notify('Request failed!', error, 'error');
//                 $.magnificPopup.close();
//                 action_to_process = "";
//             }
//             else{
//                 console.log("Action Delete Complete!");
                
//                 var row = $('#' + action_to_process);
//                 var name = row.find('.title').text();

//                 action_table.row(row).remove().draw();
//                 $.magnificPopup.close();
//                 notify('Request Succeed!', name + ' was deleted successfully!', 'success');
//                 action_to_process = "";
//             }
//         });
//     });
// }




function asyncFunction (item, cb) {
  setTimeout(() => {
      
    cb();
  }, 100);
}