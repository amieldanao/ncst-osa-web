// const { get } = require("jquery");

(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];
addUserCallback();
// var initial_role;
// var myRole;
function addUserCallback()
{
    let email = $("#user_email").text();

    let query = db.collection('users').where('email', '==', email);

    query.onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {


            if (change.type === "added") {
                let role = change.doc.data().role;
                initial_role = role;
                setRole(role);
                if(role == 2 || role == 1)
                {
                    $("#table_tr th").last().attr('id', 'actions_th');//append('<th id="actions_th" class="role_admin">Actions</th>');
                }
            }

            if (change.type === "modified") {
                setRole(change.doc.data().role);
                $("body").empty();
                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                 setTimeout(() => {
                    location.replace("index.html");
                }, 3000);
            }

            if (change.type === "removed") {
                // console.log("Removed user: ", change.doc.data());
                // window.location = "index.html";
            }
        });
    },
        function(error){
        console.log('retry add user callback');
        addUserCallback();
    });
}

function setRole(newRole)
{
    switch(newRole)
    {
        case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
        case 1 : $(".role_admin").remove();break;
    }
    
    $("#user_role").text("Role: " + roles_string[newRole]);
    $("#user_role").attr("data-done", true);
}

})();



var myConfig = {
    apiKey: "AIzaSyAACwDUbQbp_loXHhA8ykF0YNPT8XnaLYY",
    authDomain: "ncst-osa.firebaseapp.com",
    databaseURL: "https://ncst-osa.firebaseio.com",
    projectId: "ncst-osa",
    storageBucket: "ncst-osa.appspot.com",
    messagingSenderId: "18001671248",
    appId: "1:18001671248:web:8d9e8230c7afa6890f8d55"
};

var secondaryApp = firebase.initializeApp(myConfig, "Secondary");


'user strict'


var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);



function Start()
{
    // initVariables();
    // attachButtonCallbacks();
    // initializeStudentsTable();
    // reloadStudentsDB();
    sessionStorage.setItem("toEdit", '');
    addButtonsCallback();
    initTable();
    
}

$(document).ready( function () {
    observer.observe(targetNode, config);
//    initButtonCallbacks();
});



'use strict'

var students_table;
var file_input_event;
var localStudentsData = [];
var offenseListener = [];
// var rootRef;
// var student_to_proccess;
// var orig_email;

//<span class='label label-danger'>Deleted</span>





function addButtonsCallback()
{

    $("#checkbox_inactive").get(0).checked = false;
    document.getElementById("checkbox_inactive").addEventListener('change', (event) => {
        students_table.draw();
    })

    $('#editStudentModal').modal(
    {
        keyboard: false,
        backdrop: 'static',
        show:false
    });
    
    $('#date_of_birth').datepicker({
        autoclose:true,
        endDate:'-16y',
        disableTouchKeyboard: true
    });


    $("#addToTable").on('click', function(){
        sessionStorage.setItem('toEdit', '');
        $("#editStudentForm").get(0).reset();
    });

    $("#importToTable").on('click', function(){
        showImportModal();
    });

    $("#editStudentForm").keypress(function(event){

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
          if($("#editStudentForm").get(0).reportValidity())
            goSubmit(event);
          //alert('You pressed a "enter" key in somewhere');
        }
      });

      $("#submitButton").on('click', function(e){
        if($("#editStudentForm").get(0).reportValidity())
          goSubmit(e);
      });

    $("#editStudentModal").on('hidden.bs.modal', function(m){
        sessionStorage.setItem('toEdit', '');
    });

    addPastePrevent($("form input"));
}

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {

        // filter by student status
        var status = data[5];
        
        let checked = document.getElementById('checkbox_inactive').checked;

        return (checked == false && status == "Active" || checked);
    }
);

function initTable()
{

    let btns = [];
    if(document.getElementById("actions_th") != null)
    {
        console.log("has action column!");
        // myColumns.push({"sClass": "actions_buttons" });
        btns = [
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            }
        ]
    }

    students_table = $('#students_table').DataTable(
        {
        dom: 'Bflrtip',
        "scrollX": true,
        stateSave: true,
        columnDefs: [
            // {
            //     targets: [7,8,9,10],
            //     className: 'noVis',
            //     visible: false
            // }
        ],
        buttons: btns,        
        aoColumns: [
            { sClass: 'id center' },
            { sClass: 'email' },
            { sClass: 'f_name' },
            { sClass: 'l_name' },
            { sClass: 'contact_number' },            
            { sClass: 'status center' },            
            { sClass: 'gender' },
            { sClass: 'address' },
            { sClass: 'course' },
            { sClass: 'birthday' },
            { sClass: 'offense' },
            {
                bSortable: false,
                sClass: 'actions center'
                // ,
                // defaultContent: [
                //     '<a href="#" class="on-default reset-row" style="color:green;"><i class="fa fa-refresh"></i></a>',
                //     '<a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>',
                //     '<a href="#" class="on-default remove-row" style="color:red;"><i class="fa fa-trash-o"></i></a>',
                //     '<a href="#" class="on-default suspend-row" style="color:orange;"><i class="fa fa-pause"></i></a>'
                // ].join(' ')
            }
        ],
        initComplete: function(settings, json) {
            // alert( 'DataTables has finished its initialisation.' );
            let buttons = $("#students_table_wrapper .dt-buttons button");
            buttons.addClass("btn");
            buttons.addClass("btn-outline-primary");
            buttons.removeClass("dt-button");
            addDBListener();
        }
    }
    );

    // Change dt buttons style
    // let buttons = $("#students_table_wrapper .dt-buttons button");
    // buttons.addClass("btn");
    // buttons.addClass("btn-outline-primary");
    // buttons.removeClass("dt-button");
    // addDBListener();
}

function addDBListener()
{
    showLoading(true, "Loading students...");
    try {
          
        db.collection("students")
        .onSnapshot(function(snapshot) { 
            let requests = snapshot.docChanges().map((change) => {
                var key = change.doc.id;


                if (change.type === "added") {

                    let data = change.doc.data();
                    localStudentsData[data.ID] = data;
                    localStudentsData[data.ID]["key"] = change.doc.id;
                    
                    var d = {ID: "", email: "", contact_number : "", first_name : "", last_name : "", status : "", gender: "", address:"", course:"", birthday:""};
                    if('ID' in change.doc.data())
                        d['ID'] = change.doc.data().ID;

                    if('email' in change.doc.data())
                        d['email'] = change.doc.data().email;
                    
                    if('contact_number' in change.doc.data())
                        d['contact_number'] = change.doc.data().contact_number;
                    
                    if('first_name' in change.doc.data())
                        d['first_name'] = toTitleCase(change.doc.data().first_name);

                    if('last_name' in change.doc.data())
                        d['last_name'] = toTitleCase(change.doc.data().last_name);

                    if('status' in change.doc.data())
                        d['status'] = change.doc.data().status;

                    if('gender' in change.doc.data())
                        d['gender'] = change.doc.data().gender;

                    if('address' in change.doc.data())
                        d['address'] = change.doc.data().address;

                    if('course' in change.doc.data())
                        d['course'] = change.doc.data().course;

                    if('birthday' in change.doc.data())
                        d['birthday'] = change.doc.data().birthday;

                    let newData =
                    [
                        d.ID,
                        d.email,                        
                        d.first_name,
                        d.last_name,
                        d.contact_number,
                        d.status,                        
                        d.gender,
                        d.address,
                        d.course,
                        d.birthday,
                        0                    
                    ]

                    if(document.getElementById("actions_th") != null)
                        newData.push(getNewActionRow(change.doc.id));
                    else
                        newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickView(this);" class="on-default edit-row mx-2"><i class="fas fa-eye"></i></a>');
                                        
                    let newRow = students_table.row.add(newData).node().id = change.doc.id;

                    fetchOffenseCount(change.doc.id, d.first_name.toLocaleLowerCase(), d.last_name.toLowerCase());

                    students_table.draw( false );
                }
                
                if (change.type === "modified") {

                    let data = change.doc.data();
                    localStudentsData[data.ID] = data;
                    localStudentsData[data.ID]["key"] = change.doc.id;
                    // console.log("Modified city: ", change.doc.data());  
                    let tr = $('.remove-row[data-key="'+key+'"]').parent().parent();

                    

                    let updatedData = [
                        data.ID,                            
                        data.email,
                        toTitleCase(data.first_name),
                        toTitleCase(data.last_name),
                        data.contact_number,
                        data.status,
                        data.gender,
                        data.address,
                        data.course,
                        data.birthday,      
                        0,   
                        getNewActionRow(key)
                    ];

                    fetchOffenseCount(key, data.first_name.toLocaleLowerCase(), data.last_name.toLocaleLowerCase());

                    students_table.row(tr).data(updatedData).draw();                 
                    
                }
                if (change.type === "removed") {

                    delete localStudentsData[change.doc.data().ID];
                    // students_table.row('#'+change.doc.id).remove().draw();
                    // $('#'+change.doc.id).find(".status").text('deleted');
                }
                
                return new Promise((resolve) => {
                  asyncFunction(change, resolve);
                });
            })

            Promise.all(requests).then(() => {showLoading(false);});
        },
        function(error){

            bootbox.alert({
                centerVertical:true,
                size:'small',
                message:error.message
            }
                );
        });
    }
    catch(err) {
        bootbox.alert(
        {
            message:err.message,
            onHidden: function () {
                UpdateModalScrolling();
            }
        });
    }
}

function getLastOffenseResolve()
{
    $('#offense_count').popover({
        container: 'body',
        content : "No Previous offense",
        html : true
    });

    db.collection("offense").doc(thisReportData.suspect.toLowerCase() + "-" + thisReportData.suspect_surname.toLowerCase())
    .collection("myResolves")
    .orderBy("dateSolved").limit(1)
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {

            // doc.data() is never undefined for query doc snapshots
            // console.log(doc.id, " => ", doc.data());
            // alert(doc.id);
            $('#offense_count').popover('dispose');

            $('#offense_count').popover({
                container: 'body',
                content : doc.id + "<br><small>" + doc.data().dateSolved.toDate().toString().split("GMT")[0] + "</small>",
                html : true
            });
            

        });
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}

function fetchOffenseCount(tr_key, f_name, l_name)
{
    // if(offenseListener[tr_key] != undefined)
    //     offenseListener[tr_key]();
    
    
    
    // offenseListener[tr_key] = 
    // console.log(f_name + "-" + l_name);
    db.collection("offense").doc(f_name + "-" + l_name)
    .get().then(function(doc) {

        if(doc.exists)
        {
            
            // console.log(doc.data());
            let totalOffenseCount = doc.data().count;//doc.data().count;

            console.log(f_name + "-" + l_name + " FOUND = " + totalOffenseCount);

            let offenseCount = Math.floor(doc.data().count / 3);
            let warningCount = doc.data().count % 3;

            if(totalOffenseCount > 0)
            {
                let offenseText = "";
                let warningText = "";

                if(offenseCount == 1)
                    offenseText = "1st Offense";
                if(offenseCount == 2)
                    offenseText = "2nd Offense";
                if(offenseCount == 3)
                    offenseText = "3rd Offense";
                if(offenseCount > 3)
                    offenseText =  offenseCount + "th Offense";

                if(warningCount == 1)
                    warningText = "1st Warning";
                if(warningCount == 2)
                    warningText = "2nd Warning";
                if(warningCount == 3)
                    warningText = "3rd Warning";

                
                let completeText = offenseText + (offenseCount >= 1 ? "<br>" : "") + warningText;

                
                var row = students_table.row( $("#" + tr_key) );
                students_table.cell(row, 10).data(completeText).draw();

                console.log(f_name + "-" + l_name + " DRAW!");

                // $("#offense_count").html(offenseText + (offenseCount >= 1 ? "<br>" : "") + warningText);
                // $("#offense_count").show();
            }
            else
            {
                // $("#offense_count").hide();
            }

            //getLastOffenseResolve();
        }
        else
        {
            console.log(f_name + "-" + l_name + " NOT FOUND");
        }
    });
}

function clickView(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    var ID = tr.find(".id").text();
    var email = tr.find(".email").text();
    var f_name = tr.find(".f_name").text();
    var l_name = tr.find(".l_name").text();
    var contact_number = tr.find(".contact_number").text();
    var status = tr.find(".status").text();
    var gender = tr.find(".gender").text();
    var address = tr.find(".address").text();
    var course = tr.find(".course").text();
    var birthday = tr.find(".birthday").text();
    
    
    let completeMessage = 
        'Email : <br>'+ email + '<br><hr>' +
        'First Name : <br>'+ f_name + '<br><hr>' +
        'Last Name : <br>'+ l_name + '<br><hr>' +
        'Contact Number : <br>'+ contact_number + '<br><hr>' +    
        'Gender : <br>'+ gender + '<br><hr>' +
        'Address : <br>'+ address + '<br><hr>' +
        'Course : <br>'+ course + '<br><hr>' +
        'Birthday : <br>'+ birthday + '<br><hr>' +
        'Status : <br>'+ status + '<br><hr>';


    bootbox.alert(
    {
        title: ID,
        message : completeMessage,
        centerVertical : true,
        scrollable : true
    });
}

function goSubmit(e)
{
    showLoading(true, "Saving...");
    var key = sessionStorage.getItem('toEdit');
    var isNew = (key == undefined || key.length == 0);
    // Check if ID or Email already exist.
    let id_query = db.collection('students').where('ID', '==', $("#student_id").val()).where('email', '==', $("#email").val());

    try
    {
        id_query.get().then(function(querySnapshot) {
            // console.log(querySnapshot.docs[0].data());

            if(querySnapshot.size > 0 && (isNew || querySnapshot.docs[0].id != key))
            {
                showLoading(false);
                bootbox.alert({
                    message:"Student ID/Email already in use!",
                    centerVertical: true,
                    className : "topmost-modal",
                    size : 'small'
                });
                
                sessionStorage.setItem("toEdit", '');

                return;
            }
            else
            // Form is valid, continue saving
            {

                // We need to check if any admin is already using this email

                let email_query = db.collection('users').where('email', '==', $("#email").val());

                email_query.get().then(function(querySnapshot) {

                    if(querySnapshot.size > 0)
                    {
                        showLoading(false);
                        bootbox.alert({
                            message:"Student ID/Email already in use!",
                            centerVertical: true,
                            className : "topmost-modal",
                            size : 'small'
                        });
                        sessionStorage.setItem("toEdit", '');

                        return;
                    }
                    else
                    {
                        var ref = db.collection("students").doc();
    
                        if(key == undefined || key.length == 0)
                        {
                            key = ref.id;
                        }
                        else
                        {
                            ref = db.collection("students").doc(key);
                        }
                        
                        sessionStorage.setItem("toEdit", "");

                        let data = 
                        {
                            ID: $("#student_id").val(), 
                            email: $("#email").val(),
                            contact_number : $("#contact_number").val(), 
                            first_name : $("#first_name").val().toLowerCase(),
                            last_name : $("#last_name").val().toLowerCase(), 
                            status : $("#status").val(), 
                            gender: $("#gender").val(), 
                            address: $("#address").val(), 
                            course: $("#course").val(), 
                            birthday: $("#date_of_birth").val()
                        };

                        if(isNew)
                        {
                            secondaryApp.auth().createUserWithEmailAndPassword($("#email").val(), $("#student_id").val())
                            .then(function(result) {
                                
                                secondaryApp.auth().sendPasswordResetEmail(data.email).then(function() {
                                    PNotify.alert({
                                        title:'New Student added successfully.',
                                        text : 'Password reset sent to ' + data.email
                                    });

                                    secondaryApp.auth().signOut();

                                    data['password'] = $("#student_id").val();
                                    setDataOnDBFromForm(data, ref);
                                })
                                .catch(function(error){
                                    PNotify.error({
                                      title: 'Error',
                                      text: error
                                  });
                                    showLoading(false);
                                });
                                
                            })
                            .catch(error => {
                                // console.error("Error adding document: ", error);
                                PNotify.error({
                                  title: 'Error',
                                  text: error
                              });
                                showLoading(false);

                            });
                        }
                        else
                        {
                            setDataOnDBFromForm(data, ref);
                        }

                    }
                })
                .catch(error => {
                    // console.error("Error adding document: ", error);
                    PNotify.error({
                      title: 'Error',
                      text: error
                  });
                    showLoading(false);

                });
            }

        });
    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(error.message);        
    }
}

function setDataOnDBFromForm(data, ref)
{
    ref.set(data, {merge:true})
    .then(function(docRef) {
        showNotifSuccess('Saved', capitalizeFLetter(data.ID) + ' saved successfully');
        showLoading(false);

        // sessionStorage.setItem("toEdit", '');

        $('#editStudentModal').modal('hide');
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
        PNotify.error({
          title: 'Error',
          text: error
      });
        showLoading(false);
    });
}

function showImportModal()
{
    bootbox.dialog({
        show:true,
        title: 'Import from file',
        message: 
        '<small style="font-style:italic; color:red;">Note: Importing data from external file will override existing records on the database!</small>'+
        '<div class="input-group mb-3">'+
          '<div class="custom-file">'+
            '<input type="file" class="custom-file-input" id="file_input" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">'+
            '<label class="custom-file-label" for="file_input">Choose file</label>'+
          '</div>'+
        '</div>'
        // '<div class="form-check form-check-inline">'+
        //   '<input id="override" class="form-check-input" type="checkbox" value="option1">'+
        //   '<label class="form-check-label" for="override">Override Student status <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="When checked, all status of the students will be based in the file, if there\'s no status in the file, students status will be set to Active">?</span></label>'+
        // '</div>'
        ,
        size:'large',
        centerVertical:true,
        buttons: {
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function(){

                    
                }
            },
            ok: {
                label: "Ok",
                className: 'btn-success',
                callback: function(){
                    // sessionStorage.setItem("toEdit", '');

                    // processExcelFile(file_input_event, $("#override").get(0).checked);
                    processExcelFile(file_input_event);
                }
            }
        }
    });

    $("#file_input").get(0).onchange = function(e) { 
        console.log(e);
        file_input_event = e;
        $("#file_input").get(0).parentNode.getElementsByTagName('label')[0].innerText = e.target.files[0].name;
    };    
}


function processExcelFile(e){
    showLoading(true, "importing file...");

    try
    {
        var files = e.target.files, f = files[0];
        var reader = new FileReader();
        reader.onload = function(e) {
        var data = new Uint8Array(e.target.result);
        var workbook = XLSX.read(data, {type: 'array'});
        var studentsToBeDeleted = [];



        for(var k in localStudentsData) 
            studentsToBeDeleted[k]= localStudentsData[k];


        console.log("students to be deleted");
        console.log(studentsToBeDeleted);

        /* DO SOMETHING WITH workbook HERE */
            workbook.SheetNames.forEach(function(sheetName){
                if(sheetName == "students")
                {
                    var errorDatas = [];
                    var emails_supplied = new Array();

                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                    // var json_object = JSON.stringify(XL_row_object);
                    


                    let requests = XL_row_object.map((obj) => {

                        if(obj.ID in studentsToBeDeleted){
                            // console.log(obj.last_name + " exist in file");
                            delete studentsToBeDeleted[obj.ID];
                        }

                        // let requests = XL_row_object.map((obj) => {
                        // console.log(obj);
                        // addNewRow(obj);
                        var indexes = students_table
                          .rows()
                          .indexes()
                          .filter( function ( value, index ) {
                            return obj.ID === students_table.row(value).data()[0];
                        });

                        // we want to set defaults if this student is newly added
                        if(indexes.length == 0)
                        {
                            obj['password'] = obj.ID;
                            // if(!override || !'status' in obj)
                        }                      
                        

                        obj['status'] = "Active";

                        obj['contact_number'] = obj['contact_number'].toString();

                        obj['first_name'] = obj.first_name.toLowerCase();
                        obj['last_name'] = obj.last_name.toLowerCase();


                        //convert birthday to proper date format
                        if(!isNaN(obj.birthday))
                        {
                            let date = ExcelDateToJSDate(obj.birthday);
                            // console.log(date);
                            
                            obj.birthday = moment.utc(date.toString()).format("L").toString();
                        }
                        else
                        {
                            let date = moment(obj.birthday, "L").toString()
                            obj.birthday = moment.utc(date).format("L").toString();;
                        }

                        if("email" in obj)
                            obj.email = obj.email.toLowerCase();

                        var same_email = students_table
                          .rows()
                          .indexes()
                          .filter( function ( value, index ) {
                            // console.log(students_table.row(value).data()[1]);
                            return obj.email == students_table.row(value).data()[1];
                        });

                        // try
                        // {
                           // console.log("same email length " + same_email.length);

                            // If this student is already in database, and his email is not already in use by others
                            if(same_email.length == 0 && !emails_supplied.includes(obj.email))
                            {
                                if(!emails_supplied.includes(obj.email))
                                    emails_supplied.push(obj.email);
                                console.log(obj.email + " doesn't exists");
                                console.log(same_email);
                                try
                                {
                                    secondaryApp.auth().createUserWithEmailAndPassword(obj.email, obj.password)
                                    .then(function(result) {

                                        // Update sutdent info on database.
                                        
                                        setDataOnDB(obj);
                                        secondaryApp.auth().signOut();
                                        
                                    })
                                    .catch(error => {

                                        // Send a password reset
                                        if(error.code == 'auth/email-already-in-use')
                                        {                                            
                                            secondaryApp.auth().sendPasswordResetEmail(obj.email).then(function() {

                                                secondaryApp.auth().signOut();
                                                // We need to check this email if it is used by any admin
                                            let email_query = db.collection('users').where('email', '==', obj.email);

                                            email_query.get().then(function(querySnapshot) {

                                                if(querySnapshot.size > 0)
                                                {
                                                    let newErrorData = obj.ID + " : " + obj.first_name + " " + obj.last_name + " : " + obj.email;

                                                    PNotify.error({                                                        
                                                        title: 'Email already in use',
                                                        text: newErrorData
                                                    });
                                                }
                                                else
                                                {
                                                    // if(!emails_supplied.includes(obj.email))
                                                    //     emails_supplied.push(obj.email);
                                                    setDataOnDB(obj);
                                                }
                                            });

                                              // Email sent.
                                              
                                            }).catch(function(error) {
                                              // An error happened.
                                              let newErrorData = obj.ID + " : " + obj.first_name + " " + obj.last_name + " : " + obj.email;
                                                if(errorDatas.indexOf(newErrorData) == -1)
                                                    errorDatas.push(newErrorData);
                                            });
                                            
                                        }
                                        else{
                                            let newErrorData = obj.ID + " : " + obj.first_name + " " + obj.last_name + " : " + obj.email;
                                            if(errorDatas.indexOf(newErrorData) == -1)
                                                errorDatas.push(newErrorData);
                                        }
                                        
                                        console.log('error msg = ' + error.code);
                                    });
                                }
                                catch(error)
                                {
                                    let newErrorData = obj.ID + " : " + obj.first_name + " " + obj.last_name + " : " + obj.email;
                                    if(errorDatas.indexOf(newErrorData) == -1)
                                        errorDatas.push(newErrorData);
                                }
                            }
                            else
                            {
                                if(!emails_supplied.includes(obj.email))
                                    emails_supplied.push(obj.email);
                                // Email already on table, but that's his email so continue updating
                                if(same_email.length > 0 && students_table.row(same_email[0]).data()[0] == obj.ID)
                                {
                                    firebase.auth().sendPasswordResetEmail(obj.email).then(function() {
                                      // Email sent.
                                    

                                      setDataOnDB(obj);
                                    }).catch(function(error) {
                                      // An error happened.
                                      let newErrorData = obj.ID + " : " + obj.first_name + " " + obj.last_name + " : " + obj.email;
                                        if(errorDatas.indexOf(newErrorData) == -1)
                                            errorDatas.push(newErrorData);
                                    });
                                }
                                else
                                {
                                    console.log(obj.email + " already exists");
                                    console.log(same_email[0]);
                                    let newErrorData = obj.ID + " : " + obj.first_name + " " + obj.last_name + " : " + obj.email;
                                    if(errorDatas.indexOf(newErrorData) == -1)
                                        errorDatas.push(newErrorData);
                                }
                            }
                      

                        return new Promise((resolve) => {
                          asyncFunction(obj, resolve);
                        });
                    });

                    // Done processing all data from file
                    Promise.all(requests).then(() => {


                        processStudentsForDeletion(studentsToBeDeleted, errorDatas);
                        
                    });

                    // End row item
                    // Promise.all(requests).then(function() {
                        
                    // });

                }
            });

            // console.log(workbook);
            // showLoading(false);
        };

        reader.onerror = function(event){
            showLoading(false);
            bootbox.alert(e.target.error.code);
        }

        reader.readAsArrayBuffer(f);
    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(error.message);
    }
}

function processStudentsForDeletion(studentsToBeDeleted, errorDatas)
{
    console.log(studentsToBeDeleted);
    let requests = Object.keys(studentsToBeDeleted).map((studentID) => {

        // studentToDelete["status"] = "Deleted";
        console.log("this student key is " + studentsToBeDeleted[studentID].key);

        db.collection("students").doc(studentsToBeDeleted[studentID].key)
        .update({status : "Deleted"})
        .then(function() {
            console.log("Document successfully updated!");
        })
        .catch(function(error) {
            // The document probably doesn't exist.
            console.error("Error updating document: ", error);
        });

        return new Promise((resolve) => {
            asyncFunction(studentID, resolve);
        });
    });

    // Done processing all data from file
    Promise.all(requests).then(() => {

        showLoading(false);
        console.log("Show Import summary");

        console.log('errors length : ' + errorDatas.length)
        if(errorDatas.length > 0)
        {
            let errorImportMessage = '';

            errorDatas.forEach(function(e){
                errorImportMessage += errorDatas + "<br>";
            });

            errorImportMessage +='<br> The students ID(s) above will not be imported';

            bootbox.alert({
                title : 'There are email(s) that is invalid or already been taken',
                message : errorImportMessage,
                centerVertical : true,
                scrollable : true

            });
        }
        else
        {
            bootbox.alert({
                size:'small',
                title : 'Import done',
                message : 'Complete',
                centerVertical : true
            });
        }
    });
}



function setDataOnDB(obj)
{
    let id_query = db.collection('students').where('ID', '==', obj.ID);

    id_query.get().then(function(querySnapshot) {
        var ref = db.collection("students").doc();
        var key = ref.id; 

        if(querySnapshot.size > 0)
        {
            key = querySnapshot.docs[0].id;
            ref = db.collection("students").doc(key);
        }

        ref.set(obj, {merge:true})
        .then(function(docRef) {

        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
            PNotify.error({
              title: 'Error',
              text: error
          });
        });
    });
}





function getNewActionRow(key)
{
    return  '<a data-key="'+key+'" href="#" onclick="showPromptReset(this);" class="on-default reset-row mx-2" data-toggle="tooltip" data-placement="top" title="reset student status" style="color:green;"><i class="fa fa-refresh"></i></a>'+
            '<a data-key="'+key+'" href="#" onclick="showPromptEdit(this);" class="on-default edit-row mx-2" data-toggle="tooltip" data-placement="top" title="edit student information"><i class="fa fa-pencil"></i></a>'+
            '<a data-key="'+key+'" href="#" onclick="showPromptDelete(this);" class="on-default remove-row mx-2" data-toggle="tooltip" data-placement="top" title="delete student" style="color:red;"><i class="fa fa-trash-o"></i></a>'+
            '<a data-key="'+key+'" href="#" onclick="showPromptSuspend(this);" class="on-default suspend-row mx-2" data-toggle="tooltip" data-placement="top" title="suspend student" style="color:orange;"><i class="fa fa-pause"></i></a>';



                    // '<a data-key="'+change.doc.id+'" href="#" target="_blank" onclick="gotoMessage('+change.doc.id+');" class="on-default edit-row mx-2"><i class="fa fa-envelope"></i></a>',
}

function showPromptSuspend(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    let email = students_table.row(tr).data()[1];
    let g = students_table.row(tr).data()[6];
    // sessionStorage.setItem("toEdit", key);
    console.log(email);

    var gender = (g=="male")? 'him' : 'her';

    bootbox.confirm({ 
        size: "large",
        centerVertical : true,
        title: "Are you sure?",
        message : "Suspending a student will prevent " + gender + " to access to application <br> Continue? <br><b>"+ email +"</b>",
        callback: function(result){ 
            if(result)
            {
                showLoading(true, "Suspending...");
                try
                {

                    db.collection('students').doc(key).set({status: 'Suspended'}, {merge:true})
                    .then(function(docRef) {

                        showLoading(false);

                        PNotify.alert({
                            text: email + ' suspended successfully.'
                        });

                    })
                    .catch(function(error) {
                        showLoading(false);
                        bootbox.alert(error);
                    });

                    
                }
                catch(error)
                {
                    showLoading(false);
                    bootbox.alert(error.message);
                }
            }

            sessionStorage.setItem("toEdit", '');
        }
    })
}


function showPromptDelete(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    let email = students_table.row(tr).data()[1];
    let student_id = students_table.row(tr).data()[0];
    // sessionStorage.setItem("toEdit", key);
    console.log(email);

    bootbox.confirm({ 
        size: "large",
        centerVertical : true,
        title: "Are you sure?",
        message : "Deleting a student will deactivate a student and will prevent access to application <br> Continue? <br><b>"+ email +"</b>",
        callback: function(result){ 
            if(result)
            {
                showLoading(true, "Deleting...");
                try
                {

                    db.collection('students').doc(key).set({status: 'Deleted'}, {merge:true})
                    .then(function(docRef) {

                        showLoading(false);

                        PNotify.alert({
                            text: email + ' deleted successfully.'
                        });
                        // Delete profile pic
                        var picRef = firebase.storage().ref().child('profileImages/'+student_id+'.jpeg');

                        // Delete the file
                        picRef.delete().then(function() {
                          // File deleted successfully
                        }).catch(function(error) {
                          // Uh-oh, an error occurred!
                        });



                    })
                    .catch(function(error) {
                        showLoading(false);
                        bootbox.alert(error.message);
                    });

                    
                }
                catch(error)
                {
                    showLoading(false);
                    bootbox.alert(error.message);
                }
            }

            sessionStorage.setItem("toEdit", '');
        }
    })
}


function showPromptReset(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    var student_data = students_table.row(tr).data();
    console.log(student_data);
    let email = student_data[1];
    // sessionStorage.setItem("toEdit", key);
    console.log(email);

    // var student_new_data = {
    //     ID: student_data[0],
    //     email : student_data[1],
    //     address:student_data[7],
    //     birthday:student_data[9],
    //     contact_number:student_data[4],
    //     course:student_data[8],
    //     first_name:student_data[2],
    //     last_name:student_data[3],
    //     gender:student_data[6],
    //     status:"Active"
    // };

    bootbox.confirm({ 
        size: "large",
        centerVertical : true,
        title: "Are you sure?",
        message : "Resetting a student will sent an password reset to the student email and also reactivating it's status. <br> Continue? <br><b>"+ email +"</b>",
        callback: function(result){ 
            if(result)
            {
                showLoading(true, "Resetting...");

                // db.collection("students").doc(key).delete().then(function() {
                    
                // }).catch(function(error) {
                    
                // });

                try
                {
                    db.collection('students').doc(key).set({status: 'Active'}, {merge:true})
                    .then(function(docRef) {

                       firebase.auth().sendPasswordResetEmail(email).then(function() {
                            showLoading(false);

                            PNotify.alert({
                                text: email + ' reset successfully.'
                            });
                        }).catch(function(error) {
                            showLoading(false);
                            bootbox.alert(error);

                        });
                    })
                    .catch(function(error) {
                        showLoading(false);
                        bootbox.alert(error);
                    });;

                    
                }
                catch(error)
                {
                    showLoading(false);
                    bootbox.alert(error.message);
                }
            }

            sessionStorage.setItem("toEdit", '');
        }
    })
}


function showPromptEdit(elem)
{
    var key = $(elem).attr('data-key');
    sessionStorage.setItem("toEdit", key);


    var tr = $(elem).parent().parent();

    var myRow = students_table.row(tr);

    // Fill form based on selected student data.    
    $("#student_id").val(myRow.data()[0]);
    $("#email").val(myRow.data()[1]);
    $("#first_name").val(myRow.data()[2]);
    $("#last_name").val(myRow.data()[3]);
    $("#contact_number").val(myRow.data()[4]);
    $("#status").val(myRow.data()[5]);
    $("#gender").val(myRow.data()[6]);
    $("#address").val(myRow.data()[7]);
    $("#course").val(myRow.data()[8]);
    $("#date_of_birth").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    }).datepicker("update", myRow.data()[9]); 

    

    $("#editStudentModal").modal('show');
}

function asyncFunction (item, cb) {
  setTimeout(() => {

    cb();
}, 100);
}







// function updateStudent()
// {
//     //console.log("trying to save : " + student_id);

//     //check if birthday is valid
//     if(!validate($('#date_of_birth').val()))
//     {
//         $('#date_hint').text('Invalid date! Student must be atleast 16 years old!');
//         return;
//     }

//     var editedNewEmail = $('#email').val();

//     if(ValidateEmail(editedNewEmail) == false || editedNewEmail.length == 0)
//     {   
//         $("#email_hint").text("The edited email has invalid format!");
//         return;
//     }
//     else
//     {
//         $("#email_hint").text("");
//     }

//     rootRef.child("students").orderByChild('email')
//     .equalTo(editedNewEmail)
//     .once('value')
//     .then(function (snapshot) {
//         var value = snapshot.val();
//         if (value) {
//             console.log(value);
//             var modifiedID = Object.keys(value)[0];
//             console.log(student_to_proccess + " == " + modifiedID);
//             if(student_to_proccess != modifiedID){
//                 console.log("The edited email is invalid or already in use!");
//                 $("#email_hint").text("The edited email is invalid or already in use!");
//                 student_to_proccess = "";
//                 return;
//             }
//             else{
//                 continueRegister(student_to_proccess);
//             }
//         }
//         else 
//         {
//             console.log("email available");
//             if(student_to_proccess.length == 0)
//             {
//                 var pass = $("#student_id").val();
//                 firebase.auth().createUserWithEmailAndPassword(editedNewEmail, pass)
//                 .then(function(result) {
//                 	result.user.updateProfile({	  
//                 	    displayName: $('#first_name').text() + " " + $('#last_name').text()
//                   })
//                   .then(function(){
//                         continueRegister("");  
//                   });
                    
//                 })
//                 .catch(error => {
                    
//                 	switch (error.code) {
//                 		case 'auth/email-already-in-use':
//                             if(orig_email != editedNewEmail){
//                                 console.log("The edited email is invalid or already in use!");
//                                 $("#email_hint").text("The edited email is invalid or already in use!");
//                                 student_to_proccess = "";
//                                 return;
//                             }
            
//                 		break;
//                 		case 'auth/invalid-email':
//                             // console.log(`Email address ${this.state.email} is invalid.`);
//                             ShowMessage('Warning!', 'Email : ' + email + ' is invalid', 'warning');
//                 		break;
//                 		case 'auth/operation-not-allowed':
//                             // console.log(`Error during sign up.`);
//                             ShowMessage('Warning!', 'Error during sign up', 'warning');
//                 		break;
//                 		case 'auth/weak-password':
//                             // console.log('Password is not strong enough. Add additional characters including special characters and numbers.');
//                             ShowMessage('Warning!', 'Password is not strong enough. Add additional characters including special characters and numbers.', 'warning');
//                 		break;
//                 		default:
//                             // console.log(error.message);
//                             ShowMessage('Error last', error.message, 'danger');
//                 		break;
//                 	}
                    
//                 	console.log('error msg = ' + error.code);
//                 });
//             }
//             else{
//                 console.log(orig_email + " == " + editedNewEmail);
//                 if(orig_email != editedNewEmail)
//                 {
//                     //update email on firebase auth
//                     console.log("You changed the email!");
                    
//                     rootRef.child("students/" + student_to_proccess).once('value').then(function (snap){
//                         if(snap.exists()){
//                             firebase.auth()
//                             .signInWithEmailAndPassword(orig_email, snap.child("password").val())
//                             .then(function(userCredential) {
//                                 userCredential.user.updateEmail(editedNewEmail)
//                                 console.log("Log in successfull");
//                                 continueRegister(student_to_proccess);
//                             })
//                             .catch(error => {
//                                 console.log(error.code);
//                                 if(error.code == 'auth/email-already-in-use'){
//                                     $("#email_hint").text("The edited email is invalid or already in use!");
//                                     student_to_proccess = "";
//                                 }
//                             });
//                         }
//                     });
                    
//                 }
//                 else
//                 {
//                     continueRegister(student_to_proccess);
//                 }
//             }
//         }
//     });
// }

// function continueRegister(student_id){
//     var editedNewID = "";
//     if($('#student_id').val().length > 0)
//         editedNewID = $('#student_id').val().split('-').join('_');

//     //check if id already exist
//     rootRef.child('students/' + editedNewID).once('value').then(function(snap){
//         console.log(snap.exists());
//         //allow same id OR new id does not conflict with existing id 
//         var theStatus = "<span class='label label-success'>Active</span>";

//         if(student_id.length > 0)
//             theStatus = $('#row_'+student_id + ' > .status').html();
//         //OR id remains unedited AND conflict is  OR student is new
//         if((snap.exists() == false) || (snap.exists() == true && editedNewID == student_id && student_id.length > 0) || (student_id.length == 0 && snap.exists() == false))
//         {
//             //remove old node in database if edited with new student ID
//             var formattedBday = $('#date_of_birth').val().split('/').join('-');
//             var postData = {
//                 email: $('#email').val(),
//                 first_name: $('#first_name').val(),
//                 last_name: $('#last_name').val(),
//                 birthday: formattedBday,
//                 gender: $('#gender').val(),
//                 course : "BSIT",
//                 status : theStatus,
//                 contact_number : $('#contact_number').val(),
//                 address: $('#address').val(),
//                 password: $('#student_id').val()
//             };

//             //console.log(postData.birthday);

//             //Update table row only
//             if(student_id.length > 0)
//             {
//                 var affectedRow = $("#row_" + student_id);
//                 var rowIndex = affectedRow.index();
//                 var temp = students_table.row(rowIndex).data();
//                 temp[0] = editedNewID.split('_').join('-');
//                 temp[1] = postData.email;
//                 temp[2] = postData.contact_number;
//                 temp[3] = postData.first_name + " " + postData.last_name;
//                 //temp[4] = postData.status;
//                 temp[5] = generateNewActions();
//                 temp[6] = postData.gender;
//                 temp[7] = postData.address;
//                 temp[8] = postData.course;
//                 temp[9] = postData.birthday;
//                 students_table.row(rowIndex).data(temp).invalidate();

//                 //remove old node if new id is valid and doesnt exists
//                 if(snap.exists() == false && editedNewID != student_id)
//                 {
//                     rootRef.child('students/' + student_id).remove();
//                 }
//             }
//             else//if this is new student, add new row to table
//             {
//                 var new_actions_row = generateNewActions();

//                 var newData = [
//                     editedNewID.split('_').join('-'),
//                     postData.email,
//                     postData.contact_number,
//                     postData.first_name + " " + postData.last_name,
//                     "<span class='label label-success'>Active</span>",
//                     new_actions_row,
//                     postData.gender,
//                     postData.address,
//                     postData.course,
//                     $('#date_of_birth').val()
//                 ];
                
//                 students_table.row.add(newData).draw().node().id = 'row_' + editedNewID;

//                 var td = $('#row_'+ editedNewID).find(".full_name");
//                 td.attr('data-fname', postData.first_name);
//                 td.attr('data-lname', postData.last_name);
//             }

//             //Write the new post's data simultaneously in the posts list and the user's post list.
//             var updates = {};
//             //add default password for new accounts
//             if(student_id.length == 0)
//                 postData.password = $('#student_id').val();
//             updates['students/' + editedNewID] = postData;

//             rootRef.update(updates);
//             notify('Request succeed!', editedNewID + ' updated successfully', 'success');
//             $.magnificPopup.close();
//             console.log("updated !");
//         }
//         else
//         {
//             $('#dialogConfirmButton').off('click');
            
//             setGenericDialogMessage('Invalid', 'The edited student ID is invalid or already exist!', 'Ok');
            
//             $.magnificPopup.open({
//                 items: {
//                     src: '#genericDialog',
//                     type: 'inline'
//                 },
//                 preloader: false,
//                 modal: true
//             });

//             $('#dialogConfirmButton').on('click', function(){
//                 $.magnificPopup.close();
//                 //re open edit form dialog after conforming
//                 $.magnificPopup.open({
//                     items: {
//                         src: '#form',
//                         type: 'inline'
//                     },
//                     preloader: false,
//                     modal: true
//                 });
//             });
//         }
//     });
// }

// function validate(date){
//     var sixteenYearsAgo = moment().subtract(16, "years");
//     var birthday = moment(date);

//     if (!birthday.isValid()) {
//         return false;    
//     }
//     else if (sixteenYearsAgo.isAfter(birthday)) {
//         return true;    
//     }
//     else {
//         return false;    
//     }
// }

// function generateNewActions()
// {
//     var theActions = 
//     "<a href='#' onclick='resetStudent(this)' class='on-default reset-row'><i class='fa fa-refresh'></i></a>"+
//     "<a href='#' onclick='editStudent(this)' class='on-default edit-row'><i class='fa fa-pencil'></i></a>"+
//     "<a href='#' onclick='deleteStudent(this)' class='on-default remove-row'><i class='fa fa-trash-o'></i></a>"+
//     "<a href='#' onclick='suspendStudent(this)' class='on-default suspend-row'><i class='fa fa-pause'></i></a>";
//     return theActions;
// }

// function setGenericDialogMessage(title, msg, btnTxt)
// {
//     $('#dialogConfirmButton').off('click');
//     $('#genericDialogTitle').text(title);
//     $('#genericDialogMessage').text(msg);
//     $('#dialogConfirmButton').text(btnTxt);
// }

// function editStudent(me)
// {
//     $.magnificPopup.open({
//         items: {
//             src: '#form',
//             type: 'inline'
//         },
//         preloader: false,
//         modal: true
//     });

//     var evID = me.closest('tr');//.attr("val");
//     student_to_proccess = evID.id.split("row_")[1];
//     console.log(student_to_proccess);
//     //student ID
//     $('#student_id').val($('#'+evID.id + ' > .id').text().replace('_','-'));
//     //student email
//     orig_email = $('#'+evID.id + ' > .email').text();
//     $('#email').val(orig_email);
    
//     var full_name = $('#'+ evID.id).find(".full_name");
//     //student first name
//     $('#first_name').val(full_name.attr("data-fname"));
//     //student last name
//     $('#last_name').val(full_name.attr("data-lname"));

//     //for hidden columns
//     var dd = students_table.row(evID).data();
//     //console.log(dd);
//     //console.log(dd['Gender']);

//     //student birthday 
//     var bday = dd[9].split("-").join("/");
//     //console.log(bday);
//     $('#date_of_birth').val(bday);
//     //student gender
//     $('#gender').val(dd[6]);
//     //student contact number
//     $('#contact_number').val($('#'+evID.id + ' > .contact_number').text());
//     //student address
//     $('#address').val(dd[7]);

    
// }

// function suspendStudent(me)
// {
//     var evID = me.closest('tr');
//     student_to_proccess = evID.id.split("row_")[1];
//     //student_to_proccess = student_id;

//     $.magnificPopup.open({
//         items: {
//             src: '#suspend',
//             type: 'inline'
//         },
//         preloader: false,
//         modal: true
//     });
// }

// function resetStudent(me)
// {
//     var evID = me.closest('tr');
//     student_to_proccess = evID.id.split("row_")[1];

//     $.magnificPopup.open({
//         items: {
//             src: '#reset',
//             type: 'inline'
//         },
//         preloader: false,
//         modal: true
//     });
// }

// function deleteStudent(me)
// {
//     var evID = me.closest('tr');
//     student_to_proccess = evID.id.split("row_")[1];

//     $.magnificPopup.open({
//         items: {
//             src: '#dialog',
//             type: 'inline'
//         },
//         preloader: false,
//         modal: true
//     });
// }

// function reloadStudentsDB(){
//     console.log('start reading all students');
//     showLoading(true);
//     var students = [];

//     rootRef.child('students').once('value').then(function(snap){
//         //read all students from db
//         var students_count = snap.numChildren();
//         var i = 0;

//         if(students_count == 0)
//             showLoading(false);

//         snap.forEach(function(student){
//             //var id_string = student.key + "";
//             //console.log(id_string);
//             var birthday = student.child('birthday').val();
//             var formatted_bday = "";
//             if(birthday != null)
//                 formatted_bday = birthday.split('/').join("-");

//             var data = {
//                 ID : student.key,
//                 first_name: student.child('first_name').val(),
//                 last_name : student.child('last_name').val(),
//                 email: student.child('email').val(),
//                 contact_number:student.child('contact_number').val(),
//                 gender: student.child('gender').val(),
//                 address : student.child('address').val(),
//                 course : student.child('course').val(),
//                 status : student.child('status').val(),
//                 birthday : formatted_bday
//             };

//             //var status = ()? "<span class='label label-danger'>Deleted</span>": "<span class='label label-danger'>Deleted</span>";
//             var theActions = 
//             "<a href='#' onclick='resetStudent(this)' class='on-default reset-row'><i class='fa fa-refresh'></i></a>"+
//             "<a href='#' onclick='editStudent(this)' class='on-default edit-row'><i class='fa fa-pencil'></i></a>"+
//             "<a href='#' onclick='deleteStudent(this)' class='on-default remove-row'><i class='fa fa-trash-o'></i></a>"+
//             "<a href='#' onclick='suspendStudent(this)' class='on-default suspend-row'><i class='fa fa-pause'></i></a>";

//             var newData = [
//                 student.key.replace('_', '-'),
//                 data.email,
//                 data.contact_number,
//                 data.first_name + " " + data.last_name,
//                 data.status,
//                 theActions,
//                 student.child('gender').val(),
//                 student.child('address').val(),
//                 student.child('course').val(),
//                 formatted_bday
//             ];
    
//             var newRow = students_table.row.add(newData);
//             newRow.node().id = 'row_' + data.ID;
            
//             students[i] = {
//                 id : data.ID,
//                 fname : data.first_name,
//                 lname : data.last_name
//             }

//             i++;

//             if(i >= students_count)
//             {
//                 showLoading(false);
//                 console.log('done reading all students');
//                 students_table.draw();

//                 for (let z = 0; z < students.length; z++) {
//                     var element = students[z];
//                     //console.log($('#row_'+ element.id).find(".full_name"));
//                     var td = $('#row_'+ element.id).find(".full_name");
//                     td.attr('data-fname', element.fname);
//                     td.attr('data-lname', element.lname);
                    
//                     //alert(td.attr('data-fname'));
//                 }
//             }
//         });
//     });
// }

// function initVariables()
// {
//     rootRef = firebase.database().ref();
//     student_to_proccess = "";
//     $('#date_of_birth').datepicker();

//     $('#form').parent().parent().on("scroll", function() {
//         console.log('scrolling');
// 		$('#date_of_birth').datepicker('place');
// 	});
// }

// function loadDataFromFile()
// {
//     $('#importPanel').trigger('loading-overlay:show');

//     var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;  
//     /*Checks whether the file is a valid excel file*/  
//     if (regex.test($("#excelfile").val().toLowerCase())) {  
//         var xlsxflag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/  
//         if ($("#excelfile").val().toLowerCase().indexOf(".xlsx") > 0) {  
//             xlsxflag = true;  
//         }  
//         /*Checks whether the browser supports HTML5*/  
//         if (typeof (FileReader) != "undefined") {  
//             var reader = new FileReader();  
//             reader.onload = function (e) {
                
//                 var data = e.target.result;  
//                 /*Converts the excel data in to object*/  
//                 if (xlsxflag) {  
//                     var workbook = XLSX.read(data, { type: 'binary' });  
//                 }  
//                 else {  
//                     var workbook = XLS.read(data, { type: 'binary' });  
//                 }  
//                 /*Gets all the sheetnames of excel in to a variable*/  
//                 var sheet_name_list = workbook.SheetNames;  
    
//                 var cnt = 0; /*This is used for restricting the script to consider only first sheet of excel*/  
//                 sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/  
//                     /*Convert the cell value to Json*/  
//                     if (xlsxflag) {  
//                         var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);  
//                     }  
//                     else {  
//                         var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);  
//                     }

//                     var proceed = true;
//                     if(cnt == 0)
//                     {
//                         let excelRowsObjArr = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[y]);

//                         if(excelRowsObjArr.length <= 0)
//                         {
//                             console.log("error file doesn't seem to be properly formatted");
//                             $('#importPanel').trigger('loading-overlay:hide');
//                             notify('Request Failed!', 'File is corrupted!', 'error');
//                             $.magnificPopup.close();
//                             $(".importButtons").show();
//                             proceed == false;
//                         }
//                     }

//                     if (exceljson.length > 0 && cnt == 0 && proceed) {  
//                         //console.log(exceljson);
//                         BindTable(exceljson, '#students-data');  
//                     }
//                     cnt++;
//                 });  
//                 //$('#students-data').show();  
//             }

//             if (xlsxflag) {/*If excel file is .xlsx extension than creates a Array Buffer from excel*/  
//                 reader.readAsArrayBuffer($("#excelfile")[0].files[0]);  
//             }  
//             else {  
//                 reader.readAsBinaryString($("#excelfile")[0].files[0]);  
//             }  
//         }  
//         else {  
//             //alert("Sorry! Your browser does not support HTML5!");  
            
//             $('#importPanel').trigger('loading-overlay:hide');
//             notify('Request Failed!', 'Sorry! Your browser does not support HTML5!', 'error');
//             $.magnificPopup.close();
//         }  
//     }  
//     else {  
//         //alert("Please upload a valid Excel file!");

//         $('#importPanel').trigger('loading-overlay:hide');
//         notify('Request Failed!', 'Please upload a valid Excel file!', 'error');
//         $.magnificPopup.close();
//     }  
// }

// function attachButtonCallbacks()
// {
//     //allow lowercase only on email
//     $('#email').keyup(function () {
//         $('#email').val($('#email').val().toLowerCase());
//     });

//     //cancels
//     $("#importCancel").on('click', function(){$.magnificPopup.close();});
//     $("#resetCancel").on('click', function(){$.magnificPopup.close(); student_to_proccess = "";});
//     $("#dialogCancel").on('click', function(){$.magnificPopup.close(); });
//     $("#suspendCancel").on('click', function(){$.magnificPopup.close(); student_to_proccess = "";});
//     $("#formCancel").on('click', function(){$.magnificPopup.close(); student_to_proccess = ""; orig_email = "";});

//     //confirms
//     $("#importBegin").on('click', function(){loadDataFromFile();});


//     /* SAVE */
//     $("#formSave").on('click',function(e){
//         e.preventDefault();

//         updateStudent();
//     });

//     /* ADD */    
//     $("#addToTable").on('click',function(e){
//         e.preventDefault();

//         student_to_proccess = "";
//         orig_email = "";
        
//         console.log("student_to_process : " + student_to_proccess);
//         //clear form
//         $('#student_id').val("");
//         $('#email').val("");
//         $('#first_name').val("");
//         $('#last_name').val("");
//         $('#date_of_birth').val("");
//         $('#gender').val("Select Gender");
//         $('#contact_number').val("");
//         $('#address').val("");

//         $.magnificPopup.open({
//             items: {
//                 src: '#form',
//                 type: 'inline'
//             },
//             preloader: false,
//             modal: true,
//             callbacks: {
//             }
//         });
//     });


//     /* IMPORT */
//     $("#importToTable").on('click',function(e){
//         e.preventDefault();

//         $('#importPanel').trigger('loading-overlay:hide');
//         //$('#importPanel').trigger('loading-overlay:show');

//         $.magnificPopup.open({
//             items: {
//                 src: '#import',
//                 type: 'inline'
//             },
//             preloader: false,
//             modal: true,
//             callbacks: {
//             }
//         });
//     });

//     /* SUSPEND */    
//     $('#suspendConfirm').on( 'click', function( e ) {
//         e.preventDefault();

//         var updates = {};
//         updates['students/' + student_to_proccess + '/status'] = "<span class='label label-warning'>Suspended</span>";                    

//         rootRef.update(updates, function(error){
//             // Callback comes here
//             if(error){
//                 //console.log(error);
//                 //$('#importPanel').trigger('loading-overlay:hide');
//                 notify('Request failed!', error, 'error');
//                 $.magnificPopup.close();
//                 student_to_proccess = "";
//             }
//             else{
//                 //<span class='label label-danger'>Deleted</span>
//                 console.log("Student Suspend Complete!");
//                 var affectedRow = $("#row_" + student_to_proccess);
//                 var row = $('#row_'+student_to_proccess);
//                 row.find('.status').html("<span class='label label-warning'>Suspended</span>");
//                 students_table.row(affectedRow).draw();
//                 $.magnificPopup.close();
//                 notify('Request Succeed!', student_to_proccess + ' suspended successfully!', 'success');
//                 student_to_proccess = "";
//             }
//         });
//     });


//     /* RESET */
//     $('#resetConfirm').on( 'click', function( e ) {
//         e.preventDefault();

//         var updates = {};
//         updates['students/' + student_to_proccess + '/status'] = "<span class='label label-success'>Active</span>";                    

//         rootRef.update(updates, function(error){
//             // Callback comes here
//             if(error){
//                 notify('Request failed!', error, 'error');
//                 $.magnificPopup.close();
//                 student_to_proccess = "";
//             }
//             else{
//                 console.log("Student Reset Complete!");
                
//                 var affectedRow = $("#row_" + student_to_proccess);
//                 var row = $('#row_'+student_to_proccess);
//                 row.find('.status').html("<span class='label label-success'>Active</span>");

//                 students_table.row(affectedRow).draw();
//                 $.magnificPopup.close();
//                 notify('Request Succeed!', student_to_proccess + ' was reset successfully!', 'success');
//                 student_to_proccess = "";
//             }
//         });
//     });

//     /* DELETE */
//     $('#dialogConfirm').on( 'click', function( e ) {
//         e.preventDefault();
        
//         var updates = {};
//         updates['students/' + student_to_proccess + '/status'] = "<span class='label label-danger'>Deleted</span>";

//         rootRef.update(updates, function(error){
//             // Callback comes here
//             if(error){
//                 notify('Request failed!', error, 'error');
//                 $.magnificPopup.close();
//                 student_to_proccess = "";
//             }
//             else{
//                 //<span class='label label-danger'>Deleted</span>
//                 console.log("Student Delete Complete!");
//                 // var affectedRow = $("#row_" + student_to_proccess);
//                 // students_table.row(affectedRow).remove().draw();
                
//                 var affectedRow = $("#row_" + student_to_proccess);
//                 var row = $('#row_'+student_to_proccess);
//                 row.find('.status').html("<span class='label label-danger'>Deleted</span>");

//                 students_table.row(affectedRow).draw();
//                 $.magnificPopup.close();
//                 notify('Request Succeed!', student_to_proccess + ' was deleted successfully!', 'success');
//                 student_to_proccess = "";
//             }
//         });
//     });
// }

// function initializeStudentsTable(){
//     students_table = $('#students-data').DataTable(
//         {
//         dom: 'Bflrtip',
//         columnDefs: [
//             {
//                 targets: [6,7,8,9],
//                 className: 'noVis',
//                 visible: false
//             }
//         ],
//         buttons: [
//             {
//                 extend: 'colvis',
//                 columns: ':not(.noVis)'
//             }
//         ],        
//         aoColumns: [
//             { sClass: 'id center' },
//             { sClass: 'email' },
//             { sClass: 'contact_number' },
//             { sClass: 'full_name' },
//             { sClass: 'status center' },
//             {
//                 bSortable: false,
//                 sClass: 'actions center',
//                 defaultContent: [
//                     '<a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>',
//                     '<a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>',
//                     '<a href="#" class="on-default reset-row"><i class="fa fa-refresh"></i></a>',
//                     '<a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>',
//                     '<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>',
//                     '<a href="#" class="on-default suspend-row"><i class="fa fa-pause"></i></a>'
//                 ].join(' ')
//             },
//             { sClass: 'gender' },
//             { sClass: 'address' },
//             { sClass: 'course' },
//             { sClass: 'birthday' }
//         ]
//     }
//     );

//     $(".dt-buttons").hide();
// }


// function BindTable(jsondata, tableid) {/*Function used to convert the JSON array to Html Table*/  
    
//     $(".importButtons").hide();

//     var columns = BindTableHeader(jsondata, tableid); /*Gets all the column headings of Excel*/  

//     var newRowDatas = [];
    
//     for (var i = 0; i < jsondata.length; i++) {
//         //var row$ = $('<tr/>');  
//         var newRowData = {};
        
//         var JSONDATA = jsondata[i];

//         for (var colIndex = 0; colIndex < columns.length; colIndex++) {
//             var cellValue = JSONDATA[columns[colIndex]];
//             if (cellValue == null)
//                 cellValue = "";
//             newRowData[columns[colIndex]] = cellValue;
//         }

//         newRowDatas[i] = newRowData;
//     }

//     var allConflictEmails = [];
//     var z = 0;
//     var students = [];

//     for (let index = 0; index < newRowDatas.length; index++) {
        
//         var newRowData = newRowDatas[index];

//         rootRef.child('students/' + newRowData.ID.replace('-', '_')).once('value').then(function(snap){
            
//                 newRowData = newRowDatas[index];
//                 var newEmail = newRowData.email.toLowerCase();
//                 var newStudentID = newRowData.ID;
//                 //check if the email is the same, then we dont need to check email on auth database
//                 if(snap.exists())
//                 {
//                     console.log("snap exists");

//                     var newID = snap.key;//newRowData.ID.replace('-', '_');

//                     //update only new info from excel
//                     var postData = {
//                         email: newRowData.email.toLowerCase(),
//                         first_name: newRowData.first_name,
//                         last_name: newRowData.last_name,
//                         birthday: newRowData.birthday,
//                         gender: newRowData.gender,
//                         course : newRowData.course,
//                         status : snap.child("status").val(),
//                         contact_number : newRowData.contact_number,
//                         address: newRowData.address,
//                         password : snap.child("password").val()
//                     };

//                     allConflictEmails[index] = "";

//                     //student id from file
//                     //this student id exist on db already
//                     //if the student email from excel file is different from the one on db
//                     if(snap.child("email").val() != newEmail)
//                     {
//                         //we need to check the email from excel if it's not already in use
//                         firebase.auth()
//                         .signInWithEmailAndPassword(snap.child("email").val(), snap.child("password").val())
//                         .then(function(userCredential) {
//                             userCredential.user.updateEmail(newEmail)
//                             .then(function(result){
//                                 var updates = {};
//                                 postData.email = newEmail;
//                                 updates['students/' + newID] = postData;
                    
//                                 rootRef.update(updates);
                                
//                                 students[index] = {
//                                     id : newID,
//                                     fname : newRowData.first_name,
//                                     lname : newRowData.last_name
//                                 }
    
//                                 z++;
    
//                                 //done updating
//                                 if(z >= newRowDatas.length)
//                                 {
//                                     doneImportFile(students, allConflictEmails);
//                                 }
//                             }).catch(error => {
//                                 console.log(error.code);
//                                 allConflictEmails[index] = newStudentID + " : " + newEmail + " <br>";
//                                 z++;
    
//                                 //done updating
//                                 if(z >= newRowDatas.length)
//                                 {
//                                     doneImportFile(students, allConflictEmails);
//                                 }
//                             });
//                             //console.log("Log in successfull");

                            
//                         })
//                         .catch(error => {
//                             console.log(error.code);
//                             allConflictEmails[index] = newStudentID + " : " + newEmail + " <br>";
//                             z++;

//                             //done updating
//                             if(z >= newRowDatas.length)
//                             {
//                                 doneImportFile(students, allConflictEmails);
//                             }
//                         });
//                     }
//                     else
//                     {
//                         var updates = {};
//                         updates['students/' + newID] = postData;
            
//                         rootRef.update(updates);

//                         students[index] = {
//                             id : newID,
//                             fname : newRowData.first_name,
//                             lname : newRowData.last_name
//                         }

//                         z++;

//                         //done updating
//                         if(z >= newRowDatas.length)
//                         {
//                             doneImportFile(students, allConflictEmails);
//                         }
//                     }
                    
                    

//                     var td = $('#row_'+ newID);//.find(".contact_number");
//                     td.find(".contact_number").get(0).innerText = postData.contact_number;
//                     td.find(".full_name").get(0).innerText = newRowData.first_name + " " + newRowData.last_name;
//                 }
//                 else
//                 {
//                     allConflictEmails[index] = newStudentID + " : " + newEmail + " <br>";

//                     var pass = newRowData.ID;

//                     firebase.auth().createUserWithEmailAndPassword(newEmail, pass)
//                         .then(function(result) {
            
//                             newRowData = newRowDatas[index];
            
//                             // result.user.updateProfile({	  
//                             //     displayName: newRowData.first_name + " " + newRowData.last_name                        
//                             // });
            
//                             //console.log(result.user);
            
//                             var data = {
//                                 first_name: newRowData.first_name, 
//                                 last_name : newRowData.last_name, 
//                                 email: newRowData.email,
//                                 contact_number:newRowData.contact_number,
//                                 gender: newRowData.gender,
//                                 address : newRowData.address,
//                                 course : newRowData.course,
//                                 status : "<span class='label label-success'>Active</span>",
//                                 birthday : newRowData.birthday.split('/').join('-'),
//                                 password: newRowData.ID
//                             };
                            
//                             var new_actions_row = generateNewActions();
                    
//                             var newData = [
//                                 newRowData.ID,
//                                 newRowData.email,
//                                 newRowData.contact_number,
//                                 newRowData.first_name + " " + newRowData.last_name,
//                                 "<span class='label label-success'>Active</span>",
//                                 new_actions_row,
//                                 newRowData.gender,
//                                 newRowData.address,
//                                 newRowData.course,
//                                 data.birthday
//                             ];

//                             var newID = newRowData.ID.replace('-', '_');
                    
//                             var existingRow = $("#row_" + newID);
                    
//                             if(existingRow.length > 0)
//                             {
//                                 students_table.row(existingRow).remove().draw();
//                             }
                            
//                             students_table.row.add(newData).node().id = 'row_' + newID;
                    
//                             var td = $('#row_'+ newID).find(".full_name");
//                             td.attr('data-fname', data.first_name);
//                             td.attr('data-lname', data.last_name);

//                             // var td = $('#row_'+ newID).find(".full_name");
//                             students[index] = {
//                                 id : newID,
//                                 fname : data.first_name,
//                                 lname : data.last_name
//                             }
            
//                             rootRef.child("students/" + newID).set(data, function(error){
//                                 // Callback comes here
//                                 if(error){
//                                     //console.log(error);
//                                     $('#importPanel').trigger('loading-overlay:hide');
//                                     notify('Request failed!', error, 'error');
//                                     $.magnificPopup.close();
//                                 }
//                                 else{
//                                     //console.log("Data successfully");
//                                 }
//                             });
            
//                             z++;

//                             allConflictEmails[index] = "";
//                             //done updating
//                             if(z >= newRowDatas.length)
//                             {
//                                 doneImportFile(students, allConflictEmails);
//                             }
//                         })
//                         .catch(error => {
                            
//                             switch (error.code) {
//                                 case 'auth/email-already-in-use':
//                                     console.log('conflict email = ' + newEmail + " " + error.code);
//                                 break;
//                                 case 'auth/operation-not-allowed':
//                                     // console.log(`Error during sign up.`);
//                                     ShowMessage('Warning!', 'Error during sign up', 'warning');
//                                 break;
//                                 case 'auth/weak-password':
//                                     console.log('Password is not strong enough. Add additional characters including special characters and numbers.');
//                                 //ShowMessage('Warning!', 'PassIDword is not strong enough. Add additional characters including special characters and numbers.', 'warning');
//                                 break;
//                                 default:
//                                 // console.log(error.message);
//                                     ShowMessage('Error last', error.message, 'danger');
//                                 break;
//                             }
            
//                             //console.log(error);
            
//                             // if(error.code.trim() == "auth/email-already-in-use")
//                             //     allConflictEmails += newRowData.ID + " : " + newEmail + " \n";
//                             //console.log('error msg = ' + error.code);
//                             z++;
            
//                             //done updating
//                             if(z >= newRowDatas.length)
//                             {
//                                 doneImportFile(students, allConflictEmails);
//                             }
//                         });
//                 }
//         });
//     }
// }

// function doneImportFile(students, allConflictEmails)
// {
//     $('#importPanel').trigger('loading-overlay:hide');
//     notify('Request success', 'Data updated successfully', 'success');
//     $.magnificPopup.close();
//     $(".importButtons").show();

//     students_table.rows().invalidate();
//     students_table.draw();

//     for (var z = 0; z < students.length; z++) {
//         var element = students[z];
//         if(element != undefined)
//         {
//             console.log("Adding data : " + element.id);
//             var td = $('#row_'+ element.id + " > .full_name");
//             console.log(td);
//             td.attr('data-fname', element.fname);
//             td.attr('data-lname', element.lname);
//             //td.get(0).innerText = element.fname + " " + element.lname;
//         }

//         if(z >= students.length-1)
//         {
//             students_table.rows().invalidate();
//             students_table.draw();
//             console.log("DRAW? table");
//         }
//     }

//     //if(students.length == 0){
    
//    //}

//     var str = "";
//     for(var i=0; i < allConflictEmails.length; i++)
//         str += allConflictEmails[i];

//     if(str.length > 0)
//         ShowMessage('There are email(s) that is invalid or already taken : ', str + '<br> The students ID(s) above will not be imported', 'warning');
//     console.log("Done importing from file");
// }

// function userAlreadyExistOnTable(user_id)
// {
//     return (students_table.columns(0).data().toArray().toString().split(',').indexOf(user_id));
// }

// function BindTableHeader(jsondata, tableid) {/*Function used to get all column names from JSON and bind the html table header*/  
//     var columnSet = [];  
//     var headerTr$ = $('<tr/>');  
//     for (var i = 0; i < jsondata.length; i++) {  
//         var rowHash = jsondata[i];  
//         for (var key in rowHash) {  
//             if (rowHash.hasOwnProperty(key)) {  
//                 if ($.inArray(key, columnSet) == -1) {/*Adding each unique column names to a variable array*/  
//                     columnSet.push(key);  
//                     headerTr$.append($('<th/>').html(key));  
//                 }  
//             }  
//         }  
//     }  
//     //$(tableid).append(headerTr$);  
//     return columnSet;  
// }  