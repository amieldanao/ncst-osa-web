/*jslint indent: 4 */
/*global document, require, console, window*/
/*eslint no-console: 0*/

'use strict';
var usernameInput;
var passwordInput;
// var rootRef;
var signing_in;

$(document).ready(function()                    
                  {
                    // if(firebase.auth().currentUser)
                    // {
                        firebase.auth().signOut().then(function(result){
                            console.log("logged out!");
                        });
                    // }

    initVariables();

    handleLoginForm();
});



function initVariables()
{
    $("form").get(0).reset();

    usernameInput = $("#inputEmail");
    passwordInput = $("#inputPassowrd");
    // rootRef = firebase.database().ref();


    //    addAdmin();
    //addDescriptions();
}

function handleLoginForm()
{
    $('form').bind('submit', $('form'), function(event) {
        var form = this;

        event.preventDefault();
        event.stopPropagation();

        if (form.submitted) {
            return;
        }

        form.submitted = true;

        if(!signing_in)
            login();

    });
}

function login()
{
    signing_in = true;
    showLoading(true, "signing in...");
    $("#logInBtn").prop('disabled', true);
    var email = usernameInput.val().toLowerCase();
    var pword = passwordInput.val();

    try
    {

        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
            .then(function() 
                  {
            firebase_auth.signInWithEmailAndPassword(email, pword).then(function(user){
                // console.log(user);
                if(user)
                {
                    // update password on database
                    try
                    {
                        let query = db.collection('users').where('email', '==', email);

                        query.get().then(function(querySnapshot) {
                        // console.log(querySnapshot.docs[0].data());

                            if(querySnapshot.size > 0)
                            {
                                if(querySnapshot.docs[0].data().status == "Active")
                                {
                                    let key = querySnapshot.docs[0].id;

                                    db.collection('users').doc(key).set({password:pword}, {merge:true})
                                    .then(function(docRef) {
                                        // showNotifSuccess('Saved', capitalizeFLetter(data.ID) + ' saved successfully');
                                        showLoading(false);

                                        window.location = "dashboard.html";
                                    })
                                    .catch(error => {
                                        console.log("error on write");
                                        showLoading(false);
                                        bootbox.alert(error.message);
                                        errorLogin();                         

                                    });
                                }
                                else
                                {
                                    showLoading(false);
                                    bootbox.alert("Your account is not active!");
                                    errorLogin(); 
                                }

                            }
                            else
                            {
                                showLoading(false);
                                bootbox.alert("User doesn't exist!");
                                errorLogin();     
                            }
                        })
                        .catch(error => {
                            console.log("error on read");
                            showLoading(false);
                            bootbox.alert(error.message);
                            errorLogin();                     

                        });

                    }
                    catch(error)
                    {
                        console.log("error on try/catch");
                        showLoading(false);
                        bootbox.alert(error.message);
                        errorLogin();
                    }

                    

                }

            }).
            catch(function(error) {

                
                showLoading(false);
                bootbox.alert(error.message);
                errorLogin();
                
            });
        })
        .catch(function(error) {
        errorLogin();
        if(error){
            showLoading(false);
            bootbox.alert(error.message);
            errorLogin();
        }

        
        //                            var errorCode = error.code;
        //                            var errorMessage = error.message;
    });

    
    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(error.message + "<br> Try restarting your browser.");
        errorLogin();
    }
}


function errorLogin()
{
    firebase.auth().signOut();
    $("#logInBtn").prop('disabled', false);
    signing_in = false;
    $('form').get(0).submitted = false;
}
// function loginOLD()
// {
//     signing_in = true;
//     showLoading(true, "signing in...");
//     $("#logInBtn").prop('disabled', true);
//     var email = usernameInput.val();
//     var password = passwordInput.val();


//     var admins = rootRef.child("users");
//     var isValid = false;


//     admins.once('value').then(function(snap) {
//         var count = snap.numChildren();
//         var i = 0;
//         snap.forEach(function (admin) {
//             console.log("checking " + admin.child("email").val());

//             if(admin.child("email").val() == usernameInput.val() && admin.child("password").val() == passwordInput.val()){
//                 //check user account status
//                 var status = admin.child("status").val();
//                 if(status != "Verified")
//                 {
//                     isValid = false;
//                     bootbox.alert('<p><strong>Authentication error!</strong><br>This account is '+ status +' </p>');
//                 }
//                 else
//                 {
//                     isValid = true;

//                     firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
//                         .then(function() 
//                               {
//                         firebase_auth.signInWithEmailAndPassword(email, password).then(function(user){

//                             if(user)
//                                 window.location = "dashboard.html";
//                             // console.log(user);
//                         }).
//                         catch(function(error) {
//                             // Handle Errors here.
//                             //                                var errorCode = error.code;
//                             //                                var errorMessage = error.message;


//                             if(error)
//                             {
//                                 $("#logInBtn").prop('disabled', false);     
//                                 showLoading(false);
//                                 bootbox.alert(error.message);
                                
//                             }
//                             // else
//                             // {
//                             //    window.location = "browse.html";
//                             // }

//                             console.log(error);
//                             // ...
//                         });
//                     })
//                         .catch(function(error) {
//                         // Handle Errors here.
//                         bootbox.alert(error);
//                         //                            var errorCode = error.code;
//                         //                            var errorMessage = error.message;
//                     });
//                 }

//                 i = count;
//                 return;
//             }

//             i++;

//             if(i >= count)
//             {
//                 if(isValid == false)
//                 {
//                     bootbox.alert('<p><strong>Authentication error!</strong><br>Unable to locate any account related to your e-mail </p>');
//                 }
//             }
//         }

//                     );
//     });

// }





//    function addDescriptions()
//    {    
//        // rootRef.child('descriptions').set(descriptions);
//    }

// function addAdmin()
// {
//     rootRef.child("users")
//     .push({"email" : "admin@email.com", "password": "adminadminyespapa"})
//     .then((snap) => {
//      const key = snap.key;
//   });
// }

// function logIn()
// {
//     console.log("trying to log in");
//     //check log in credentials on admin database
//     var admins = rootRef.child("users");
//     var isValid = false;
//     invalidMessage = '<div id="invalidMsg" class="alert alert-danger">'+
// 		'<p><strong>Authentication error!</strong><br>Unable to locate any account related to your e-mail </p>';

//     admins.once('value').then(function(snap) {
//         var count = snap.numChildren();
//         var i = 0;
//         snap.forEach(function (admin) {
//             console.log("checking " + admin.child("email").val());

//             if(admin.child("email").val() == usernameInput.val() && admin.child("password").val() == passwordInput.val()){
//                 //check user account status
//                 var status = admin.child("status").val();
//                 if(status != "Verified")
//                 {
//                     isValid = false;
//                     invalidMessage = '<div id="invalidMsg" class="alert alert-danger">'+
// 		            '<p><strong>Authentication error!</strong><br>This account is '+ status +' </p>';
//                 }
//                 else
//                 {
//                     isValid = true;
//                     sessionStorage.setItem("logged", usernameInput.val());
//                     sessionStorage.setItem("username", admin.child("first_name").val() + " " + admin.child("last_name").val());
//                     window.location = "dashboard.html";                    
//                     invalidMessage = '';
//                     i = count;
//                     return;
//                 }
//             }

//             i++;

//             if(i >= count)
//             {
//                 if(isValid == false)
//                 {
//                     if($("#invalidMsg") != null)
//                         $("#invalidMsg").remove();
//                     $("#panel_body").prepend(invalidMessage + '<strong>' + usernameInput.val() + '</strong></div>' );
//                 }
//             }
//         });
//     });
// }




