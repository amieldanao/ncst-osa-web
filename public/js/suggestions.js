
(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];
addUserCallback();
// var initial_role;
// var myRole;
function addUserCallback()
{
    let email = $("#user_email").text();

    let query = db.collection('users').where('email', '==', email);

    query.onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {


            if (change.type === "added") {
                let role = change.doc.data().role;
                initial_role = role;
                setRole(role);
                if(role == 2 || role == 1)
                {
                    $("#table_tr th").last().attr('id', 'actions_th');//append('<th id="actions_th" class="role_admin">Actions</th>');
                }
            }

            if (change.type === "modified") {
                setRole(change.doc.data().role);
                $("body").empty();
                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                 setTimeout(() => {
                    location.replace("index.html");
                }, 3000);
            }

            if (change.type === "removed") {
                // console.log("Removed user: ", change.doc.data());
                // window.location = "index.html";
            }
        });
    },
        function(error){
        console.log('retry add user callback');
        addUserCallback();
    });
}

function setRole(newRole)
{
    switch(newRole)
    {
        case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
        case 1 : $(".role_admin").remove();break;
    }
    
    $("#user_role").text("Role: " + roles_string[newRole]);
    $("#user_role").attr("data-done", true);
}

})();





'user strict'


var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);



function Start()
{
    loadCategories();
    
}

$(document).ready( function () {
    observer.observe(targetNode, config);
//    initButtonCallbacks();
});


'user strict'

var suggestion_table;
var suggestion_count = 0;
var storedSuggestions = [];
var categories = [];
var edit_dialog;

// $(document).ready(function () 
// {
    
// });


function loadCategories() {
    db.collection('categories').get().then(function(querySnapshot){
        // console.log(querySnapshot);

        let requests = querySnapshot.docs.map((category) => {
            
            categories[category.id] = category.data();
        });

        Promise.all(requests).then(() => {
            initTable();
        });


    });
}

function initTable()
{
    let btns = [];
    if(document.getElementById("actions_th") != null)
    {
        console.log("has action column!");
        // myColumns.push({"sClass": "actions_buttons" });
        btns = [
        {
            text: 'Add <i class="fa fa-plus"></i>',
            action: function ( e, dt, node, config ) {
                showPromptEdit()
            },
            "className": 'btn btn-primary'
        }
        ]
    }


    suggestion_table = $('#suggestion_table').DataTable(
    {
        dom: 'Bfrltip',
        "scrollX": true,
        buttons: btns,
        "aoColumns": [
        {"sClass": "col_categories" },
        {"sClass": "col_suggestion" },
        {"sClass": "col_active" },
        // {"sClass": "col_lower_limit" },
        // {"sClass": "col_upper_limit" },
        {"sClass": "actions_buttons" }
        ],
        "columnDefs": [ {
            "targets": 3,
            "orderable": false
        }],
        initComplete: function(settings, json) {
            // alert( 'DataTables has finished its initialisation.' );
            addDBListener();
        }
    }
    );

    // addDBListener();
}

function addDBListener()
{
    showLoading(true, "Loading suggestions...");
    try {
          
        db.collection("suggestions")
        .onSnapshot(function(snapshot) { 
            let requests = snapshot.docChanges().map((change) => {
                
                var d = {category: "", suggestion : "", active: true, lower_limit : 0, upper_limit : 0};
                if('category' in change.doc.data())
                    d['category'] = change.doc.data().category;
                
                if('suggestion' in change.doc.data())
                    d['suggestion'] = change.doc.data().suggestion;

                if('active' in change.doc.data())
                    d['active'] = (change.doc.data().active?'on':'off');
                
                if('lower_limit' in change.doc.data())
                    d['lower_limit'] = change.doc.data().lower_limit;
                
                if('upper_limit' in change.doc.data())
                    d['upper_limit'] = change.doc.data().upper_limit;
                
                let newData =
                [
                    d.category,
                    getTextFromHTML(d.suggestion),
                    d.active
                    // d.lower_limit,
                    // d.upper_limit
                ]

                if(document.getElementById("actions_th") != null)
                    newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickEdit(this);" class="on-default edit-row mx-2"><i class="fa fa-pencil"></i></a><a data-key="'+change.doc.id+'" href="#" onclick="showPromptDelete(this);" class="on-default remove-row mx-2"><i class="fa fa-trash-o"></i></a>');
                else
                    newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickView(this);" class="on-default edit-row mx-2"><i class="fas fa-eye"></i></a>');

                if (change.type === "added") {

                    let newRow = suggestion_table.row.add(newData).node().id = change.doc.id;

                    suggestion_table.draw( false );
                    
                    storedSuggestions[change.doc.id] = d.suggestion;
                }
                
                if (change.type === "modified") {
                    storedSuggestions[change.doc.id] = d.suggestion;
                    suggestion_table.row($("#" + change.doc.id)).data(newData).draw(false);
                    console.log("Modified city: ", change.doc.data());                    
                }
                if (change.type === "removed") {
                    // $('tr[data-key="'+change.doc.id+'"]').parent().parent().remove();
                    // suggestion_table.row('#'+change.doc.id).remove().draw();
                    //location.reload();
                    suggestion_table.draw();
                    
                    // announcements_table.draw();
                }
                
                return new Promise((resolve) => {
                  asyncFunction(change, resolve);
                });
            })

            Promise.all(requests).then(() => {showLoading(false);});
        });
    }
    catch(err) {
        bootbox.alert(
        {
            message:err.message,
            onHidden: function () {
                UpdateModalScrolling();
            }
        });
    }
}


function clickView(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    var category = tr.find(".col_categories").text();
    var suggestion = tr.find(".col_suggestion").text();
    var reportRange = "from : " + tr.find(".col_lower_limit").text() + "  to : " + tr.find(".col_upper_limit").text();
    // var color = tr.find(".col_color").text();
    

    bootbox.alert(
    {
        title: category,
        message : suggestion + '<br><hr>Report range limit : <br>' + reportRange,
        centerVertical : true,
        scrollable : true
    });
}

function showPromptEdit(_category, _suggestion, _active)
{
    var categorySelectOptions = '';
    var selected_category_from_data = '';

    Object.keys(categories).forEach(function(e, i){
        categorySelectOptions += '<option value="'+e+'">'+categories[e].name+'</option>';
        if(_category == categories[e].name)
        {
            selected_category_from_data = e;
        }
    });

    edit_dialog = bootbox.dialog({
        title: 'Manage Suggestion',
        message: '<form id="infos" action="">'+
        'Active:<input class="col_active" type="checkbox" data-toggle="toggle" data-onstyle="success" data-offstyle="danger"><br>'+
        '<label for="category_selection" style="width:100%;">Suggestion category:<select id="category_selection" style="width: 100%">'+
        categorySelectOptions+
        '</select></label><br>'+
        'Suggestion:<textarea id="summernote" style="width:100%; border-radius:5px; "></textarea><br>'+'</form>',
        // '<label for="lower_limit">Lower limit: <span id="up_popover" class="badge badge-info" data-toggle="popover" data-trigger="hover focus click"  data-content="This suggestion will be shown if the percentage increase of reports exceeded this number" >?</span></label><div class="input-group"><input id="lower_limit" class="col_lower_limit form-control inputNumericOnly" size="16" type="text" value="1" min="0"><div class="input-group-append"><span class="input-group-text"><i class="fas fa-sort-numeric-down"></i></span></div></div><br>'+
        // '<label for="upper_limit">Upper limit: <span id="low_popover" class="badge badge-info" data-toggle="popover" data-trigger="hover focus click" data-content="This suggestion will be shown as long as the percentage increase of reports is between the lower limit and this number">?</span></label><div class="input-group"><input id="upper_limit" class="col_upper_limit form-control inputNumericOnly" size="16" type="text" value="100" min="0"><div class="input-group-append"><span class="input-group-text"><i class="fas fa-sort-numeric-up"></i></span></div></div></form>',
        buttons: {
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function(){
                    sessionStorage.setItem("toEdit", '');
                }
            },
            ok: {
                label: 'Save <i class="fa fa-save"></i>',
                className: 'btn-success',
                callback: function(){

                    console.log('Custom OK clicked');

                    // let selected_category = $('#infos #category_selection').find(':selected');
                    var e = document.getElementById("category_selection");
                    var selected_category = e.options[e.selectedIndex].text;

                    if(selected_category.length == 0)
                    {
                        bootbox.alert({
                            message:"Please select a category",
                            centerVertical: true,
                            className : "topmost-modal",
                            size : 'small',
                            onHidden: function () {
                                UpdateModalScrolling();
                            }
                        });
                        return false;
                    }

                    var suggestion = $('#summernote').summernote('code');
                    let active = $('#infos .col_active').prop('checked');
                    
                    // let lower_limit = $('#infos .col_lower_limit').val();
                    // let upper_limit = $('#infos .col_upper_limit').val();
                    
                    saveSuggestion(selected_category, suggestion, active);
                }
            }
        }
    });

    // Set form values from selected suggestion to edit.
    // edit_dialog.on('shown.bs.modal', function(e){
        $("#up_popover").popover();
        $("#low_popover").popover();

        let select = $('#infos #category_selection');
        select.val(selected_category_from_data);

        edit_dialog.on('shown.bs.modal', function(e){
            $("body").addClass('modal-open');
        });
        
        $('#infos .col_active').bootstrapToggle();

        console.log("active = " + _active);
        if(_active != undefined)
            $('#infos .col_active').bootstrapToggle(_active);
        else
            $('#infos .col_active').bootstrapToggle('on');

        // $('#infos .col_active').prop('checked', true).change();w

        $('#summernote').summernote({
            height: '200px',
            toolbar: defaultToolbar
        });
        
        if(_suggestion)
        {
            $('#summernote').summernote('code', _suggestion);
        }
        
        // if(_lower_limit){
        //     $('#infos #lower_limit').val(_lower_limit);
        // }
        
        // if(_upper_limit){
        //     $('#infos #upper_limit').val(_upper_limit);
        // }

        // addPastePrevent($('#infos #lower_limit'));
        // addPastePrevent($('#infos #upper_limit'));
    // });
}

function saveSuggestion(_category, _suggestion, _active)
{
    var key = sessionStorage.getItem("toEdit");//$('#infos').attr('data-key');
    var ref = db.collection("suggestions").doc();
    
    if(key == undefined || key.length == 0)
    {
        key = ref.id;
    }
    else
    {
        ref = db.collection("suggestions").doc(key);
    }
    
    sessionStorage.setItem("toEdit", "");
    
    showLoading(true, "Saving...");
    ref.set({
        category: _category,
        suggestion: _suggestion,
        active: _active
        // ,
        // lower_limit : parseInt(_lower_limit),
        // upper_limit : parseInt(_upper_limit)
    })
    .then(function(docRef) {
        showNotifSuccess('Saved', 'Suggestion saved successfully');
        showLoading(false);
        
        $('.remove-row[data-key="'+key+'"]').parent().parent().find(".col_categories").text(_category);
        
        $('.remove-row[data-key="'+key+'"]').parent().parent().find(".col_suggestion").text(getTextFromHTML(_suggestion));
        
        $('.remove-row[data-key="'+key+'"]').parent().parent().find(".col_active").text(_active? 'on' : 'off');
        
        // $('.remove-row[data-key="'+key+'"]').parent().parent().find(".col_lower_limit").text(_lower_limit);
        
        // $('.remove-row[data-key="'+key+'"]').parent().parent().find(".col_upper_limit").text(_upper_limit);
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
        PNotify.error({
          title: 'Error',
          text: error
        });
        showLoading(false);
    });
}

function clickEdit(elem)
{
    var category = $(elem).parent().parent().find(".col_categories").text();
    var suggestion = $(elem).parent().parent().find(".col_suggestion");
    var active = $(elem).parent().parent().find(".col_active").text();
    
    var lower_limit = $(elem).parent().parent().find(".col_lower_limit").text();
    var upper_limit = $(elem).parent().parent().find(".col_upper_limit").text();
    
    
    var key = $(elem).attr('data-key');
    
    showPromptEdit(category, storedSuggestions[key], active);
    
    sessionStorage.setItem("toEdit", $(elem).attr('data-key'));
}

function showPromptDelete(elem)
{
    sessionStorage.setItem("toEdit", $(elem).attr('data-key'));
    
    bootbox.confirm({
        message: "Are you sure you want to delete this suggestion?",
        size:'small',
        centerVertical: true,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            console.log('This was logged in the callback: ' + result);
            if(result)
            {
                clickRemove();
            }
            else
            {
                sessionStorage.setItem("toEdit", '');
            }
            
        }
    });
}

function clickRemove()
{  
    var key = sessionStorage.getItem("toEdit");
    
    if(key.trim().length > 0)
    {
        showLoading(true, "Deleting...");
        db.collection("suggestions").doc(key).delete().then(function() {
            console.log("Document successfully deleted!");
            PNotify.info({
              title: 'Deleted',
              text: 'Suggestion deleted successfully'
            });
            showLoading(false);
            
            // announcements_table.row('tr[data-key="'+key+'"]').remove().draw();
            suggestion_table.row('#'+key).remove().draw();
            // announcements_table.draw();

            sessionStorage.setItem('toEdit', '');
            // $('.dataTables_scrollHeadInner, table').css('width', '100%');
            
        }).catch(function(error) {
            console.error("Error removing document: ", error);
            PNotify.error({
              title: 'Error',
              text: error
            });
            showLoading(false);
            sessionStorage.setItem('toEdit', '');
        });
    }
}






// function initVariables()
// {
//     rootRef = firebase.database().ref();
    
//     $('#enable_suggestion').bootstrapToggle();

//     $('#suggestion_editor').summernote({
//         theme: "classic",
//         height: 100
        
//         }
//     );
    
//     //get categories and add it as an option
//     rootRef.child("categories").once('value', function(snap){
//         var count = snap.numChildren();

//         snap.forEach(function(category){
//             $("#category").append('<option value="' + category.key +'">'+category.key+'</option>');
//         });
//     });

//     $('.js-example-basic-single').select2({
//         dropdownParent: $("#form")
//     });

//     //$("#enable_suggestion").prop('checked');
// }

// function editSuggestion(me)
// {
//     var evID = me.closest('tr');//.attr("val");
//     suggestion_to_proccess = evID.id;//.split("row_")[1];
//     console.log(suggestion_to_proccess);

//     //category name
//     // $('#category').val($('#'+evID.id + ' > .category').text());
    
//     $("#category").select2("val", $('#'+evID.id + ' > .category').text());
//     $('#category').attr('readonly', 'readonly');
    
//     $("#enable_suggestion").prop('checked', $('#'+evID.id + ' > .enabled').text() == "Yes");

//     $('#suggestion_editor').summernote('code', suggestion_messages[suggestion_to_proccess]);
    
//     $('#lower_limit').val($('#'+evID.id + ' > .lower_limit').text());
//     $('#upper_limit').val($('#'+evID.id + ' > .upper_limit').text());
    
    

//     $.magnificPopup.open({
//         items: {
//             src: '#form',
//             type: 'inline'
//         },
//         preloader: false,
//         modal: true
//     });
// }

// function deleteSuggestion(me)
// {
//     var evID = me.closest('tr');
//     suggestion_to_proccess = evID.id;//.split("row_")[1];

//     $.magnificPopup.open({
//         items: {
//             src: '#dialog',
//             type: 'inline'
//         },
//         preloader: false,
//         modal: true
//     });
// }

// function loadSuggestionsFromDB(){
//     showLoading(true);
//     console.log('start reading all Suggestions');
    
//     rootRef.child('suggestions').once('value').then(function(snap){
//         suggestion_count = snap.numChildren();

//         if(suggestion_count == 0)
//             showLoading(false);
//         var i = 0;
//         //read all students from db
//         snap.forEach(function(suggestion){            
//             var data = {
//                 category: suggestion.child('category').val(),
//                 message : suggestion.child('message').val(),
//                 enabled : suggestion.child('enabled').val(),
//                 lower_limit : suggestion.child('lower_limit').val(),
//                 upper_limit : suggestion.child('upper_limit').val()
//             };

//             //var status = ()? "<span class='label label-danger'>Deleted</span>": "<span class='label label-danger'>Deleted</span>";
//             var theActions = generateNewActions();

//             suggestion_messages[suggestion.key] = data.message;

//             var newData = [
//                 data.category,
//                 extractContent(data.message),
//                 data.enabled,
//                 data.lower_limit,
//                 data.upper_limit,
//                 theActions
//             ];
    
//             var newRow = suggestion_table.row.add(newData).node();
//             newRow.id = suggestion.key;
//             console.log("This suggestion : " + suggestion.key);
            
//             i++;
//             if(i >= suggestion_count)
//             {
//                 showLoading(false);
//                 console.log('done reading all suggestions');
//                 suggestion_table.draw();
//             }
//         });        
//     });
// }

// function extractContent(s) {
//     var span = document.createElement('span');
//     span.innerHTML = s;
//     return span.textContent || span.innerText;
// };

// function updateSuggestion(suggestion_id)
// {
//     console.log("trying to save : " + suggestion_id);
        
//     var categoryName = $('#category').val();
//     var suggestion_message = $(".note-editable").get(0).innerHTML;

//     //check if id already exist
//     var postData = {
//         category: categoryName,
//         message: suggestion_message,
//         enabled: ($("#enable_suggestion").get(0).checked == true? "Yes": "No"),
//         lower_limit: $("#lower_limit").val(),
//         upper_limit: $("#upper_limit").val()
//     };

//     var new_actions_row = generateNewActions();
//     //Update table row only
//     if(suggestion_id.length > 0)
//     {
//         console.log("updating suggestion node");
//         var row = $("#" + suggestion_id);
//         var temp = suggestion_table.row(row).data();
//         temp[0] = postData.category;
//         temp[1] = extractContent(postData.message);        
//         temp[2] = postData.enabled;        
//         temp[3] = postData.lower_limit;
//         temp[4] = postData.upper_limit;
//         temp[5] = new_actions_row;

//         // row.find(".color").css("background-color", postData.color);
//         suggestion_messages[suggestion_id] = postData.message;

//         suggestion_table.row(row).data(temp).draw();

//         //Write the new post's data simultaneously in the posts list and the user's post list.
//         var updates = {};
//         updates['suggestions/' + suggestion_id] = postData;

//         rootRef.update(updates);
//     }
//     else//if this is new user, add new row to table
//     {
//         console.log("creating new suggestion_id node");
        
//         rootRef.child("suggestions")
//         .push(postData)
//         .then((snap) => {
//             const key = snap.key;
//             var newData = [
//                 postData.category,
//                 extractContent(postData.message),                
//                 postData.enabled,                
//                 postData.lower_limit,
//                 postData.upper_limit,
//                 new_actions_row
//             ];

//             suggestion_messages[key] = postData.message;
            
//             suggestion_table.row.add(newData).draw().node().id = 'row_' + key;
//         });

//         //category_count += 1;
//     }
    
//     notify('Request succeed!', 'Suggestion updated successfully', 'success');
//     $.magnificPopup.close();
//     console.log("updated!");
//     suggestion_to_proccess = "";
// }

// function generateNewActions()
// {
//     return '<a href="#" onclick="editSuggestion(this)" class="on-default edit-row"><i class="fa fa-pencil"></i></a>'+
//     '<a href="#" onclick="deleteSuggestion(this)" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>';
// }


// function recomputeLimits(input)
// {
//     //this will make sure upper limit is always greater or equal than lower limit
//     if(input.get(0).id == "lower_limit")
//     {
//         var val1 = Math.min(input.val(), $("#upper_limit").val());

//         input.val(val1);
//     }
//     else
//     {
//         var val2 = Math.min(input.val(), Math.max($("#lower_limit").val(), input.attr("max")));

//         input.val(val2);

//         var val3 = Math.min($("#lower_limit").val(), $("#upper_limit").val());

//         $("#lower_limit").val(val3);
//     }
// }

// function attachButtonCallbacks()
// {    
//     //cancels
//     $("#dialogCancel").on('click', function(){$.magnificPopup.close();});
//     //$("#suspendCancel").on('click', function(){$.magnificPopup.close(); user_to_proccess = "";});
//     $("#formCancel").on('click', function(){$.magnificPopup.close(); suggestion_to_proccess = "";});

//     /* SAVE */
//     $("#formSave").on('click',function(e){
//         e.preventDefault();
//         if ($('#suggestion_editor').summernote('isEmpty'))
//         {
//             //alert('editor content is empty');
//             $("#suggestion_editor_hint").text("Please enter suggestion message");
//             return;
//         }
//         else
//         {
//             $("#suggestion_editor_hint").text("");
//         }

//         updateSuggestion(suggestion_to_proccess);
//     });

//     $('#lower_limit, #upper_limit').bind('contextmenu', function (e) {
// 		return false;
// 	});

// 	//disable pasting on amount input
// 	$('#lower_limit, #upper_limit').on('paste', function (e) {
// 		e.preventDefault();
// 		return false;
//     });
    
//     $('#lower_limit, #upper_limit').focusout(function(){
//         if($(this).val().length == 0)
//             $(this).val(0);
//         recomputeLimits($(this));
//     });

//     $('#lower_limit, #upper_limit').on('keypress', function (event) {
// 		if ((event.keyCode >= 48 && event.keyCode <= 57 && event.keyCode != 46) || event.keyCode == 8) {
//             return true;
// 		} else {
// 			return false;
// 		}
//     });

//     $('#lower_limit, #upper_limit').change(function(){
//         recomputeLimits($(this));
//     });

//     $('#lower_limit, #upper_limit').keyup(function () {
//         if ($(this).val().length == 0)
//             $(this).val(0);
//         else{
//             recomputeLimits($(this));
//         }
//     });


//     /* ADD */    
//     $("#addToTable").on('click',function(e){
//         e.preventDefault();
//         suggestion_to_proccess = "";
//         // $('#enable_suggestion').prop('checked', true);
//         $('#enable_suggestion').bootstrapToggle('on');
//         $('#suggestion_editor').summernote("reset");

//         // $("#category").val($("#category option:first").val());
//         // $("#category")[0].selectedIndex = 0;
//         $('#category').select2();//.select2('val', $('.select2 option:eq(1)').val());

//         $('#lower_limit').val(0);
//         $('#upper_limit').val(0);   

//         $.magnificPopup.open({
//             items: {
//                 src: '#form',
//                 type: 'inline'
//             },
//             preloader: false,
//             modal: true,
//             callbacks: {
//             }
//         });
//     });


//     /* DELETE */
//     $('#dialogConfirm').on( 'click', function( e ) {
//         e.preventDefault();
        
//         if(suggestion_to_proccess.length == 0)
//             return;

//         rootRef.child('suggestions/' + suggestion_to_proccess).remove().then(function(error){
//             // Callback comes here
//             if(error){
//                 notify('Request failed!', error, 'error');
//                 $.magnificPopup.close();
//                 suggestion_to_proccess = "";
//             }
//             else{
//                 console.log("Suggestion Delete Complete!");
                
//                 var row = $('#' + suggestion_to_proccess);
//                 var name = row.find('.category').text();

//                 suggestion_table.row(row).remove().draw();
//                 $.magnificPopup.close();
//                 notify('Request Succeed!', name + ' was deleted successfully!', 'success');
//                 suggestion_to_proccess = "";
//             }
//         });
//     });
// }

// function initializeSuggestionTable(){

//     suggestion_table = $('#suggestion_table').DataTable(
//         {
//         dom: 'flrtip',
//         // columnDefs: [
//         //     {
//         //         targets: [4,5],
//         //         className: 'noVis',
//         //         visible: false
//         //     }
//         // ],
//         aoColumns: [
//             { sClass: 'category' },
//             { sClass: 'suggestion' },
//             { sClass: 'enabled' },            
//             { sClass: 'lower_limit' },
//             { sClass: 'upper_limit' },
//             { 
//                 bSortable: false,
//                 sClass: 'actions center',
//                 defaultContent: [
//                     '<a href="#" onclick="editSuggestion(this)" class="on-default edit-row"><i class="fa fa-pencil"></i></a>',
//                     '<a href="#" onclick="deleteSuggestion(this)" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>'
//                 ].join(' ')
//             }           
//         ]
//     }
//     );

//     $(".dt-buttons").hide();
// }


// function UpdateModalScrolling()
// {
//     if($('.modal').length == 0)
//     {
//         $('body').removeClass('modal-open');
//         console.log('no modal');
//     }
//     else
//     {
//         $('body').addClass('modal-open');
//         console.log("there's modal");
//     }
// }


function asyncFunction (item, cb) {
  setTimeout(() => {
      
    cb();
  }, 100);
}