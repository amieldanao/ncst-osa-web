
(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];
addUserCallback();
// var initial_role;
// var myRole;
function addUserCallback()
{
    let email = $("#user_email").text();

    let query = db.collection('users').where('email', '==', email);

    query.onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {


            if (change.type === "added") {
                let role = change.doc.data().role;
                initial_role = role;
                setRole(role);
                if(role == 2 || role == 1)
                {
                    $("#table_tr th").last().attr('id', 'actions_th');//append('<th id="actions_th" class="role_admin">Actions</th>');
                }
            }

            if (change.type === "modified") {
                setRole(change.doc.data().role);
                $("body").empty();
                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                 setTimeout(() => {
                    location.replace("index.html");
                }, 3000);
            }

            if (change.type === "removed") {
                // console.log("Removed user: ", change.doc.data());
                // window.location = "index.html";
            }
        });
    },
        function(error){
        console.log('retry add user callback');
        addUserCallback();
    });
}

function setRole(newRole)
{
    switch(newRole)
    {
        case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
        case 1 : $(".role_admin").remove();break;
    }
    
    $("#user_role").text("Role: " + roles_string[newRole]);
    $("#user_role").attr("data-done", true);
}

})();





'user strict'


var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);



function Start()
{
    sessionStorage.setItem("toEdit", '');
    fetchUsedColors();
    // initTable();    
}

$(document).ready( function () {
    observer.observe(targetNode, config);
//    initButtonCallbacks();
});



'user strict'

var categories_table;
var edit_dialog;
var category_count = 0;
var used_category_colors = new Array();

function fetchUsedColors()
{
    db.collection("usedColors")
    .get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            // console.log(doc.id, " => ", doc.data());
            used_category_colors.push(doc.data().color);
        });

        initTable();
    });
}

function initTable()
{   
    let btns = [];
    if(document.getElementById("actions_th") != null)
    {
        console.log("has action column!");
        // myColumns.push({"sClass": "actions_buttons" });
        btns = [
            {
                text: 'Add <i class="fa fa-plus"></i>',
                action: function ( e, dt, node, config ) {
                    showPromptEdit()
                },
                "className": 'btn btn-primary'
            }
        ]
    }

    categories_table = $('#categories_table').DataTable(
    {
        dom: 'Bfrltip',
        "scrollX": true,
        buttons: btns,
        "aoColumns": [
        {"sClass": "col_name" },
        {"sClass": "col_description" },
        // {"sClass": "col_color" },
        {"sClass": "actions_buttons" }
        ],
        "columnDefs": [ {
            "targets": 2,
            "orderable": false
        }],
        initComplete: function(settings, json) {
            // alert( 'DataTables has finished its initialisation.' );
            addDBListener();
        }
    }
    );

    // addDBListener();
}


function addDBListener()
{
    showLoading(true, "Loading Categories...");
    try {

        db.collection("categories")
        .onSnapshot(function(snapshot) { 
            let requests = snapshot.docChanges().map((change) => {

                var d = {name: "", description : "", color : ""};


                if('name' in change.doc.data())
                    d['name'] = change.doc.data().name;
                
                if('description' in change.doc.data())
                    d['description'] = change.doc.data().description;
                
                if('color' in change.doc.data())
                    d['color'] = change.doc.data().color;

                if (change.type === "added") {

                    let newData =
                    [
                        '<span class="badge badge-info" style="background-color:'+d.color+';font-size: 1rem;">' + capitalizeFLetter(d.name) + '</span>',
                        d.description                  
                    ]


                    if(document.getElementById("actions_th") != null)
                        newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickEdit(this);" class="on-default edit-row mx-2"><i class="fa fa-pencil"></i></a><a data-key="'+change.doc.id+'" href="#" onclick="showPromptDelete(this);" class="on-default remove-row mx-2"><i class="fa fa-trash-o"></i></a>');
                    else
                        newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickView(this);" class="on-default edit-row mx-2"><i class="fas fa-eye"></i></a>');
                    
                    let newRow = categories_table.row.add(newData).node();

                    

                    newRow.id = change.doc.id;

                    categories_table.row($(newRow)).invalidate().draw();

                    $(newRow).find(".col_name > span").css('color', invertColor(d.color, true));

                    // console.log(newRow);
                    // $(newRow).find(".col_color").css('background-color', d.color);
                    

                    // console.log("Added category: ", change.doc.data());
                }
                
                if (change.type === "modified") {
                    // console.log("Modified category: ", change.doc.data());
                    let key = change.doc.id;
                    let tr = $('.remove-row[data-key="'+key+'"]').parent().parent();
            
                    tr.find(".col_name").html('<span class="badge badge-info" style="background-color:'+d.color+';font-size: 1rem;">' + capitalizeFLetter(d.name) + '</span>');
                    
                    tr.find(".col_description").text(d.description);

                    tr.find(".col_name > span").css('color', invertColor(d.color, true));
                }
                if (change.type === "removed") {
                    // $('tr[data-key="'+change.doc.id+'"]').parent().parent().remove();
                    categories_table.row('#'+change.doc.id).remove().draw();
                    
                    categories_table.draw();
                }
                
                return new Promise((resolve) => {
                  asyncFunction(change, resolve);
              });
            });

            Promise.all(requests).then(() => {showLoading(false);});
        });
    }
    catch(err) {
      bootbox.alert(err.message);
  }
}


function clickView(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    var name = tr.find(".col_name").text();
    var description = tr.find(".col_description").text();
    var color = tr.find(".col_name > span").get(0).style.backgroundColor;
    

    bootbox.alert(
    {
        title: name,
        message : description + '<br><hr>Color: <br><p style="background-color:' + color + '; color:white;">'+color+'</p>',
        centerVertical : true,
        scrollable : true
    });
}

function clickEdit(elem)
{
    var name = $(elem).parent().parent().find(".col_name").text();
    var description = $(elem).parent().parent().find(".col_description").text();    
    var color = $(elem).parent().parent().find(".col_color").text();
    
    var key = $(elem).attr('data-key');
    sessionStorage.setItem("toEdit", key);
    
    showPromptEdit(name, description);
}


function showPromptEdit(_name, _description)
{
    var isNew = (_name == undefined && _description == undefined);
    edit_dialog = bootbox.dialog({
        show:false,
        title: 'Manage Category',
        message: '<form id="infos" action="">'+
        'Name:<input style="width:100%;" class="col_name" type="text"><br/>'+
        'Description:<input style="width:100%;" class="col_description" type="text"><br>',
        size:'large',
        centerVertical:true,
        buttons: {
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function(){
                    sessionStorage.setItem("toEdit", '');

                    destroyEditModal();
                }
            },
            ok: {
                label: 'Save <i class="fa fa-save"></i>',
                className: 'btn-success',
                callback: function(){

                    var name = $('#infos .col_name').val();
                    if(name == undefined || name.trim().length == 0)
                    {
                        bootbox.alert({
                            message:"Category name can't be blank!",
                            centerVertical: true,
                            className : "topmost-modal",
                            size : 'small'
                        });
                        return false;
                    }

                    // Check if category name doesn't exist yet.
                    let query = db.collection("categories").where("name", "==", name.toLowerCase());

                    query.get().then(function(querySnapshot) {

                        console.log(querySnapshot.size);

                        if(querySnapshot.size > 0 && (_name == undefined || (_name != undefined && _name.toLowerCase() != name.toLowerCase())))
                        {
                            bootbox.alert({
                                message:"Category name already exists!",
                                centerVertical: true,
                                className : "topmost-modal",
                                size : 'small'
                            });

                            return false;
                        }

                        let description = $('#infos .col_description').val();
                        let color = null;
                        
                        if(isNew){
                            color = getRandomColor();//$('#infos .col_color').val();

                            while(used_category_colors.includes(color))
                            {
                                color = getRandomColor();
                            }
                        }
                        
                        saveCategory(name.toLowerCase(), description, color);

                    });

                    return false;
                }
            }
        }
    });

    edit_dialog.modal('show');

    edit_dialog.on('shown.bs.modal', function(e){
        $("#infos .col_name").val(capitalizeFLetter(_name));
        $("#infos .col_description").val(_description);
        // $("#infos .col_color").val(_color);
    });
}

function destroyEditModal() {
  edit_dialog.remove();
  $(".modal-backdrop").remove();
  // removeClass("in");
  // $(".modal-backdrop").remove();
  // $('body').removeClass('modal-open');
  // $('body').css('padding-right', '');
  // edit_dialog.hide();
}


function saveCategory(_name, _description, _color)
{
    var key = sessionStorage.getItem("toEdit");//$('#infos').attr('data-key');
    var ref = db.collection("categories").doc();
    
    if(key == undefined || key.length == 0)
    {
        key = ref.id;
    }
    else
    {
        ref = db.collection("categories").doc(key);
    }
    
    sessionStorage.setItem("toEdit", "");
    
    showLoading(true, "Saving Category...");    

    try
    {
        var newCategoryData = {};
        newCategoryData["name"] = _name;
        newCategoryData["description"] = _description;

        if(_color != null)
            newCategoryData["color"] = _color;

        ref.set(
            newCategoryData,
        {merge:true})
        .then(function(docRef) {
            showNotifSuccess('Saved', capitalizeFLetter(_name) + ' saved successfully');
            showLoading(false);
            
            if(_color != null){
                db.collection("usedColors").add({color:_color});
                used_category_colors.push(_color);
            }

            destroyEditModal();
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
            PNotify.error({
              title: 'Error',
              text: error
          });
            showLoading(false);
        });
    }
    catch(err) {
      bootbox.alert(err.message);
  }
}

function showPromptDelete(elem)
{
    sessionStorage.setItem("toEdit", $(elem).attr('data-key'));
    
    bootbox.confirm({
        title:$(elem).parent().parent().find(".col_name").text(),
        message: "Are you sure you want to delete this Category?",
        size:'small',
        centerVertical: true,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {

            if(result)
            {
                clickRemove();
            }
            else
            {
                sessionStorage.setItem("toEdit", '');
            }
            
        }
    });
}

function clickRemove()
{   
    var key = sessionStorage.getItem("toEdit");
    
    if(key.trim().length > 0)
    {
        showLoading(true, "Deleting...");

        try
        {
            db.collection("categories").doc(key).delete().then(function() {

                PNotify.info({
                  title: 'Deleted',
                  text: 'Category deleted successfully'
              });

                showLoading(false);

                categories_table.row('#'+key).remove().draw();

                sessionStorage.setItem('toEdit', '');
                
            }).catch(function(error) {
                console.error("Error removing document: ", error);
                PNotify.error({
                  title: 'Error',
                  text: error
              });
                showLoading(false);
                sessionStorage.setItem('toEdit', '');
            });
        }
        catch(error)
        {
            bootbox.alert(error.message);
            showLoading(false);
        }
    }
}

function asyncFunction (item, cb) {
  setTimeout(() => {

    cb();
}, 100);
}