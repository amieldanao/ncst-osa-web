(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];
addUserCallback();
// var initial_role;
// var myRole;
function addUserCallback()
{
    let email = $("#user_email").text();

    let query = db.collection('users').where('email', '==', email);

    query.onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {


            if (change.type === "added") {
                let role = change.doc.data().role;
                initial_role = role;
                setRole(role);
                if(role == 2)
                {
                    $("#users_table_tr").append('<th id="actions_th" class="role_admin">Actions</th>');
                }
            }

            if (change.type === "modified") {
                setRole(change.doc.data().role);
                $("body").empty();
                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                 setTimeout(() => {
                    location.replace("index.html");
                }, 3000);
            }

            if (change.type === "removed") {
                // console.log("Removed user: ", change.doc.data());
                // window.location = "index.html";
            }
        });
    },
        function(error){
        console.log('retry add user callback');
        addUserCallback();
    });
}

function setRole(newRole)
{
    switch(newRole)
    {
        case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
        case 1 : $(".role_admin").remove();break;
        // case 2 : if(modified){
                    // location.reload();}break;
    }

    
    $("#user_role").text("Role: " + roles_string[newRole]);
    $("#user_role").attr("data-done", true);

    // initial_role = newRole;


}

})();


'user strict'

var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);

observer.observe(targetNode, config);

function Start()
{
    initPermissions();
}

$(document).ready(function () 
{   
    // Start observing the target node for configured mutations
    
});




var pages = 
[
	'dashboard',	
	'guidelines',	
	'announcements',	
	// 'actions',	
	'reports',	
	'categories',	
	'suggestions',	
	'students',	
	'users',	
	'permissions',
    'messaging'
];

function initPermissions()
{
    bootbox.setDefaults({size:'small', centerVertical:true});
    populateCheckboxes(0);
    // addButtonsCallback();
	addPermissionCallback();
}

function populateCheckboxes(index)
{
    let i = 0;
    let requests = pages.map((change) => {


        let toAppend = '<li class="list-group-item">'+
            '<div class="form-check form-check-inline">'+
              '<input class="form-check-input" type="checkbox" id="chk'+i+ index+ '" value="'+pages[i]+'">'+
              '<label class="form-check-label" for="chk'+i+ index+'">'+capitalizeFLetter(pages[i])+'</label>'+
            '</div>'+
        '</li>';

        $("#page_list_" + index).append(toAppend);

        console.log(i);

        i++;

        return new Promise((resolve) => {
          asyncFunction(change, resolve);
        });

    });

    Promise.all(requests).then(() => {
        
        // callback(index);
        console.log('done populating page_list_' + index);
        if(document.getElementById("page_list_" + (index+1)) != null)
        {
            populateCheckboxes(index + 1);
        }
        else
        {
            addButtonsCallback();
        }
    });
}

function addButtonsCallback()
{
    $("form").each(function(i){
        $(this).get(0).reset();
    });

    $('input[type="checkbox"]').change(function(){
        if($(this).is(':checked')) {
            // Checkbox is checked..
            $(this).parent().parent().css('background-color', 'white');
        } else {
            // Checkbox is not checked..
            $(this).parent().parent().css('background-color', 'lightgray');
        }
    });

    $("#saveButton0").on('click', function(){
        savePermissions(0);
    });

    $("#saveButton1").on('click', function(){
        savePermissions(1);
    });
}

function savePermissions(thisRole)
{
    showLoading(true, "Saving permissions");
    let role_query = db.collection('permissions').where('role', '==', thisRole);

    try
    {
        role_query.get().then(function(roleQuerySnapshot){

            let ref = db.collection('permissions').doc();
            let selectedPagesArray = new Array();

            $("#page_list_" + thisRole + " input").each(function(index){
                console.log($(this).val());
                if($(this).get(0).checked == true)
                {
                    selectedPagesArray.push($(this).val());
                }
            });

            let data = 
            {
                pages: selectedPagesArray,
                role: thisRole
            };

            if(roleQuerySnapshot.size > 0)
            {
                ref = db.collection('permissions').doc(roleQuerySnapshot.docs[0].id);

                writePermissionData(data, ref);
            }
            else
            {
                writePermissionData(data, ref);
            }

        })
        .catch(function(error){
            showLoading(false);
            bootbox.alert(error.message);
        });

    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(error.message);
    }
}

function writePermissionData(data, ref)
{
    try
    {
        ref.set(data, {merge:true}).then(function(docRef){
            showLoading(false);
            showNotifSuccess("Permissions saved successfully.");

        })
        .catch(function(error){
            showLoading(false);
            bootbox.alert(error.message);
        });
    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(error.message);
    }
}

function addPermissionCallback()
{
	showLoading(true, "Loading permissions...");

	db.collection("permissions")
	.onSnapshot(function(snapshot) { 
        let requests = snapshot.docChanges().map((change) => {
            var key = change.doc.id;


            if (change.type === "added") {
            	let data = change.doc.data();
            	let thisRole = data.role;
                console.log("this role {0}", thisRole);

            	$("#page_list_" + thisRole + " input").each(function(index){
                    // console.log($(this).val());
            		if(data.pages.indexOf($(this).val()) > -1)
            		{
            			$(this).get(0).checked = true;
                        $(this).parent().parent().css('background-color', 'white');
            		}
                    else
                    {
                        $(this).get(0).checked = false;
                        $(this).parent().parent().css('background-color', 'lightgray');
                    }
            	});
            }
            
            if (change.type === "modified") {

            }

            if (change.type === "removed") {

            }


            return new Promise((resolve) => {
              asyncFunction(change, resolve);
            });

        });

        Promise.all(requests).then(() => {showLoading(false);});

    });
}

function asyncFunction (item, cb) {
  setTimeout(() => {

    cb();
}, 100);
}