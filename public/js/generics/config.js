/*jslint indent: 4 */
/*global requirejs*/

requirejs.config({
    baseUrl : 'js',
    paths : {        
        "jquery" : 'https://code.jquery.com/jquery-3.5.1.min',
        "bootstrap" : 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min',
        "bootbox" : 'https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min',
        "anime" : 'https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.0/anime.min',        
        "generic" : 'generics/generic',
        "loading" : 'generics/loading',
        "login" : 'login',
        "@firebase/app" : 'https://cdnjs.cloudflare.com/ajax/libs/firebase/7.15.1-0/firebase-app.min',
        "@firebase/auth" : 'https://cdnjs.cloudflare.com/ajax/libs/firebase/7.15.1-0/firebase-auth.min',
        "firebase_init" : 'generics/firebase-init'        
    },
    onNodeCreated: function(node, config, module, path) {
    // Here's  alist of differet integrities for different scripts
    // Append to this list for all scripts that you want SRI for
    var sri = {
        jquery : 'sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=',
        bootstrap : 'sha384-1CmrxMRARb6aLqgBO7yyAxTOQE2AKb9GfXnEo760AUcUmFx3ibVJJAzGytlQcNXd',
        bootbox : 'sha256-sfG8c9ILUB8EXQ5muswfjZsKICbRIJUG/kBogvvV5sY=',
        anime : 'sha256-hBMojZuWKocCflyaG8T19KBq9OlTlK39CTxb8AUWKhY=',
        firebase : 'sha256-Zm2WHPWMAhBxC+4G/Uw0FmZqW7a7auqogsCBkaY1KfU=',
        firebase_auth : 'sha256-TYK57RI56cdDTNliivARORkv5WtyH8hjS8xlMDKJmgY='
    };
    
    if (sri[module]) {
      node.setAttribute('integrity', sri[module]);
      node.setAttribute('crossorigin', 'anonymous');
    }
  }
});