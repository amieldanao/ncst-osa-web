
var myRole;

'use strict';
// var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var defaultToolbar = [
              ['style', ['style']],
              ['font', ['bold', 'underline', 'clear']],
              ['fontname', ['fontname']],
              ['color', ['color']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['table', ['table']],
              ['insert', ['link', 'picture', 'video']],
              ['view', ['fullscreen', 'codeview']],
            ];


$("#content > .navbar > .navbar-nav").prepend(`<li class="nav-item dropdown no-arrow">
    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="mr-2  text-gray-600 small" id="user_role">Role:</span>
    </a>
</li>`);






$(document).ready(function () {
    

    // var is_root = location.pathname == "/";
    var is_root = window.location.href.includes('index.html') || window.location.toString().substr(window.location.toString().length - 1) == "/";
    if(is_root == false){
    // if(!is_root){
        $.unblockUI();


        

    } 

    PNotify.defaultModules.set(PNotifyBootstrap4, {});
    PNotify.defaultModules.set(PNotifyFontAwesome5Fix, {});
    PNotify.defaultModules.set(PNotifyFontAwesome5, {});
    PNotify.defaultModules.set(PNotifyAnimate, {});
    PNotify.defaultModules.set(PNotifyMobile, {swipeDismiss: false, styling: true});

    // Align modal when it is displayed
    $("#modal_loading").on("shown.bs.modal", alignLoadingModal);
    // Align modal when user resize the window
    $(window).on("resize", function(){
        $("#tempModal:visible").each(alignLoadingModal);
    });

    toggleSidebar();
    
    $('.datepicker').each(function(){
      var dom = $(this).get(0);

      dom.onpaste = function(e) {
       e.preventDefault();
     }
   });


    $('body').on('keyup', '.inputNumericOnly', function(e){
        let str = $(this).val().replace(/[^0-9]/g, '');
        console.log("keyup!");

        $(this).val(str);
    });



    $('body').on('keyup', '.inputNumericOnlyWithDash', function(e){
        let str = $(this).val().replace(/[^0-9\-]/g, '');
        console.log("keyup!");

        $(this).val(str);
      });

    // for input names prevent special characters
    
    $('body').on('keyup', '.inputNamesOnly', function(e){
        var nospecial=/[*|\":<>[\]{}`\\()';@&#_%^+/?!~$0-9-]/;

        let str = $(this).val().replace(nospecial, '');
        // console.log("keyup!");

        $(this).val(str);
    });

    $('body').on('keydown', '.inputNamesOnly', function(e){
        var nospecial=/[*|\":<>[\]{}`\\()';@&#_%^+/?!~$0-9-]/;

        let str = $(this).val().replace(nospecial, '');
        // console.log("keyup!");

        $(this).val(str);
    });

    $('body').on('hidden.bs.modal', '.modal', function (e) {
      // do something...
        UpdateModalScrolling();
    });

    $( "body" ).bind("DOMNodeRemoved", function(e){
      UpdateModalScrolling();
      // if(e.target.id == "id") {
      //    // do something
      // }
    });

  // $('.inputNumericOnly').each(function(){
  //       var dom = $(this).get(0);

  //       dom.onpaste = function(e) {
  //        e.preventDefault();
  //      }

  //   });

});


const toTitleCase = (phrase) => {
  return phrase
    .toLowerCase()
    .split(' ')
    .map(word => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');
};



function ExcelDateToJSDate(serial) {
   var utc_days  = Math.floor(serial - 25569);
   var utc_value = utc_days * 86400;                                        
   var date_info = new Date(utc_value * 1000);

   var fractional_day = serial - Math.floor(serial) + 0.0000001;

   var total_seconds = Math.floor(86400 * fractional_day);

   var seconds = total_seconds % 60;

   total_seconds -= seconds;

   var hours = Math.floor(total_seconds / (60 * 60));
   var minutes = Math.floor(total_seconds / 60) % 60;

   return new Date(date_info.getFullYear(), date_info.getMonth(), date_info.getDate(), hours, minutes, seconds);
}


function UpdateModalScrolling()
{
    if($('.modal:visible').length == 0)
    {
        $('body').removeClass('modal-open');
        // console.log('no modal');
    }
    else
    {
        $('body').addClass('modal-open');
        // console.log("there's modal");
    }
}


function addPastePrevent(elem)
{
    
    elem.each(function(e, i){

        var dom = elem.get(e);
        dom.onpaste = function(e) {
        e.preventDefault();
    }
    });
    
}

function capitalizeFLetter(str) { 
    if(str == undefined)
        return "";
    let res = str[0].toUpperCase() + str.slice(1); 
    return res;
} 


function invertColor(hex, bw) {
    if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
    }
    // convert 3-digit hex to 6-digits.
    if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    if (hex.length !== 6) {
        console.error(hex);
        throw new Error('Invalid HEX color.');
    }
    var r = parseInt(hex.slice(0, 2), 16),
        g = parseInt(hex.slice(2, 4), 16),
        b = parseInt(hex.slice(4, 6), 16);
    if (bw) {
        // http://stackoverflow.com/a/3943023/112731
        return (r * 0.299 + g * 0.587 + b * 0.114) > 186
            ? '#000000'
            : '#FFFFFF';
    }
    // invert color components
    r = (255 - r).toString(16);
    g = (255 - g).toString(16);
    b = (255 - b).toString(16);
    // pad each with zeros and return
    return "#" + padZero(r) + padZero(g) + padZero(b);
}

function padZero(str, len) {
    len = len || 2;
    var zeros = new Array(len).join('0');
    return (zeros + str).slice(-len);
}

function getTextFromHTML(str)
{
    var html = str;
    var div = document.createElement("div");
    div.innerHTML = html;
    var text = div.textContent || div.innerText || "";
    return text;
}

function fadeInOutElement(elem, newHTML, fadeTime = 1000)
{
    $(elem).fadeOut(fadeTime, function() {
        $(this).html(newHTML).fadeIn(fadeTime);
    });
}


function toggleSidebar()
{
    $(".sidebar-toggle").on('click', function(){
        if($("html").hasClass("sidebar-left-collapsed"))
        {
            sessionStorage.setItem("sidebar", "hide");
            $('#accordionSidebar > .nav-item > .nav-link  > span').css('display', 'none !important');
        }
        else
        {
            sessionStorage.setItem("sidebar", "show");
            $('#accordionSidebar > .nav-item > .nav-link  > span').css('display', 'block !important');
        }
        console.log("nav sidebar clicked!");
    });

    var sidebarSetting = sessionStorage.getItem("sidebar");
    if(sidebarSetting == undefined || sidebarSetting == "hide")
        $("html").addClass("sidebar-left-collapsed");
    else
        $("html").removeClass("sidebar-left-collapsed");
}

function ShowMessage(title, msg, type)
{
    closeMessageModal();

    var htmlModal = 
        '<div id="tempModal" class="modal fade" role="dialog">'+
        '<div class="modal-dialog">'+
        '<div class="modal-content">'+
        '<div class="modal-header">'+
        '<div class="alert alert-' + type + '">' +
        '<h6 class="modal-title">'+title+'</h6>'+
        '</div>'+
        '</div>'+
        '<div class="modal-body">'+			
        '<p>'+msg+'</p>'+			
        '</div>'+
        '<div class="modal-footer">'+
        '<button type="button" class="btn btn-default" onclick="closeMessageModal()">Close</button>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>';

    var newMessage = $('body').append(htmlModal);

    $('#tempModal').modal('show');
    $('#loadingModal').hide();
}

function closeMessageModal()
{
    $("#tempModal").next().remove();//eq( $(".modal-backdrop").index( 1 )).remove();
    $("#tempModal").remove();
}

function alignLoadingModal(){
    var modalDialog = $("#tempModal > .modal-dialog");//$(this).find(".modal-dialog");
    /* Applying the top margin on modal dialog to align it vertically center */
    modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
    modalDialog.css("margin-left", Math.max(0, ($(window).width() - modalDialog.width()) / 2));
}

function ValidateEmail(mail) 
{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
    {
        return (true)
    }
    //alert("You have entered an invalid email address!")
    return (false);
}

// Validates that the input string is a valid date formatted as "mm/dd/yyyy"
function isValidDate(dateString)
{
    // First check for the pattern
    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("/");
    var day = parseInt(parts[1], 10);
    var month = parseInt(parts[0], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if(year < 1000 || year > 3000 || month == 0 || month > 12)
        return false;

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
}


function showNotifSuccess(_title, _text)
{
    PNotify.notice({
      title: _title,
      text: _text,
      delay : 2000,
      styling: 'custom_success',
      // maxTextHeight: null,
      // addModelessClass: 'nonblock',
      icon: 'fas fa-check-circle'
      // ,      
      // buttons: { closer: true, 
      //     labels: {close: "Fechar", stick: "Manter"}
      // }
    });
}

function showNotifError(_title, _text)
{
    PNotify.notice({
      title: _title,
      text: _text,
      delay : 2000,
      styling: 'custom_error',
      // maxTextHeight: null,
      // addModelessClass: 'nonblock',
      icon: 'fas fa-exclamation-circle'
    });
}

function shadeColor(color, percent) {

    var R = parseInt(color.substring(1,3),16);
    var G = parseInt(color.substring(3,5),16);
    var B = parseInt(color.substring(5,7),16);

    R = parseInt(R * (100 + percent) / 100);
    G = parseInt(G * (100 + percent) / 100);
    B = parseInt(B * (100 + percent) / 100);

    R = (R<255)?R:255;  
    G = (G<255)?G:255;  
    B = (B<255)?B:255;  

    var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
    var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
    var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

    return "#"+RR+GG+BB;
}

function lerpColor(a, b, amount) { 

    var ah = parseInt(a.replace(/#/g, ''), 16),
        ar = ah >> 16, ag = ah >> 8 & 0xff, ab = ah & 0xff,
        bh = parseInt(b.replace(/#/g, ''), 16),
        br = bh >> 16, bg = bh >> 8 & 0xff, bb = bh & 0xff,
        rr = ar + amount * (br - ar),
        rg = ag + amount * (bg - ag),
        rb = ab + amount * (bb - ab);

    return '#' + ((1 << 24) + (rr << 16) + (rg << 8) + rb | 0).toString(16).slice(1);
}

function sameDay(d1, d2) {
  return d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate();
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function getRandomColor(){
    let n = (Math.random() * 0xfffff * 1000000).toString(16);
    return "#" + n.slice(0, 6);
}

// function trimCanvasNEW(canvas){
//     const context = canvas.getContext('2d');

//     const topLeft = {
//         x: canvas.width,
//         y: canvas.height,
//         update(x,y){
//             this.x = Math.min(this.x,x);
//             this.y = Math.min(this.y,y);
//         }
//     };

//     const bottomRight = {
//         x: 0,
//         y: 0,
//         update(x,y){
//             this.x = Math.max(this.x,x);
//             this.y = Math.max(this.y,y);
//         }
//     };

//     const imageData = context.getImageData(0,0,canvas.width,canvas.height);

//     for(let x = 0; x < canvas.width; x++){
//         for(let y = 0; y < canvas.height; y++){
//             const alpha = imageData.data[((y * (canvas.width * 4)) + (x * 4)) + 3];
//             if(alpha !== 0){
//                 topLeft.update(x,y);
//                 bottomRight.update(x,y);
//             }
//         }
//     }

//     const width = bottomRight.x - topLeft.x;
//     const height = bottomRight.y - topLeft.y;

//     const croppedCanvas = context.getImageData(topLeft.x,topLeft.y,width,height);
//     canvas.width = width;
//     canvas.height = height;
//     context.putImageData(croppedCanvas,0,0);

//     return canvas;
// }


function trimCanvas(c) {
    var ctx = c.getContext('2d'),
        copy = document.createElement('canvas').getContext('2d'),
        pixels = ctx.getImageData(0, 0, c.width, c.height),
        l = pixels.data.length,
        i,
        bound = {
            top: null,
            left: null,
            right: null,
            bottom: null
        },
        x, y;
    
    // Iterate over every pixel to find the highest
    // and where it ends on every axis ()
    for (i = 0; i < l; i += 4) {
        if (pixels.data[i + 3] !== 0) {
            x = (i / 4) % c.width;
            y = ~~((i / 4) / c.width);

            if (bound.top === null) {
                bound.top = y;
            }

            if (bound.left === null) {
                bound.left = x;
            } else if (x < bound.left) {
                bound.left = x;
            }

            if (bound.right === null) {
                bound.right = x;
            } else if (bound.right < x) {
                bound.right = x;
            }

            if (bound.bottom === null) {
                bound.bottom = y;
            } else if (bound.bottom < y) {
                bound.bottom = y;
            }
        }
    }
    
    // Calculate the height and width of the content
    var trimHeight = bound.bottom - bound.top;
    var trimWidth = bound.right - bound.left;

    var trimmed = ctx.getImageData(bound.left, bound.top, trimWidth, trimHeight);

    copy.canvas.width = trimWidth;
    copy.canvas.height = trimHeight;
    copy.putImageData(trimmed, 0, 0);

    // Return trimmed canvas
    return copy.canvas;
}

var TimeAgo = (function() {
    var self = {};
    
    // Public Methods
    self.locales = {
      prefix: '',
      sufix:  'ago',
      
      seconds: 'less than a minute',
      minute:  'about a minute',
      minutes: '%d minutes',
      hour:    'about an hour',
      hours:   'about %d hours',
      day:     'a day',
      days:    '%d days',
      month:   'about a month',
      months:  '%d months',
      year:    'about a year',
      years:   '%d years'
    };
    
    self.inWords = function(timeAgo, fromDate) {
      var seconds = Math.floor((fromDate - parseInt(timeAgo)) / 1000),
          separator = this.locales.separator || ' ',
          words = this.locales.prefix + separator,
          interval = 0,
          intervals = {
            year:   seconds / 31536000,
            month:  seconds / 2592000,
            day:    seconds / 86400,
            hour:   seconds / 3600,
            minute: seconds / 60
          };
      
      var distance = this.locales.seconds;
      
      for (var key in intervals) {
        interval = Math.floor(intervals[key]);
        
        if (interval > 1) {
          distance = this.locales[key + 's'];
          break;
        } else if (interval === 1) {
          distance = this.locales[key];
          break;
        }
      }
      
      distance = distance.replace(/%d/i, interval);
      words += distance + separator + this.locales.sufix;
  
      return words.trim();
    };
    
    return self;
  }());


var DurationAgo = (function() {
    var self = {};
    
    // Public Methods
    self.locales = {
      prefix: '',
      sufix:  '',
      
      seconds: 'less than a minute',
      minute:  'about a minute',
      minutes: '%d minutes',
      hour:    'hour',
      hours:   '%d hours',
      day:     'day',
      days:    '%d days',
      month:   'month',
      months:  '%d months',
      year:    'year',
      years:   '%d years'
    };
    
    self.inWords = function(timeAgo, fromDate) {
      var seconds = Math.floor((fromDate - parseInt(timeAgo)) / 1000),
          separator = this.locales.separator || ' ',
          words = this.locales.prefix + separator,
          interval = 0,
          intervals = {
            year:   seconds / 31536000,
            month:  seconds / 2592000,
            day:    seconds / 86400,
            hour:   seconds / 3600,
            minute: seconds / 60
          };
      
      var distance = this.locales.seconds;
      
      for (var key in intervals) {
        interval = Math.floor(intervals[key]);
        
        if (interval > 1) {
          distance = this.locales[key + 's'];
          break;
        } else if (interval === 1) {
          distance = this.locales[key];
          break;
        }
      }
      
      distance = distance.replace(/%d/i, interval);
      words += distance + separator + this.locales.sufix;
  
      return words.trim();
    };
    
    return self;
  }());

  function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  function toTimeZone(time, zone) {
    var format = 'YYYY/MM/DD HH:mm:ss ZZ';
    return moment(time, format).tz(zone).format(format);
}