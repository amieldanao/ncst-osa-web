var lastState = false;
var callingLoading = false;
var myBackdrop = null;


$(document).ready(function () 
                  {
    //    $('body').append('<div id="modal_loading" class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display:none;" aria-hidden:"true">  <div class="modal-dialog modal-sm" id="loadingModal"></div> <h2 style="color: white; margin:auto; text-align:center; z-index=-99999; vertical-align: bottom;" id="loadingText"></h2></div>');
    //    alignModal();
    //    initLoadingSpinner();
    

    $('body').append('<div id="loadingModal" class="modal fade topmost-modal" role="dialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-body text-center d-block"><div class="loader d-inline-block"></div><h3 id="loadingText"></h3></div></div></div></div>');

    $('#loadingModal').modal({show : false, keyboard : false, focus : false, backdrop: 'static'});

    $("body").on("shown.bs.modal", '#loadingModal', function(){
//        callingLoading = false;
        if(lastState == false)
            showLoading(false);

        myBackdrop = $(".modal-backdrop").last();
        // backdrop.id = 'loadingModalBackdrop';
        // console.log(backdrop);
        myBackdrop.css('z-index', parseInt($("#loadingModal").css('z-index'))-1);
//        alignModal();
    });


    $("body").on("hidden.bs.modal", '#loadingModal', function(){
        if(myBackdrop)
            myBackdrop.remove();
        myBackdrop = null;
    });

    // $("body").on("domChanged", function () {
    //     //dom is changed 
    //     if(myBackdrop == null)
    //     {
    //         myBackdrop
    //     }
    // });
});


//function initLoadingSpinner()
//{
//    var opts = {
//        lines: 13, // The number of lines to draw
//        length: 38, // The length of each line
//        width: 17, // The line thickness
//        radius: 45, // The radius of the inner circle
//        scale: 1, // Scales overall size of the spinner
//        corners: 1, // Corner roundness (0..1)
//        color: ["#ffd100", "#2e3192"], // CSS color or array of colors
//        fadeColor: 'transparent', // CSS color or array of colors
//        speed: 1, // Rounds per second
//        rotate: 0, // The rotation offset
//        animation: 'spinner-line-shrink', // The CSS animation name for the lines
//        direction: 1, // 1: clockwise, -1: counterclockwise
//        zIndex: 2e9, // The z-index (defaults to 2000000000)
//        className: 'spinner', // The CSS class to assign to the spinner
//        top: '50%', // Top position relative to parent
//        left: '50%', // Left position relative to parent
//        shadow: '0 0 1px transparent', // Box-shadow for the lines
//        position: 'absolute' // Element positioning
//    };
//
//    var target = document.getElementById('loadingModal');
//    // var spinner = new Spinner(opts).spin(target);
//    var spinner = new Spin.Spinner(opts).spin(target);
//
//    // Align modal when it is displayed
//    $("#modal_loading").on("shown.bs.modal", function(){
//        callingLoading = false;
//        if(lastState == false)
//            showLoading(false);
//        alignModal();
//    });
//
//    // Align modal when user resize the window
//    $(window).on("resize", function(){
//        $("#modal_loading:visible").each(alignModal);
//    });
//
//    $('#modal_loading').modal('hide');
//    showLoading(false);
//}


//function alignModal(){  
//    var modalDialog = $("#loadingModal");//$(this).find(".modal-dialog");  
//    /* Applying the top margin on modal dialog to align it vertically center */            
//    $("#loadingText").css("margin-top", Math.max(0, ($(window).height() - $("#loadingText").height()) / 1.4));
//    modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));    
//}


function showLoading(show, txt="")
{
    if(show)
    {
        $("#loadingText").text(txt);
        $('#loadingModal').modal('show');
        // $("#loadingModal").css("z-index", "99999999999");
        lastState = true;
    }
    else{
        $('#loadingModal').modal('hide');

        // setTimeout(() => {
        //     if(myBackdrop)
        //         myBackdrop.remove();
        // }, 1);
        


        lastState = false;    
    }
}