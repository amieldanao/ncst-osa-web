(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];
var myRole = 0;

addPageAccessCallback();
// var initial_role;
// var myRole;



function addPageAccessCallback()
{

    // showLoading(true, "Loading admin user");
    var is_root = window.location.href.includes('index.html') || window.location.toString().substr(window.location.toString().length - 1) == "/";
    if(is_root == false){
    // var is_root = location.pathname == "/";
    // if(!is_root){
    
    // $("#dimScreen").css({'height':'100%', 'margin' : 'auto', 'vertical-align':'middle'});
    if($('#dimScreen').length > 0){
            $("#dimScreen").prepend('<img src="images/ncst_logo_256px.png"/>');
            $("#dimScreen").append('<img src="images/loader.gif" />');
            $.blockUI({fadeIn: 1, message: $('#dimScreen') });
        }
    }

    firebase.auth().onAuthStateChanged(function(user) {
        if(is_root == false)
        {
            if (user) {
                

                // check if this role has page access
                let role_query = db.collection('users').where('email', '==', user.email.toLowerCase());
                $("#user_email").text(user.email.toLowerCase());

                
                role_query.get().then(function(roleQuerySnapshot){
                    if(roleQuerySnapshot.size > 0)
                    {
                        myRole = roleQuerySnapshot.docs[0].data().role;
                        var path = window.location.pathname;
                        var thisPage = path.split("/").pop().replace(/.html/g, '').replace(/#/g, '');
                        
                        // Set username
                        let myName = roleQuerySnapshot.docs[0].data().name;
                        $("#user_email").attr('data-myName', myName);
                        // console.log(myName);

                        // let access_query = db.collection('permissions').where('role', '==', myRole).where('pages', 'array-contains', thisPage);

                        // access_query.get().then(function(accessQuerySnapshot){

                        let query = db.collection('permissions').where('role', '==', myRole).where('pages', 'array-contains', thisPage);

                        query.onSnapshot(function(snapshot) {
                            // console.log("snapshot size " + snapshot.size);
                            if(snapshot.size > 0)
                            {
                                snapshot.docChanges().forEach(function(change) {

                                    // console.log("change type " + change.type);
                                    if (change.type === "added") {
                                        if(change.doc.data().role == myRole || myRole == 2){
                                            // console.log("proceed to Start");

                                            setResult('');
                                        }
                                    }

                                    if (change.type === "modified") {

                                        // if(change.doc.data().role == myRole)
                                    }

                                    if (change.type === "removed") {
                                        if(change.doc.data().role == myRole)
                                            setResult('no-acess');
                                    }

                                });
                            }
                            else
                            {
                                setResult('no-acess');
                            }

                        });
                    }
                    else
                    {
                        // bootbox.alert(error.message);
                        // showUnAuthorized();
                        setResult('un-authorized');
                    }

                })
                .catch(function(error){
                    // bootbox.alert(error.message);
                    setResult('un-authorized');
                });

            } else {
                if(is_root == false)
                {
                    // showUnAuthorized();
                    setResult('un-authorized');
                }
            }
        }

        // showLoading(false);
    });

    // let email = $("#user_email").text();

    // let query = db.collection('users').where('email', '==', email);

    // query.onSnapshot(function(snapshot) {
    //     snapshot.docChanges().forEach(function(change) {


    //         if (change.type === "added") {
    //             let role = change.doc.data().role;
    //             initial_role = role;
    //             setRole(role);
    //             if(role == 2)
    //             {
    //                 $("#users_table_tr").append('<th id="actions_th" class="role_admin">Actions</th>');
    //             }
    //         }

    //         if (change.type === "modified") {
    //             setRole(change.doc.data().role);
    //             $("body").empty();
    //             $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

    //              setTimeout(() => {
    //                 location.replace("index.html");
    //             }, 3000);
    //         }

    //         if (change.type === "removed") {
    //             // console.log("Removed user: ", change.doc.data());
    //             // window.location = "index.html";
    //         }
    //     });
    // },
    //     function(error){
    //     console.log('retry add user callback');
    //     addUserCallback();
    // });
}


function setResult(result)
{
    $("footer").attr("data-done", result);
}

})();


var targetNode = document.getElementsByTagName('footer')[0];

// Options for the observer (which mutations to observe)
// const config = { attributes: true, childList: true, subtree: true };
var config = { attributes: true};

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            // console.log('The ' + mutation.attributeName + ' attribute was modified.');

            // console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);


observer.observe(targetNode, config);
var newMessageListeners = [];


function Start()
{
    // PNotify.defaultModules.set(PNotifyDesktop, {});

    
    // if()
    // {
    //     let notice = PNotify.notice({
    //       title: "Notifications permissions",
    //       text: "Please allow message notifications",
    //       modules: new Map([
    //         ...PNotify.defaultModules,
    //         [PNotifyDesktop, {
    //           icon: false
    //         }]
    //       ])
    //     });
    // }


    let result = $('footer').attr('data-done');

    // console.log("result is : " + result);

    if(result == "no-acess")
        showNoAccessPage();
    else if(result == "un-authorized")
        showUnAuthorized();
    else
        LoadScripts();

    AddNewMessageNotfication();
}


$(document).ready(function(){
    PNotifyDesktop.permission();
});

var reportsListener;
var notif_options = {
    maxNotifications: 1,
    durations :
    {
        global : 5000
    },
    icons:
    {
        enabled: false
    }
    
   
}
var notifier = new AWN(notif_options);


function AddNewMessageNotfication()
{    
    notifier.closeToasts();
    fetchServerTime(function()
    {
        var audio = document.createElement("AUDIO");
        audio.src = "audio/piece-of-cake.mp3";
        var startFetchDate = getCurrentDateServerOffset();

        Object.keys(newMessageListeners).forEach((key) => {
            if(newMessageListeners[key] != null)
                newMessageListeners[key]();
        });

        newMessageListeners = [];
        if(reportsListener != null)
            reportsListener();

        reportsListener = db.collection("reports").where('status', '>' , 0)
        .onSnapshot(function(snapshot) {
                // console.log("snapshot size " + snapshot.size);
            if(snapshot.size > 0)
            {
                // var viewedMessages = new Array();;
                // let stored = localStorage.getItem("viewedMessages");
                // if(stored != null)
                //     viewedMessages = JSON.parse(stored);

                snapshot.docChanges().forEach(function(change) {
                    if(change.type == "added")
                    {
                        if(newMessageListeners[change.doc.id] != null)
                            newMessageListeners[change.doc.id]();
                            
                        newMessageListeners[change.doc.id] =
                        db.collection("reports").doc(change.doc.id)
                        .collection("messages")
                        .onSnapshot(function(msgSnapshot){

                            msgSnapshot.docChanges().forEach(function(msgChange){

                                // dont receive notification if we're the sender
                                if(msgChange.type == "added" && firebase.auth().currentUser.email != msgChange.doc.data().user)
                                {
                                    // if(viewedMessages == null)
                                    //     viewedMessages = new Array();

                                    // if(viewedMessages.indexOf(msgChange.doc.id) == -1)
                                    if(moment(msgChange.doc.data().date.toDate()).isAfter(moment(startFetchDate)))
                                    {
                                        let title = "new message!";
                                        let reportId = msgChange.doc.data().reportId;

                                        if(reportId != null)
                                            title = reportId + " new message!";

                                        let origMessage = msgChange.doc.data().message;
                                        let theMessage = origMessage;

                                        if(msgChange.doc.data().is_media)
                                        {
                                            theMessage = msgChange.doc.data().user_id + " sent an attachment";
                                        }

                                        audio.play();

                                        var optionsCopy = notif_options;
                                        optionsCopy["labels"] = {info : title};

                                        var newNotif = new AWN(optionsCopy);
                                        var elem = newNotif.info(theMessage, {});

                                        $(elem).on('click', function(){
                                            sessionStorage.setItem("selectedCase", reportId);
                                            window.open("messaging.html", '_blank');
                                        });

                                        // let notice = PNotify.notice({
                                        //   title: reportId + " new message!",
                                        //   text: theMessage,
                                        //   modules: new Map([
                                        //     ...PNotify.defaultModules,
                                        //     [PNotifyDesktop, {
                                        //       icon: false
                                        //     }]
                                        //   ])
                                        // });

                                        // notice.on('click', e => {
                                        //   if ([...notice.refs.elem.querySelectorAll('.pnotify-closer *, .pnotify-sticker *')].indexOf(e.target) !== -1) {
                                        //     return;
                                        //   }
                                          
                                        //     sessionStorage.setItem("selectedCase", reportId);
                                        //     window.open("messaging.html", '_blank');
                                        // });

                                    }
                                }
                            });
                        });
                    }
                    
                });
            }
        });


    });
}


function showNoAccessPage()
{
    document.head.innerHTML = "";
    document.body.innerHTML = '<h1>You cannot access this page, please contact the admin to grant you permission.</h1>';
}

function showUnAuthorized()
{
    document.head.innerHTML = "";
    document.body.innerHTML = '<h1>Unauthorized access! Please log in <a href="index.html">here</a> first</h1>';
}

function LoadScripts(async)
{
    if( async === undefined ) {
        async = false;
    }
    var scripts = [];
    var _scripts = ['js/generics/sb-admin-2.js', 'js/generics/generic.js', 'js/generics/loading.js'];
    
    // console.log(window.location.href);
    let splitted_link = window.location.href.split("/");
    let current_page = splitted_link[splitted_link.length-1];

    switch(current_page.replace(/#/g, ''))
    {
        case "dashboard.html" :  _scripts.push('js/dashboard.js'); break;
        case "guidelines.html" :  _scripts.push('js/guidelines.js'); break;
        case "announcements.html" :  _scripts.push('js/announcements.js'); break;
        case "actions.html" :  _scripts.push('js/actions.js'); break;
        case "penalties.html" :  _scripts.push('js/penalties.js'); break;
        case "reports.html" :  _scripts.push('js/report.js'); break;
        case "categories.html" :  _scripts.push('js/categories.js'); break;
        case "suggestions.html" :  _scripts.push('js/suggestions.js'); break;
        case "students.html" :  _scripts.push('js/students.js'); break;
        case "users.html" :  _scripts.push('js/users.js'); break;
        case "permissions.html" :  _scripts.push('js/permissions.js'); break;
        case "messaging.html" :  _scripts.push('js/messaging.js');break;
    }

    if(async){
        LoadScriptsAsync(_scripts, scripts)
    }else{
        LoadScriptsSync(_scripts,scripts)
    }
}

// what you are looking for :
function LoadScriptsSync (_scripts, scripts) {

    var x = 0;
    var loopArray = function(_scripts, scripts) {
        // call itself
        loadScript(_scripts[x], scripts[x], function(){
            // set x to next item
            x++;
            // any more items in array?
            if(x < _scripts.length) {
                loopArray(_scripts, scripts);   
            }
        }); 
    }
    loopArray(_scripts, scripts);      
}

// async load as in your code
function LoadScriptsAsync (_scripts, scripts){
    for(var i = 0;i < _scripts.length;i++) {
        loadScript(_scripts[i], scripts[i], function(){});
    }
}

// load script function with callback to handle synchronicity 
function loadScript( src, script, callback ){

    script = document.createElement('script');
    script.onerror = function() { 
        // handling error when loading script
        alert('Error to handle')
    }
    script.onload = function(){
        // console.log(src + ' loaded ')
        callback();
    }
    script.src = src;
    document.getElementsByTagName('head')[0].appendChild(script);
}





var deviceTime,
    serverTime,
    actualTime,
    timeOffset;


function fetchServerTime(callback) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onload = function() {
        var dateHeader = xmlhttp.getResponseHeader('Date');
            
        // Just store the current time on device for display purpose
        deviceTime = moment();
        
        // Turn the "Date:" header field into a "moment" object,
        // use JavaScript Date() object as parser
        serverTime = moment(new Date(dateHeader)); // Read
        
        // Store the differences between device time and server time
        timeOffset = serverTime.diff(moment());
        
        // Now when we've got all data, trigger the timer for the first time
        actualTime = moment();
        
        // Add the calculated offset
        actualTime.add(timeOffset);

        if(callback)
            callback();
    }
    xmlhttp.open("HEAD", window.location.href);
    xmlhttp.send();
}


// function fetchServerTime(callback) {
//         console.log('beginning requesting server time');

//         try
//         {
//         db.collection('servertime').doc('current').set({servertime:firebase.firestore.FieldValue.serverTimestamp()}, { merge: true })
//         .then(function(){
            
//                 db.collection('servertime').doc('current')
//                 .get().then(function(doc){
//                     // Just store the current time on device for display purpose
//                     deviceTime = moment();
                    
//                     // Turn the "Date:" header field into a "moment" object,
//                     // use JavaScript Date() object as parser
//                     // if(doc.data().servertime != null)
//                     // {
//                         console.log(doc.data());
//                         serverTime = moment(doc.data().servertime.toDate()); // Read
//                         // console.log(serverTime);
                        
                        
//                         // Store the differences between device time and server time
//                         timeOffset = serverTime.diff(moment());
                        
//                         // if(callback)
//                         if(callback)
//                             callback();
//                     // }
//                     // Now when we've got all data, trigger the timer for the first time
//                     // timerHandler();
//                 })
//                 .catch((error) => {
//                     // console.error(error.message);
//                     // console.log('retrying requesting server time');
//                     fetchServerTime(callback);
//                 });            
//         })
//         .catch((error) => {
//             // console.error(error.message);
//             // console.log('retrying requesting server time');
//             fetchServerTime(callback);
//         });
//     }
//     catch(error)
//     {
//         // console.error(error.message);
//         // console.log('retrying requesting server time');
//         fetchServerTime(callback);
//     }

// }

// fetchServerTime();    


function getCurrentDateServerOffset() {
    // Get current time on the device
    actualTime = moment();
    
    // Add the calculated offset
    actualTime.add(timeOffset);

    return actualTime.toDate();
    // console.log(actualTime.tz('Asia/Manila').format(myDateFormat));
};