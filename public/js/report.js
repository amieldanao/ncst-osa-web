(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];
addUserCallback();
// var initial_role;
// var myRole;
function addUserCallback()
{
    let email = $("#user_email").text();

    let query = db.collection('users').where('email', '==', email);

    query.onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {


            if (change.type === "added") {
                let role = change.doc.data().role;
                initial_role = role;
                setRole(role);
                if(role == 2 || role == 1)
                {
                    $("#table_tr").append('<th id="actions_th" class="role_admin">Actions</th>');
                }
            }

            if (change.type === "modified") {
                setRole(change.doc.data().role);
                $("body").empty();
                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                 setTimeout(() => {
                    location.replace("index.html");
                }, 3000);
            }

            if (change.type === "removed") {
                // console.log("Removed user: ", change.doc.data());
                // window.location = "index.html";
            }
        });
    },
        function(error){
        console.log('retry add user callback');
        addUserCallback();
    });
}

function setRole(newRole)
{
    switch(newRole)
    {
        case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
        case 1 : $(".role_admin").remove();break;
    }
    
    $("#user_role").text("Role: " + roles_string[newRole]);
    $("#user_role").attr("data-done", true);
}

})();




var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);



function Start()
{
    addElementCallbacks();

    initTable();    
}

$(document).ready( function () {
    observer.observe(targetNode, config);
//    initButtonCallbacks();
});

function addElementCallbacks()
{
    $("#status_select").selectpicker('selectAll');
    $('#status_select').selectpicker('setStyle', 'btn-info');
    $("#checkbox_yesterday").get(0).checked = false;
    document.getElementById("checkbox_yesterday").addEventListener('change', (event) => {
        reports_table.draw();
    })

    $("#status_select, #category_filter").on('changed.bs.select', function(e, clickedIndex, isSelected, previousValue){
        reports_table.draw();
        console.log("changed");
    });

    // $("#").on('changed.bs.select', function(e, clickedIndex, isSelected, previousValue){
    //     reports_table.draw();
    //     console.log("changed");
    // });

    
}



'user strict'

var reports_table;
var students_reported = [];
// var rootRef;
// var report_to_proccess;
var status_text = ['Report has been resolved.', 'Report Verified. Processing...', 'Reported. Waiting for response ...', 'Pending ...'];
var status_class = ['badge badge-success', 'badge badge-warning', 'badge badge-primary', 'badge badge-info'];
var suspect_report_counts = new Array();
var cached_suspects = new Array();
var renderingStudentsName = false;
var studentsEmailNames = [];
var all_categories = new Array();
// var myReporters = []; // array of students who reported a student


function getStatusIndex(str)
{
    for (var i = status_text.length - 1; i >= 0; i--) {
        let thisText = status_text[i];

        let regex = RegExp('^.*'+str+'*$');
        if(regex.test(thisText))
            return i;
    }

    return -1;
}

/* Custom filtering function which will search data in column four between two values */
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {

        // filter by report status
        var status = data[5];
        let indexNum = getStatusIndex(status).toString();

        // let statusText = "Invalid Report";//getStatusIndex(status).toString();
        // if(indexNum != -2){
        //     statusText = status_text[indexNum];
        // }

        let selected = $("#status_select").val();

        // filter by report yesterday only
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        let str_yesterday = yesterday.toDateString().split(" ")[0];
        str_yesterday += " " + moment(yesterday).format("MMMM DD YYYY");

        // filter by report status
        let splitted_report_date = data[4].split(" ");
        let thisReportDate = splitted_report_date[0] + " " + splitted_report_date[1] + " " + splitted_report_date[2] + " " + splitted_report_date[3];

        let checked = document.getElementById('checkbox_yesterday').checked;

        // filter by category
        let selected_categories = $("#category_filter").val();
        let splitted_report_categories = data[1].split(",");

        if(dataIndex == 0)
        {
            // console.log(splitted_report_categories);
            // console.log(selected_categories);
        }
        let hasCategory = findOne(selected_categories, splitted_report_categories);//selected_categories.some(r=> splitted_report_categories.indexOf(r) >= 0);

        return (selected.indexOf(indexNum) != -1) && (checked == false || checked && thisReportDate == str_yesterday) && hasCategory;
    }
);

var findOne = function (haystack, arr) {
    return arr.some(function (v) {
        return haystack.indexOf(v) >= 0;
    });
};


function initTable()
{
    let col_classes = [
        {"sClass": "col_case" },
        {"sClass": "col_categories" },
        {"sClass": "col_senders" },
        {"sClass": "col_suspect" },
        {"sClass": "col_date_posted" },
        {"sClass": "col_status status" },
        {"sClass": "col_resolved_by" }
    ];

    let col_defaults = [];

    if(document.getElementById('actions_th') != null){
        col_classes.push({"sClass": "actions_buttons" });
        col_defaults = [ {
            "targets": 6,
            "orderable": false
        },
        { "width": "20%", "targets": 4 }
        ];
    }

    reports_table = $('#reports_table').DataTable(
    {
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i> Export',
                className: 'btn btn-outline-secondary text-success',
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary')
                },
                action: function(e, dt, node, config) {
                    var that = this;
                    // showLoading(true, 'Exporting excel'); // function to show a loading spin
                    if(loading_export == null){
                        showExportLoading(true, 'Exporting excel');
                        console.log("Clicked export");
                        
                        

                        // loading_export = PNotify.notice({
                        //     title: 'Loading',
                        //     text: "Exporting resolved reports",
                        //     icon: 'fas fa-spinner fa-pulse',
                        //     hide: false,
                        //     closer: false,
                        //     sticker: false,
                        //     styling : 'custom_success',
                        //     addModelessClass: 'nonblock'
                        // });

                    
                        setTimeout(function() { // it will download and hide the loading spin when excel is ready
                            exportExtension = 'Excel';
                            $.fn.DataTable.ext.buttons.excelHtml5.action.call(that, e, dt, node, config);
                            // swal.close(); // close spin
                            showExportLoading(false);
                        }, 1000);

                    }
                
                }
            }
        ],
        "autoWidth": false,
        dom: 'Bfrtilp',
        "scrollX": true,
        "aoColumns": col_classes,
        "columnDefs": col_defaults,
        "order": [[ 0, "desc" ]],
        initComplete: function(settings, json) {
            let buttons = $("#reports_table_wrapper .dt-buttons button");
            buttons.addClass("btn");
            buttons.addClass("btn-outline-primary");
            // buttons.addClass("text-success");
            buttons.removeClass("dt-button");
            addDBListener();

            fetchCategories();
            // reRenderSendersColumn();
        },
        "drawCallback": function( settings ) {
            // alert( 'DataTables has redrawn the table' );
            setTimeout(function(){
                refreshColumnHeaders();
            }, 200);

            
            
        }
    });

    $('#reports_table').on( 'page.dt', function () {
        // var info = table.page.info();
        
        
        reRenderSendersColumn();
        console.log("change page");
        

        setTimeout(function(){ reRenderSendersColumn(); }, 100);

        setTimeout(function(){
            refreshColumnHeaders();
            // reports_table.draw();
        }, 1500);
        
    } );

    var options = {
        content: "none",
        html: true,
        placement: 'auto'
    }

    $('#pop_students_report_count').popover('dispose');

    $('#pop_students_report_count').popover(options);

    // addDBListener();
}

var loading_export = null;
function showExportLoading(show, txt="")
{
    if(show)
    {
        // if (window.permanotice != null) {
        //     window.permanotice.open();
        // } else {
            
            // loading_export.close();

            loading_export = PNotify.notice({
                title: 'Please wait...',
                text: txt,
                icon: 'fas fa-spinner fa-pulse',
                hide: false,
                closer: false,
                sticker: false,
                styling: 'custom_success',
                addModelessClass: 'nonblock'
                // ,
                // modules: new Map([
                //     ...PNotify.defaultModules,
                //     [PNotifyMobile, {
                //     swipeDismiss: false
                //     }]
                // ])
            });
            loading_export.open();

            let temp = PNotify.notice({
                title: "",
                text: "",
                hide: true,
                closer: true,
                delay : 1000,
                remove: true,
                destroy : true
            });

            temp.close();
        // }
    }
    else
    {
        if (loading_export != null) {
            loading_export.close();
            console.log("CLose");
            loading_export = null;
        }
    }
}

function fetchCategories()
{
    try {
        db.collection("categories")
        .onSnapshot(function(snapshot) { 
            let requests = snapshot.docChanges().map((change) => {

                if (change.type === "added") {
                    // if(!all_categories.includes(change.doc.data().name))
                    addCategoryOption(change.doc.data().name);
                }
                
                if (change.type === "modified") {
                    // console.log("Modified city: ", change.doc.data());
                    
                    
                }
                if (change.type === "removed") {
                    // if(all_categories.includes(change.doc.data().name))
                    //     all_categories.
                    //     addCategoryOption(change.doc.data().name);
                    // $('tr[data-key="'+change.doc.id+'"]').parent().parent().remove();
                    // categories_table.row('#'+change.doc.id).remove().draw();
                    
                    // categories_table.draw();
                }
                
                return new Promise((resolve) => {
                    asyncFunction(change, resolve);
                });
            });

            Promise.all(requests).then(() => {
                $("#category_filter").selectpicker('selectAll');
                $('#category_filter').selectpicker('setStyle', 'btn-info');
            });
        });
    }
    catch(err) {
      bootbox.alert(err.message);
  }
}

function addCategoryOption(category_name)
{
    if(all_categories.includes(category_name))
        return;
    let final_name = toTitleCase(category_name);
    all_categories.push(category_name);
    $("#category_filter").append('<option value="'+final_name+'">'+final_name+'</option>');
    $("#category_filter").attr("data-selected-text-format", "count > "+ (all_categories.length-1));
    $("#category_filter").selectpicker('refresh');
}

function gotoMessage(elem)
{
    // var evID = me.closest('tr');//.attr("val");
    // category_to_proccess = evID.id.split("row_")[1];
    var reportId = $(elem).attr('data-reportId');
    sessionStorage.setItem("selectedCase", reportId);
    window.open("messaging.html", '_blank');
    // window.location = "messaging.html";
}


function reRenderSendersColumn()
{
    // if(renderingStudentsName == true || reports_table == undefined)
    if(reports_table == undefined)
        return;

    renderingStudentsName = true;
    var senders_cols = reports_table.columns(2).nodes()[0];//$(".col_senders");
    // let requests = Object.keys(senders_cols).map((s) => {
    var columnCounter = 0;
    let requests = senders_cols.map((s) => {

        // console.log(s);
        let senderCol = s;//senders_cols[columnCounter];
    // $(".col_senders").each(function(){
        // if(!senderCol.hasClass("sorting"))
        if(!senderCol.classList.contains("sorting"))
        {
            // console.log("s? " + columnCounter);
            let data = reports_table.columns(2).data().eq(0)[columnCounter];//senders_cols[s];//$(senderCol).text();
            
            let sender_emails = data.split(",");
            let sender_names = new Array();

            // console.log(sender_emails);

            Object.keys(students_reported).forEach(function(i, e) {
                let thisStudentData = students_reported[i];
                // console.log(i);

                if(sender_emails.indexOf(thisStudentData.email) > -1)
                    sender_names.push(toTitleCase(thisStudentData.first_name) + " " + toTitleCase(thisStudentData.last_name));
            });

            let toRet = data;
            // console.log(data);

            if(sender_names.length > 0)
                toRet = sender_names.toString();

            // console.log(sender_names);

            senderCol.innerText = toRet;
            
            // $(this).fadeOut(1000, function() {
            //     $(this).html(toRet).fadeIn(1000);
            // });
            
            // console.log(toRet);
            
            
        }

        columnCounter++;

    //     setTimeout(function(){
    //         refreshColumnHeaders();
    //     }, 1000);
    // });

    setTimeout(function(){
        refreshColumnHeaders();
    }, 1000);


    return new Promise((resolve) => {
        asyncFunction(s, resolve);
      });
  });

  Promise.all(requests).then(() => {
    renderingStudentsName = false;
    console.log("done rendering students name");
    // reports_table.draw(false);
      // reports_table.column( 'col_senders' ).cells().invalidate();
      // reRenderSendersColumn();
      // refreshColumnHeaders();
  });
}

function getStudentData(array_of_emails)
{
    // console.log("loading students reported");
    try
    {
        for (let index = 0; index < array_of_emails.length; index++) {

            let thisSenderEmail = array_of_emails[index];
                
            //Solution for firestore limit of 10, array elements in query!!!
            // We dont need to process emails that has been processed earlier
            var i = 0;
            if(!(thisSenderEmail in studentsEmailNames))
            {
                // console.log(array_of_emails);
                db.collection("students").where('email', '==', thisSenderEmail).get()
                .then((querySnapshot) => {

                    // let requests = querySnapshot.docs.map((doc) => {
                        // doc.data() is never undefined for query doc snapshots
                        // console.log(doc.id, " => ", doc.data());

                    if(querySnapshot.size > 0)
                    {
                        let doc = querySnapshot.docs[0];
                        // console.log(doc);
                        students_reported[doc.data().email] = doc.data();
                        studentsEmailNames[doc.data().email] = doc.data().first_name + " " + doc.data().last_name;
                    }
                    
                    i++;

                    if(i >= array_of_emails.length)
                        reRenderSendersColumn();

                }).catch(function(error) {
                    console.log(thisSenderEmail);
                    console.log("Error getting cached document:", error);

                    i++;

                    if(i >= array_of_emails.length)
                        reRenderSendersColumn();
                    
                });
            }
            else
            {
                i++;

                if(i >= array_of_emails.length)
                    reRenderSendersColumn();
            }
        }
    }
    catch(error)
    {
        console.log(error);
    }
}

function refreshSuspectCountPop()
{

    // let sorted_suspect_report_counts = suspect_report_counts.sort(function (a, b) {
    //     // return b.val.localeCompare( a.val );
    //     return b - a;
    // });
    let sorted_suspect_report_counts = suspect_report_counts.sort((a, b) => (a.val < b.val) ? 1 : -1)


    let html_suspects = "";

    let requests = sorted_suspect_report_counts.map((suspect) => {

        let studentName = "";

        if(suspect.key in cached_suspects)
            studentName = toTitleCase(cached_suspects[suspect.key].first_name + " " + cached_suspects[suspect.key].last_name);
        html_suspects += '<span data-count="'+suspect.val+'" class="suspect_entry badge badge badge-primary my-1" style="border-style:solid; border-width: 1px; border-color: gray; color: white; background-color: red !important;"><h5>' + studentName + " : <b>" + suspect.val + '</b></h5></span>';
        
        // let r = parseInt(Math.floor(Math.random() * 100));
        // html_categories += '<span data-count="'+r+'" class="suspect_entry badge badge badge-primary my-1" style="border-style:solid; border-width: 1px; border-color: gray; color: white; background-color: red !important;"><h5>' + studentName + " : <b>" + r + '</b></h5></span>';
        // r = parseInt(Math.floor(Math.random() * 100));
        // html_categories += '<span data-count="'+r+'" class="suspect_entry badge badge badge-primary my-1" style="border-style:solid; border-width: 1px; border-color: gray; color: white; background-color: red !important;"><h5>' + studentName + " : <b>" + r + '</b></h5></span>';
        // r = parseInt(Math.floor(Math.random() * 100));
        // html_categories += '<span data-count="'+r+'" class="suspect_entry badge badge badge-primary my-1" style="border-style:solid; border-width: 1px; border-color: gray; color: white; background-color: red !important;"><h5>' + studentName + " : <b>" + r + '</b></h5></span>';
        // r = parseInt(Math.floor(Math.random() * 100));
        // html_categories += '<span data-count="'+r+'" class="suspect_entry badge badge badge-primary my-1" style="border-style:solid; border-width: 1px; border-color: gray; color: white; background-color: red !important;"><h5>' + studentName + " : <b>" + r + '</b></h5></span>';

        return new Promise((resolve) => {
            asyncFunction(suspect, resolve);
        });
    });

    Promise.all(requests).then(() => {

        var options = {
            content: html_suspects,
            html: true,
            placement: 'auto'
        }

        $('#pop_students_report_count').popover('dispose');

        $('#pop_students_report_count').popover(options);
        $("#pop_students_report_count").off('click');

        $("#pop_students_report_count").on('click', function() {
            $("#pop_students_report_count").popover('toggle');
        });

        $('#pop_students_report_count').on('shown.bs.popover', function () {
          // do something…
            resortStudentReported();
        });
    });
}

function resortStudentReported()
{
    
    let parent = "#"+$("#pop_students_report_count").attr("aria-describedby");
    console.log($(parent));
    $(parent + " .popover-body .suspect_entry").sort(sort_li) // sort elements
            .appendTo(parent + ' .popover-body'); // append again to the list
}


// sort function callback
function sort_li(a, b){
    // return ($(b).data('msg_date')) < ($(a).data('msg_date')) ? 1 : -1;    
    return (parseInt($(a).attr("data-count")) < parseInt($(b).attr("data-count")) ? 1 : -1);
}

function suspectExists(suspect_id)
{
    suspect_report_counts.forEach((e) => {
        // console.log(e["key"] + "==" + suspect_id);
        if(e["key"] == suspect_id)
            return true;
    });

    return false;
}

function getSuspectById(suspect_id)
{
    for (var i = 0; i < suspect_report_counts.length; i++) {
        let suspect = suspect_report_counts[i];
        if(suspect.key == suspect_id)
            return i;
    }

    return -1;
}

function tryAddSuspectReportCount(data, howManyReported)
{
    try
    {
        db.collection("students").where("first_name", "==", data.suspect).where("last_name", "==", data.suspect_surname)
        .get().then(function(querySnapshot){

            querySnapshot.forEach(function(doc) {
                // doc.data() is never undefined for query doc snapshots
                // console.log(doc.id, " => ", doc.data());

                // if(!(doc.data().ID in suspect_report_counts))
                let suspectIndex = getSuspectById(doc.data().ID);
                // if(!suspectExists(doc.data().ID))
                if(suspectIndex == -1)
                {
                    console.log(doc.data().ID + " doesn't exist yet");
                    suspect_report_counts.push({key : doc.data().ID, val : howManyReported});
                }
                else
                {
                    
                    if(suspectIndex > -1)
                    {
                        let currVal = suspect_report_counts[suspectIndex].val;
                        currVal += howManyReported;
                        suspect_report_counts[suspectIndex] = {key : doc.data().ID, val : currVal};
                    }
                    
                    // suspect_report_counts[doc.data().ID] += howManyReported;
                }

                cached_suspects[doc.data().ID] = doc.data();

                refreshSuspectCountPop();
            });

            // if(docs.size > 0){
                
            // }
            
        })
        .catch(function(error){
            console.error(error.message);
        });
    }
    catch(error)
    {

    }
}

function showPromptDeleteThisReport(elem)
{
    var key = $(elem).attr('data-key');
    var reportIdToDelete = $(elem).attr('data-reportId');
    
    bootbox.confirm({
        title: reportIdToDelete,
        message: "Are you sure you want to delete this report? \n" + reportIdToDelete + "\n Deleting a report will take time depending on the report size.",
        size:'small',
        centerVertical: true,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {

            if(result)
            {
                showLoading(true, "Deleting report : " + reportIdToDelete);
                // try
                // {
                //     db.collection("reports").doc(key)
                //     .delete().then(function() {
                //         // showLoading(false);
                //         // showNotifSuccess("Deleted", reportIdToDelete + " was deleted successfully.");
                //     }).catch(function(error) {
                //         showLoading(false);
                //         showNotifError("Error", error.message);
                //     });
                // }
                // catch(error)
                // {
                //     console.error(error.message);
                // }

                try
                {
                    db.collection("reports").doc(key).delete().then(function() {
                        console.log(key);
                        PNotify.info({
                            title: 'Deleted',
                            text: reportIdToDelete + " was deleted successfully."
                        });

                        showLoading(false);

                        // penalty_table.row('#'+key).remove().draw();
                        
                    }).catch(function(error) {
                        console.error("Error removing document: ", error);
                            PNotify.error({
                            title: 'Error',
                            text: error.message
                        });

                        showLoading(false);
                    });
                }
                catch(error)
                {
                    bootbox.alert(error.message);
                    showLoading(false);
                }

            }            
        }
    });
}

function addDBListener()
{
    showLoading(true, "Loading reports...");
    try {
          
        db.collection("reports")
        .onSnapshot(function(snapshot) { 
            let requests = snapshot.docChanges().map((change) => {

                if (change.type === "added" || change.type === "modified") {
                    
                    var d = {reportId: "", categories: new Array(), senders : new Array(), suspect_full_name: "", postedAt : "", status : ""};
                    if('reportId' in change.doc.data())
                        d['reportId'] = change.doc.data().reportId;

                    if('categories' in change.doc.data())
                        d['categories'] = change.doc.data().categories;
                    
                    if('senders' in change.doc.data())
                        d['senders'] = change.doc.data().senders;

                    if('suspect' in change.doc.data() && 'suspect_surname' in change.doc.data())
                        d['suspect_full_name'] = toTitleCase(change.doc.data().suspect + " " + change.doc.data().suspect_surname);

                    if('postedAt' in change.doc.data())
                        d['postedAt'] = change.doc.data().postedAt;
                    
                    if('status' in change.doc.data())
                        d['status'] = parseInt(change.doc.data().status);

                    if('resolve_seminars' in change.doc.data())
                        d['resolve_seminars'] = change.doc.data().resolve_seminars;
                    
                    let theCategories = new Array();

                    d.categories.forEach((value, key, map) => {
                        let thisCategoryName = toTitleCase(value["name"]);
                        theCategories.push(thisCategoryName);
                        
                        addCategoryOption(value["name"]);
                    });

                    let splitted_date = [""];
                    if(d.postedAt.length > 0)
                        splitted_date = d.postedAt.split("GMT");

                    getStudentData(d.senders);

                    if(d.suspect_full_name.trim().length == 0)
                        d.suspect_full_name = change.doc.data().descriptions.toString();

                    var final_status_text = "Invalid Report";
                    var final_status_class = "badge badge-danger";

                    if(d.status > -1){
                        final_status_text = status_text[d.status];
                        final_status_class = status_class[d.status];
                    }

                    var final_resolve_text = "N/A";

                    if(d.resolve_seminars != null && d.status == 0)
                        final_resolve_text = d.resolve_seminars.toString();

                    var realReportId = d.reportId;
                    if(d.status == 3 || d.status == 2)
                        realReportId = "Report#" + d.reportId.split("#")[1];
                    
                    let newData =
                    [
                        realReportId,
                        theCategories.toString(),
                        d.senders.toString(),
                        d.suspect_full_name.toString(),
                        splitted_date[0],
                        '<h5><span data-status="'+d.status+'" class="label '+final_status_class+'">'+final_status_text+'</span></h5>',
                        final_resolve_text
                    ];


                    if(document.getElementById("actions_th") != null){
                        var actionButton = '<a data-reportId="'+d.reportId+'" href="#" onclick="gotoMessage(this);" class="on-default edit-row mx-2"><i class="fa fa-envelope"></i></a>';
                        
                        if(d.status == -1)
                        {
                            actionButton += '<a data-reportId="'+d.reportId+'" data-key="'+change.doc.id+'" href="#" onclick="showPromptDeleteThisReport(this);" class="on-default delete_row mx-2"><i class="fa fa-trash-o"></i></a>';
                        }
                        newData.push(actionButton);
                        
                    }
                    // else
                    //     newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickView(this);" class="on-default edit-row mx-2"><i class="fas fa-eye"></i></a>');
                    
                    if(change.type === "added")
                    {
                        let newRow = reports_table.row.add(newData).node().id = change.doc.id;
                        reports_table.draw( false );
                        
                        if(change.doc.data().suspect.length > 0 && change.doc.data().status > 0)
                        {
                            // addMyReporter(d.suspect_full_name, d.senders);
                            tryAddSuspectReportCount(change.doc.data(), d.senders.length);                            
                        }
                    }
                    
                    if(change.type === "modified")
                    {
                        if(change.doc.data().suspect.length > 0 && change.doc.data().status > 0)
                        {
                            tryAddSuspectReportCount(change.doc.data(), d.senders.length);
                        }
                        var myRow = $("#" + change.doc.id);
                        if(change.doc.data().status != -1)
                        {
                            myRow.find(".delete_row").remove();
                        }
                        reports_table.row(myRow).data(newData).draw(false);
                    }
                }
                
                if (change.type === "removed") {
                    reports_table.row('#'+change.doc.id).remove().draw();
                }

                refreshColumnHeaders();
                
                return new Promise((resolve) => {
                  asyncFunction(change, resolve);
                });
            });

            Promise.all(requests).then(() => {
                showLoading(false);
                refreshColumnHeaders();
                reRenderSendersColumn();
            });
        });
    }
    catch(err) {
        bootbox.alert(
        {
            message:err.message,
            onHidden: function () {
                UpdateModalScrolling();
            }
        });
    }
}



function refreshColumnHeaders()
{
    if(reports_table != undefined)
        reports_table.columns.adjust();//.draw();
}

function clickView(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    var cc = tr.find(".col_case").text();
    var myCategories = tr.find(".col_categories").text();
    var mySenders = tr.find(".col_senders").text();
    var status = tr.find(".col_status").text();
    

    bootbox.alert(
    {
        title: cc,
        message : myCategories + "<br><hr>" + mySenders +"<br><hr>Status: <br>" + status,
        centerVertical : true,
        scrollable : true
    });
}


function asyncFunction (item, cb) {
  setTimeout(() => {
      
    cb();
  }, 100);
}