(function() {
    // just drag your whole code from your script.js and drop it here
var roles_string = ['Viewer', 'Editor', 'Admin'];


addUserCallback();
// var initial_role;
// var myRole;
function addUserCallback()
{
    let email = $("#user_email").text();

    let query = db.collection('users').where('email', '==', email);

    query.onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {


            if (change.type === "added") {
                let role = change.doc.data().role;
                initial_role = role;
                setRole(role);
                if(role == 2 || role == 1)
                {
                    $("#table_tr th").last().attr('id', 'actions_th');//append('<th id="actions_th" class="role_admin">Actions</th>');
                }
            }

            if (change.type === "modified") {
                setRole(change.doc.data().role);
                $("body").empty();
                $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

                 setTimeout(() => {
                    location.replace("index.html");
                }, 3000);
            }

            if (change.type === "removed") {
                // console.log("Removed user: ", change.doc.data());
                // window.location = "index.html";
            }
        });
    },
        function(error){
        console.log('retry add user callback');
        addUserCallback();
    });
}

function setRole(newRole)
{
    switch(newRole)
    {
        case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
        case 1 : $(".role_admin").remove();break;
    }
    
    $("#user_role").text("Role: " + roles_string[newRole]);
    $("#user_role").attr("data-done", true);
}

})();
'user strict'


var targetNode = document.getElementById('user_role');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);



function Start()
{
    loadCategories();
}

$(document).ready( function () {
    observer.observe(targetNode, config);
//    initButtonCallbacks();
});



var penalty_table;
var edit_dialog;
var cached_penalties = [];
var categories = [];

function loadCategories() {
    db.collection('categories').get().then(function(querySnapshot){
        // console.log(querySnapshot);

        let requests = querySnapshot.docs.map((category) => {
            
            categories[category.id] = category.data();
        });

        Promise.all(requests).then(() => {
            initTable();
        });


    });
}

function initTable()
{
    let btns = [];
    if(document.getElementById("actions_th") != null)
    {
        console.log("has action column!");
        // myColumns.push({"sClass": "actions_buttons" });
        btns = [
        {
            text: 'Add <i class="fa fa-plus"></i>',
            action: function ( e, dt, node, config ) {
                showPromptEdit()
            },
            "className": 'btn btn-primary'
        }
        ]
    }

    penalty_table = $('#penalty_table').DataTable(
    {
        dom: 'Bfrltip',
        "scrollX": true,
        buttons: btns,
        "aoColumns": [
        {"sClass": "col_title" },
        {"sClass": "col_message" },
        {"sClass": "col_categories" },
        {"sClass": "col_offense" },
        {"sClass": "actions_buttons" }
        ],
        "columnDefs": [ {
            "targets": 4,
            "orderable": false
        }],
        initComplete: function(settings, json) {
            // alert( 'DataTables has finished its initialisation.' );
            addDBListener();
        }
    });

    // addDBListener();
}

function showPromptEdit(penalty_key)
{

    var penaltyObj;
    if(penalty_key in cached_penalties)
        penaltyObj = cached_penalties[penalty_key];

    var isNew = (penaltyObj == undefined);

    var categorySelectOptions = '';
    var selected_categories_from_data = new Array();

    Object.keys(categories).forEach(function(e, i){
        categorySelectOptions += '<option value="'+e+'">'+categories[e].name+'</option>';
        if(isNew == false)
        {
            if(penaltyObj.categories != undefined && penaltyObj.categories.indexOf(categories[e].name) != -1)
            {
                selected_categories_from_data.push(e);
            }
        }
    });

    edit_dialog = bootbox.dialog({
        title: 'Manage Penalty',
        message: '<form id="infos" action="">'+
        '<label for="title_input">Title:</label><input id="title_input" style="width:100%;" class="col_title" type="text"><br/>'+
        '<label for="category_selection" style="width:100%;">Action categories:<select id="category_selection" multiple="multiple" style="width: 100%">'+
        categorySelectOptions+
        '</select></label><br>'+
        'Message:<textarea id="summernote" style="width:100%; border-radius:5px; "></textarea><br>'+
        '<label for="offense_select">Offense no:</label><select id="offense_select" style="width:100%;">'+
        '<option value="0">0</option>'+
        '<option value="1">1</option>'+
        '<option value="2">2</option>'+
        '<option value="3">3</option>'+
        '</select>'+
        '</form>',
        

        buttons: {
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function(){
                    
                }
            },
            ok: {
                label: 'Save <i class="fa fa-save"></i>',
                className: 'btn-success',
                callback: function(){

                    console.log('Custom OK clicked');
                    var title = $('#infos .col_title').val();
                    if(title == undefined || title.trim().length == 0)
                    {
                        bootbox.alert({
                            message:"Title can't be blank!",
                            centerVertical: true,
                            className : "topmost-modal",
                            size : 'small',
                            onHidden: function () {
                                UpdateModalScrolling();
                            }
                        });
                        
                        return false;
                    }

                    var message = $('#summernote').summernote('code');

                    let selected_categories = $('#infos #category_selection').find(':selected');

                    if(selected_categories.length == 0)
                    {
                        bootbox.alert({
                            message:"Please select atleast one category",
                            centerVertical: true,
                            className : "topmost-modal",
                            size : 'small',
                            onHidden: function () {
                                UpdateModalScrolling();
                            }
                        });
                        return false;
                    }
                    
                    savePenalty(penalty_key, title, message, selected_categories, $("#infos #offense_select").val());
                }
            }
        }
    });
    


    edit_dialog.on('shown.bs.modal', function(e){

        
        
        $('#summernote').summernote({
            height: '200px',
            toolbar: defaultToolbar
        });
        
        if(isNew == false)
        {
            $("#infos .col_title").val(penaltyObj.title);
            if(penaltyObj.message)
            {
                $('#summernote').summernote('code', penaltyObj.message);
            }

            $("#infos #offense_select").val(penaltyObj.offense);
        }

        let select = $('#infos #category_selection');
        select.select2({dropdownParent: edit_dialog, width: 'resolve', theme: "classic"});
        select.val(selected_categories_from_data).trigger('change');
    });
}


function savePenalty(key, _title, _message, _categories, _offense)
{
    var selectCategories = new Array();
    _categories.each(function(i, elem){
        // console.log(elem);
        selectCategories.push(elem.innerText);
    });

    // var key = sessionStorage.getItem("toEdit");
    var ref = db.collection("Penalties").doc();
    
    if(key == undefined || key.length == 0)
    {
        key = ref.id;
    }
    else
    {
        ref = db.collection("Penalties").doc(key);
    }
    
    // sessionStorage.setItem("toEdit", "");
    
    showLoading(true, "Saving...");
    try
    {
        var newData = {};
        newData["title"] = _title;
        newData["message"] = _message;
        newData["categories"] = selectCategories;
        newData["offense"] = parseInt(_offense);

        ref.set(newData, {merge:true})
        .then(function(docRef) {
            showNotifSuccess('Saved', _title + ' saved successfully');
            showLoading(false);
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
            PNotify.error({
              title: 'Error',
              text: error
            });
            showLoading(false);
        });
    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(
        {
            message:error.message, 
            onHidden: function () {
                UpdateModalScrolling();
            }
        });
    }
}

function addDBListener()
{
    showLoading(true, "Loading penalties...");
    try {
          
        db.collection("Penalties")
        .onSnapshot(function(snapshot) { 
            
            let requests = snapshot.docChanges().map((change) => {
                
                var d = {title: "", message : "", categories: new Array(), offense : ""};
                if('title' in change.doc.data())
                    d['title'] = change.doc.data().title;
                
                if('message' in change.doc.data())
                    d['message'] = change.doc.data().message;
                
                if('categories' in change.doc.data())
                    d['categories'] = change.doc.data().categories;

                if('offense' in change.doc.data())
                    d['offense'] = change.doc.data().offense;

                if (change.type === "added") {
                    
                    let newData =
                    [
                        capitalizeFLetter(d.title),
                        getTextFromHTML(d.message),
                        d.categories,
                        d.offense
                    ]

                    if(document.getElementById("actions_th") != null)
                        newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickEdit(this);" class="on-default edit-row mx-2"><i class="fa fa-pencil"></i></a><a data-key="'+change.doc.id+'" href="#" onclick="showPromptDelete(this);" class="on-default remove-row mx-2"><i class="fa fa-trash-o"></i></a>');
                    else
                        newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickView(this);" class="on-default edit-row mx-2"><i class="fas fa-eye"></i></a>');

                    let newRow = penalty_table.row.add(newData).node().id = change.doc.id;

                    penalty_table.draw( false );

                    
                    cached_penalties[change.doc.id] = change.doc.data();
                }
                
                if (change.type === "modified") {
                    // console.log("Modified city: ", change.doc.data());
                    
                    
                    let key = change.doc.id;
                    let tr = $("#"+key);//.parent().parent();
            
                    tr.find(".col_title").text( capitalizeFLetter(d.title) );
            
                    tr.find(".col_message").text(getTextFromHTML(d.message));

                    tr.find(".col_categories").text(d.categories);

                    tr.find(".col_offense").text(d.offense);


                    cached_penalties[change.doc.id] = change.doc.data();
                }

                if (change.type === "removed") {
                    // $('tr[data-key="'+change.doc.id+'"]').parent().parent().remove();
                    penalty_table.row('#'+change.doc.id).remove().draw();
                    
                    if(change.doc.id in cached_penalties)
                    {
                        delete cached_penalties[change.doc.id];
                    }
                    // announcements_table.draw();
                }
                
                return new Promise((resolve) => {
                  asyncFunction(change, resolve);
                });
            });

            Promise.all(requests).then(() => {
                showLoading(false);
            });
        });
    }
    catch(err) {
        bootbox.alert(
        {
            message:err.message,
            onHidden: function () {
                UpdateModalScrolling();
            }
        });
    }
}


function clickView(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    var section = tr.find(".col_title").text();
    var myCategories = tr.find(".col_categories").text();
    

    bootbox.alert(
    {
        title: section,
        message : messages + "<br><hr>" + myCategories +"<br>",
        centerVertical : true,
        scrollable : true
    });
}

function clickEdit(elem)
{    
    var key = $(elem).attr('data-key');

    if(key in cached_penalties)
    {
        showPromptEdit(key);
    }
}

function showPromptDelete(elem)
{
    var key = $(elem).attr('data-key');
    
    bootbox.confirm({
        title:$(elem).parent().parent().find(".col_title").text(),
        message: "Are you sure you want to delete this Penalty?",
        size:'small',
        centerVertical: true,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {

            if(result)
            {
                clickRemove(key);
            }            
        }
    });
}

function clickRemove(key)
{   
    
    if(key.trim().length > 0)
    {
        showLoading(true, "Deleting...");

        try
        {
            db.collection("Penalties").doc(key).delete().then(function() {

                PNotify.info({
                  title: 'Deleted',
                  text: 'Penalty deleted successfully'
              });

                showLoading(false);

                // penalty_table.row('#'+key).remove().draw();
                
            }).catch(function(error) {
                console.error("Error removing document: ", error);
                PNotify.error({
                  title: 'Error',
                  text: error.message
              });
                showLoading(false);
                sessionStorage.setItem('toEdit', '');
            });
        }
        catch(error)
        {
            bootbox.alert(error.message);
            showLoading(false);
        }
    }
}

function asyncFunction (item, cb) {
  setTimeout(() => {
      
    cb();
  }, 100);
}