/* Add here all your JS customizations */

function notify( $title, $message, $class ) {

	if ($title === undefined) {
		return false;
	} else {
		new PNotify({
			title: $title, text: $message, type: $class,
			nonblock: {
				nonblock: true,
				nonblock_opacity: .2
		   }
		});
	}
}

function setAlert( $title, $message, $class, $tag ) {

	if ($title === undefined) {
		return false;
	} else {
		var $html = [
			'<div class="alert alert-'+$class+'">',
				'<p><strong>'+$title+'</strong><br />'+$message+'</p>',
			'</div>'
		].join(' ');

		$('#'+$tag).html( $html );

		setTimeout(function() {
			$('#'+$tag).html( '' );	
		}, 5000);
	}
}