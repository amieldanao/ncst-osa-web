(function( $ ) {

	'use strict';
	$.ajaxSetup({ headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') } });

	var EditableTable = {

		options: {
			addButton: '#addToTable',
			table: '#announcements-data',
			dialog: {
				wrapper: '#dialog',
				cancelButton: '#dialogCancel',
				confirmButton: '#dialogConfirm',
			},
			form: {
				wrapper: '#form',
				cancelButton: '#formCancel',
				saveButton: '#formSave',
			}
		},

		initialize: function() {
			this
				.setVars()
				.build()
				.events();
		},

		setVars: function() {
			this.$table				= $( this.options.table );
			this.$addButton			= $( this.options.addButton );

			// dialog
			this.dialog				= {};
			this.dialog.$wrapper	= $( this.options.dialog.wrapper );
			this.dialog.$cancel		= $( this.options.dialog.cancelButton );
			this.dialog.$confirm	= $( this.options.dialog.confirmButton );

			// form
			this.form				= {};
			this.form.$wrapper		= $( this.options.form.wrapper );
			this.form.$cancel		= $( this.options.form.cancelButton );
			this.form.$save			= $( this.options.form.saveButton );

			return this;
		},

		build: function() {
			this.datatable = this.$table.DataTable({
				bProcessing: true,
				sAjaxSource: this.$table.data('url'),
				aoColumns: [
					{ sClass: 'title' },
					{ sClass: 'content' },
					{ sClass: 'media center' },
					{
						bSortable: false,
						sClass: 'actions center',
						defaultContent: [
							'<a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>',
							'<a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>',
							'<a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>',
							'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>'
						].join(' ')
					}
				]
			});

			window.dt = this.datatable;

			return this;
		},

		events: function() {
			var _self = this;

			this.$table
				.on('click', 'a.save-row', function( e ) {
					e.preventDefault();

					_self.rowSave( $(this).closest( 'tr' ) );
				})
				.on('click', 'a.cancel-row', function( e ) {
					e.preventDefault();

					_self.rowCancel( $(this).closest( 'tr' ) );
				})
				.on('click', 'a.edit-row', function( e ) {
					e.preventDefault();

					_self.clearFields();
					_self.clearErrors();

					var $check_div = $('div.checkbox');
					$check_div.show();

					var $row = $(this).closest( 'tr' );

					$.ajax({
						url: '/announcements/actions/update',
						type: 'GET',
						dataType: 'json',
						data: { id: $row[0].id },
						success: function( data ) {
							if ( data.error ) {
								notify('Request error', data.message, 'error');
							} else {
								_self.rowEdit( $row, data.data );
							}
						},
						error: function( xhr ) {
							notify(xhr.status+': '+xhr.statusText, xhr.responseText, 'error');
						}
					});
				})
				.on( 'click', 'a.remove-row', function( e ) {
					e.preventDefault();

					var $row = $(this).closest( 'tr' );

					$.magnificPopup.open({
						items: {
							src: _self.options.dialog.wrapper,
							type: 'inline'
						},
						preloader: false,
						modal: true,
						callbacks: {
							change: function() {
								_self.dialog.$confirm.on( 'click', function( e ) {
									e.preventDefault();

									$.ajax({
										url: '/announcements/actions/delete',
										type: 'POST',
										dataType: 'json',
										data: { id: $row[0].id },
										success: function( data ) {
											if ( data.error ) {
												setAlert( 'Request error', data.message, 'danger', 'ann-del' );
											} else {
												_self.rowRemove( $row );
												$.magnificPopup.close();

												notify('Request success', data.message, 'success');
											}
										},
										error: function( xhr ) {
											setAlert(
												xhr.status+': '+xhr.statusText, xhr.responseText, 'danger', 'ann-del'
											);
										}
									});
								});
							},
							close: function() {
								_self.dialog.$confirm.off( 'click' );
							}
						}
					});
				});

			this.$addButton.on( 'click', function(e) {
				e.preventDefault();

				_self.clearFields();
				_self.clearErrors();

				var $check_div = $('div.checkbox');
				$check_div.hide();

				// _self.rowAdd();
				$.magnificPopup.open({
					items: {
						src: _self.options.form.wrapper,
						type: 'inline'
					},
					preloader: false,
					modal: true,
					callbacks: {
						change: function() {
							_self.form.$save.on( 'click', function( e ) {
								e.preventDefault();

								var formData = new FormData();
								formData.append('title', $('#title').val() );
								formData.append('content', $('#content').code() );
								formData.append('do_replace', $('#do_replace').val() );

								var ins = $('#media')[0].files.length;
								for (var x = 0; x < ins; x++) {
									formData.append('media[]', $('#media')[0].files[x]);
								}

								$.ajax({
									url: '/announcements/actions/create',
									type: 'POST',
									dataType: 'json',
									processData: false,
									contentType: false,
									data: formData,
									success: function( data ) {
										if ( data.error ) {
											if ( data.data ) {
												$.each(data.data, function(i, val) {
													$('#'+i).closest('div.form-group').addClass('has-error')
														.find('span.text-danger').text( val );
												});
											} else {
												setAlert( 'Request error', data.message, 'danger', 'ann-manage' );
											}
										} else {
											_self.rowAdd( data.data );
											$.magnificPopup.close();

											notify('Request success', data.message, 'success');
										}
									},
									error: function( xhr ) {
										setAlert(
											xhr.status+': '+xhr.statusText, xhr.responseText, 'danger', 'ann-manage'
										);
									}
								});

								// $.magnificPopup.close();
							});
						},
						close: function() {
							_self.form.$save.off( 'click' );
						}
					}
				});
			});

			this.form.$cancel.on( 'click', function( e ) {
				e.preventDefault();
				$.magnificPopup.close();
			});

			this.dialog.$cancel.on( 'click', function( e ) {
				e.preventDefault();
				$.magnificPopup.close();
			});

			return this;
		},

		// ==========================================================================================
		// ROW FUNCTIONS
		// ==========================================================================================
		clearFields: function() {
			$('#form-manage').find(':input').val('');
			$('#content').code('');
		},

		clearErrors: function() {
			$('#form-manage').find('span.text-danger').text('');
			$('#form-manage').find('div.form-group').removeClass('has-error');
		},

		rowAdd: function( data ) {
			data = this.datatable.row.add( data );

			this.datatable.row( data[0] ).nodes().to$();
			this.datatable.order([0,'asc']).draw();
		},

		rowEdit: function( $row, $data ) {
			var _self = this;

			$('#title').val( $data.title );
			$('#content').code( $data.content );
			
			$.magnificPopup.open({
				items: {
					src: _self.options.form.wrapper,
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					change: function() {
						_self.form.$save.on( 'click', function( e ) {
							e.preventDefault();

							var formData = new FormData();
							formData.append('id', $row[0].id);
							formData.append('title', $('#title').val() );
							formData.append('content', $('#content').code() );
							formData.append('do_replace', $('#do_replace').val() );

							var ins = $('#media')[0].files.length;
							for (var x = 0; x < ins; x++) {
								formData.append('media[]', $('#media')[0].files[x]);
							}

							$.ajax({
								url: '/announcements/actions/update',
								type: 'POST',
								dataType: 'json',
								processData: false,
								contentType: false,
								data: formData,
								success: function( data ) {
									if ( data.error ) {
										if ( data.data ) {
											$.each(data.data, function(i, val) {
												$('#'+i).closest('div.form-group').addClass('has-error')
													.find('span.text-danger').text( val );
											});
										} else {
											setAlert( 'Request error', data.message, 'danger', 'ann-manage' );
										}
									} else {
										_self.rowSave( $row, data.data );

										notify('Request success', data.message, 'success');
										$.magnificPopup.close();
									}
								},
								error: function( xhr ) {
									setAlert(
										xhr.status+': '+xhr.statusText, xhr.responseText, 'danger', 'ann-manage'
									);
								}
							});

							// $.magnificPopup.close();
						});
					},
					close: function() {
						_self.form.$save.off( 'click' );
					}
				}
			});
		},

		rowSave: function( $row, $data ) {
			var _self     = this;

			this.datatable.row( $row.get(0) ).data( $data );
			this.datatable.draw();
		},

		rowRemove: function( $row ) {
			if ( $row.hasClass('adding') ) {
				this.$addButton.removeAttr( 'disabled' );
			}

			this.datatable.row( $row.get(0) ).remove().draw();
		},

		rowSetActionsEditing: function( $row ) {
			$row.find( '.on-editing' ).removeClass( 'hidden' );
			$row.find( '.on-default' ).addClass( 'hidden' );
		},

		rowSetActionsDefault: function( $row ) {
			$row.find( '.on-editing' ).addClass( 'hidden' );
			$row.find( '.on-default' ).removeClass( 'hidden' );
		}

	};

	$(function() {
		EditableTable.initialize();
	});

}).apply( this, [ jQuery ]);