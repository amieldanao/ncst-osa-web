(function( $ ) {

	'use strict';

    window.setInterval(function() {
		$.ajax({
	        url: '/dashboard/data',
	        type: 'GET',
	        dataType: 'json',
	        success: function(data) {
	            if (! data.error) {
	            	initChart( data.chart );

	            	var $stat = data.stat;

	            	$('.t_reports').html( $stat.t_reports );
	            	$('.t_resolved').html( $stat.t_resolved );
	            	$('.t_users').html( $stat.t_users );
	            	$('.t_students').html( $stat.t_students );

	            	var $feature = data.feature;
	            	var $notice = $('#dash-notice');

	            	if ($feature.count != 0) {
	            		$notice.removeAttr('hidden').html( $feature.notice );
	            	} else {
						$notice.attr('hidden', 'hidden').html( null );
	            	}
	            }
	        }
	    });
	}, 5000);

    function initChart( $data ) {
		console.log($data);

    	$.plot('#flotDashBasic', $data, {
			series: {
				lines: {
					show: true,
					fill: true,
					lineWidth: 1,
					fillColor: {
						colors: [{
							opacity: 0.45
						}, {
							opacity: 0.45
						}]
					}
				},
				points: {
					show: true
				},
				shadowSize: 0
			},
			grid: {
				hoverable: true,
				clickable: true,
				borderColor: 'rgba(0,0,0,0.1)',
				borderWidth: 1,
				labelMargin: 15,
				backgroundColor: 'transparent'
			},
			yaxis: {
				min: 0,
				color: 'rgba(0,0,0,0.1)'
			},
			xaxis: {
				mode: 'categories',
				color: 'rgba(0,0,0,0)'
			},
			legend: {
				show: false
			},
			tooltip: true,
			tooltipOpts: {
				content: '%s: Total report of %y',
				shifts: {
					x: -60,
					y: 25
				},
				defaultTheme: false
			}
		});
    }

}).apply( this, [ jQuery ]);