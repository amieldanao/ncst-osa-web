// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
// const moment = require('moment');
const moment = require('moment-timezone');
moment().format(); 

// The Firebase Admin SDK to access Cloud Firestore.
const admin = require('firebase-admin');
admin.initializeApp();

const status_text = ['Report has been resolved.', 'Report Verified. Processing. Investigation on going ...', 'Reported. Waiting for response ...', 'Pending ...'];
const autoInvalidateDays = 3;
const autoInvalidateMinutes = 3-1;
const autoNotifMinutes = 2-1;
// Take the text parameter passed to this HTTP endpoint and insert it into 
// Cloud Firestore under the path /messages/:documentId/original
exports.addMessage = functions.https.onRequest(async (req, res) => {
    // Grab the text parameter.
    const original = req.query.text;
    const sender_admin = req.query.is_admin;
    var docId = req.query.key;
    // Push the new message into Cloud Firestore using the Firebase Admin SDK.
    var writeResult = writeResult = await admin.firestore().collection('mockReports').doc().collection("messages").doc();
    if(docId != null)
        writeResult = await admin.firestore().collection('mockReports').doc().collection("messages").doc(docId);

    writeResult.set({is_admin: sender_admin, message: original}, {merge:true});
    // Send back a message that we've succesfully written the message
    res.json({result: `Message with ID: ${writeResult.id} added.`});
  });


  exports.reportStatusChange = functions.firestore.document('/reports/{report_key}')
  .onUpdate((change, context) => {
      
    
    console.log(change);
    functions.logger.log(change);

    if (change.before.data().status !== change.after.data().status) {

        var afterStatus = change.after.data().status;
        var previousReportStatus = change.before.data().status;

        if(afterStatus != 0)
        {
            var newProcessName = "Report status changed to " + ((afterStatus > -1)? status_text[afterStatus] : " Invalidated");
            // This report has re-open
            if(previousReportStatus == 0 && afterStatus > 0)
            {
                newProcessName = "Report has been re-opened.";
            }

            var newProcessData = {
                name: newProcessName,
                message: "Report status changed from " + ((previousReportStatus > -1)? status_text[previousReportStatus] : " Invalidated") + " to " + ((afterStatus > -1)? status_text[afterStatus] : " Invalidated"),
                timestamp: admin.firestore.FieldValue.serverTimestamp()
            };

            admin.firestore().collection("reports").doc(context.params.report_key).collection("Process").doc().set(newProcessData, {merge:true})
            .then(function(){
                
            })
            .catch(function(error){
                
            });
        }
        
      functions.logger.log('You changed report status! ', context.params.report_key, "before = " + change.before.data().status, "after = " + change.after.data().status);
    
      //we should stop monitoring notif for inactive reports
      if(change.after.data().status <= 0)
      { 
          return admin.firestore().collection('ongoingNotifs').doc(context.params.report_key).set({active:false}, {merge:true});
      }
      else
      {
          //we need to reactivate this notif if report has been re-opened
          return admin.firestore().collection('ongoingNotifs').doc(context.params.report_key).set({timestamp:admin.firestore.FieldValue.serverTimestamp(), active:true}, {merge:true});
      }


      
        
    }

    return null;
  });
  

  // Listens for new messages added to /messages/:documentId/original and creates an
// uppercase version of the message to /messages/:documentId/uppercase
exports.makeUppercase = functions.firestore.document('/reports/{report_key}/messages/{message_key}')
.onCreate((snap, context) => {
  // Grab the current value of what was written to Cloud Firestore.
//   const original = snap.data().original;

  // Access the parameter `{documentId}` with `context.params`
  
  
//   const uppercase = original.toUpperCase();

  var isAdmin = (snap.data().is_admin == true);
  if(isAdmin)
  {
      //lets only reset the timestamp if the last sender is not an admin, and new sender is admin
      admin.firestore().collection("reports").doc(context.params.report_key).collection("messages").orderBy("date", "desc").limit(2)
      .get()
        .then(function(querySnapshot) {
            if(querySnapshot.docs.length > 0)
            {
                var last = querySnapshot.docs[Math.max(0, querySnapshot.docs.length-1)];

                if(last.data().is_admin == false)
                {
                    functions.logger.log('Im Adding ', context.params.report_key, isAdmin);
                    admin.firestore().collection('ongoingNotifs').doc(context.params.report_key).set({timestamp:admin.firestore.FieldValue.serverTimestamp(), active:true}, {merge:true});
                }
            }
            
            
            // querySnapshot.forEach(function(doc) {
            //     // doc.data() is never undefined for query doc snapshots
            //     console.log(doc.id, " => ", doc.data());
            // });
        })
        .catch(function(error) {
            console.log("Error getting documents: ", error);
        });
        
  }
  else
  {
        functions.logger.log('Im Deleting ', context.params.report_key, isAdmin);
        return admin.firestore().collection('ongoingNotifs').doc(context.params.report_key).set({timestamp:null, active:false}, {merge:true});
  }
  
  // You must return a Promise when performing asynchronous tasks inside a Functions such as
  // writing to Cloud Firestore.
  // Setting an 'uppercase' field in Cloud Firestore document returns a Promise.
//   return admin.firestore().collection('mockNotifs').doc().set();//snap.ref.set({uppercase}, {merge: true});
});


function toTimeZone(time, zone) {
    var format = 'YYYY/MM/DD HH:mm:ss ZZ';
    return moment(time, format).tz(zone).format(format);
}

exports.scheduledNotifCheckerFunction = functions.pubsub.schedule('every 1 minutes').onRun((context) => {
    // console.log('This will be run every 10 minutes!');
    // functions.logger.log('Im Adding ', new Date());
    admin.firestore().collection("ongoingNotifs").where("active", "==", true)
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            // console.log(doc.id, " => ", doc.data());
            // let nowDate = new Date();
            // let diffTime = Math.abs(nowDate - doc.data().timestamp.toDate());
            // let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 


            var nowDate = moment(toTimeZone(new Date(), "Asia/Manila"));
            var thisMessageDate = doc.data().timestamp.toDate();
            let diffMinutes = nowDate.diff(moment(thisMessageDate), "minutes", true);

            functions.logger.log('Difference in minutes = ' + diffMinutes, nowDate, thisMessageDate, doc.id);

            if(diffMinutes > autoNotifMinutes)
            {
                admin.firestore().collection("reports").doc(doc.id)
                .get()
                .then(function(report){
                    var thisReportData = report.data();
                    var prevStatus = thisReportData.status;

                    // functions.logger.log('Difference in days = ' + diffDays, thisReportData.reportId);
                    //send notif 1 day before invalidating this report
                    let newNotifData = {
                        title: thisReportData.reportId + " will be invalidated soon!", 
                        dateSent: admin.firestore.FieldValue.serverTimestamp(), 
                        recipients : thisReportData.senders, 
                        message : thisReportData.reportId + " will be invalidated soon! If no responses received from any participants!", 
                        reportId: thisReportData.reportId,
                        resolved : false,
                        seenedBy : new Array(),
                        type : "invalidation"
                    };

                    if(diffMinutes >= autoInvalidateMinutes)
                    {
                        newNotifData.title = thisReportData.reportId + " has been invalidated";
                        newNotifData.message = thisReportData.reportId + " has been invalidated due to inactivity or lack of reply.";
                        
                        admin.firestore().collection('notifications').doc(doc.id).set(newNotifData, {merge:true});
                        admin.firestore().collection("reports").doc(report.id).set({status: -1}, {merge:true});

                        //we should take back offense count if this report is verified
                        if(prevStatus == 1){
                            var offense_doc_key = thisReportData.suspect.toLowerCase() + "-" + thisReportData.suspect_surname.toLowerCase() + ":" + thisReportData.gender;

                            admin.firestore().collection("offense").doc(offense_doc_key).set({count:admin.firestore.FieldValue.increment(-1)}, {merge:true})
                            .then(function(){
                                
                                functions.logger.log('Decrement offense of ' + offense_doc_key);

                            }).catch(function(error){
                                // showLoading(false);
                                console.error(error.message);

                                functions.logger.log(error.message);
                            });
                        }

                        functions.logger.log('Report invalidated! ' , thisReportData.reportId);
                    }
                    else
                    {
                        admin.firestore().collection('notifications').doc(doc.id).set(newNotifData, {merge:true})
                        .then(function() {
                        })
                        .catch(function(error) {
                        });

                        functions.logger.log('Notification sent ' , thisReportData.reportId);
                    }
                })
                .catch(function(error) {
                });
                
            }
        });
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });


    return null;
  });
  